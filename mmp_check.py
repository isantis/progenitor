#!/usr/bin/python3

"""
 =============
 = MMP Check =
 =============
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) for Spring Quarter, 2018

 Goal: Check how often the most massive progenitor contributes more than 5% of it's stars to the host

"""

#### Import all of the tools for analysis
import rockstar_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle

# Read in the halo catalogs at each redshift
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.Read.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory='/group/awetzelgrp2/m12f/m12f_res7100')
# Done for m12i. Set up to work on m12f; still need to do on m12m

#### Some functions for analysis

"""
 Function that gets halo indices for a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [8, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [1000, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z]['lowres.mass.frac'], [0, 0.02], ind)
    return ind

"""
 Function that takes the sorted vector, gets element i (which will be first, second, third etc., most massive object)
 then, returns where it is in the unsorted vector.

	unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
	sort: 'unsort', sorted
	i: the element in 'sort' that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
	mass_i: the mass of element i in 'sort'
	index: the index in the unsorted vector of 'mass_i'
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis

# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]

"""
 Read in the contribution fractions for the halos at each redshift for both distance cuts
 Each element ratio_300[i] will contain the contribution fractions for each halo at redshifts[i]
 
 This was done in the "Halo Properties" script
 
 NOTE: They will not all be the same length since the number of halos changes with redshift
 
    ratio_300: List
    ratio_20: List
        
        Each element ratio_300(20)[i] is an array of contribution fractions for each halo at redshifts[i+1]
"""
# d = [0, 300 kpc]
Filep1 = open("contribution_300_fullres.p", "rb")
ratio_300 = pickle.load(Filep1)
Filep1.close()
# d = [0, 20 kpc]
Filep2 = open("contribution_20_fullres.p", "rb")
ratio_20 = pickle.load(Filep2)
Filep2.close()

## d = [0, 300 kpc]

"""
 Find the indices of the most massive halos at each redshift
 
	M_i: List
		 
		 Each element M_i[i] corresponds to redshifts[i+1] and gives the most massive halo at that redshift
"""
M_i = [mass_index(hal[i]['star.mass'][his[i]], 0) for i in range(1, len(redshifts))]

"""
 Find the indices of the most massive halos at each redshift that contribute > 5% of stars to host at z = 0
 
	mass_ind: List
			  
			  Each element mass_ind[i] corresponds to redshifts[i+1] and gives the index of the most massive
			  halo that contributes > 5% of its stars to the host by z = 0 (or within 300 kpc of it)
			  
	Loops over all redshifts, finds the most massive halo at that redshift, then, it checks to see if it
	contributes > 5%, if not, it finds the second most massive, checks, etc.
"""
mass_ind = []
for i in range(0, len(ratio_300)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_300[i][temp]
    mass_ind.append(temp)
    if temp2 <= 0.05:
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_300[i][temp]
        mass_ind[i] = temp

# Find the mass ratios of the halos with the two indices
M_ratio = []
M_ratio.append(1.0)
for i in range(0, len(ratio_300)):
    M_ratio.append(hal[i+1]['star.mass'][mass_ind[i]]/hal[i+1]['star.mass'][M_i[i]])

# Plot this ratio
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, np.asarray(M_ratio))
plt.xlabel('$z$', fontsize=20)
plt.ylabel('$f$', fontsize=20)
plt.title('Something', fontsize=25)
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(ymin=0)
plt.xlim(9, 0)
plt.tight_layout()
plt.savefig('contribution_plot_300.pdf')
plt.show()

## d = [0, 20 kpc]

"""
 Find the indices of the most massive halos at each redshift that contribute > 5% of stars to host at z = 0
 
	mass_ind: List
			  
			  Each element mass_ind[i] corresponds to redshifts[i+1] and gives the index of the most massive
			  halo that contributes > 5% of its stars to the host by z = 0 (or within 20 kpc of it)
			  
	Loops over all redshifts, finds the most massive halo at that redshift, then, it checks to see if it
	contributes > 5%, if not, it finds the second most massive, checks, etc.
"""
mass_ind = []
for i in range(0, len(ratio_20)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_20[i][temp]
    mass_ind.append(temp)
    if temp2 <= 0.05:
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_20[i][temp]
        mass_ind[i] = temp

# Find the mass ratios of the halos with the two indices
M_ratio = []
M_ratio.append(1.0)
for i in range(0, len(ratio_20)):
    M_ratio.append(hal[i+1]['star.mass'][mass_ind[i]]/hal[i+1]['star.mass'][M_i[i]])

# Plot this ratio
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, np.asarray(M_ratio))
plt.xlabel('$z$', fontsize=20)
plt.ylabel('$f$', fontsize=20)
plt.title('Something', fontsize=25)
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(ymin=0)
plt.xlim(9, 0)
plt.tight_layout()
plt.savefig('contribution_plot_20.pdf')
plt.show()