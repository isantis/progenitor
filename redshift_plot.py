#!/usr/bin/python3

"""
 =================
 = Redshift Plot =
 =================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2018

 Goal: Plot the redshift estimates for progenitor formation
  
 NOTE: Going to do this for both values taken by eye and from files: last_insitu_redshifts.p & last_mr_redshifts.p
"""

#### Import all of the tools for analysis
from utilities import cosmology
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc

home_dir_stam = '/home1/05400/ibsantis'
cosmo_class = cosmology.CosmologyClass()

##### Need to read in the data for one simulation that way you can figure out what all the lookback times are
#     for each redshift, and then plot those on the other y-axis later on...

# Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/last_insitu_redshifts_v2.p', 'rb')
ie1 = pickle.load(File1) # Raw
ie2 = pickle.load(File1) # Smoothed
File1.close()
File2 = open(home_dir_stam+'/scripts/pickles/last_insitu_cum_redshifts_v2.p', 'rb')
ie_cum1 = pickle.load(File2) # Raw
ie_cum2 = pickle.load(File2) # Smoothed
File2.close()
File3 = open(home_dir_stam+'/scripts/pickles/last_mr_redshifts_v2.p', 'rb')
mr1 = pickle.load(File3) # Raw
mr2 = pickle.load(File3) # Smoothed
File3.close()
# Organize the mass ratio data to be like the insitu data
mr1_05 = [mr1[0][0], mr1[1][0], mr1[2][0], mr1[3][0]] # Raw
mr2_05 = [mr2[0][0], mr2[1][0], mr2[2][0], mr2[3][0]] # Smoothed
mr1_033 = [mr1[0][1], mr1[1][1], mr1[2][1], mr1[3][1]]
mr2_033 = [mr2[0][1], mr2[1][1], mr2[2][1], mr2[3][1]]
mr1_025 = [mr1[0][2], mr1[1][2], mr1[2][2], mr1[3][2]]
mr2_025 = [mr2[0][2], mr2[1][2], mr2[2][2], mr2[3][2]]

# Organize the data yet again...?
ie300s = [ie1[0], ie2[0], ie_cum1[0], ie_cum2[0]]
ie15s = [ie1[1], ie2[1], ie_cum1[1], ie_cum2[1]]
ie2s = [ie1[2], ie2[2], ie_cum1[2], ie_cum2[2]]
ie415s = [ie1[3], ie2[3], ie_cum1[3], ie_cum2[3]]
mr300s = [mr1_033[0], mr2_033[0], mr1_025[0], mr2_025[0]]
mr15s = [mr1_033[1], mr2_033[1], mr1_025[1], mr2_025[1]]
mr2s = [mr1_033[2], mr2_033[2], mr1_025[2], mr2_025[2]]
mr415s = [mr1_033[3], mr2_033[3], mr1_025[3], mr2_025[3]]

# Organize the data! AGAIN!
ieraw = [ie300s[0], ie15s[0], ie2s[0], ie415s[0]]
iesmooth = [ie300s[1], ie15s[1], ie2s[1], ie415s[1]]
iecumraw = [ie300s[2], ie15s[2], ie2s[2], ie415s[2]]
iecumsmooth = [ie300s[3], ie15s[3], ie2s[3], ie415s[3]]
mrraw033 = [mr300s[0], mr15s[0], mr2s[0], mr415s[0]]
mrsmooth033 = [mr300s[1], mr15s[1], mr2s[1], mr415s[1]]
mrraw025 = [mr300s[2], mr15s[2], mr2s[2], mr415s[2]]
mrsmooth025 = [mr300s[3], mr15s[3], mr2s[3], mr415s[3]]

## Read in individual data to calculate one and two sigma error bars
# Raw insitu
File4 = open(home_dir_stam+'/scripts/pickles/last_insitu_redshifts_indiv_v2.p', 'rb')
ind_ie_300 = pickle.load(File4)
ind_ie_15 = pickle.load(File4)
ind_ie_2 = pickle.load(File4)
ind_ie_415 = pickle.load(File4)
ind_ie_300_sm = pickle.load(File4)
ind_ie_15_sm = pickle.load(File4)
ind_ie_2_sm = pickle.load(File4)
ind_ie_415_sm = pickle.load(File4)
File4.close()
# Group the raw data together
ind_ie_raw = [ind_ie_300, ind_ie_15, ind_ie_2, ind_ie_415]
# Group the smoothed data together
ind_ie_sm = [ind_ie_300_sm, ind_ie_15_sm, ind_ie_2_sm, ind_ie_415_sm]
# Cumulative Insitu
File5 = open(home_dir_stam+'/scripts/pickles/last_insitu_redshifts_indiv_cum_v2.p', 'rb')
ind_ie_300_cum = pickle.load(File5)
ind_ie_15_cum = pickle.load(File5)
ind_ie_2_cum = pickle.load(File5)
ind_ie_415_cum = pickle.load(File5)
ind_ie_300_cum_sm = pickle.load(File5)
ind_ie_15_cum_sm = pickle.load(File5)
ind_ie_2_cum_sm = pickle.load(File5)
ind_ie_415_cum_sm = pickle.load(File5)
File5.close()
# Group the raw data together
ind_iec_raw = [ind_ie_300_cum, ind_ie_15_cum, ind_ie_2_cum, ind_ie_415_cum]
# Group the smoothed data together
ind_iec_sm = [ind_ie_300_cum_sm, ind_ie_15_cum_sm, ind_ie_2_cum_sm, ind_ie_415_cum_sm]
# Mass Ratios
File6 = open(home_dir_stam+'/scripts/pickles/last_mr_redshifts_ind_v2.p', 'rb')
ind_mr_300 = pickle.load(File6)
ind_mr_15 = pickle.load(File6)
ind_mr_2 = pickle.load(File6)
ind_mr_415 = pickle.load(File6)
ind_mr_300_sm = pickle.load(File6)
ind_mr_15_sm = pickle.load(File6)
ind_mr_2_sm = pickle.load(File6)
ind_mr_415_sm = pickle.load(File6)
File6.close()
# Group the raw 0.33 data together
ind_mr_raw_033 = [ind_mr_300[1], ind_mr_15[1], ind_mr_2[1], ind_mr_415[1]]
# Group the smoothed 0.33 data together
ind_mr_sm_033 = [ind_mr_300_sm[1], ind_mr_15_sm[1], ind_mr_2_sm[1], ind_mr_415_sm[1]]
# Group the raw 0.25 data together
ind_mr_raw_025 = [ind_mr_300[2], ind_mr_15[2], ind_mr_2[2], ind_mr_415[2]]
# Group the smoothed 0.25 data together
ind_mr_sm_025 = [ind_mr_300_sm[2], ind_mr_15_sm[2], ind_mr_2_sm[2], ind_mr_415_sm[2]]

# Calculate the one and two sigma error bars
onesigp = 84.13
onesigm = 15.87
twosigp = 97.72
twosigm = 2.28
# Insitu raw
err_ieraw_op = np.zeros(len(ind_ie_raw))
err_ieraw_om = np.zeros(len(ind_ie_raw))
err_ieraw_tp = np.zeros(len(ind_ie_raw))
err_ieraw_tm = np.zeros(len(ind_ie_raw))
for i in range(0, len(ind_ie_raw)):
    temp_r = np.asarray(ind_ie_raw[i])
    err_ieraw_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_ieraw_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_ieraw_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_ieraw_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Insitu smooth
err_iesm_op = np.zeros(len(ind_ie_sm))
err_iesm_om = np.zeros(len(ind_ie_sm))
err_iesm_tp = np.zeros(len(ind_ie_sm))
err_iesm_tm = np.zeros(len(ind_ie_sm))
for i in range(0, len(ind_ie_sm)):
    temp_r = np.asarray(ind_ie_sm[i])
    err_iesm_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iesm_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iesm_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iesm_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Insitu cumulative raw
err_iecraw_op = np.zeros(len(ind_iec_raw))
err_iecraw_om = np.zeros(len(ind_iec_raw))
err_iecraw_tp = np.zeros(len(ind_iec_raw))
err_iecraw_tm = np.zeros(len(ind_iec_raw))
for i in range(0, len(ind_iec_raw)):
    temp_r = np.asarray(ind_iec_raw[i])
    err_iecraw_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iecraw_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iecraw_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iecraw_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Insitu cumulative smooth
err_iecsm_op = np.zeros(len(ind_iec_sm))
err_iecsm_om = np.zeros(len(ind_iec_sm))
err_iecsm_tp = np.zeros(len(ind_iec_sm))
err_iecsm_tm = np.zeros(len(ind_iec_sm))
for i in range(0, len(ind_iec_sm)):
    temp_r = np.asarray(ind_iec_sm[i])
    err_iecsm_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iecsm_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iecsm_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iecsm_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.33 raw
err_mrraw033_op = np.zeros(len(ind_mr_raw_033))
err_mrraw033_om = np.zeros(len(ind_mr_raw_033))
err_mrraw033_tp = np.zeros(len(ind_mr_raw_033))
err_mrraw033_tm = np.zeros(len(ind_mr_raw_033))
for i in range(0, len(ind_mr_raw_033)):
    temp_r = np.asarray(ind_mr_raw_033[i])
    err_mrraw033_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_mrraw033_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_mrraw033_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_mrraw033_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.33 smoothed
err_mrsm033_op = np.zeros(len(ind_mr_sm_033))
err_mrsm033_om = np.zeros(len(ind_mr_sm_033))
err_mrsm033_tp = np.zeros(len(ind_mr_sm_033))
err_mrsm033_tm = np.zeros(len(ind_mr_sm_033))
for i in range(0, len(ind_mr_sm_033)):
    temp_r = np.asarray(ind_mr_sm_033[i])
    err_mrsm033_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_mrsm033_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_mrsm033_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_mrsm033_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.25 raw
err_mrraw025_op = np.zeros(len(ind_mr_raw_025))
err_mrraw025_om = np.zeros(len(ind_mr_raw_025))
err_mrraw025_tp = np.zeros(len(ind_mr_raw_025))
err_mrraw025_tm = np.zeros(len(ind_mr_raw_025))
for i in range(0, len(ind_mr_raw_025)):
    temp_r = np.asarray(ind_mr_raw_025[i])
    err_mrraw025_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_mrraw025_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_mrraw025_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_mrraw025_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.33 smoothed
err_mrsm025_op = np.zeros(len(ind_mr_sm_025))
err_mrsm025_om = np.zeros(len(ind_mr_sm_025))
err_mrsm025_tp = np.zeros(len(ind_mr_sm_025))
err_mrsm025_tm = np.zeros(len(ind_mr_sm_025))
for i in range(0, len(ind_mr_sm_025)):
    temp_r = np.asarray(ind_mr_sm_025[i])
    err_mrsm025_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_mrsm025_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_mrsm025_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_mrsm025_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)

# Plot the data
colors = dc.get_distinct(4)
xs = np.arange(1,5)
inc = [0.0, 0.1, 0.2, 0.3]
leg = ['300 kpc cut', '15 kpc cut', '2 kpc cut', '4-15 kpc cut']

fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
"""
# Plot all 16 errors for ieraw
plt.errorbar(xs[0]+inc[0], ieraw[0], yerr=np.array([[ieraw[0]-err_ieraw_tm[0]], [err_ieraw_tp[0]]-ieraw[0]]), color=colors[0], alpha=0.30) # 300 kpc ie raw 2 sigma?
plt.errorbar(xs[0]+inc[0], ieraw[0], yerr=np.array([[ieraw[0]-err_ieraw_om[0]], [err_ieraw_op[0]]-ieraw[0]]), color=colors[0], alpha=0.80) # 300 kpc ie raw 1 sigma?
plt.errorbar(xs[0]+inc[1], ieraw[1], yerr=np.array([[ieraw[1]-err_ieraw_tm[1]], [err_ieraw_tp[1]]-ieraw[1]]), color=colors[1], alpha=0.30) # 15 kpc
plt.errorbar(xs[0]+inc[1], ieraw[1], yerr=np.array([[ieraw[1]-err_ieraw_om[1]], [err_ieraw_op[1]]-ieraw[1]]), color=colors[1], alpha=0.80)
plt.errorbar(xs[0]+inc[2], ieraw[2], yerr=np.array([[ieraw[2]-err_ieraw_tm[2]], [err_ieraw_tp[2]]-ieraw[2]]), color=colors[2], alpha=0.30) # 2 kpc
plt.errorbar(xs[0]+inc[2], ieraw[2], yerr=np.array([[ieraw[2]-err_ieraw_om[2]], [err_ieraw_op[2]]-ieraw[2]]), color=colors[2], alpha=0.80)
plt.errorbar(xs[0]+inc[3], ieraw[3], yerr=np.array([[ieraw[3]-err_ieraw_tm[3]], [err_ieraw_tp[3]]-ieraw[3]]), color=colors[3], alpha=0.30) # 4-15 kpc
plt.errorbar(xs[0]+inc[3], ieraw[3], yerr=np.array([[ieraw[3]-err_ieraw_om[3]], [err_ieraw_op[3]]-ieraw[3]]), color=colors[3], alpha=0.80)
# Plot ieraw
[plt.scatter(xs[0]+inc[i], ieraw[i], color=colors[i], s=50.0, label=leg[i]) for i in range(0, len(inc))]
"""
# Plot all 16 errors for iesmooth
plt.errorbar(xs[0]+inc[0], iesmooth[0], yerr=np.array([[iesmooth[0]-err_iesm_tm[0]], [err_iesm_tp[0]]-iesmooth[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[0]+inc[0], iesmooth[0], yerr=np.array([[iesmooth[0]-err_iesm_om[0]], [err_iesm_op[0]]-iesmooth[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[0]+inc[1], iesmooth[1], yerr=np.array([[iesmooth[1]-err_iesm_tm[1]], [err_iesm_tp[1]]-iesmooth[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[0]+inc[1], iesmooth[1], yerr=np.array([[iesmooth[1]-err_iesm_om[1]], [err_iesm_op[1]]-iesmooth[1]]), color=colors[1], alpha=0.80)
plt.errorbar(xs[0]+inc[2], iesmooth[2], yerr=np.array([[iesmooth[2]-err_iesm_tm[2]], [err_iesm_tp[2]]-iesmooth[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[0]+inc[2], iesmooth[2], yerr=np.array([[iesmooth[2]-err_iesm_om[2]], [err_iesm_op[2]]-iesmooth[2]]), color=colors[2], alpha=0.80)
plt.errorbar(xs[0]+inc[3], iesmooth[3], yerr=np.array([[iesmooth[3]-err_iesm_tm[3]], [err_iesm_tp[3]]-iesmooth[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[0]+inc[3], iesmooth[3], yerr=np.array([[iesmooth[3]-err_iesm_om[3]], [err_iesm_op[3]]-iesmooth[3]]), color=colors[3], alpha=0.80)
# Plot iesmooth
[plt.scatter(xs[0]+inc[i], iesmooth[i], color=colors[i], s=50.0, label=leg[i]) for i in range(0, len(inc))]
"""
# Plot all 16 errors for iecumraw
plt.errorbar(xs[2]+inc[0], iecumraw[0], yerr=np.array([[iecumraw[0]-err_iecraw_tm[0]], [err_iecraw_tp[0]]-iecumraw[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[2]+inc[0], iecumraw[0], yerr=np.array([[iecumraw[0]-err_iecraw_om[0]], [err_iecraw_op[0]]-iecumraw[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[2]+inc[1], iecumraw[1], yerr=np.array([[iecumraw[1]-err_iecraw_tm[1]], [err_iecraw_tp[1]]-iecumraw[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[2]+inc[1], iecumraw[1], yerr=np.array([[iecumraw[1]-err_iecraw_om[1]], [err_iecraw_op[1]]-iecumraw[1]]), color=colors[1], alpha=0.80
plt.errorbar(xs[2]+inc[2], iecumraw[2], yerr=np.array([[iecumraw[2]-err_iecraw_tm[2]], [err_iecraw_tp[2]]-iecumraw[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[2]+inc[2], iecumraw[2], yerr=np.array([[iecumraw[2]-err_iecraw_om[2]], [err_iecraw_op[2]]-iecumraw[2]]), color=colors[2], alpha=0.80
plt.errorbar(xs[2]+inc[3], iecumraw[3], yerr=np.array([[iecumraw[3]-err_iecraw_tm[3]], [err_iecraw_tp[3]]-iecumraw[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[2]+inc[3], iecumraw[3], yerr=np.array([[iecumraw[3]-err_iecraw_om[3]], [err_iecraw_op[3]]-iecumraw[3]]), color=colors[3], alpha=0.80
# Plot ie cumulative raw
[plt.scatter(xs[2]+inc[i], iecumraw[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
"""
# Plot all 16 errors for iecumsmooth
plt.errorbar(xs[1]+inc[0], iecumsmooth[0], yerr=np.array([[iecumsmooth[0]-err_iecsm_tm[0]], [err_iecsm_tp[0]]-iecumsmooth[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[1]+inc[0], iecumsmooth[0], yerr=np.array([[iecumsmooth[0]-err_iecsm_om[0]], [err_iecsm_op[0]]-iecumsmooth[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[1]+inc[1], iecumsmooth[1], yerr=np.array([[iecumsmooth[1]-err_iecsm_tm[1]], [err_iecsm_tp[1]]-iecumsmooth[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[1]+inc[1], iecumsmooth[1], yerr=np.array([[iecumsmooth[1]-err_iecsm_om[1]], [err_iecsm_op[1]]-iecumsmooth[1]]), color=colors[1], alpha=0.80)
plt.errorbar(xs[1]+inc[2], iecumsmooth[2], yerr=np.array([[iecumsmooth[2]-err_iecsm_tm[2]], [err_iecsm_tp[2]]-iecumsmooth[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[1]+inc[2], iecumsmooth[2], yerr=np.array([[iecumsmooth[2]-err_iecsm_om[2]], [err_iecsm_op[2]]-iecumsmooth[2]]), color=colors[2], alpha=0.80)
plt.errorbar(xs[1]+inc[3], iecumsmooth[3], yerr=np.array([[iecumsmooth[3]-err_iecsm_tm[3]], [err_iecsm_tp[3]]-iecumsmooth[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[1]+inc[3], iecumsmooth[3], yerr=np.array([[iecumsmooth[3]-err_iecsm_om[3]], [err_iecsm_op[3]]-iecumsmooth[3]]), color=colors[3], alpha=0.80)
# Plot ie cumulative smooth
[plt.scatter(xs[1]+inc[i], iecumsmooth[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
"""
# Plot all 16 errors for mrraw033
plt.errorbar(xs[4]+inc[0], mrraw033[0], yerr=np.array([[mrraw033[0]-err_mrraw033_tm[0]], [err_mrraw033_tp[0]]-mrraw033[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[4]+inc[0], mrraw033[0], yerr=np.array([[mrraw033[0]-err_mrraw033_om[0]], [err_mrraw033_op[0]]-mrraw033[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[4]+inc[1], mrraw033[1], yerr=np.array([[mrraw033[1]-err_mrraw033_tm[1]], [err_mrraw033_tp[1]]-mrraw033[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[4]+inc[1], mrraw033[1], yerr=np.array([[mrraw033[1]-err_mrraw033_om[1]], [err_mrraw033_op[1]]-mrraw033[1]]), color=colors[1], alpha=0.80
plt.errorbar(xs[4]+inc[2], mrraw033[2], yerr=np.array([[mrraw033[2]-err_mrraw033_tm[2]], [err_mrraw033_tp[2]]-mrraw033[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[4]+inc[2], mrraw033[2], yerr=np.array([[mrraw033[2]-err_mrraw033_om[2]], [err_mrraw033_op[2]]-mrraw033[2]]), color=colors[2], alpha=0.80
plt.errorbar(xs[4]+inc[3], mrraw033[3], yerr=np.array([[mrraw033[3]-err_mrraw033_tm[3]], [err_mrraw033_tp[3]]-mrraw033[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[4]+inc[3], mrraw033[3], yerr=np.array([[mrraw033[3]-err_mrraw033_om[3]], [err_mrraw033_op[3]]-mrraw033[3]]), color=colors[3], alpha=0.80
# Plot mass ratio raw 0.33
[plt.scatter(xs[4]+inc[i], mrraw033[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
"""
# Plot all 16 errors for mrsmooth033
plt.errorbar(xs[2]+inc[0], mrsmooth033[0], yerr=np.array([[mrsmooth033[0]-err_mrsm033_tm[0]], [err_mrsm033_tp[0]]-mrsmooth033[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[2]+inc[0], mrsmooth033[0], yerr=np.array([[mrsmooth033[0]-err_mrsm033_om[0]], [err_mrsm033_op[0]]-mrsmooth033[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[2]+inc[1], mrsmooth033[1], yerr=np.array([[mrsmooth033[1]-err_mrsm033_tm[1]], [err_mrsm033_tp[1]]-mrsmooth033[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[2]+inc[1], mrsmooth033[1], yerr=np.array([[mrsmooth033[1]-err_mrsm033_om[1]], [err_mrsm033_op[1]]-mrsmooth033[1]]), color=colors[1], alpha=0.80)
plt.errorbar(xs[2]+inc[2], mrsmooth033[2], yerr=np.array([[mrsmooth033[2]-err_mrsm033_tm[2]], [err_mrsm033_tp[2]]-mrsmooth033[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[2]+inc[2], mrsmooth033[2], yerr=np.array([[mrsmooth033[2]-err_mrsm033_om[2]], [err_mrsm033_op[2]]-mrsmooth033[2]]), color=colors[2], alpha=0.80)
plt.errorbar(xs[2]+inc[3], mrsmooth033[3], yerr=np.array([[mrsmooth033[3]-err_mrsm033_tm[3]], [err_mrsm033_tp[3]]-mrsmooth033[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[2]+inc[3], mrsmooth033[3], yerr=np.array([[mrsmooth033[3]-err_mrsm033_om[3]], [err_mrsm033_op[3]]-mrsmooth033[3]]), color=colors[3], alpha=0.80)
# Plot mass ratio smooth 0.33
[plt.scatter(xs[2]+inc[i], mrsmooth033[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
"""
# Plot all 16 errors for mrraw025
plt.errorbar(xs[6]+inc[0], mrraw025[0], yerr=np.array([[mrraw025[0]-err_mrraw025_tm[0]], [err_mrraw025_tp[0]]-mrraw025[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[6]+inc[0], mrraw025[0], yerr=np.array([[mrraw025[0]-err_mrraw025_om[0]], [err_mrraw025_op[0]]-mrraw025[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[6]+inc[1], mrraw025[1], yerr=np.array([[mrraw025[1]-err_mrraw025_tm[1]], [err_mrraw025_tp[1]]-mrraw025[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[6]+inc[1], mrraw025[1], yerr=np.array([[mrraw025[1]-err_mrraw025_om[1]], [err_mrraw025_op[1]]-mrraw025[1]]), color=colors[1], alpha=0.80
plt.errorbar(xs[6]+inc[2], mrraw025[2], yerr=np.array([[mrraw025[2]-err_mrraw025_tm[2]], [err_mrraw025_tp[2]]-mrraw025[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[6]+inc[2], mrraw025[2], yerr=np.array([[mrraw025[2]-err_mrraw025_om[2]], [err_mrraw025_op[2]]-mrraw025[2]]), color=colors[2], alpha=0.80
plt.errorbar(xs[6]+inc[3], mrraw025[3], yerr=np.array([[mrraw025[3]-err_mrraw025_tm[3]], [err_mrraw025_tp[3]]-mrraw025[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[6]+inc[3], mrraw025[3], yerr=np.array([[mrraw025[3]-err_mrraw025_om[3]], [err_mrraw025_op[3]]-mrraw025[3]]), color=colors[3], alpha=0.80
# Plot mass ratio raw 0.25
[plt.scatter(xs[6]+inc[i], mrraw025[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
"""
# Plot all 16 errors for mrsmooth025
plt.errorbar(xs[3]+inc[0], mrsmooth025[0], yerr=np.array([[mrsmooth025[0]-err_mrsm025_tm[0]], [err_mrsm025_tp[0]]-mrsmooth025[0]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[3]+inc[0], mrsmooth025[0], yerr=np.array([[mrsmooth025[0]-err_mrsm025_om[0]], [err_mrsm025_op[0]]-mrsmooth025[0]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[3]+inc[1], mrsmooth025[1], yerr=np.array([[mrsmooth025[1]-err_mrsm025_tm[1]], [err_mrsm025_tp[1]]-mrsmooth025[1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[3]+inc[1], mrsmooth025[1], yerr=np.array([[mrsmooth025[1]-err_mrsm025_om[1]], [err_mrsm025_op[1]]-mrsmooth025[1]]), color=colors[1], alpha=0.80)
plt.errorbar(xs[3]+inc[2], mrsmooth025[2], yerr=np.array([[mrsmooth025[2]-err_mrsm025_tm[2]], [err_mrsm025_tp[2]]-mrsmooth025[2]]), color=colors[2], alpha=0.30)
plt.errorbar(xs[3]+inc[2], mrsmooth025[2], yerr=np.array([[mrsmooth025[2]-err_mrsm025_om[2]], [err_mrsm025_op[2]]-mrsmooth025[2]]), color=colors[2], alpha=0.80)
plt.errorbar(xs[3]+inc[3], mrsmooth025[3], yerr=np.array([[mrsmooth025[3]-err_mrsm025_tm[3]], [err_mrsm025_tp[3]]-mrsmooth025[3]]), color=colors[3], alpha=0.30)
plt.errorbar(xs[3]+inc[3], mrsmooth025[3], yerr=np.array([[mrsmooth025[3]-err_mrsm025_om[3]], [err_mrsm025_op[3]]-mrsmooth025[3]]), color=colors[3], alpha=0.80)
# Plot mass ratio smooth 0.33
[plt.scatter(xs[3]+inc[i], mrsmooth025[i], color=colors[i], s=50.0) for i in range(0, len(inc))]
my_xticks1 = ['insitu','insitu (cumulative)']
my_xticks2 = ['mass ratio (0.33)','mass ratio (0.25)']
my_xticks_tot = my_xticks1+my_xticks2
my_xs = np.arange(1, 5)
plt.xticks(my_xs, my_xticks_tot, rotation=45)
plt.ylabel('redshift', fontsize=40)
plt.title('Progenitor Formation Redshifts', fontsize=36, y=1.02)
plt.legend(prop={'size': 14}, loc='best')
plt.tick_params(axis='x', which='major', labelsize=20)
plt.tick_params(axis='y', which='major', labelsize=32)
plt.ylim(1, 6)
plt.yticks(np.arange(1, 7, 1.0))

# put the new axis stuff here 
ax2 = ax1.twinx()
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [1,2,3,4,5,6]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_locations = [0,1,2,3,4,5]

ax2.set_yticks(axis_2_tick_locations)
ax2.set_yticklabels(axis_2_tick_labels)
ax2.set_ylabel(axis_2_label, labelpad=9)
ax2.tick_params(pad=3)

plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/redshift_form_v5.pdf')