#!/usr/bin/python3

"""
 ==============================
 = Weighted Mass Functions LG =
 ==============================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) for Summer Session I, 2019

 Goal: Create new kinds of mass functions, need to review how these are supposed to be exactly...
       What I'm doing right now seems a little strange...
"""

### Import all of the tools for analysis and read in the data
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import distinct_colours as dc
import pickle

# 	Read in the halo catalogs at each redshift
gal1 = 'Thelma'
gal2 = 'Louise'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

# Set up the colors
colors = dc.get_distinct(8)

# Some functions that help with analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
    d: distance
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Infinity], ind)
    return ind

# Read in the contribution fractions for the halos at each redshift
pickle300_1 = home_dir_stam+'/scripts/pickles/contribution_300_fullres_'+gal1+'.p'
pickle15_1 = home_dir_stam+'/scripts/pickles/contribution_15_fullres_'+gal1+'.p'
pickle2_1 = home_dir_stam+'/scripts/pickles/contribution_2_fullres_'+gal1+'.p'
pickle300_2 = home_dir_stam+'/scripts/pickles/contribution_300_fullres_'+gal2+'.p'
pickle15_2 = home_dir_stam+'/scripts/pickles/contribution_15_fullres_'+gal2+'.p'
pickle2_2 = home_dir_stam+'/scripts/pickles/contribution_2_fullres_'+gal2+'.p'
## Galaxy 1
# d = [0, 300 kpc]
Filep1 = open(pickle300_1, "rb")
ratio_300_1 = pickle.load(Filep1)
Filep1.close()
# d = [0, 15 kpc]
Filep2 = open(pickle15_1, "rb")
ratio_15_1 = pickle.load(Filep2)
Filep2.close()
#d = [0, 2 kpc]
Filep3 = open(pickle2_1, "rb")
ratio_2_1 = pickle.load(Filep3)
Filep3.close()
## Galaxy 2
# d = [0, 300 kpc]
Filep4 = open(pickle300_2, "rb")
ratio_300_2 = pickle.load(Filep4)
Filep4.close()
# d = [0, 15 kpc]
Filep5 = open(pickle15_2, "rb")
ratio_15_2 = pickle.load(Filep5)
Filep5.close()
#d = [0, 2 kpc]
Filep6 = open(pickle2_2, "rb")
ratio_2_2 = pickle.load(Filep6)
Filep6.close()

# Read in the data of stars at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, simulation_directory=simulation_dir_stam)

# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]

# Set up some indices
hal_inds = np.array([10,20,30,40,45,50,51])
contr_inds = np.array([9,19,29,39,44,49,50])
# Set up mass vector
Ms = np.array([4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11])

# Get the indices of the most massive host that contributes stars at each redshift, going to exclude this from results
# Define the function to do this
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

## Galaxy 1 ############################################################################################################
mass_ind_1_1 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
for i in range(0, len(ratio_300_1)):
    # Get the most massive right away
    m1 = 0
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    # Check the ratio
    tt1 = ratio_300_1[i][t1]
    # Set the indices to the initial values
    mass_ind_1_1[i+1] = t1
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    # Make sure MMH has a contribution > 1%
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_300_1[i])):
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_300_1[i][t1]
        mass_ind_1_1[i+1] = t1
        check1[i+1] = tt1

### d = 300 kpc
# Get the mass of stars within 300 kpc at z = 0 for different redshift formation phases
ind300 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, 300])
M300_1 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind300)
    M300_1[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_300_1 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_1[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_300_1[contr_inds[i]][mask])/M300_1[i]
    fractions_300_1.append(f_mass)

### d = 15 kpc
# Get the mass of stars within 15 kpc at z = 0 for different redshift formation phases
ind15 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, 15])
M15_1 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind15)
    M15_1[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_15_1 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_1[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_15_1[contr_inds[i]][mask])/M15_1[i]
    fractions_15_1.append(f_mass)

### d = 2 kpc
# Get the mass of stars within 2 kpc at z = 0 for different redshift formation phases
ind2 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, 2])
M2_1 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind2)
    M2_1[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_2_1 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_1[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_2_1[contr_inds[i]][mask])/M2_1[i]
    fractions_2_1.append(f_mass)

## Galaxy 2 ############################################################################################################
mass_ind_1_2 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
for i in range(0, len(ratio_300_2)):
    # Get the most massive right away
    m1 = 0
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    # Check the ratio
    tt1 = ratio_300_2[i][t1]
    # Set the indices to the initial values
    mass_ind_1_2[i+1] = t1
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    # Make sure MMH has a contribution > 1%
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_300_2[i])):
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_300_2[i][t1]
        mass_ind_1_2[i+1] = t1
        check1[i+1] = tt1

### d = 300 kpc
# Get the mass of stars within 300 kpc at z = 0 for different redshift formation phases
ind300 = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [0, 300])
M300_2 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind300)
    M300_2[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_300_2 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_2[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_300_2[contr_inds[i]][mask])/M300_2[i]
    fractions_300_2.append(f_mass)

### d = 15 kpc
# Get the mass of stars within 15 kpc at z = 0 for different redshift formation phases
ind15 = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [0, 15])
M15_2 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind15)
    M15_2[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_15_2 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_2[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_15_2[contr_inds[i]][mask])/M15_2[i]
    fractions_15_2.append(f_mass)

### d = 2 kpc
# Get the mass of stars within 2 kpc at z = 0 for different redshift formation phases
ind2 = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [0, 2])
M2_2 = np.zeros(len(hal_inds))
for i in range(0, len(hal_inds)):
    inds = ut.array.get_indices(part['star'].prop('form.redshift'), [redshifts[hal_inds[i]], np.Inf], ind2)
    M2_2[i] = np.sum(part['star']['mass'][inds])
# Get the fraction vectors for each redshift
fractions_2_2 = []
for i in range(0, len(hal_inds)):
    f_mass = np.zeros(len(Ms)-1)
    for j in range(0, len(Ms)-1):
        mmp_mask = (his[hal_inds[i]] != his[hal_inds[i]][int(mass_ind_1_2[hal_inds[i]])])
        mass_mask = (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) >= Ms[j]) & (np.log10(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]]) <= Ms[j+1])
        mask = mmp_mask & mass_mask
        f_mass[j] = np.sum(hal[hal_inds[i]]['star.mass'][his[hal_inds[i]]][mask]*ratio_2_2[contr_inds[i]][mask])/M2_2[i]
    fractions_2_2.append(f_mass)

#########################################################################################################################
## Galaxy 1
# Plot these fractions for each redshift (Differential)
Ms_plot = np.array([4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75])
colors = dc.get_distinct(3)
plt.figure(1)
plt.figure(figsize=(8.5, 11.5))
# z = 1
ax = plt.subplot(421)
plt.plot(10**(Ms_plot), fractions_300_1[0], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[0], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[0], color=colors[2], label='f$_{2}$')
plt.legend(prop={'size': 12})
#plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
tix = 10**(np.arange(4,11,0.5))
plt.xticks(tix)
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 1', fontsize=12)
# z = 2
ax = plt.subplot(422)
plt.plot(10**(Ms_plot), fractions_300_1[1], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[1], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[1], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 2', fontsize=12)
# z = 3
ax = plt.subplot(423)
plt.plot(10**(Ms_plot), fractions_300_1[2], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[2], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[2], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 3', fontsize=12)
# z = 4
ax = plt.subplot(424)
plt.plot(10**(Ms_plot), fractions_300_1[3], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[3], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[3], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 4', fontsize=12)
# z = 5
ax = plt.subplot(425)
plt.plot(10**(Ms_plot), fractions_300_1[4], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[4], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[4], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 5', fontsize=12)
# z = 6
ax = plt.subplot(426)
plt.plot(10**(Ms_plot), fractions_300_1[5], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[5], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[5], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 6', fontsize=12)
# z = 7
ax = plt.subplot(427)
plt.plot(10**(Ms_plot), fractions_300_1[6], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1[6], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1[6], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 7', fontsize=12)
#
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_'+gal1+'_v5.pdf')
plt.close()

# Cumulatively sum up the fractions for each redshift
fractions_300_1_cum = [np.cumsum(fractions_300_1[i]) for i in range(0, len(fractions_300_1))]
fractions_15_1_cum = [np.cumsum(fractions_15_1[i]) for i in range(0, len(fractions_15_1))]
fractions_2_1_cum = [np.cumsum(fractions_2_1[i]) for i in range(0, len(fractions_2_1))]

# Plot the cumulative fractions as a function of mass for each redshift
plt.figure(2)
plt.figure(figsize=(8.5, 11.5))
# z = 1
ax = plt.subplot(421)
plt.plot(10**(Ms_plot), fractions_300_1_cum[0], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[0], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[0], color=colors[2], label='f$_{2}$')
plt.legend(prop={'size': 12})
#plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
tix = 10**(np.arange(4,11,0.5))
plt.xticks(tix)
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 1', fontsize=12)
# z = 2
ax = plt.subplot(422)
plt.plot(10**(Ms_plot), fractions_300_1_cum[1], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[1], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[1], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 2', fontsize=12)
# z = 3
ax = plt.subplot(423)
plt.plot(10**(Ms_plot), fractions_300_1_cum[2], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[2], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[2], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 3', fontsize=12)
# z = 4
ax = plt.subplot(424)
plt.plot(10**(Ms_plot), fractions_300_1_cum[3], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[3], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[3], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 4', fontsize=12)
# z = 5
ax = plt.subplot(425)
plt.plot(10**(Ms_plot), fractions_300_1_cum[4], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[4], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[4], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 5', fontsize=12)
# z = 6
ax = plt.subplot(426)
plt.plot(10**(Ms_plot), fractions_300_1_cum[5], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[5], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[5], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 6', fontsize=12)
# z = 7
ax = plt.subplot(427)
plt.plot(10**(Ms_plot), fractions_300_1_cum[6], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_1_cum[6], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_1_cum[6], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 7', fontsize=12)
#
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_cum_'+gal1+'_v5.pdf')
plt.close()


########################################################################################################################
## Galaxy 2
# Plot these fractions for each redshift (Differential)
Ms_plot = np.array([4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75])
colors = dc.get_distinct(3)
plt.figure(3)
plt.figure(figsize=(8.5, 11.5))
# z = 1
ax = plt.subplot(421)
plt.plot(10**(Ms_plot), fractions_300_2[0], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[0], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[0], color=colors[2], label='f$_{2}$')
plt.legend(prop={'size': 12})
#plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
tix = 10**(np.arange(4,11,0.5))
plt.xticks(tix)
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 1', fontsize=12)
# z = 2
ax = plt.subplot(422)
plt.plot(10**(Ms_plot), fractions_300_2[1], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[1], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[1], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 2', fontsize=12)
# z = 3
ax = plt.subplot(423)
plt.plot(10**(Ms_plot), fractions_300_2[2], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[2], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[2], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 3', fontsize=12)
# z = 4
ax = plt.subplot(424)
plt.plot(10**(Ms_plot), fractions_300_2[3], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[3], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[3], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 4', fontsize=12)
# z = 5
ax = plt.subplot(425)
plt.plot(10**(Ms_plot), fractions_300_2[4], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[4], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[4], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 5', fontsize=12)
# z = 6
ax = plt.subplot(426)
plt.plot(10**(Ms_plot), fractions_300_2[5], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[5], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[5], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 6', fontsize=12)
# z = 7
ax = plt.subplot(427)
plt.plot(10**(Ms_plot), fractions_300_2[6], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2[6], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2[6], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 7', fontsize=12)
#
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_'+gal2+'_v5.pdf')
plt.close()

# Cumulatively sum up the fractions for each redshift
fractions_300_2_cum = [np.cumsum(fractions_300_2[i]) for i in range(0, len(fractions_300_2))]
fractions_15_2_cum = [np.cumsum(fractions_15_2[i]) for i in range(0, len(fractions_15_2))]
fractions_2_2_cum = [np.cumsum(fractions_2_2[i]) for i in range(0, len(fractions_2_2))]

# Plot the cumulative fractions as a function of mass for each redshift
plt.figure(4)
plt.figure(figsize=(8.5, 11.5))
# z = 1
ax = plt.subplot(421)
plt.plot(10**(Ms_plot), fractions_300_2_cum[0], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[0], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[0], color=colors[2], label='f$_{2}$')
plt.legend(prop={'size': 12})
#plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
tix = 10**(np.arange(4,11,0.5))
plt.xticks(tix)
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 1', fontsize=12)
# z = 2
ax = plt.subplot(422)
plt.plot(10**(Ms_plot), fractions_300_2_cum[1], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[1], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[1], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 2', fontsize=12)
# z = 3
ax = plt.subplot(423)
plt.plot(10**(Ms_plot), fractions_300_2_cum[2], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[2], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[2], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 3', fontsize=12)
# z = 4
ax = plt.subplot(424)
plt.plot(10**(Ms_plot), fractions_300_2_cum[3], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[3], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[3], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 4', fontsize=12)
# z = 5
ax = plt.subplot(425)
plt.plot(10**(Ms_plot), fractions_300_2_cum[4], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[4], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[4], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 5', fontsize=12)
# z = 6
ax = plt.subplot(426)
plt.plot(10**(Ms_plot), fractions_300_2_cum[5], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[5], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[5], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 6', fontsize=12)
# z = 7
ax = plt.subplot(427)
plt.plot(10**(Ms_plot), fractions_300_2_cum[6], color=colors[0], label='f$_{300}$')
plt.plot(10**(Ms_plot), fractions_15_2_cum[6], color=colors[1], label='f$_{15}$')
plt.plot(10**(Ms_plot), fractions_2_2_cum[6], color=colors[2], label='f$_{2}$')
plt.xticks(10**(Ms))
plt.tick_params(axis='x', which='both', labelsize=10, width=1.0)
plt.tick_params(axis='y', which='both', labelsize=10, width=1.0)
plt.xlim(10**4,10**11)
plt.ylim(10**(-7), 1)
plt.xscale('log')
plt.yscale('log')
plt.ylabel('f$_{\\rm m}$(m)', fontsize=12)
plt.xlabel('M$_{\\rm star}$', fontsize=12)
plt.title('z = 7', fontsize=12)
#
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_cum_'+gal2+'_v5.pdf')
plt.close()

# Save the mass function vectors to a pickle file to use in the median curves
Filep1 = open(home_dir_stam+"/scripts/pickles/mf_weighted_"+gal1+".p", "wb")
pickle.dump(fractions_300_1, Filep1)
pickle.dump(fractions_15_1, Filep1)
pickle.dump(fractions_2_1, Filep1)
pickle.dump(fractions_300_1_cum, Filep1)
pickle.dump(fractions_15_1_cum, Filep1)
pickle.dump(fractions_2_1_cum, Filep1)
Filep1.close()
# Save the mass function vectors to a pickle file to use in the median curves
Filep2 = open(home_dir_stam+"/scripts/pickles/mf_weighted_"+gal2+".p", "wb")
pickle.dump(fractions_300_2, Filep2)
pickle.dump(fractions_15_2, Filep2)
pickle.dump(fractions_2_2, Filep2)
pickle.dump(fractions_300_2_cum, Filep2)
pickle.dump(fractions_15_2_cum, Filep2)
pickle.dump(fractions_2_2_cum, Filep2)
Filep2.close()
