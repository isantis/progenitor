#!/usr/bin/python3

"""
 =================
 = N vs z padder =
 =================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2019

 Goal: Pad all of the N vs z data so that the smoothed curves converge to the right answer
 
 NOTE: Uses the files made from "Nvz_v2.py" or "Nvz_v3.py" or "Nvz_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import pickle

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12b.p', 'rb')
N300b = pickle.load(File1)
N15b = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12c.p', 'rb')
N300c = pickle.load(File2)
N15c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12f.p', 'rb')
N300f = pickle.load(File3)
N15f = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12i.p', 'rb')
N300i = pickle.load(File4)
N15i = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12m.p', 'rb')
N300m = pickle.load(File5)
N15m = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12w.p', 'rb')
N300w = pickle.load(File6)
N15w = pickle.load(File6)
File6.close()

# m12b
File7 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12b_v2.p', 'rb')
N2b = pickle.load(File7)
N415b = pickle.load(File7)
File7.close()
# m12c
File8 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12c_v2.p', 'rb')
N2c = pickle.load(File8)
N415c = pickle.load(File8)
File8.close()
# m12f
File9 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12f_v2.p', 'rb')
N2f = pickle.load(File9)
N415f = pickle.load(File9)
File9.close()
# m12i
File10 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12i_v2.p', 'rb')
N2i = pickle.load(File10)
N415i = pickle.load(File10)
File10.close()
# m12m
File11 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12m_v2.p', 'rb')
N2m = pickle.load(File11)
N415m = pickle.load(File11)
File11.close()
# m12w
File12 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_m12w_v2.p', 'rb')
N2w = pickle.load(File12)
N415w = pickle.load(File12)
File12.close()

# Romeo
File13 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romeo300.p', 'rb')
N300rom = pickle.load(File13)
File13.close()
File14 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romeo15.p', 'rb')
N15rom = pickle.load(File14)
File14.close()
File15 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romeo2.p', 'rb')
N2rom = pickle.load(File15)
File15.close()
File16 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romeo415.p', 'rb')
N415rom = pickle.load(File16)
File16.close()

# Juliet
File17 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Juliet300.p', 'rb')
N300jul = pickle.load(File17)
File17.close()
File18 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Juliet15.p', 'rb')
N15jul = pickle.load(File18)
File18.close()
File19 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Juliet2.p', 'rb')
N2jul = pickle.load(File19)
File19.close()
File20 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Juliet415.p', 'rb')
N415jul = pickle.load(File20)
File20.close()

# Thelma
File21 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Thelma300.p', 'rb')
N300the = pickle.load(File21)
File21.close()
File22 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Thelma15.p', 'rb')
N15the = pickle.load(File22)
File22.close()
File23 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Thelma2.p', 'rb')
N2the = pickle.load(File23)
File23.close()
File24 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Thelma415.p', 'rb')
N415the = pickle.load(File24)
File24.close()

# Louise
File25 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Louise300.p', 'rb')
N300lou = pickle.load(File25)
File25.close()
File26 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Louise15.p', 'rb')
N15lou = pickle.load(File26)
File26.close()
File27 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Louise2.p', 'rb')
N2lou = pickle.load(File27)
File27.close()
File28 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Louise415.p', 'rb')
N415lou = pickle.load(File28)
File28.close()

# Romulus
File29 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romulus300.p', 'rb')
N300romu = pickle.load(File29)
File29.close()
File30 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romulus15.p', 'rb')
N15romu = pickle.load(File30)
File30.close()
File31 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romulus2.p', 'rb')
N2romu = pickle.load(File31)
File31.close()
File32 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Romulus415.p', 'rb')
N415romu = pickle.load(File32)
File32.close()

# Remus
File33 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Remus300.p', 'rb')
N300rem = pickle.load(File33)
File33.close()
File34 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Remus15.p', 'rb')
N15rem = pickle.load(File34)
File34.close()
File35 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Remus2.p', 'rb')
N2rem = pickle.load(File35)
File35.close()
File36 = open(home_dir_stam+'/scripts/pickles/Nvz_1percent_Remus415.p', 'rb')
N415rem = pickle.load(File36)
File36.close()

Nb = [N300b, N15b, N2b, N415b]
Nb_new = []
for i in range(0, len(Nb)):
    Nb_temp = []
    for j in range(0, len(Nb[i])):
        Nb_temp.append(np.append(np.ones(20)*Nb[i][j][0], Nb[i][j]))
    Nb_new.append(Nb_temp)

Nc = [N300c, N15c, N2c, N415c]
Nc_new = []
for i in range(0, len(Nc)):
    Nc_temp = []
    for j in range(0, len(Nc[i])):
        Nc_temp.append(np.append(np.ones(20)*Nc[i][j][0], Nc[i][j]))
    Nc_new.append(Nc_temp)

Nf = [N300f, N15f, N2f, N415f]
Nf_new = []
for i in range(0, len(Nf)):
    Nf_temp = []
    for j in range(0, len(Nf[i])):
        Nf_temp.append(np.append(np.ones(20)*Nf[i][j][0], Nf[i][j]))
    Nf_new.append(Nf_temp)

Ni = [N300i, N15i, N2i, N415i]
Ni_new = []
for i in range(0, len(Ni)):
    Ni_temp = []
    for j in range(0, len(Ni[i])):
        Ni_temp.append(np.append(np.ones(20)*Ni[i][j][0], Ni[i][j]))
    Ni_new.append(Ni_temp)

Nm = [N300m, N15m, N2m, N415m]
Nm_new = []
for i in range(0, len(Nm)):
    Nm_temp = []
    for j in range(0, len(Nm[i])):
        Nm_temp.append(np.append(np.ones(20)*Nm[i][j][0], Nm[i][j]))
    Nm_new.append(Nm_temp)

Nw = [N300w, N15w, N2w, N415w]
Nw_new = []
for i in range(0, len(Nw)):
    Nw_temp = []
    for j in range(0, len(Nw[i])):
        Nw_temp.append(np.append(np.ones(20)*Nw[i][j][0], Nw[i][j]))
    Nw_new.append(Nw_temp)

Nrom = [N300rom, N15rom, N2rom, N415rom]
Nrom_new = []
for i in range(0, len(Nrom)):
    Nrom_temp = []
    for j in range(0, len(Nrom[i])):
        Nrom_temp.append(np.append(np.ones(20)*Nrom[i][j][0], Nrom[i][j]))
    Nrom_new.append(Nrom_temp)

Njul = [N300jul, N15jul, N2jul, N415jul]
Njul_new = []
for i in range(0, len(Njul)):
    Njul_temp = []
    for j in range(0, len(Njul[i])):
        Njul_temp.append(np.append(np.ones(20)*Njul[i][j][0], Njul[i][j]))
    Njul_new.append(Njul_temp)

Nthe = [N300the, N15the, N2the, N415the]
Nthe_new = []
for i in range(0, len(Nthe)):
    Nthe_temp = []
    for j in range(0, len(Nthe[i])):
        Nthe_temp.append(np.append(np.ones(20)*Nthe[i][j][0], Nthe[i][j]))
    Nthe_new.append(Nthe_temp)

Nlou = [N300lou, N15lou, N2lou, N415lou]
Nlou_new = []
for i in range(0, len(Nlou)):
    Nlou_temp = []
    for j in range(0, len(Nlou[i])):
        Nlou_temp.append(np.append(np.ones(20)*Nlou[i][j][0], Nlou[i][j]))
    Nlou_new.append(Nlou_temp)

Nromu = [N300romu, N15romu, N2romu, N415romu]
Nromu_new = []
for i in range(0, len(Nromu)):
    Nromu_temp = []
    for j in range(0, len(Nromu[i])):
        Nromu_temp.append(np.append(np.ones(20)*Nromu[i][j][0], Nromu[i][j]))
    Nromu_new.append(Nromu_temp)

Nrem = [N300rem, N15rem, N2rem, N415rem]
Nrem_new = []
for i in range(0, len(Nrem)):
    Nrem_temp = []
    for j in range(0, len(Nrem[i])):
        Nrem_temp.append(np.append(np.ones(20)*Nrem[i][j][0], Nrem[i][j]))
    Nrem_new.append(Nrem_temp)

# Print all the values to a file.
Fileptest = open(home_dir_stam+"/scripts/pickles/nvz_all.p", "wb")
pickle.dump(Nb_new, Fileptest)
pickle.dump(Nc_new, Fileptest)
pickle.dump(Nf_new, Fileptest)
pickle.dump(Ni_new, Fileptest)
pickle.dump(Nm_new, Fileptest)
pickle.dump(Nw_new, Fileptest)
pickle.dump(Nrom_new, Fileptest)
pickle.dump(Njul_new, Fileptest)
pickle.dump(Nthe_new, Fileptest)
pickle.dump(Nlou_new, Fileptest)
pickle.dump(Nromu_new, Fileptest)
pickle.dump(Nrem_new, Fileptest)
Fileptest.close()