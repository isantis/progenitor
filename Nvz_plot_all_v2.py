#!/usr/bin/python3

"""
 ===================
 = N vs z plot all =
 ===================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2018

 Goal: Read in the values for N(>10^5) and plot this vs redshift for all galaxies
 
 NOTE: Uses the files made from "Nvz_v2.py" or "Nvz_v3.py" or "Nvz_lg.py" 
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
file1 = open(home_dir_stam+'/scripts/pickles/nvz_all.p', 'rb')
Nb = pickle.load(file1)
Nc = pickle.load(file1)
Nf = pickle.load(file1)
Ni = pickle.load(file1)
Nm = pickle.load(file1)
Nw = pickle.load(file1)
Nrom = pickle.load(file1)
Njul = pickle.load(file1)
Nthe = pickle.load(file1)
Nlou = pickle.load(file1)
Nromu = pickle.load(file1)
Nrem = pickle.load(file1)
file1.close()

# d = [0, 300 kpc]
# Calculate the medians
N5 = [Nb[0][0], Nc[0][0], Nf[0][0], Ni[0][0], Nm[0][0], Nw[0][0], Nrom[0][0], Njul[0][0], Nthe[0][0], Nlou[0][0], Nromu[0][0], Nrem[0][0]]
N6 = [Nb[0][1], Nc[0][1], Nf[0][1], Ni[0][1], Nm[0][1], Nw[0][1], Nrom[0][1], Njul[0][1], Nthe[0][1], Nlou[0][1], Nromu[0][1], Nrem[0][1]]
N7 = [Nb[0][2], Nc[0][2], Nf[0][2], Ni[0][2], Nm[0][2], Nw[0][2], Nrom[0][2], Njul[0][2], Nthe[0][2], Nlou[0][2], Nromu[0][2], Nrem[0][2]]
N8 = [Nb[0][3], Nc[0][3], Nf[0][3], Ni[0][3], Nm[0][3], Nw[0][3], Nrom[0][3], Njul[0][3], Nthe[0][3], Nlou[0][3], Nromu[0][3], Nrem[0][3]]
N9 = [Nb[0][4], Nc[0][4], Nf[0][4], Ni[0][4], Nm[0][4], Nw[0][4], Nrom[0][4], Njul[0][4], Nthe[0][4], Nlou[0][4], Nromu[0][4], Nrem[0][4]]
N5300med = np.median(N5,0)
N6300med = np.median(N6,0)
N7300med = np.median(N7,0)
N8300med = np.median(N8,0)
N9300med = np.median(N9,0)

# Calculate medians for isolated and LG seperately
N5300med_iso = np.median(N5[:6],0)
N6300med_iso = np.median(N6[:6],0)
N7300med_iso = np.median(N7[:6],0)
N8300med_iso = np.median(N8[:6],0)
N9300med_iso = np.median(N9[:6],0)
N5300med_lg = np.median(N5[6:],0)
N6300med_lg = np.median(N6[6:],0)
N7300med_lg = np.median(N7[6:],0)
N8300med_lg = np.median(N8[6:],0)
N9300med_lg = np.median(N9[6:],0)

# Smooth the medians
window_size, poly_order = 41, 3
N5300medsmooth = savgol_filter(N5300med, window_size, poly_order)
N6300medsmooth = savgol_filter(N6300med, window_size, poly_order)
N7300medsmooth = savgol_filter(N7300med, window_size, poly_order)
N8300medsmooth = savgol_filter(N8300med, window_size, poly_order)
N9300medsmooth = savgol_filter(N9300med, window_size, poly_order)
N5300medsmooth_iso = savgol_filter(N5300med_iso, window_size, poly_order)
N6300medsmooth_iso = savgol_filter(N6300med_iso, window_size, poly_order)
N7300medsmooth_iso = savgol_filter(N7300med_iso, window_size, poly_order)
N8300medsmooth_iso = savgol_filter(N8300med_iso, window_size, poly_order)
N9300medsmooth_iso = savgol_filter(N9300med_iso, window_size, poly_order)
N5300medsmooth_lg = savgol_filter(N5300med_lg, window_size, poly_order)
N6300medsmooth_lg = savgol_filter(N6300med_lg, window_size, poly_order)
N7300medsmooth_lg = savgol_filter(N7300med_lg, window_size, poly_order)
N8300medsmooth_lg = savgol_filter(N8300med_lg, window_size, poly_order)
N9300medsmooth_lg = savgol_filter(N9300med_lg, window_size, poly_order)
# Smooth all of the individual curves
N5smooth = [savgol_filter(N5[i], window_size, poly_order) for i in range(0, len(N5))]
N6smooth = [savgol_filter(N6[i], window_size, poly_order) for i in range(0, len(N6))]
N7smooth = [savgol_filter(N7[i], window_size, poly_order) for i in range(0, len(N7))]
N8smooth = [savgol_filter(N8[i], window_size, poly_order) for i in range(0, len(N8))]
N9smooth = [savgol_filter(N9[i], window_size, poly_order) for i in range(0, len(N9))]
# Plot the ratios
colors = dc.get_distinct(6)
a5 = np.percentile(N5smooth, 15.87, axis=0)
a6 = np.percentile(N6smooth, 15.87, axis=0)
a7 = np.percentile(N7smooth, 15.87, axis=0)
a8 = np.percentile(N8smooth, 15.87, axis=0)
a9 = np.percentile(N9smooth, 15.87, axis=0)
b5 = np.percentile(N5smooth, 84.13, axis=0)
b6 = np.percentile(N6smooth, 84.13, axis=0)
b7 = np.percentile(N7smooth, 84.13, axis=0)
b8 = np.percentile(N8smooth, 84.13, axis=0)
b9 = np.percentile(N9smooth, 84.13, axis=0)
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#plt.plot(redshifts, N5300medsmooth, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\\rm star}$')
plt.plot(redshifts, N5300medsmooth_iso, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\odot}$')
plt.plot(redshifts, N5300medsmooth_lg, color=colors[0], linestyle='--', linewidth=2.0)
plt.fill_between(redshifts, b5, a5, color=colors[0], alpha=0.35)
#plt.plot(redshifts, N6300medsmooth, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\\rm star}$')
plt.plot(redshifts, N6300medsmooth_iso, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\odot}$')
plt.plot(redshifts, N6300medsmooth_lg, color=colors[1], linestyle='--', linewidth=2.0)
plt.fill_between(redshifts, b6, a6, color=colors[1], alpha=0.35)
#plt.plot(redshifts, N7300medsmooth, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\\rm star}$')
plt.plot(redshifts, N7300medsmooth_iso, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\odot}$')
plt.plot(redshifts, N7300medsmooth_lg, color=colors[2], linestyle='--', linewidth=2.0)
plt.fill_between(redshifts, b7, a7, color=colors[2], alpha=0.35)
#plt.plot(redshifts, N8300medsmooth, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\\rm star}$')
plt.plot(redshifts, N8300medsmooth_iso, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\odot}$')
plt.plot(redshifts, N8300medsmooth_lg, color=colors[3], linestyle='--', linewidth=2.0)
plt.fill_between(redshifts, b8, a8, color=colors[3], alpha=0.35)
#plt.plot(redshifts, N9300medsmooth, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\\rm star}$')
plt.plot(redshifts, N9300medsmooth_iso, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\odot}$')
plt.plot(redshifts, N9300medsmooth_lg, color=colors[4], linestyle='--', linewidth=2.0)
plt.fill_between(redshifts, b9, a9, color=colors[4], alpha=0.35)
plt.xlabel('redshift', fontsize=40)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylabel('N$_{\\rm{galaxy}}$(M$_{\\rm star}$> $X$)', fontsize=40)
plt.title('d(z = 0) < 300 kpc', fontsize=44, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0.5, 210)
plt.xlim(6, 0)
plt.yscale('log')
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Nvz_300_medallshade_smooth_v5_lgiso.pdf')
plt.close()

# d = [0, 15 kpc]
# Calculate the medians
N5 = [Nb[1][0], Nc[1][0], Nf[1][0], Ni[1][0], Nm[1][0], Nw[1][0], Nrom[1][0], Njul[1][0], Nthe[1][0], Nlou[1][0], Nromu[1][0], Nrem[1][0]]
N6 = [Nb[1][1], Nc[1][1], Nf[1][1], Ni[1][1], Nm[1][1], Nw[1][1], Nrom[1][1], Njul[1][1], Nthe[1][1], Nlou[1][1], Nromu[1][1], Nrem[1][1]]
N7 = [Nb[1][2], Nc[1][2], Nf[1][2], Ni[1][2], Nm[1][2], Nw[1][2], Nrom[1][2], Njul[1][2], Nthe[1][2], Nlou[1][2], Nromu[1][2], Nrem[1][2]]
N8 = [Nb[1][3], Nc[1][3], Nf[1][3], Ni[1][3], Nm[1][3], Nw[1][3], Nrom[1][3], Njul[1][3], Nthe[1][3], Nlou[1][3], Nromu[1][3], Nrem[1][3]]
N9 = [Nb[1][4], Nc[1][4], Nf[1][4], Ni[1][4], Nm[1][4], Nw[1][4], Nrom[1][4], Njul[1][4], Nthe[1][4], Nlou[1][4], Nromu[1][4], Nrem[1][4]]
N515med = np.median(N5,0)
N615med = np.median(N6,0)
N715med = np.median(N7,0)
N815med = np.median(N8,0)
N915med = np.median(N9,0)

# Calculate medians for isolated and LG seperately
N515med_iso = np.median(N5[:6],0)
N615med_iso = np.median(N6[:6],0)
N715med_iso = np.median(N7[:6],0)
N815med_iso = np.median(N8[:6],0)
N915med_iso = np.median(N9[:6],0)
N515med_lg = np.median(N5[6:],0)
N615med_lg = np.median(N6[6:],0)
N715med_lg = np.median(N7[6:],0)
N815med_lg = np.median(N8[6:],0)
N915med_lg = np.median(N9[6:],0)

# Smooth the medians
window_size, poly_order = 41, 3
N515medsmooth = savgol_filter(N515med, window_size, poly_order)
N615medsmooth = savgol_filter(N615med, window_size, poly_order)
N715medsmooth = savgol_filter(N715med, window_size, poly_order)
N815medsmooth = savgol_filter(N815med, window_size, poly_order)
N915medsmooth = savgol_filter(N915med, window_size, poly_order)
N515medsmooth_iso = savgol_filter(N515med_iso, window_size, poly_order)
N615medsmooth_iso = savgol_filter(N615med_iso, window_size, poly_order)
N715medsmooth_iso = savgol_filter(N715med_iso, window_size, poly_order)
N815medsmooth_iso = savgol_filter(N815med_iso, window_size, poly_order)
N915medsmooth_iso = savgol_filter(N915med_iso, window_size, poly_order)
N515medsmooth_lg = savgol_filter(N515med_lg, window_size, poly_order)
N615medsmooth_lg = savgol_filter(N615med_lg, window_size, poly_order)
N715medsmooth_lg = savgol_filter(N715med_lg, window_size, poly_order)
N815medsmooth_lg = savgol_filter(N815med_lg, window_size, poly_order)
N915medsmooth_lg = savgol_filter(N915med_lg, window_size, poly_order)
# Smooth all of the individual curves
N5smooth = [savgol_filter(N5[i], window_size, poly_order) for i in range(0, len(N5))]
N6smooth = [savgol_filter(N6[i], window_size, poly_order) for i in range(0, len(N6))]
N7smooth = [savgol_filter(N7[i], window_size, poly_order) for i in range(0, len(N7))]
N8smooth = [savgol_filter(N8[i], window_size, poly_order) for i in range(0, len(N8))]
N9smooth = [savgol_filter(N9[i], window_size, poly_order) for i in range(0, len(N9))]
# Plot the ratios
a5 = np.percentile(N5smooth, 15.87, axis=0)
a6 = np.percentile(N6smooth, 15.87, axis=0)
a7 = np.percentile(N7smooth, 15.87, axis=0)
a8 = np.percentile(N8smooth, 15.87, axis=0)
a9 = np.percentile(N9smooth, 15.87, axis=0)
b5 = np.percentile(N5smooth, 84.13, axis=0)
b6 = np.percentile(N6smooth, 84.13, axis=0)
b7 = np.percentile(N7smooth, 84.13, axis=0)
b8 = np.percentile(N8smooth, 84.13, axis=0)
b9 = np.percentile(N9smooth, 84.13, axis=0)
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#plt.plot(redshifts, N515medsmooth, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\\rm star}$')
plt.plot(redshifts, N515medsmooth_iso, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\odot}$')
plt.plot(redshifts, N515medsmooth_lg, color=colors[0], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b5, a5, color=colors[0], alpha=0.35)
#plt.plot(redshifts, N615medsmooth, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\\rm star}$')
plt.plot(redshifts, N615medsmooth_iso, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\odot}$')
plt.plot(redshifts, N615medsmooth_lg, color=colors[1], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b6, a6, color=colors[1], alpha=0.35)
#plt.plot(redshifts, N715medsmooth, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\\rm star}$')
plt.plot(redshifts, N715medsmooth_iso, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\odot}$')
plt.plot(redshifts, N715medsmooth_lg, color=colors[2], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b7, a7, color=colors[2], alpha=0.35)
#plt.plot(redshifts, N815medsmooth, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\\rm star}$')
plt.plot(redshifts, N815medsmooth_iso, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\odot}$')
plt.plot(redshifts, N815medsmooth_lg, color=colors[3], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b8, a8, color=colors[3], alpha=0.35)
#plt.plot(redshifts, N915medsmooth, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\\rm star}$')
plt.plot(redshifts, N915medsmooth_iso, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\odot}$')
plt.plot(redshifts, N915medsmooth_lg, color=colors[4], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b9, a9, color=colors[4], alpha=0.35)
plt.xlabel('redshift', fontsize=40)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylabel('N$_{\\rm{galaxy}}$(M$_{\\rm star}$> $X$)', fontsize=40)
plt.title('d(z = 0) < 15 kpc', fontsize=44, y=1.02)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0.5, 100)
plt.xlim(6, 0)
plt.yscale('log')
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Nvz_15_medallshade_smooth_v5_lgiso.pdf')
plt.close()

# d = [0, 2 kpc]
# Calculate the medians
N5 = [Nb[2][0], Nc[2][0], Nf[2][0], Ni[2][0], Nm[2][0], Nw[2][0], Nrom[2][0], Njul[2][0], Nthe[2][0], Nlou[2][0], Nromu[2][0], Nrem[2][0]]
N6 = [Nb[2][1], Nc[2][1], Nf[2][1], Ni[2][1], Nm[2][1], Nw[2][1], Nrom[2][1], Njul[2][1], Nthe[2][1], Nlou[2][1], Nromu[2][1], Nrem[2][1]]
N7 = [Nb[2][2], Nc[2][2], Nf[2][2], Ni[2][2], Nm[2][2], Nw[2][2], Nrom[2][2], Njul[2][2], Nthe[2][2], Nlou[2][2], Nromu[2][2], Nrem[2][2]]
N8 = [Nb[2][3], Nc[2][3], Nf[2][3], Ni[2][3], Nm[2][3], Nw[2][3], Nrom[2][3], Njul[2][3], Nthe[2][3], Nlou[2][3], Nromu[2][3], Nrem[2][3]]
N9 = [Nb[2][4], Nc[2][4], Nf[2][4], Ni[2][4], Nm[2][4], Nw[2][4], Nrom[2][4], Njul[2][4], Nthe[2][4], Nlou[2][4], Nromu[2][4], Nrem[2][4]]
N52med = np.median(N5,0)
N62med = np.median(N6,0)
N72med = np.median(N7,0)
N82med = np.median(N8,0)
N92med = np.median(N9,0)

# Calculate medians for isolated and LG seperately
N52med_iso = np.median(N5[:6],0)
N62med_iso = np.median(N6[:6],0)
N72med_iso = np.median(N7[:6],0)
N82med_iso = np.median(N8[:6],0)
N92med_iso = np.median(N9[:6],0)
N52med_lg = np.median(N5[6:],0)
N62med_lg = np.median(N6[6:],0)
N72med_lg = np.median(N7[6:],0)
N82med_lg = np.median(N8[6:],0)
N92med_lg = np.median(N9[6:],0)

# Smooth the medians
window_size, poly_order = 41, 3
N52medsmooth = savgol_filter(N52med, window_size, poly_order)
N62medsmooth = savgol_filter(N62med, window_size, poly_order)
N72medsmooth = savgol_filter(N72med, window_size, poly_order)
N82medsmooth = savgol_filter(N82med, window_size, poly_order)
N92medsmooth = savgol_filter(N92med, window_size, poly_order)
N52medsmooth_iso = savgol_filter(N52med_iso, window_size, poly_order)
N62medsmooth_iso = savgol_filter(N62med_iso, window_size, poly_order)
N72medsmooth_iso = savgol_filter(N72med_iso, window_size, poly_order)
N82medsmooth_iso = savgol_filter(N82med_iso, window_size, poly_order)
N92medsmooth_iso = savgol_filter(N92med_iso, window_size, poly_order)
N52medsmooth_lg = savgol_filter(N52med_lg, window_size, poly_order)
N62medsmooth_lg = savgol_filter(N62med_lg, window_size, poly_order)
N72medsmooth_lg = savgol_filter(N72med_lg, window_size, poly_order)
N82medsmooth_lg = savgol_filter(N82med_lg, window_size, poly_order)
N92medsmooth_lg = savgol_filter(N92med_lg, window_size, poly_order)
# Smooth all of the individual curves
N5smooth = [savgol_filter(N5[i], window_size, poly_order) for i in range(0, len(N5))]
N6smooth = [savgol_filter(N6[i], window_size, poly_order) for i in range(0, len(N6))]
N7smooth = [savgol_filter(N7[i], window_size, poly_order) for i in range(0, len(N7))]
N8smooth = [savgol_filter(N8[i], window_size, poly_order) for i in range(0, len(N8))]
N9smooth = [savgol_filter(N9[i], window_size, poly_order) for i in range(0, len(N9))]
# Plot the ratios
a5 = np.percentile(N5smooth, 15.87, axis=0)
a6 = np.percentile(N6smooth, 15.87, axis=0)
a7 = np.percentile(N7smooth, 15.87, axis=0)
a8 = np.percentile(N8smooth, 15.87, axis=0)
a9 = np.percentile(N9smooth, 15.87, axis=0)
b5 = np.percentile(N5smooth, 84.13, axis=0)
b6 = np.percentile(N6smooth, 84.13, axis=0)
b7 = np.percentile(N7smooth, 84.13, axis=0)
b8 = np.percentile(N8smooth, 84.13, axis=0)
b9 = np.percentile(N9smooth, 84.13, axis=0)
plt.figure(3)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
#plt.plot(redshifts, N52medsmooth, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\\rm star}$')
plt.plot(redshifts, N52medsmooth_iso, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\odot}$')
plt.plot(redshifts, N52medsmooth_lg, color=colors[0], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b5, a5, color=colors[0], alpha=0.35)
#plt.plot(redshifts, N62medsmooth, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\\rm star}$')
plt.plot(redshifts, N62medsmooth_iso, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\odot}$')
plt.plot(redshifts, N62medsmooth_lg, color=colors[1], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b6, a6, color=colors[1], alpha=0.35)
#plt.plot(redshifts, N72medsmooth, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\\rm star}$')
plt.plot(redshifts, N72medsmooth_iso, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\odot}$')
plt.plot(redshifts, N72medsmooth_lg, color=colors[2], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b7, a7, color=colors[2], alpha=0.35)
#plt.plot(redshifts, N82medsmooth, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\\rm star}$')
plt.plot(redshifts, N82medsmooth_iso, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\odot}$')
plt.plot(redshifts, N82medsmooth_lg, color=colors[3], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b8, a8, color=colors[3], alpha=0.35)
#plt.plot(redshifts, N92medsmooth, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\\rm star}$')
plt.plot(redshifts, N92medsmooth_iso, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\odot}$')
plt.plot(redshifts, N92medsmooth_lg, color=colors[4], linewidth=2.0, linestyle='--')
plt.fill_between(redshifts, b9, a9, color=colors[4], alpha=0.35)
plt.xlabel('redshift', fontsize=40)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylabel('N$_{\\rm{galaxy}}$(M$_{\\rm star}$> $X$)', fontsize=40)
plt.title('d(z = 0) < 2 kpc', fontsize=44, y=1.02)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0.5, 30)
plt.xlim(6, 0)
plt.yscale('log')
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Nvz_2_medallshade_smooth_v5_lgiso.pdf')
plt.close()

"""
# d = [4, 15 kpc]
# Calculate the medians
N5 = [Nb[3][0], Nc[3][0], Nf[3][0], Ni[3][0], Nm[3][0], Nw[3][0], Nrom[3][0], Njul[3][0], Nthe[3][0], Nlou[3][0], Nromu[3][0], Nrem[3][0]]
N6 = [Nb[3][1], Nc[3][1], Nf[3][1], Ni[3][1], Nm[3][1], Nw[3][1], Nrom[3][1], Njul[3][1], Nthe[3][1], Nlou[3][1], Nromu[3][1], Nrem[3][1]]
N7 = [Nb[3][2], Nc[3][2], Nf[3][2], Ni[3][2], Nm[3][2], Nw[3][2], Nrom[3][2], Njul[3][2], Nthe[3][2], Nlou[3][2], Nromu[3][2], Nrem[3][2]]
N8 = [Nb[3][3], Nc[3][3], Nf[3][3], Ni[3][3], Nm[3][3], Nw[3][3], Nrom[3][3], Njul[3][3], Nthe[3][3], Nlou[3][3], Nromu[3][3], Nrem[3][3]]
N9 = [Nb[3][4], Nc[3][4], Nf[3][4], Ni[3][4], Nm[3][4], Nw[3][4], Nrom[3][4], Njul[3][4], Nthe[3][4], Nlou[3][4], Nromu[3][4], Nrem[3][4]]
N5415med = np.median(N5,0)
N6415med = np.median(N6,0)
N7415med = np.median(N7,0)
N8415med = np.median(N8,0)
N9415med = np.median(N9,0)
# Smooth the medians
window_size, poly_order = 41, 3
N5415medsmooth = savgol_filter(N5415med, window_size, poly_order)
N6415medsmooth = savgol_filter(N6415med, window_size, poly_order)
N7415medsmooth = savgol_filter(N7415med, window_size, poly_order)
N8415medsmooth = savgol_filter(N8415med, window_size, poly_order)
N9415medsmooth = savgol_filter(N9415med, window_size, poly_order)
# Smooth all of the individual curves
N5smooth = [savgol_filter(N5[i], window_size, poly_order) for i in range(0, len(N5))]
N6smooth = [savgol_filter(N6[i], window_size, poly_order) for i in range(0, len(N6))]
N7smooth = [savgol_filter(N7[i], window_size, poly_order) for i in range(0, len(N7))]
N8smooth = [savgol_filter(N8[i], window_size, poly_order) for i in range(0, len(N8))]
N9smooth = [savgol_filter(N9[i], window_size, poly_order) for i in range(0, len(N9))]
# Plot the ratios
a5 = np.percentile(N5smooth, 15.87, axis=0)
a6 = np.percentile(N6smooth, 15.87, axis=0)
a7 = np.percentile(N7smooth, 15.87, axis=0)
a8 = np.percentile(N8smooth, 15.87, axis=0)
a9 = np.percentile(N9smooth, 15.87, axis=0)
b5 = np.percentile(N5smooth, 84.13, axis=0)
b6 = np.percentile(N6smooth, 84.13, axis=0)
b7 = np.percentile(N7smooth, 84.13, axis=0)
b8 = np.percentile(N8smooth, 84.13, axis=0)
b9 = np.percentile(N9smooth, 84.13, axis=0)
plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, N5415medsmooth, color=colors[0], linewidth=2.0, label='$10^5$ M$_{\\rm star}$')
plt.fill_between(redshifts, b5, a5, color=colors[0], alpha=0.35)
plt.plot(redshifts, N6415medsmooth, color=colors[1], linewidth=2.0, label='$10^6$ M$_{\\rm star}$')
plt.fill_between(redshifts, b6, a6, color=colors[1], alpha=0.35)
plt.plot(redshifts, N7415medsmooth, color=colors[2], linewidth=2.0, label='$10^7$ M$_{\\rm star}$')
plt.fill_between(redshifts, b7, a7, color=colors[2], alpha=0.35)
plt.plot(redshifts, N8415medsmooth, color=colors[3], linewidth=2.0, label='$10^8$ M$_{\\rm star}$')
plt.fill_between(redshifts, b8, a8, color=colors[3], alpha=0.35)
plt.plot(redshifts, N9415medsmooth, color=colors[4], linewidth=2.0, label='$10^9$ M$_{\\rm star}$')
plt.fill_between(redshifts, b9, a9, color=colors[4], alpha=0.35)
plt.xlabel('redshift', fontsize=40)
plt.xticks(np.arange(0, 8, 1.0))
plt.yticks(np.array([1,10,100]))
plt.ylabel('N$_{\\rm{galaxy}}$(M$_{\\rm star}$> $X$', fontsize=40)
plt.title('d(z = 0) = [4, 15 kpc]', fontsize=44, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0.5, 200)
plt.xlim(6, 0)
plt.yscale('log')
ax = plt.axes()
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Nvz_415_medallshade_smooth_v5.pdf')
plt.close()
"""