#!/usr/bin/python3

"""
 =======================================
 = Median Weighted Mass Function Plots =
 =======================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2019

 Goal: To create median weighted mass function plots by loading in the data, calculating the median, 
       and then plotting it all on the same figure.

 NOTE: Uses files made from 'mf_weighted.py' and 'mf_weighted_lg.py'
"""

### Import all of the tools for analysis and read in the data
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import distinct_colours as dc
import pickle

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12b.p', 'rb')
mfb300 = pickle.load(File1)
mfb15 = pickle.load(File1)
mfb2 = pickle.load(File1)
mfb300c = pickle.load(File1)
mfb15c = pickle.load(File1)
mfb2c = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12c.p', 'rb')
mfc300 = pickle.load(File2)
mfc15 = pickle.load(File2)
mfc2 = pickle.load(File2)
mfc300c = pickle.load(File2)
mfc15c = pickle.load(File2)
mfc2c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12f.p', 'rb')
mff300 = pickle.load(File3)
mff15 = pickle.load(File3)
mff2 = pickle.load(File3)
mff300c = pickle.load(File3)
mff15c = pickle.load(File3)
mff2c = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12i.p', 'rb')
mfi300 = pickle.load(File4)
mfi15 = pickle.load(File4)
mfi2 = pickle.load(File4)
mfi300c = pickle.load(File4)
mfi15c = pickle.load(File4)
mfi2c = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12m.p', 'rb')
mfm300 = pickle.load(File5)
mfm15 = pickle.load(File5)
mfm2 = pickle.load(File5)
mfm300c = pickle.load(File5)
mfm15c = pickle.load(File5)
mfm2c = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/mf_weighted_m12w.p', 'rb')
mfw300 = pickle.load(File6)
mfw15 = pickle.load(File6)
mfw2 = pickle.load(File6)
mfw300c = pickle.load(File6)
mfw15c = pickle.load(File6)
mfw2c = pickle.load(File6)
File6.close()
# Romeo
File7 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Romeo.p', 'rb')
mfrom300 = pickle.load(File7)
mfrom15 = pickle.load(File7)
mfrom2 = pickle.load(File7)
mfrom300c = pickle.load(File7)
mfrom15c = pickle.load(File7)
mfrom2c = pickle.load(File7)
File7.close()
# Juliet
File8 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Juliet.p', 'rb')
mfjul300 = pickle.load(File8)
mfjul15 = pickle.load(File8)
mfjul2 = pickle.load(File8)
mfjul300c = pickle.load(File8)
mfjul15c = pickle.load(File8)
mfjul2c = pickle.load(File8)
File8.close()
# Thelma
File9 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Thelma.p', 'rb')
mfthe300 = pickle.load(File9)
mfthe15 = pickle.load(File9)
mfthe2 = pickle.load(File9)
mfthe300c = pickle.load(File9)
mfthe15c = pickle.load(File9)
mfthe2c = pickle.load(File9)
File9.close()
# Louise
File10 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Louise.p', 'rb')
mflou300 = pickle.load(File10)
mflou15 = pickle.load(File10)
mflou2 = pickle.load(File10)
mflou300c = pickle.load(File10)
mflou15c = pickle.load(File10)
mflou2c = pickle.load(File10)
File10.close()
# Romulus
File11 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Romulus.p', 'rb')
mfromu300 = pickle.load(File11)
mfromu15 = pickle.load(File11)
mfromu2 = pickle.load(File11)
mfromu300c = pickle.load(File11)
mfromu15c = pickle.load(File11)
mfromu2c = pickle.load(File11)
File11.close()
# Remus
File12 = open(home_dir_stam+'/scripts/pickles/mf_weighted_Remus.p', 'rb')
mfrem300 = pickle.load(File12)
mfrem15 = pickle.load(File12)
mfrem2 = pickle.load(File12)
mfrem300c = pickle.load(File12)
mfrem15c = pickle.load(File12)
mfrem2c = pickle.load(File12)
File12.close()

# Group all of the galaxies together
mf_300 = [mfb300, mfc300, mff300, mfi300, mfm300, mfw300, mfrom300, mfjul300, mfthe300, mflou300, mfromu300, mfrem300]
mf_15 = [mfb15, mfc15, mff15, mfi15, mfm15, mfw15, mfrom15, mfjul15, mfthe15, mflou15, mfromu15, mfrem15]
mf_2 = [mfb2, mfc2, mff2, mfi2, mfm2, mfw2, mfrom2, mfjul2, mfthe2, mflou2, mfromu2, mfrem2]
mf_300c = [mfb300c, mfc300c, mff300c, mfi300c, mfm300c, mfw300c, mfrom300c, mfjul300c, mfthe300c, mflou300c, mfromu300c, mfrem300c]
mf_15c = [mfb15c, mfc15c, mff15c, mfi15c, mfm15c, mfw15c, mfrom15c, mfjul15c, mfthe15c, mflou15c, mfromu15c, mfrem15c]
mf_2c = [mfb2c, mfc2c, mff2c, mfi2c, mfm2c, mfw2c, mfrom2c, mfjul2c, mfthe2c, mflou2c, mfromu2c, mfrem2c]
# Calculate the medians
mf_300_med = np.median(mf_300, 0)
mf_15_med = np.median(mf_15, 0)
mf_2_med = np.median(mf_2, 0)
mf_300c_med = np.median(mf_300c, 0)
mf_15c_med = np.median(mf_15c, 0)
mf_2c_med = np.median(mf_2c, 0)
# Calculate the medians for the LG and isolated galaxies 
mf_300_med_iso = np.median(mf_300[:6], 0)
mf_15_med_iso = np.median(mf_15[:6], 0)
mf_2_med_iso = np.median(mf_2[:6], 0)
mf_300c_med_iso = np.median(mf_300c[:6], 0)
mf_15c_med_iso = np.median(mf_15c[:6], 0)
mf_2c_med_iso = np.median(mf_2c[:6], 0)
#
mf_300_med_lg = np.median(mf_300[6:], 0)
mf_15_med_lg = np.median(mf_15[6:], 0)
mf_2_med_lg = np.median(mf_2[6:], 0)
mf_300c_med_lg = np.median(mf_300c[6:], 0)
mf_15c_med_lg = np.median(mf_15c[6:], 0)
mf_2c_med_lg = np.median(mf_2c[6:], 0)

# Calculate the scatter for one of the redshifts in each distance cut, for each metric; all mf_*s are the same length
a300 = [np.percentile(mf_300, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b300 = [np.percentile(mf_300, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]
a300c = [np.percentile(mf_300c, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b300c = [np.percentile(mf_300c, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]
a15 = [np.percentile(mf_15, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b15 = [np.percentile(mf_15, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]
a15c = [np.percentile(mf_15c, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b15c = [np.percentile(mf_15c, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]
a2 = [np.percentile(mf_2, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b2 = [np.percentile(mf_2, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]
a2c = [np.percentile(mf_2c, 15.87, axis=0)[i] for i in range(0, len(mf_300[0]))]
b2c = [np.percentile(mf_2c, 84.13, axis=0)[i] for i in range(0, len(mf_300[0]))]

# Set the zero scatters to the median...
# Data is in the form: data[simulation, redshift, mass bin]
data = np.asarray(mf_300)
a300_new = []
# Loop over all of the redshifts
for i in range(0, 7):
      a300_new.append(np.percentile(data[:,i,:],15.87,axis=0))
      temp_m = np.median(data[:,i,:],axis=0)
      # Loop over all of the mass bins to check for 0 percentiles
      for k in range(0, 14):
            if a300_new[i][k] < 10**(-5):
                  a300_new[i][k] = temp_m[k]

data = np.asarray(mf_15)
a15_new = []
# Loop over all of the redshifts
for i in range(0, 7):
      a15_new.append(np.percentile(data[:,i,:],15.87,axis=0))
      temp_m = np.median(data[:,i,:],axis=0)
      # Loop over all of the mass bins to check for 0 percentiles
      for k in range(0, 14):
            if a15_new[i][k] < 10**(-5):
                  a15_new[i][k] = temp_m[k]

data = np.asarray(mf_2)
a2_new = []
# Loop over all of the redshifts
for i in range(0, 7):
      a2_new.append(np.percentile(data[:,i,:],15.87,axis=0))
      temp_m = np.median(data[:,i,:],axis=0)
      # Loop over all of the mass bins to check for 0 percentiles
      for k in range(0, 14):
            if a2_new[i][k] < 10**(-5):
                  a2_new[i][k] = temp_m[k]
      

# Plot the data (300 kpc)
Ms_plot = np.array([4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75])
colors = ['#0077BB','#33BBEE','#762A83','#009988','#CC3311','#AA3377']
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
plt.fill_between(10**(Ms_plot)[mf_300_med[0] != 0], b300[0][mf_300_med[0] != 0], a300_new[0][mf_300_med[0] != 0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_300_med[0] != 0], mf_300_med[0][mf_300_med[0] != 0], color=colors[1], label='z = 1, age > 7.8 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_iso[0] != 0], mf_300_med_iso[0][mf_300_med_iso[0] != 0], color=colors[1], label='z = 1, age > 7.8 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_lg[0] != 0], mf_300_med_lg[0][mf_300_med_lg[0] != 0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_300_med[1] != 0], b300[1][mf_300_med[1] != 0], a300_new[1][mf_300_med[1] != 0], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_300_med[1] != 0], mf_300_med[1][mf_300_med[1] != 0], color=colors[2], label='z = 2, age > 10.4 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_iso[1] != 0], mf_300_med_iso[1][mf_300_med_iso[1] != 0], color=colors[2], label='z = 2, age > 10.4 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_lg[1] != 0], mf_300_med_lg[1][mf_300_med_lg[1] != 0], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_300_med[2] != 0], b300[2][mf_300_med[2] != 0], a300_new[2][mf_300_med[2] != 0], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_300_med[2] != 0], mf_300_med[2][mf_300_med[2] != 0], color=colors[3], label='z = 3, age > 11.6 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_iso[2] != 0], mf_300_med_iso[2][mf_300_med_iso[2] != 0], color=colors[3], label='z = 3, age > 11.6 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_lg[2] != 0], mf_300_med_lg[2][mf_300_med_lg[2] != 0], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_300_med[3] != 0], b300[3][mf_300_med[3] != 0], a300_new[3][mf_300_med[3] != 0], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_300_med[3] != 0], mf_300_med[3][mf_300_med[3] != 0], color=colors[4], label='z = 4, age > 12.2 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_iso[3] != 0], mf_300_med_iso[3][mf_300_med_iso[3] != 0], color=colors[4], label='z = 4, age > 12.2 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_lg[3] != 0], mf_300_med_lg[3][mf_300_med_lg[3] != 0], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_300_med[5] != 0], b300[5][mf_300_med[5] != 0], a300_new[5][mf_300_med[5] != 0], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_300_med[5] != 0], mf_300_med[5][mf_300_med[5] != 0], color=colors[5], label='z = 6, age > 12.8 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_iso[5] != 0], mf_300_med_iso[5][mf_300_med_iso[5] != 0], color=colors[5], label='z = 6, age > 12.8 Gyr')
#plt.plot(10**(Ms_plot)[mf_300_med_lg[5] != 0], mf_300_med_lg[5][mf_300_med_lg[5] != 0], color=colors[5], linestyle='--')
plt.legend(prop={'size': 20})
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**9)
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 300 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_300_med_v5.pdf')
plt.close()
# Cumulative 300
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
Ms_plot = np.array([4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11])
plt.fill_between(10**(Ms_plot), b300c[0], a300c[0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot), mf_300c_med[0]+0.1925, color=colors[1], label='z = 1, age > 7.8 Gyr')  # notice that I'm adding in the excess from 1 - f_insitu - f_exsitu, calculated this not in a script but in interactive sessions
#plt.plot(10**(Ms_plot), mf_300c_med_iso[0], color=colors[1], label='z = 1, age > 7.8 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_lg[0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot), b300c[1], a300c[1], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot), mf_300c_med[1]+0.19, color=colors[2], label='z = 2, age > 10.4 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_iso[1], color=colors[2], label='z = 2, age > 10.4 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_lg[1], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot), b300c[2], a300c[2], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot), mf_300c_med[2]+0.203, color=colors[3], label='z = 3, age > 11.6 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_iso[2], color=colors[3], label='z = 3, age > 11.6 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_lg[2], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot), b300c[3], a300c[3], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot), mf_300c_med[3]+0.126, color=colors[4], label='z = 4, age > 12.2 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_iso[3], color=colors[4], label='z = 4, age > 12.2 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_lg[3], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot), b300c[5], a300c[5], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot), mf_300c_med[5]+0.154, color=colors[5], label='z = 6, age > 12.8 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_iso[5], color=colors[5], label='z = 6, age > 12.8 Gyr')
#plt.plot(10**(Ms_plot), mf_300c_med_lg[5], color=colors[5], linestyle='--')
plt.legend(prop={'size': 20})
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**(10))
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm <M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 300 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_300_cum_med_v5.pdf')
plt.close()

# Plot the data (15 kpc)
Ms_plot = np.array([4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75])
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
plt.fill_between(10**(Ms_plot)[mf_15_med[0] != 0], b15[0][mf_15_med[0] != 0], a15_new[0][mf_15_med[0] != 0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_15_med[0] != 0], mf_15_med[0][mf_15_med[0] != 0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot)[mf_15_med_iso[0] != 0], mf_15_med_iso[0][mf_15_med_iso[0] != 0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot)[mf_15_med_lg[0] != 0], mf_15_med_lg[0][mf_15_med_lg[0] != 0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_15_med[1] != 0], b15[1][mf_15_med[1] != 0], a15_new[1][mf_15_med[1] != 0], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_15_med[1] != 0], mf_15_med[1][mf_15_med[1] != 0], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot)[mf_15_med_iso[1] != 0], mf_15_med_iso[1][mf_15_med_iso[1] != 0], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot)[mf_15_med_lg[1] != 0], mf_15_med_lg[1][mf_15_med_lg[1] != 0], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_15_med[2] != 0], b15[2][mf_15_med[2] != 0], a15_new[2][mf_15_med[2] != 0], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_15_med[2] != 0], mf_15_med[2][mf_15_med[2] != 0], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot)[mf_15_med_iso[2] != 0], mf_15_med_iso[2][mf_15_med_iso[2] != 0], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot)[mf_15_med_lg[2] != 0], mf_15_med_lg[2][mf_15_med_lg[2] != 0], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_15_med[3] != 0], b15[3][mf_15_med[3] != 0], a15_new[3][mf_15_med[3] != 0], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_15_med[3] != 0], mf_15_med[3][mf_15_med[3] != 0], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot)[mf_15_med_iso[3] != 0], mf_15_med_iso[3][mf_15_med_iso[3] != 0], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot)[mf_15_med_lg[3] != 0], mf_15_med_lg[3][mf_15_med_lg[3] != 0], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_15_med[5] != 0], b15[5][mf_15_med[5] != 0], a15_new[5][mf_15_med[5] != 0], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_15_med[5] != 0], mf_15_med[5][mf_15_med[5] != 0], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot)[mf_15_med_iso[5] != 0], mf_15_med_iso[5][mf_15_med_iso[5] != 0], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot)[mf_15_med_lg[5] != 0], mf_15_med_lg[5][mf_15_med_lg[5] != 0], color=colors[5], linestyle='--')
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**9)
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 15 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_15_med_v5.pdf')
plt.close()
# Cumulative 15
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
Ms_plot = np.array([4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11])
plt.fill_between(10**(Ms_plot), b15c[0], a15c[0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot), mf_15c_med[0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot), mf_15c_med_iso[0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot), mf_15c_med_lg[0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot), b15c[1], a15c[1], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot), mf_15c_med[1], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot), mf_15c_med_iso[1], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot), mf_15c_med_lg[1], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot), b15c[2], a15c[2], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot), mf_15c_med[2], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot), mf_15c_med_iso[2], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot), mf_15c_med_lg[2], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot), b15c[3], a15c[3], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot), mf_15c_med[3], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot), mf_15c_med_iso[3], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot), mf_15c_med_lg[3], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot), b15c[5], a15c[5], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot), mf_15c_med[5], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot), mf_15c_med_iso[5], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot), mf_15c_med_lg[5], color=colors[5], linestyle='--')
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**(10))
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm <M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 15 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_15_cum_med_v5.pdf')
plt.close()

# Plot the data (2 kpc)
Ms_plot = np.array([4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25, 8.75, 9.25, 9.75, 10.25, 10.75])
plt.figure(1)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
plt.fill_between(10**(Ms_plot)[mf_2_med[0] != 0], b2[0][mf_2_med[0] != 0], a2_new[0][mf_2_med[0] != 0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_2_med[0] != 0], mf_2_med[0][mf_2_med[0] != 0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot)[mf_2_med_iso[0] != 0], mf_2_med_iso[0][mf_2_med_iso[0] != 0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot)[mf_2_med_lg[0] != 0], mf_2_med_lg[0][mf_2_med_lg[0] != 0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_2_med[1] != 0], b2[1][mf_2_med[1] != 0], a2_new[1][mf_2_med[1] != 0], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_2_med[1] != 0], mf_2_med[1][mf_2_med[1] != 0], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot)[mf_2_med_iso[1] != 0], mf_2_med_iso[1][mf_2_med_iso[1] != 0], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot)[mf_2_med_lg[1] != 0], mf_2_med_lg[1][mf_2_med_lg[1] != 0], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_2_med[2] != 0], b2[2][mf_2_med[2] != 0], a2_new[2][mf_2_med[2] != 0], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_2_med[2] != 0], mf_2_med[2][mf_2_med[2] != 0], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot)[mf_2_med_iso[2] != 0], mf_2_med_iso[2][mf_2_med_iso[2] != 0], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot)[mf_2_med_lg[2] != 0], mf_2_med_lg[2][mf_2_med_lg[2] != 0], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_2_med[3] != 0], b2[3][mf_2_med[3] != 0], a2_new[3][mf_2_med[3] != 0], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_2_med[3] != 0], mf_2_med[3][mf_2_med[3] != 0], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot)[mf_2_med_iso[3] != 0], mf_2_med_iso[3][mf_2_med_iso[3] != 0], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot)[mf_2_med_lg[3] != 0], mf_2_med_lg[3][mf_2_med_lg[3] != 0], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot)[mf_2_med[5] != 0], b2[5][mf_2_med[5] != 0], a2_new[5][mf_2_med[5] != 0], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot)[mf_2_med[5] != 0], mf_2_med[5][mf_2_med[5] != 0], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot)[mf_2_med_iso[5] != 0], mf_2_med_iso[5][mf_2_med_iso[5] != 0], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot)[mf_2_med_lg[5] != 0], mf_2_med_lg[5][mf_2_med_lg[5] != 0], color=colors[5], linestyle='--')
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**9)
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 2 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_2_med_v5.pdf')
plt.close()
# Cumulative 2
plt.figure(2)
plt.figure(figsize=(10, 8))
ax = plt.subplot(111)
Ms_plot = np.array([4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11])
plt.fill_between(10**(Ms_plot), b2c[0], a2c[0], color=colors[1], alpha=0.25)
plt.plot(10**(Ms_plot), mf_2c_med[0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot), mf_2c_med_iso[0], color=colors[1], label='z = 1')
#plt.plot(10**(Ms_plot), mf_2c_med_lg[0], color=colors[1], linestyle='--')
plt.fill_between(10**(Ms_plot), b2c[1], a2c[1], color=colors[2], alpha=0.25)
plt.plot(10**(Ms_plot), mf_2c_med[1], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot), mf_2c_med_iso[1], color=colors[2], label='z = 2')
#plt.plot(10**(Ms_plot), mf_2c_med_lg[1], color=colors[2], linestyle='--')
plt.fill_between(10**(Ms_plot), b2c[2], a2c[2], color=colors[3], alpha=0.25)
plt.plot(10**(Ms_plot), mf_2c_med[2], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot), mf_2c_med_iso[2], color=colors[3], label='z = 3')
#plt.plot(10**(Ms_plot), mf_2c_med_lg[2], color=colors[3], linestyle='--')
plt.fill_between(10**(Ms_plot), b2c[3], a2c[3], color=colors[4], alpha=0.25)
plt.plot(10**(Ms_plot), mf_2c_med[3], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot), mf_2c_med_iso[3], color=colors[4], label='z = 4')
#plt.plot(10**(Ms_plot), mf_2c_med_lg[3], color=colors[4], linestyle='--')
plt.fill_between(10**(Ms_plot), b2c[5], a2c[5], color=colors[5], alpha=0.25)
plt.plot(10**(Ms_plot), mf_2c_med[5], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot), mf_2c_med_iso[5], color=colors[5], label='z = 6')
#plt.plot(10**(Ms_plot), mf_2c_med_lg[5], color=colors[5], linestyle='--')
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**(10))
plt.ylim(10**(-5),1)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('$\\rm M_{\\rm star }$ [$\\rm M_{\odot}$]', fontsize=38)
plt.ylabel('f$_{\\rm ex-situ}(\\rm <M_{\\rm star}, >z)$', fontsize=38)
plt.title('d(z = 0) < 2 kpc', fontsize=44, y=1.03)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_weighted_2_cum_med_v5.pdf')
plt.close()