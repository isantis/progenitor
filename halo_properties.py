#!/usr/bin/python3

"""
 ================================
 = Halo properties for galaxies =
 ================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) for Spring Quarter, 2018

 Goal: Determine the contribution fractions for halos at each redshift for a galaxy
	   and write these to a file for the following distance cuts:

	   d = [0, 300 kpc]
	   d = [0, 15 kpc]
	   d = [0, 2 kpc]
	   R = [4, 15 kpc], z = [-2, 2 kpc]

	   The contribution fractions are defined as the following ratio:

			(Number of stars in a halo at some z' that end up within D kpc of the host at z = 0)
			------------------------------------------------------------------------------------
			                 (Total number of stars in a halo at some z')

 NOTES:
 ------
 - Files written have the names: contribution_+'distance'+_fullres_+'galaxy'.p
 - This file is written to work only on the isolated m12 galaxies for the above distance cuts
"""

### Import all of the tools for analysis
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import pickle
import matplotlib
from matplotlib import pyplot as plt
print('Read in the tools')

#### Read in the halo information
galaxy = 'm12i'
resolution = '_res7100'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir, all_snapshot_list=False)
# Array needs to be flipped so that it starts with z = 0
hal = np.flip(hal)
print('Read in the halos')

#### Some functions for analysis
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Infinity], ind)
    return ind
	
#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
print('Halo indices defined')

# Read in the star data at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, simulation_directory=simulation_dir, assign_host_principal_axes=True)
print('Stars at z = 0 read in')

# Get the snapshot indices from the halo info
snapind = [hal[i].snapshot['index'] for i in range(1, len(redshifts))]
# Get pointers at other redshifts
temp = [gizmo.track.ParticlePointerIO.io_pointers(snapshot_index=i, track_directory=simulation_dir+'/track') for i in snapind]
# I don't actually need the regular pointers anymore...
sis_at_z = [temp[i]['z0.to.z.index'] for i in range(0, len(snapind))]
# Get all of the reverse pointers together in an array/list
sis_at_z_rev = [temp[i].get_pointers('star','star',forward=True) for i in range(0, len(snapind))]
print('Pointers and reverse pointers read in')

"""
 Get the number of halos at each redshift. This is used to loop over when calculating the contribution fractions.
	
	N: A list of lists
	
	   Each element, N[i], corresponds to redshifts[i] and gives the number of
	   halos at that redshift
"""
N = [len(hal[i]['star.mass'][his[i]]) for i in range(0, len(redshifts))]
print('Calculated the number of halos at each redshift')

"""
 Get the number of stars in each halo at each redshift. This is used in the contribution fraction calculations.
 
	N_tot: A list of lists
	
		   Each element, N_tot[i], corresponds to redshifts[i] and gives the number of stars
		   in each halo for that redshift
"""
N_tot = [[len(hal[i]['star.indices'][his[i]][j]) for j in range(0, N[i])] for i in range(0, len(redshifts))]
print('Calculated number of stars in halos at each redshift')

### Loop over the distance list to calculate the contribution fractions
dist_list = [300, 15, 2]
ratios = []
for d in dist_list:

	"""
	 Find the number of stars that merge into the host at z = 0 as well as the ratio of merged/tot stars for each halo

		ratio_temp: A list of lists

				   Each element ratio_temp[i] corresponds to redshifts[i] and gives the fraction of stars in each halo j
				   that merge into the host (or within d kpc of it) by z = 0

		Method:
				1. Create some empty lists to store the quantities that I want
				2. Loop over redshifts (except for z = 0)
				3. Create temporary vectors to later append to the empty lists above
				   Create a mask for only stars in some distance cut
				   Using that mask, select only the stars that existed at redshifts[i+1]
				4. Loop over the number of halos at redshifts[i+1]
				5. Using the mask, get the stars that belong to each halo and sum them up
				6. Divide the sum by the total number of stars in that halo to get the ratio

		Some quantities:
						masks_d0 : mask for only the stars that are within some distance at z = 0
						masks_d0_at_z : re-ordered mask corresponding to the particles at redshifts[i+1]
	"""
	ratio_temp = []
	for i in range(0, len(sis_at_z)):
	    n_merge = np.zeros(N[i+1])
	    rr = np.zeros(N[i+1])
	    masks_d0 = (part['star'].prop('star.host.distance.total') < d)
	    masks_d0_at_z = masks_d0[sis_at_z_rev[i]]
	    for j in range(0, N[i+1]):
	        n_merge[j] = np.sum(masks_d0_at_z[hal[i+1]['star.indices'][his[i+1]][j]])
	        rr[j] = n_merge[j]/N_tot[i+1][j]
	    ratio_temp.append(rr)
	ratios.append(ratio_temp)
	print('d < %d contributions done' % d)

## d = [4, 15 kpc]
"""
 Find the number of stars that merge into the host at z = 0 as well as the ratio of merged/tot stars for each halo
					 
	ratio_temp:  A list of lists
	
			   Each element ratio_temp[i] corresponds to redshifts[i] and gives the fraction of stars in each halo j
			   that merge into the disk (or within 4-15 kpc of it) by z = 0
			   
	Method:
			1. Create some empty lists to store the quantities that I want
			2. Create a mask that selects stars with certain position and velocity cuts
			3. Loop over redshifts (except for z = 0)
			4. Create temporary vectors to later append to the empty lists above
			   Create a mask for only stars in some distance cut
			   Using that mask, select only the stars that existed at redshifts[i+1]
			5. Loop over the number of halos at redshifts[i+1]
			6. Using the mask, get the stars that belong to each halo and sum them up
			7. Divide the sum by the total number of stars in that halo to get the ratio
			
	Some quantities:
					masks_v: rotational velocity mask
					masks_pos: position mask
					masks_d0: combined position and velocity mask
					masks_d0_at_z : re-ordered mask corresponding to the particles at redshifts[i+1]
"""
ratio_temp = []
# Create masks for R
masks_d01 = (part['star'].prop('host.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part['star'].prop('host.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part['star'].prop('host.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part['star'].prop('host.distance.principal.cylindrical')[:,1] > -2)
# Combine to create position mask
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
# Create velocity mask & calculate the rotational velocity
v_r = part['star'].prop('host.velocity.principal.cylindrical')[:,0]
v_z = part['star'].prop('host.velocity.principal.cylindrical')[:,1]
v_phi = part['star'].prop('host.velocity.principal.cylindrical')[:,2]
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Combine all of the masks
masks_d0 = masks_pos & masks_v
# Calculate the contribution fraction
for i in range(0, len(sis_at_z)):
    n_merge = np.zeros(N[i+1])
    rr = np.zeros(N[i+1])
    masks_d0_at_z = masks_d0[sis_at_z_rev[i]]
    for j in range(0, N[i+1]):
        n_merge[j] = np.sum(masks_d0_at_z[hal[i+1]['star.indices'][his[i+1]][j]])
        rr[j] = n_merge[j]/N_tot[i+1][j]
    ratio_temp.append(rr)
ratios.append(ratio_temp)
print('4 < d < 15 contributions done')

"""
 Write the ratios to a file
 
 How to call/read the file:
    
    Filepp = open("contribution_300_fullres.p", "rb")
    test = pickle.load(Filepp)
    Filepp.close()
    
    'test' will be the variable which is a list of arrays containing the contribution fractions for each halo in each redshift
    test[0]   : the contribution fractions for the first redshift (z = 0.1)
    test[0][0]: the contribution fraction for the first halo in the first redshift, etc.
"""

r_tot_300 = ratios[0]
r_tot_15 = ratios[1]
r_tot_2 = ratios[2]
r_tot_415 = ratios[3]

Filep1 = open(home_dir+"/scripts/pickles/contribution_300_fullres_"+galaxy+".p", "wb")
pickle.dump(r_tot_300, Filep1)
Filep1.close()

Filep2 = open(home_dir+"/scripts/pickles/contribution_15_fullres_"+galaxy+".p", "wb")
pickle.dump(r_tot_15, Filep2)
Filep2.close()

Filep3 = open(home_dir+"/scripts/pickles/contribution_415_fullres_"+galaxy+".p", "wb")
pickle.dump(r_tot_415, Filep3)
Filep3.close()

Filep4 = open(home_dir+"/scripts/pickles/contribution_2_fullres_"+galaxy+".p", "wb")
pickle.dump(r_tot_2, Filep4)
Filep4.close()

print('Contribution fractions written to files')
print('Using a median rotational velocity of', v_rot, 'for galaxy', galaxy)
"""
# Make a histogram of the age distribution of stars
# NOTE: this was done for all galaxies, but only for the d < 2 kpc cut!!!
star_ages = part['star'].prop('age')[masks_d0]
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.hist(star_ages, 14)
plt.xlabel('Age (Gyr)')
plt.ylabel('N')
plt.savefig(home_dir+'/scripts/plots/velocity_cut_ages_'+galaxy+'.pdf')

print('Velocity histogram plot made')
"""
print('Done with halo_properties.py')
