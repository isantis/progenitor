#!/usr/bin/python3

"""
 ===========================
 = Mass Ratio summary data =
 ===========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Break

 Goal: Read in the mass ratios for all galaxies and get when the ratios are
       last above 0.5, 0.33, 0.25 and save the data ro a file
 
 NOTE: Used files made from "mass_ratio_v2.py", "mass_ratio_v3.py", and "mass_ratio_lg.py"

"""

#### Import all of the tools for analysis
import numpy as np
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

cuts = [0.5, 0.33, 0.25]

## Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/mass_ratios_all.p', 'rb')
massb = pickle.load(File1)
massc = pickle.load(File1)
massf = pickle.load(File1)
massi = pickle.load(File1)
massm = pickle.load(File1)
massw = pickle.load(File1)
massrom = pickle.load(File1)
massjul = pickle.load(File1)
massthe = pickle.load(File1)
masslou = pickle.load(File1)
massromu = pickle.load(File1)
massrem = pickle.load(File1)
File1.close()

# Calculate the median mass ratio for each distance cut
# d = [0, 300 kpc]
two_one_ratio_300 = [massb[0][0], massc[0][0], massf[0][0], massi[0][0], massm[0][0], massw[0][0], massrom[0][0], massjul[0][0], massthe[0][0], masslou[0][0], massromu[0][0], massrem[0][0]]
two_one_ratio_300_med = np.median(two_one_ratio_300, 0)

# d = [0, 15 kpc]
two_one_ratio_15 = [massb[1][0], massc[1][0], massf[1][0], massi[1][0], massm[1][0], massw[1][0], massrom[1][0], massjul[1][0], massthe[1][0], masslou[1][0], massromu[1][0], massrem[1][0]]
two_one_ratio_15_med = np.median(two_one_ratio_15, 0)

# d = [0, 2 kpc]
two_one_ratio_2 = [massb[2][0], massc[2][0], massf[2][0], massi[2][0], massm[2][0], massw[2][0], massrom[2][0], massjul[2][0], massthe[2][0], masslou[2][0], massromu[2][0], massrem[2][0]]
two_one_ratio_2_med = np.median(two_one_ratio_2, 0)

# d = [4, 15 kpc]
two_one_ratio_415 = [massb[3][0], massc[3][0], massf[3][0], massi[3][0], massm[3][0], massw[3][0], massrom[3][0], massjul[3][0], massthe[3][0], masslou[3][0], massromu[3][0], massrem[3][0]]
two_one_ratio_415_med = np.median(two_one_ratio_415, 0)

## Get the last time that the fractions were above each of the ratio cuts
red_temp = np.flip(redshifts)
last_red_300 = [red_temp[np.max(np.where(np.flip(two_one_ratio_300_med,0) > i)[0])] for i in cuts]
last_red_15 = [red_temp[np.max(np.where(np.flip(two_one_ratio_15_med,0) > i)[0])] for i in cuts]
last_red_2 = [red_temp[np.max(np.where(np.flip(two_one_ratio_2_med,0) > i)[0])] for i in cuts]
last_red_415 = [red_temp[np.max(np.where(np.flip(two_one_ratio_415_med,0) > i)[0])] for i in cuts]

last_mr = [last_red_300, last_red_15, last_red_2, last_red_415]

# example: last_mr[0] for 300 cut for 0.5, then 0.33, then 0.25

## Do the same thing, but with the smoothed data
# d = [0, 300 kpc]
window_size, poly_order = 41, 3
mr300smooth = savgol_filter(two_one_ratio_300_med, window_size, poly_order)

last_red_300_smooth = [red_temp[np.max(np.where(np.flip(mr300smooth) > i)[0])] for i in cuts]

# d = [0, 15 kpc]
mr15smooth = savgol_filter(two_one_ratio_15_med, window_size, poly_order)

last_red_15_smooth = [red_temp[np.max(np.where(np.flip(mr15smooth) > i)[0])] for i in cuts]

# d = [0, 2 kpc]
mr2smooth = savgol_filter(two_one_ratio_2_med, window_size, poly_order)

last_red_2_smooth = [red_temp[np.max(np.where(np.flip(mr2smooth) > i)[0])] if len(np.where(np.flip(mr2smooth) > i)[0]) !=0 else 9 for i in cuts]

# d = [4, 15 kpc]
mr415smooth = savgol_filter(two_one_ratio_415_med, window_size, poly_order)

last_red_415_smooth = [red_temp[np.max(np.where(np.flip(mr415smooth) > i)[0])] if len(np.where(np.flip(mr415smooth) > i)[0]) !=0 else 9 for i in cuts]

last_mr_smooth = [last_red_300_smooth, last_red_15_smooth, last_red_2_smooth, last_red_415_smooth]

# print out the values before saving them to a file in case you dont want to read the file
print(last_mr, 'for raw data')
print(last_mr_smooth, 'for smoothed data')

# Save the redshifts to a file
Filep = open(home_dir_stam+"/scripts/pickles/last_mr_redshifts_v2.p", "wb")
pickle.dump(last_mr, Filep)
pickle.dump(last_mr_smooth, Filep)
Filep.close()