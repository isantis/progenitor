#!/usr/bin/python3

"""
 =================================
 = In-situ / Ex-situ Fraction LG =
 =================================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: Determine the in-situ and ex-situ fractions for the local group pairs for a distance cut
        applying a 1% contribution criteria.

       Save the data in pickle files.

       ie_1 : the insitu, exsitu, and total fractions for host 1
       ie_2 : the insitu, exsitu, and total fractions for host 2
 
       Saves the file named as
       in_ex_1percent_+gal1+dist.p
"""

#### Import all the necessary tools for analysis
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import pickle

#### Read in halos for all redshifts
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
dist = 2
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

#### Some functions for analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

    z: redshift
    ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind
"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

 unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
 sort: unsort, sorted
 i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
 mass_i: the mass of element i in sort
 index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
# Read in the data of stars at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, host_number=2, simulation_directory=simulation_dir_stam, assign_host_principal_axes=True)
# Get the snapshot indices from the halo info
snapind = [hal[i].snapshot['index'] for i in range(1, len(redshifts))]
# Get pointers at other redshifts
temp = [gizmo.track.ParticlePointerIO.io_pointers(snapshot_index=i, track_directory=simulation_dir_stam+'/track') for i in snapind]
# I don't actually need the regular pointers anymore...
sis_at_z = [temp[i]['z0.to.z.index'] for i in range(0, len(snapind))]
# Get all of the reverse pointers together in an array/list
sis_at_z_rev = [temp[i].get_pointers('star','star',forward=True) for i in range(0, len(snapind))]

# Read in the contribution fractions for the halos at each redshift
distn = str(dist)
# GALAXY 1
pickle_1 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal1+'.p'
# d = [0, d kpc]
Filep1 = open(pickle_1, "rb")
ratio_1 = pickle.load(Filep1)
Filep1.close()
# GALAXY 2
pickle_2 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal2+'.p'
# d = [0, d kpc]
Filep2 = open(pickle_2, "rb")
ratio_2 = pickle.load(Filep2)
Filep2.close()
print('Done reading in all the data and defining things.')


# GALAXY 1
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind1 = []
for i in range(0, len(ratio_1)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_1[i][temp]
    mass_ind1.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_1[i][temp]
        mass_ind1[i] = temp
### In-situ Fraction
'''
 Find the mass of stars that formed in the progenitor between z = 0 and z = 0.1
 z = 0 shouldn't be in the loop below, do it manually here
'''
# Get indices of stars within d kpc of host at z = 0.
temp = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, a[0]*dist])
# First, find the stars that formed between these redshifts in the most massive progenitor
i_0 = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[0], redshifts[1]], temp)
M_0_01_insitu = np.sum(part['star']['mass'][i_0])
# Create a vector to attach all of the masses in the numerator for f_insitu
M_in_num = np.zeros(len(sis_at_z))
M_in_num[0] = M_0_01_insitu
'''
 Find the numerator for f_insitu for all other redshift bins
 (Around 13.64 seconds)

    M_in_num : Array
        M_in_num[i] corresponds to the mass of stars that formed in-situ (or within the most massive progenitor)
                    in some redshift bin that also merged in within d kpc of the host at z = 0

    Method:
        1. Create some masks that select stars that formed in a redshift bin that eventually
           merged into the host at z = 0
        2. Combine all of those masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Out of the entire mask, only select stars that are members of the MMP (now a subset of total mask)
        5. Use the new (subset) mask back on the MMP to only select stars that follow the criteria in step 1
        6. Use the reverse pointers to get the indices of these stars at z = 0
        7. Add up the mass of these stars for each redshift bin
'''
# Create the distance mask
mask1 = (part['star'].prop('star.host.distance.total') < a[0]*dist)
for i in range(1, len(sis_at_z)):
    # Create the redshift formation masks, combine them, and re-order
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    tot_mask_0 = mask1 & mask2 & mask3
    # Mask out the negative pointers
    negatives = (sis_at_z_rev[i-1] < 0)
    sis_at_z_rev[i-1][negatives] = 10537
    # Re-order total mask
    tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
    # Get subset of the total mask for only stars in MMP
    tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind1[i-1]]]
    # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
    hal_star_good = hal[i]['star.indices'][his[i]][mass_ind1[i-1]][tot_mask_z_sub]
    # Use these indices on the reverse pointer array
    rev_point_good = sis_at_z_rev[i-1][hal_star_good]
    M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])
'''
 Find the denominator for f_insitu for all redshifts
 (Around 16.55 seconds)

    M_in_den : Array
        M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
        eventually merge into the host (or within d kpc of it) at z = 0

    Method:
        1. Get the indices of all stars at z = 0 within d kpc of the host
        2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
        3. Add up all of the mass in each redshift bin.
'''
# Initialize an empty vector
M_in_den1 = np.zeros(len(sis_at_z))
# Get the indices of stars in some distance cut
temp1 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, a[0]*dist])
# Loop through all of the redshift bins and add up the mass
for i in range(0, len(sis_at_z)):
    temp2 = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[i], redshifts[i+1]], temp1)
    M_in_den1[i] = np.sum(part['star']['mass'][temp2])
# Calculate the in-situ fraction
insitu_1 = M_in_num/M_in_den1
print('Done with insitu for', gal1)
# GALAXY 2
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind2 = []
for i in range(0, len(ratio_2)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_2[i][temp]
    mass_ind2.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_2[i][temp]
        mass_ind2[i] = temp
### In-situ Fraction
'''
 Find the mass of stars that formed in the progenitor between z = 0 and z = 0.1
 z = 0 shouldn't be in the loop below, do it manually here
'''
# Get indices of stars within d kpc of host at z = 0.
temp = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [0, a[0]*dist])
# First, find the stars that formed between these redshifts in the most massive progenitor
i_0 = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[0], redshifts[1]], temp)
M_0_01_insitu = np.sum(part['star']['mass'][i_0])
# Create a vector to attach all of the masses in the numerator for f_insitu
M_in_num = np.zeros(len(sis_at_z))
M_in_num[0] = M_0_01_insitu
'''
 Find the numerator for f_insitu for all other redshift bins
 (Around 13.64 seconds)

    M_in_num : Array
        M_in_num[i] corresponds to the mass of stars that formed in-situ (or within the most massive progenitor)
                    in some redshift bin that also merged in within d kpc of the host at z = 0

    Method:
        1. Create some masks that select stars that formed in a redshift bin that eventually
           merged into the host at z = 0
        2. Combine all of those masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Out of the entire mask, only select stars that are members of the MMP (now a subset of total mask)
        5. Use the new (subset) mask back on the MMP to only select stars that follow the criteria in step 1
        6. Use the reverse pointers to get the indices of these stars at z = 0
        7. Add up the mass of these stars for each redshift bin
'''
# Create the distance mask
mask1 = (part['star'].prop('star.host2.distance.total') < a[0]*dist)
for i in range(1, len(sis_at_z)):
    # Create the redshift formation masks, combine them, and re-order
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    tot_mask_0 = mask1 & mask2 & mask3
    tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
    # Get subset of the total mask for only stars in MMP
    tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind2[i-1]]]
    # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
    hal_star_good = hal[i]['star.indices'][his[i]][mass_ind2[i-1]][tot_mask_z_sub]
    # Use these indices on the reverse pointer array
    rev_point_good = sis_at_z_rev[i-1][hal_star_good]
    M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])
'''
 Find the denominator for f_insitu for all redshifts
 (Around 16.55 seconds)

    M_in_den : Array
        M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
        eventually merge into the host (or within d kpc of it) at z = 0

    Method:
        1. Get the indices of all stars at z = 0 within d kpc of the host
        2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
        3. Add up all of the mass in each redshift bin.
'''
# Initialize an empty vector
M_in_den2 = np.zeros(len(sis_at_z))
# Get the indices of stars in some distance cut
temp1 = ut.array.get_indices(part['star'].prop('star.host2.distance.total'), [0, a[0]*dist])
# Loop through all of the redshift bins and add up the mass
for i in range(0, len(sis_at_z)):
    temp2 = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[i], redshifts[i+1]], temp1)
    M_in_den2[i] = np.sum(part['star']['mass'][temp2])
# Calculate the in-situ fraction
insitu_2 = M_in_num/M_in_den2
print('Done with insitu for', gal2)
# GALAXY 1
### Ex-situ Fraction (Counting all stars in all halos)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num = np.zeros(len(sis_at_z))
M_ex_num[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins
 (Around 13 seconds)

    M_ex_num : Array
        M_ex_num[i] corresponds to the mass of stars that formed in all halos (other than the MMP) in a redshift
        bin, that also merge into the host at z = 0

    Method:
        1. Create some masks for stars that merge within d kpc of the host at z = 0 that also form
           in a redshift bin
        2. Combine all of the masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Set up a vector that includes the indices for all of the halos at a redshift except for the
           index of the MMP
        5. Loop through all halos at a redshift
        6. Out of the entire mask, only select stars that are members of a halo (now a subset of total mask)
        7. Use the mask back on that halo to get the indices of the stars that follow the criteria in 1
        8. Use the reverse pointers on these indices to get their indices at z = 0
        9. Add up the mass of all of these stars in all halos other than the MMP
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
mask1 = (part['star'].prop('star.host.distance.total') < a[0]*dist)
# Loop over all other redshift bins
for i in range(1, len(sis_at_z)):
    # Set up initial mass
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    total_mask_0 = mask1 & mask2 & mask3
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Set up vector to loop through halos; vector not containing index of MMP
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind1[i-1] != N)
    N = N[mask]
    # Loop over all halos aside from the MMP
    for j in N:
        # Get a subset of the total mask for only stars in a halo
        tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
        # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
        hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
        # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
        rev_point_good = sis_at_z_rev[i-1][hal_star_good]
        # Add up the mass for all of these particles for each halo (aside from the MMP)
        mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num[i] = mass
# Calculate the ex-situ fraction
exsitu_1 = M_ex_num/M_in_den1
print('Done with exsitu for', gal1)
# GALAXY 2
### Ex-situ Fraction (Counting all stars in all halos)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num = np.zeros(len(sis_at_z))
M_ex_num[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins
 (Around 13 seconds)

    M_ex_num : Array
        M_ex_num[i] corresponds to the mass of stars that formed in all halos (other than the MMP) in a redshift
        bin, that also merge into the host at z = 0

    Method:
        1. Create some masks for stars that merge within d kpc of the host at z = 0 that also form
           in a redshift bin
        2. Combine all of the masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Set up a vector that includes the indices for all of the halos at a redshift except for the
           index of the MMP
        5. Loop through all halos at a redshift
        6. Out of the entire mask, only select stars that are members of a halo (now a subset of total mask)
        7. Use the mask back on that halo to get the indices of the stars that follow the criteria in 1
        8. Use the reverse pointers on these indices to get their indices at z = 0
        9. Add up the mass of all of these stars in all halos other than the MMP
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
mask1 = (part['star'].prop('star.host2.distance.total') < a[0]*dist)
# Loop over all other redshift bins
for i in range(1, len(sis_at_z)):
    # Set up initial mass
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    total_mask_0 = mask1 & mask2 & mask3
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Set up vector to loop through halos; vector not containing index of MMP
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind2[i-1] != N)
    N = N[mask]
    # Loop over all halos aside from the MMP
    for j in N:
        # Get a subset of the total mask for only stars in a halo
        tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
        # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
        hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
        # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
        rev_point_good = sis_at_z_rev[i-1][hal_star_good]
        # Add up the mass for all of these particles for each halo (aside from the MMP)
        mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num[i] = mass
# Calculate the ex-situ fraction
exsitu_2 = M_ex_num/M_in_den2
print('Done with exsitu for', gal2)
# GALAXY 1
## Ex-situ Fraction (Counting only stars that formed in halos with f_contr > 0.01)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num_contr = np.zeros(len(sis_at_z))
M_ex_num_contr[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins for halos that have f_contr > 0.01
 (Around 13 seconds)

    M_ex_num_contr : Array
        M_ex_num_contr[i] corresponds to the mass of stars that formed in all halos other than the MMP
        that formed in a redshift bin, but only for halos that contribute > 1% of their stars to the
        host by z = 0 (or within d kpc of it)

    Method:
        Similar to that above, but with the added condition that mass only gets added if a halo has
        a contribution fraction > 0.01
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
mask1 = (part['star'].prop('star.host.distance.total') < a[0]*dist)
# Loop over the other redshift bins
for i in range(1, len(sis_at_z)):
    # Set initial mass to be zero
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    total_mask_0 = mask1 & mask2 & mask3
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Create a vector to loop through each halo
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind1[i-1] != N)
    N = N[mask]
    for j in N:
        if ratio_1[i-1][j] > 0.01:
            # Get a subset of the total mask for only stars in a halo
            tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
            # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
            hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
            # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
            rev_point_good = sis_at_z_rev[i-1][hal_star_good]
            # Add up the mass for all of these particles for each halo (aside from the MMP)
            mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num_contr[i] = mass
# Calculate the ex-situ fraction
exsitu_contr_1 = M_ex_num_contr/M_in_den1
print('Done with exsitu_contr for', gal1)

# Calculate the totals and group them all together to be later stored in pickle files
total_1 = np.asarray(insitu_1) + np.asarray(exsitu_1)
total_contr_1 = np.asarray(insitu_1) + np.asarray(exsitu_contr_1)
ie_1 = [insitu_1, exsitu_1, exsitu_contr_1, total_1, total_contr_1]
print('Done with totals for', gal1)
# GALAXY 2
## Ex-situ Fraction (Counting only stars that formed in halos with f_contr > 0.01)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num_contr = np.zeros(len(sis_at_z))
M_ex_num_contr[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins for halos that have f_contr > 0.01
 (Around 13 seconds)

    M_ex_num_contr : Array
        M_ex_num_contr[i] corresponds to the mass of stars that formed in all halos other than the MMP
        that formed in a redshift bin, but only for halos that contribute > 1% of their stars to the
        host by z = 0 (or within d kpc of it)

    Method:
        Similar to that above, but with the added condition that mass only gets added if a halo has
        a contribution fraction > 0.01
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
mask1 = (part['star'].prop('star.host2.distance.total') < a[0]*dist)
# Loop over the other redshift bins
for i in range(1, len(sis_at_z)):
    # Set initial mass to be zero
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
    mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
    total_mask_0 = mask1 & mask2 & mask3
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Create a vector to loop through each halo
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind2[i-1] != N)
    N = N[mask]
    for j in N:
        if ratio_2[i-1][j] > 0.01:
            # Get a subset of the total mask for only stars in a halo
            tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
            # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
            hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
            # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
            rev_point_good = sis_at_z_rev[i-1][hal_star_good]
            # Add up the mass for all of these particles for each halo (aside from the MMP)
            mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num_contr[i] = mass
# Calculate the ex-situ fraction
exsitu_contr_2 = M_ex_num_contr/M_in_den2
print('Done with exsitu_contr for', gal2)

# Calculate the totals and group them all together to be later stored in pickle files
total_2 = np.asarray(insitu_2) + np.asarray(exsitu_2)
total_contr_2 = np.asarray(insitu_2) + np.asarray(exsitu_contr_2)
ie_2 = [insitu_2, exsitu_2, exsitu_contr_2, total_2, total_contr_2]
print('Done with totals for', gal2)

# Save the data to a file
Filep = open(home_dir_stam+"/scripts/pickles/in_ex_1percent_"+gal1+distn+".p", "wb")
pickle.dump(ie_1, Filep)
Filep.close()
Filep = open(home_dir_stam+"/scripts/pickles/in_ex_1percent_"+gal2+distn+".p", "wb")
pickle.dump(ie_2, Filep)
Filep.close()

"""
#############################################################################################
### d = [4, 15 kpc], f_contr > 0.01 ###
#############################################################################################
# GALAXY 1
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind1 = []
for i in range(0, len(ratio_1)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_1[i][temp]
    mass_ind1.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_1[i][temp]
        mass_ind1[i] = temp
### In-situ Fraction
'''
 Find the mass of stars that formed in the progenitor between z = 0 and z = 0.1
 z = 0 shouldn't be in the loop below, do it manually here
'''
# Get indices of stars
# Create masks for R
masks_d01 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r = part['star'].prop('star.host.velocity.principal.cylindrical')[:,0]
v_z = part['star'].prop('star.host.velocity.principal.cylindrical')[:,1]
v_phi = part['star'].prop('star.host.velocity.principal.cylindrical')[:,2]
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Create redshift formation masks
masks_z = ((part['star'].prop('star.form.redshift') > 0) & (part['star'].prop('star.form.redshift') <= 0.1))
# Combine all of the masks
masks_d0 = masks_pos & masks_v & masks_z

# First, find the stars that formed between these redshifts in the most massive progenitor
M_0_01_insitu = np.sum(part['star']['mass'][masks_d0])
# Create a vector to attach all of the masses in the numerator for f_insitu
M_in_num = np.zeros(len(sis_at_z))
M_in_num[0] = M_0_01_insitu
'''
 Find the numerator for f_insitu for all other redshift bins
 (Around 13.64 seconds)

    M_in_num : Array
        M_in_num[i] corresponds to the mass of stars that formed in-situ (or within the most massive progenitor)
                    in some redshift bin that also merged in within 4-15 kpc of the host at z = 0

    Method:
        1. Create some masks that select stars that formed in a redshift bin that eventually
           merged into the host at z = 0
        2. Combine all of those masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Out of the entire mask, only select stars that are members of the MMP (now a subset of total mask)
        5. Use the new (subset) mask back on the MMP to only select stars that follow the criteria in step 1
        6. Use the reverse pointers to get the indices of these stars at z = 0
        7. Add up the mass of these stars for each redshift bin
'''
# Create the distance mask
masks_pos_v = masks_pos & masks_v
for i in range(1, len(sis_at_z)):
    # Create the redshift formation masks, combine them, and re-order
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    tot_mask_0 = masks_pos_v & maskz
    # Mask out all of the negative particles...
    negatives = (sis_at_z_rev[i-1] < 0)
    sis_at_z_rev[i-1][negatives] =  10537
    # Re-order mask
    tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
    # Get subset of the total mask for only stars in MMP
    tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind1[i-1]]]
    # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
    hal_star_good = hal[i]['star.indices'][his[i]][mass_ind1[i-1]][tot_mask_z_sub]
    # Use these indices on the reverse pointer array
    rev_point_good = sis_at_z_rev[i-1][hal_star_good]
    M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])
'''
 Find the denominator for f_insitu for all redshifts
 (Around 16.55 seconds)

    M_in_den : Array
        M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
        eventually merge into the host (or within 4-15 kpc of it) at z = 0

    Method:
        1. Get the indices of all stars at z = 0 within 4-15 kpc of the host
        2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
        3. Add up all of the mass in each redshift bin.
'''
# Initialize an empty vector
M_in_den1 = np.zeros(len(sis_at_z))
# Get the indices of stars in some distance cut
#masks_pos_v
# Loop through all of the redshift bins and add up the mass
for i in range(0, len(sis_at_z)):
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    mask_tot = masks_pos_v & maskz
    M_in_den1[i] = np.sum(part['star']['mass'][mask_tot])
# Calculate the in-situ fraction
insitu_415_1 = M_in_num/M_in_den1
print('Done with insitu for', gal1)

### Ex-situ Fraction (Counting all stars in all halos)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num = np.zeros(len(sis_at_z))
M_ex_num[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins
 (Around 13 seconds)

    M_ex_num : Array
        M_ex_num[i] corresponds to the mass of stars that formed in all halos (other than the MMP) in a redshift
        bin, that also merge into the host at z = 0

    Method:
        1. Create some masks for stars that merge within 2-15 kpc of the host at z = 0 that also form
           in a redshift bin
        2. Combine all of the masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Set up a vector that includes the indices for all of the halos at a redshift except for the
           index of the MMP
        5. Loop through all halos at a redshift
        6. Out of the entire mask, only select stars that are members of a halo (now a subset of total mask)
        7. Use the mask back on that halo to get the indices of the stars that follow the criteria in 1
        8. Use the reverse pointers on these indices to get their indices at z = 0
        9. Add up the mass of all of these stars in all halos other than the MMP
'''
# Create a distance mask outside of the loop
# masks_pos_v (already done)
# Loop over all other redshift bins
for i in range(1, len(sis_at_z)):
    # Set up initial mass
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    total_mask_0 = masks_pos_v & maskz
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Set up vector to loop through halos; vector not containing index of MMP
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind1[i-1] != N)
    N = N[mask]
    # Loop over all halos aside from the MMP
    for j in N:
        # Get a subset of the total mask for only stars in a halo
        tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
        # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
        hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
        # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
        rev_point_good = sis_at_z_rev[i-1][hal_star_good]
        # Add up the mass for all of these particles for each halo (aside from the MMP)
        mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num[i] = mass
# Calculate the ex-situ fraction
exsitu_415_1 = M_ex_num/M_in_den1
print('Done with exsitu for', gal1)

## Ex-situ Fraction (Counting only stars that formed in halos with f_contr > 0.01)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num_contr = np.zeros(len(sis_at_z))
M_ex_num_contr[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins for halos that have f_contr > 0.01
 (Around 13 seconds)

    M_ex_num_contr : Array
        M_ex_num_contr[i] corresponds to the mass of stars that formed in all halos other than the MMP
        that formed in a redshift bin, but only for halos that contribute > 1% of their stars to the
        host by z = 0 (or within 2-15 kpc of it)

    Method:
        Similar to that above, but with the added condition that mass only gets added if a halo has
        a contribution fraction > 0.01
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
# masks_pos_v
# Loop over the other redshift bins
for i in range(1, len(sis_at_z)):
    # Set initial mass to be zero
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    total_mask_0 = masks_pos_v & maskz
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Create a vector to loop through each halo
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind1[i-1] != N)
    N = N[mask]
    for j in N:
        if ratio_1[i-1][j] > 0.01:
            # Get a subset of the total mask for only stars in a halo
            tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
            # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
            hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
            # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
            rev_point_good = sis_at_z_rev[i-1][hal_star_good]
            # Add up the mass for all of these particles for each halo (aside from the MMP)
            mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num_contr[i] = mass
# Calculate the ex-situ fraction
exsitu_415_contr_1 = M_ex_num_contr/M_in_den1
print('Done with exsitu_contr for', gal1)

# Calculate the totals and group them all together to be later stored in pickle files
total_415_1 = np.asarray(insitu_415_1) + np.asarray(exsitu_415_1)
total_415_contr_1 = np.asarray(insitu_415_1) + np.asarray(exsitu_415_contr_1)
ie_415_1 = [insitu_415_1, exsitu_415_1, exsitu_415_contr_1, total_415_1, total_415_contr_1]
print('Done with totals for', gal1)
#############################################################################################
# GALAXY 2
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind2 = []
for i in range(0, len(ratio_2)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_2[i][temp]
    mass_ind2.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_2[i][temp]
        mass_ind2[i] = temp
### In-situ Fraction
'''
 Find the mass of stars that formed in the progenitor between z = 0 and z = 0.1
 z = 0 shouldn't be in the loop below, do it manually here
'''
# Get indices of stars
# Create masks for R
masks_d01 = (part['star'].prop('star.host2.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part['star'].prop('star.host2.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part['star'].prop('star.host2.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part['star'].prop('star.host2.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r = part['star'].prop('star.host2.velocity.principal.cylindrical')[:,0]
v_z = part['star'].prop('star.host2.velocity.principal.cylindrical')[:,1]
v_phi = part['star'].prop('star.host2.velocity.principal.cylindrical')[:,2]
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Create redshift formation masks
masks_z = ((part['star'].prop('star.form.redshift') > 0) & (part['star'].prop('star.form.redshift') <= 0.1))
# Combine all of the masks
masks_d0 = masks_pos & masks_v & masks_z

# First, find the stars that formed between these redshifts in the most massive progenitor
M_0_01_insitu = np.sum(part['star']['mass'][masks_d0])
# Create a vector to attach all of the masses in the numerator for f_insitu
M_in_num = np.zeros(len(sis_at_z))
M_in_num[0] = M_0_01_insitu
'''
 Find the numerator for f_insitu for all other redshift bins
 (Around 13.64 seconds)

    M_in_num : Array
        M_in_num[i] corresponds to the mass of stars that formed in-situ (or within the most massive progenitor)
                    in some redshift bin that also merged in within 4-15 kpc of the host at z = 0

    Method:
        1. Create some masks that select stars that formed in a redshift bin that eventually
           merged into the host at z = 0
        2. Combine all of those masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Out of the entire mask, only select stars that are members of the MMP (now a subset of total mask)
        5. Use the new (subset) mask back on the MMP to only select stars that follow the criteria in step 1
        6. Use the reverse pointers to get the indices of these stars at z = 0
        7. Add up the mass of these stars for each redshift bin
'''
# Create the distance mask
masks_pos_v = masks_pos & masks_v
for i in range(1, len(sis_at_z)):
    # Create the redshift formation masks, combine them, and re-order
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    tot_mask_0 = masks_pos_v & maskz
    tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
    # Get subset of the total mask for only stars in MMP
    tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind2[i-1]]]
    # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
    hal_star_good = hal[i]['star.indices'][his[i]][mass_ind2[i-1]][tot_mask_z_sub]
    # Use these indices on the reverse pointer array
    rev_point_good = sis_at_z_rev[i-1][hal_star_good]
    M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])
'''
 Find the denominator for f_insitu for all redshifts
 (Around 16.55 seconds)

    M_in_den : Array
        M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
        eventually merge into the host (or within 4-15 kpc of it) at z = 0

    Method:
        1. Get the indices of all stars at z = 0 within 4-15 kpc of the host
        2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
        3. Add up all of the mass in each redshift bin.
'''
# Initialize an empty vector
M_in_den2 = np.zeros(len(sis_at_z))
# Get the indices of stars in some distance cut
#masks_pos_v
# Loop through all of the redshift bins and add up the mass
for i in range(0, len(sis_at_z)):
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    mask_tot = masks_pos_v & maskz
    M_in_den2[i] = np.sum(part['star']['mass'][mask_tot])
# Calculate the in-situ fraction
insitu_415_2 = M_in_num/M_in_den2
print('Done with insitu for', gal2)

### Ex-situ Fraction (Counting all stars in all halos)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num = np.zeros(len(sis_at_z))
M_ex_num[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins
 (Around 13 seconds)

    M_ex_num : Array
        M_ex_num[i] corresponds to the mass of stars that formed in all halos (other than the MMP) in a redshift
        bin, that also merge into the host at z = 0

    Method:
        1. Create some masks for stars that merge within 2-15 kpc of the host at z = 0 that also form
           in a redshift bin
        2. Combine all of the masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Set up a vector that includes the indices for all of the halos at a redshift except for the
           index of the MMP
        5. Loop through all halos at a redshift
        6. Out of the entire mask, only select stars that are members of a halo (now a subset of total mask)
        7. Use the mask back on that halo to get the indices of the stars that follow the criteria in 1
        8. Use the reverse pointers on these indices to get their indices at z = 0
        9. Add up the mass of all of these stars in all halos other than the MMP
'''
# Create a distance mask outside of the loop
# masks_pos_v (already done)
# Loop over all other redshift bins
for i in range(1, len(sis_at_z)):
    # Set up initial mass
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    total_mask_0 = masks_pos_v & maskz
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Set up vector to loop through halos; vector not containing index of MMP
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind2[i-1] != N)
    N = N[mask]
    # Loop over all halos aside from the MMP
    for j in N:
        # Get a subset of the total mask for only stars in a halo
        tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
        # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
        hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
        # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
        rev_point_good = sis_at_z_rev[i-1][hal_star_good]
        # Add up the mass for all of these particles for each halo (aside from the MMP)
        mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num[i] = mass
# Calculate the ex-situ fraction
exsitu_415_2 = M_ex_num/M_in_den2
print('Done with exsitu for', gal2)

## Ex-situ Fraction (Counting only stars that formed in halos with f_contr > 0.01)
'''
 Since none of the stars that form between z = 0 and z = 0.1 merge into the host, the ex-situ fraction is set to zero
'''
M_0_01_exsitu = 0
M_ex_num_contr = np.zeros(len(sis_at_z))
M_ex_num_contr[0] = M_0_01_exsitu
'''
 Find the numerator for f_exsitu for all other redshift bins for halos that have f_contr > 0.01
 (Around 13 seconds)

    M_ex_num_contr : Array
        M_ex_num_contr[i] corresponds to the mass of stars that formed in all halos other than the MMP
        that formed in a redshift bin, but only for halos that contribute > 1% of their stars to the
        host by z = 0 (or within 2-15 kpc of it)

    Method:
        Similar to that above, but with the added condition that mass only gets added if a halo has
        a contribution fraction > 0.01
'''
# Create a distance mask outside of the loop. Inefficient to keep redefining it.
# masks_pos_v
# Loop over the other redshift bins
for i in range(1, len(sis_at_z)):
    # Set initial mass to be zero
    mass = 0
    # Do the redshift formation masks first and re-arrange them to the order of the z pointers
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    total_mask_0 = masks_pos_v & maskz
    total_mask_z = total_mask_0[sis_at_z_rev[i-1]]
    # Create a vector to loop through each halo
    N = np.arange(len(hal[i]['star.indices'][his[i]]))
    mask = (mass_ind2[i-1] != N)
    N = N[mask]
    for j in N:
        if ratio_2[i-1][j] > 0.01:
            # Get a subset of the total mask for only stars in a halo
            tot_mask_z_sub = total_mask_z[hal[i]['star.indices'][his[i]][j]]
            # Use mask back on halo indices to only get the stars that satisfy the conditions of the masks
            hal_star_good = hal[i]['star.indices'][his[i]][j][tot_mask_z_sub]
            # Use the star indices above (which are at z) to get the pointers (indices at z = 0)
            rev_point_good = sis_at_z_rev[i-1][hal_star_good]
            # Add up the mass for all of these particles for each halo (aside from the MMP)
            mass += np.sum(part['star']['mass'][rev_point_good])
    M_ex_num_contr[i] = mass
# Calculate the ex-situ fraction
exsitu_415_contr_2 = M_ex_num_contr/M_in_den2
print('Done with exsitu_contr for', gal2)

# Calculate the totals and group them all together to be later stored in pickle files
total_415_2 = np.asarray(insitu_415_2) + np.asarray(exsitu_415_2)
total_415_contr_2 = np.asarray(insitu_415_2) + np.asarray(exsitu_415_contr_2)
ie_415_2 = [insitu_415_2, exsitu_415_2, exsitu_415_contr_2, total_415_2, total_415_contr_2]
print('Done with exsitu_contr for', gal2)

Filep = open(home_dir_stam+"/scripts/pickles/in_ex_1percent_"+gal1+"415"+".p", "wb")
pickle.dump(ie_415_1, Filep)
Filep.close()
Filep = open(home_dir_stam+"/scripts/pickles/in_ex_1percent_"+gal2+"415"+".p", "wb")
pickle.dump(ie_415_2, Filep)
Filep.close()
#############################################################################################
"""