#!/usr/bin/python3

"""
 =============
 = N vs z LG =
 =============
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Break, 2018

 Goal: Calculate the number of halos with masses above 10^5 M_star for each redshift
       for the following distance cuts:
        - d = [0, 300 kpc]
        - d = [0, 15 kpc]
        - d = [0, 2 kpc]
        - d = [4, 15 kpc]
  
       and save them to pickle files to be plotted in another script.
"""

### Import all of the tools for analysis and read in the data
import halo_analysis as rockstar
import utilities as ut
import numpy as np
import pickle

#### Read in the halo information
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
dist = 2
simulation_dir_pel = '/home/awetzel/scratch/'+galaxy+'/'+galaxy+resolution
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

# Read in the contribution fractions for the halos at each redshift
distn = str(dist)
# GALAXY 1
pickle_1 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal1+'.p'
# d = [0, d kpc]
Filep1 = open(pickle_1, "rb")
ratio_1 = pickle.load(Filep1)
Filep1.close()
# GALAXY 2
pickle_2 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal2+'.p'
# d = [0, d kpc]
Filep2 = open(pickle_2, "rb")
ratio_2 = pickle.load(Filep2)
Filep2.close()

# Some functions that help with analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
'''
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
    d: distance
	ind: indices that satisfy the conditions below
'''
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind

# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
# Set up mass vectors
M1 = [5.,6.,7.,8.,9.,10.]

# GALAXY 1
# Create some masks for the halos that contribute more than 1% of their stars; Use this on his[i].
hal_masks_h1 = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_1[i][k] >= 0.01:
            check[k] = True
    hal_masks_h1.append(check)
# Add in the halos that are within d kpc of the host at z = 0
hal_masks_h1.insert(0,(hal[0].prop('host.distance.total')[his[0]] < dist))
'''
 Find the Number of halos with masses above 10^5 for each redshift

    N* : List
        N[i] corresponds to the number of halos with mass above 10^5 for redshifts[i]
        
    Method:
        1. Set up mass value
        2. Calculate N by adding all of the halos that have masses
           greater than the initial mass value    
'''
# Count the number of halos at each redshift above a certain value, M
N5_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[0]) for i in range(0, len(redshifts))]
N6_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[1]) for i in range(0, len(redshifts))]
N7_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[2]) for i in range(0, len(redshifts))]
N8_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[3]) for i in range(0, len(redshifts))]
N9_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[4]) for i in range(0, len(redshifts))]
N10_h1 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h1[i]]) > M1[5]) for i in range(0, len(redshifts))]
N_h1 = [N5_h1, N6_h1, N7_h1, N8_h1, N9_h1, N10_h1]

# GALAXY 2
# Create some masks for the halos that contribute more than 1% of their stars; Use this on his[i].
hal_masks_h2 = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_2[i][k] >= 0.01:
            check[k] = True
    hal_masks_h2.append(check)
# Add in the halos that are within d kpc of the host at z = 0
hal_masks_h2.insert(0,(hal[0].prop('host2.distance.total')[his[0]] < dist))
'''
 Find the Number of halos with masses above 10^5 for each redshift

    N* : List
        N[i] corresponds to the number of halos with mass above 10^5 for redshifts[i]
        
    Method:
        1. Set up mass value
        2. Calculate N by adding all of the halos that have masses
           greater than the initial mass value    
'''
# Count the number of halos at each redshift above a certain value, M
N5_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[0]) for i in range(0, len(redshifts))]
N6_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[1]) for i in range(0, len(redshifts))]
N7_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[2]) for i in range(0, len(redshifts))]
N8_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[3]) for i in range(0, len(redshifts))]
N9_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[4]) for i in range(0, len(redshifts))]
N10_h2 = [np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks_h2[i]]) > M1[5]) for i in range(0, len(redshifts))]
N_h2 = [N5_h2, N6_h2, N7_h2, N8_h2, N9_h2, N10_h2]

Filep1 = open(home_dir_stam+"/scripts/pickles/Nvz_1percent_"+gal1+distn+".p", "wb")
pickle.dump(N_h1, Filep1)
Filep1.close()
Filep2 = open(home_dir_stam+"/scripts/pickles/Nvz_1percent_"+gal2+distn+".p", "wb")
pickle.dump(N_h2, Filep2)
Filep2.close()
