#!/usr/bin/python3

"""
 ============================
 = Insitu Cumulative Padder =
 ============================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2019

 Goal: Read in the insitu fractions and pad the data at z = 0 to smooth better later on

 NOTE: These use the files made from "in_ex_v2.py" or "in_ex_v3.py" or "in_ex_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import pickle

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12b_cum.p', 'rb')
ieb = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12c_cum.p', 'rb')
iec = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12f_cum.p', 'rb')
ief = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12i_cum.p', 'rb')
iei = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12m_cum.p', 'rb')
iem = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12w_cum.p', 'rb')
iew = pickle.load(File6)
File6.close()
# Romeo
File7 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romeo_cum.p', 'rb')
ierom = pickle.load(File7)
File7.close()
# Juliet
File8 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Juliet_cum.p', 'rb')
iejul = pickle.load(File8)
File8.close()
# Thelma
File9 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Thelma_cum.p', 'rb')
iethe = pickle.load(File9)
File9.close()
# Louise
File10 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Louise_cum.p', 'rb')
ielou = pickle.load(File10)
File10.close()
# Romulus
File11 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romulus_cum.p', 'rb')
ieromu = pickle.load(File11)
File11.close()
# Remus
File12 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Remus_cum.p', 'rb')
ierem = pickle.load(File12)
File12.close()

ieb_new = []
for i in range(0, len(ieb)):
    ieb_new.append(np.append(np.ones(20)*ieb[i][0], ieb[i]))

iec_new = []
for i in range(0, len(iec)):
    iec_new.append(np.append(np.ones(20)*iec[i][0], iec[i]))

ief_new = []
for i in range(0, len(ief)):
    ief_new.append(np.append(np.ones(20)*ief[i][0], ief[i]))

iei_new = []
for i in range(0, len(iei)):
    iei_new.append(np.append(np.ones(20)*iei[i][0], iei[i]))

iem_new = []
for i in range(0, len(iem)):
    iem_new.append(np.append(np.ones(20)*iem[i][0], iem[i]))

iew_new = []
for i in range(0, len(iew)):
    iew_new.append(np.append(np.ones(20)*iew[i][0], iew[i]))

ierom_new = []
for i in range(0, len(ierom)):
    ierom_new.append(np.append(np.ones(20)*ierom[i][0], ierom[i]))

iejul_new = []
for i in range(0, len(iejul)):
    iejul_new.append(np.append(np.ones(20)*iejul[i][0], iejul[i]))

iethe_new = []
for i in range(0, len(iethe)):
    iethe_new.append(np.append(np.ones(20)*iethe[i][0], iethe[i]))

ielou_new = []
for i in range(0, len(ielou)):
    ielou_new.append(np.append(np.ones(20)*ielou[i][0], ielou[i]))

ieromu_new = []
for i in range(0, len(ieromu)):
    ieromu_new.append(np.append(np.ones(20)*ieromu[i][0], ieromu[i]))

ierem_new = []
for i in range(0, len(ierem)):
    ierem_new.append(np.append(np.ones(20)*ierem[i][0], ierem[i]))

# Save the data
Fileptest = open(home_dir_stam+"/scripts/pickles/insitu_cumulative_all.p", "wb")
pickle.dump(ieb_new, Fileptest)
pickle.dump(iec_new, Fileptest)
pickle.dump(ief_new, Fileptest)
pickle.dump(iei_new, Fileptest)
pickle.dump(iem_new, Fileptest)
pickle.dump(iew_new, Fileptest)
pickle.dump(ierom_new, Fileptest)
pickle.dump(iejul_new, Fileptest)
pickle.dump(iethe_new, Fileptest)
pickle.dump(ielou_new, Fileptest)
pickle.dump(ieromu_new, Fileptest)
pickle.dump(ierem_new, Fileptest)
Fileptest.close()