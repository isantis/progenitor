#!/usr/bin/python3

"""
 ==================================
 = Insitu summary data cumulative =
 ==================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2019

 Goal: Read in the insitu fractions for all galaxies and get the redshift when the
       fraction was LAST below some value

 NOTE: These use the files made from "insitu_cumulative.py and insitu_cumulative_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

ifrac = 0.5

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/insitu_cumulative_all.p', 'rb')
ieb = pickle.load(File1)
iec = pickle.load(File1)
ief = pickle.load(File1)
iei = pickle.load(File1)
iem = pickle.load(File1)
iew = pickle.load(File1)
ierom = pickle.load(File1)
iejul = pickle.load(File1)
iethe = pickle.load(File1)
ielou = pickle.load(File1)
ieromu = pickle.load(File1)
ierem = pickle.load(File1)
File1.close()

## Calculate the median insitu fractions for each distance cut
# d = [0, 300 kpc]
insitu300 = [ieb[0], iec[0], ief[0], iei[0], iem[0], iew[0], ierom[0], iejul[0], iethe[0], ielou[0], ieromu[0], ierem[0]]
ie300med = np.median(insitu300,0)
# d = [0, 15 kpc]
insitu15 = [ieb[1], iec[1], ief[1], iei[1], iem[1], iew[1], ierom[1], iejul[1], iethe[1], ielou[1], ieromu[1], ierem[1]]
ie15med = np.median(insitu15,0)
# d = [0, 2 kpc]
insitu2 = [ieb[2], iec[2], ief[2], iei[2], iem[2], iew[2], ierom[2], iejul[2], iethe[2], ielou[2], ieromu[2], ierem[2]]
ie2med = np.median(insitu2,0)
# d = [4, 15 kpc]
insitu415 = [ieb[3], iec[3], ief[3], iei[3], iem[3], iew[3], ierom[3], iejul[3], iethe[3], ielou[3], ieromu[3], ierem[3]]
ie415med = np.median(insitu415,0)

# Get the last time that the fraction was below ifrac
red_temp = np.flip(redshifts)
last_red_300 = red_temp[np.max(np.where(np.flip(ie300med) < ifrac)[0])] # flip so that it goes from z=7 to z=0, get where its below ifrac, then get max value (smallest redshift)
last_red_15 = red_temp[np.max(np.where(np.flip(ie15med) < ifrac)[0])]
last_red_2 = red_temp[np.max(np.where(np.flip(ie2med) < ifrac)[0])]
last_red_415 = red_temp[np.max(np.where(np.flip(ie415med) < ifrac)[0])]

last_insitu = [last_red_300, last_red_15, last_red_2, last_red_415]

## Do the same thing for the smoothed insitu fractions
# d = [0, 300 kpc]
redss = [0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05,2.15,2.25,2.35,2.45,2.55,2.65,2.75,2.85,2.95,3.05,3.15,3.25,3.35,3.45,3.55,3.65,3.75,3.85,3.95,4.1,4.3,4.5,4.7,4.9,5.1,5.3,5.5,5.7,5.9,6.5,7.5,8.5]
window_size, poly_order = 41, 3
ie300smooth = savgol_filter(ie300med, window_size, poly_order)

redss = np.flip(redss)
last_red_300_smooth = redss[np.max(np.where(np.flip(ie300smooth) < ifrac)[0])]

# d = [0, 15 kpc]
ie15smooth = savgol_filter(ie15med, window_size, poly_order)

last_red_15_smooth = redss[np.max(np.where(np.flip(ie15smooth) < ifrac)[0])]

# d = [0, 2 kpc]
ie2smooth = savgol_filter(ie2med, window_size, poly_order)

last_red_2_smooth = redss[np.max(np.where(np.flip(ie2smooth) < ifrac)[0])]

# d = [4, 15 kpc]
ie415smooth = savgol_filter(ie415med, window_size, poly_order)

last_red_415_smooth = redss[np.max(np.where(np.flip(ie415smooth) < ifrac)[0])]

last_insitu_smooth = [last_red_300_smooth, last_red_15_smooth, last_red_2_smooth, last_red_415_smooth]

# print out the values before saving them to a file in case you dont want to read the file
print(last_insitu, 'for raw data')
print(last_insitu_smooth, 'for smoothed median data')

# Save the redshifts to a file
Filep = open(home_dir_stam+"/scripts/pickles/last_insitu_cum_redshifts_v2.p", "wb")
pickle.dump(last_insitu, Filep)
pickle.dump(last_insitu_smooth, Filep)
Filep.close()