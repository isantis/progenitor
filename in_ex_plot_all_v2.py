#!/usr/bin/python3

"""
 ==========================
 = Insitu v2 Plot all =
 ==========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2018

 Goal: Read in the insitu fractions for all galaxies and plot them
       Calculate and plot the median insitu fraction.

 NOTE: These use the files made from "in_ex_v2.py" or "in_ex_v3.py" or "in_ex_lg.py"

 For REFERENCE:
      Some of the functions I used to smooth/interpolate the data were:
      ie300medra1 = np.ma.average(insitu, axis=0)
      ie300medra2 = np.ma.average(insitu, axis=0, weights=dens)
      ie300sg4 = savgol_filter(ie300med, axis=0, polyorder=3, window_length=31)
      ie300spl2 = interpolate.UnivariateSpline(redss, ie300med, w=ws)
      dens = [denb, denc, denf, deni, denm, denq, denw]
      ws = np.median(dens,0)
      ie300box = convolve(ie300med, Box1DKernel(5))
      ie300spl3 = interp1d(redss, ie300med, kind='cubic')
      ie300spl4 = interpolate.InterpolatedUnivariateSpline(redss, ie300med)
      ie300spl5 = interpolate.InterpolatedUnivariateSpline(redss, ie300med, w=ws)
      from scipy.interpolate import interp1d
      from astropy.convolution import convolve, Box1DKernel
      from scipy.signal import savgol_filter
      File1 = open(home_dir_stam+'/scripts/pickles/mindenom_m12b.p', 'rb')
      denb = pickle.load(File1)
      File1.close()

      ALSO, tried to smooth before taking the median of the data. Seems better to do it before, then smooth...
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

iecut = 0.5
colors = ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#CC6677','#882255','#AA4499','#BBBBBB','#A50026','#726A83']

## Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/insitu_all.p', 'rb')
ieb = pickle.load(File1)
iec = pickle.load(File1)
ief = pickle.load(File1)
iei = pickle.load(File1)
iem = pickle.load(File1)
iew = pickle.load(File1)
ierom = pickle.load(File1)
iejul = pickle.load(File1)
iethe = pickle.load(File1)
ielou = pickle.load(File1)
ieromu = pickle.load(File1)
ierem = pickle.load(File1)
File1.close()

# d = [0, 300 kpc]
# Calculate the median insitu fraction
redss = [-1.95,-1.85,-1.75,-1.65,-1.55,-1.45,-1.35,-1.25,-1.15,-1.05,-0.95,-0.85,-0.75,-0.65,-0.55,-0.45,-0.35,-0.25,-0.15,-0.05,0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05,2.15,2.25,2.35,2.45,2.55,2.65,2.75,2.85,2.95,3.05,3.15,3.25,3.35,3.45,3.55,3.65,3.75,3.85,3.95,4.1,4.3,4.5,4.7,4.9,5.1,5.3,5.5,5.7,5.9,6.5,7.5,8.5]
insitu = [ieb[0], iec[0], ief[0], iei[0], iem[0], iew[0], ierom[0], iejul[0], iethe[0], ielou[0], ieromu[0], ierem[0]]
ie300med = np.median(insitu,0)
window_size, poly_order = 41, 3
ie300smooth = savgol_filter(ie300med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
"""
Not needed anymore, but leaving here just in case we need to go back to it...
a = np.percentile(insitu, 15.87, axis=0)
b = np.percentile(insitu, 84.13, axis=0)
asmooth = np.percentile(insitu_smooth, 15.87, axis=0)
bsmooth = np.percentile(insitu_smooth, 84.13, axis=0)
"""
# Plot the individuals (smooth), median, and smoothed
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redss, insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redss, insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redss, insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redss, insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redss, insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redss, insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redss, insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redss, insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redss, insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redss, insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.35)
plt.plot(redss, insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.35)
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redss, ie300smooth, 'k', linewidth=4.0, label='Median')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, inst.}}$', fontsize=46)
plt.title('d(z = 0) = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 14})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_300_med_ind_smooth_v5.pdf')
plt.close()

# d = [0, 15 kpc]
# Calculate the median insitu fraction
insitu = [ieb[1], iec[1], ief[1], iei[1], iem[1], iew[1], ierom[1], iejul[1], iethe[1], ielou[1], ieromu[1], ierem[1]]
ie15med = np.median(insitu,0)
window_size, poly_order = 41, 3
ie15smooth = savgol_filter(ie15med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Plot the individuals (smooth), median, and smoothed
plt.figure(2)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redss, insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redss, insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redss, insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redss, insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redss, insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redss, insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redss, insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redss, insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redss, insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redss, insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.35)
plt.plot(redss, insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.35)
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redss, ie15smooth, 'k', linewidth=4.0, label='Median')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, inst.}}$', fontsize=46)
plt.title('d(z = 0) = [0, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 14})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_15_med_ind_smooth_v5.pdf')
plt.close()

# d = [0, 2 kpc]
# Calculate the median insitu fraction
insitu = [ieb[2], iec[2], ief[2], iei[2], iem[2], iew[2], ierom[2], iejul[2], iethe[2], ielou[2], ieromu[2], ierem[2]]
ie2med = np.median(insitu,0)
window_size, poly_order = 41, 3
ie2smooth = savgol_filter(ie2med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Plot the individuals (smooth), median, and smoothed
plt.figure(3)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redss, insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redss, insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redss, insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redss, insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redss, insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redss, insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redss, insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redss, insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redss, insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redss, insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.35)
plt.plot(redss, insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.35)
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redss, ie2smooth, 'k', linewidth=4.0, label='Median')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, inst.}}$', fontsize=46)
plt.title('d(z = 0) = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 14})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_2_med_ind_smooth_v5.pdf')
plt.close()

# d = [4, 15 kpc]
# Calculate the median insitu fraction
insitu = [ieb[3], iec[3], ief[3], iei[3], iem[3], iew[3], ierom[3], iejul[3], iethe[3], ielou[3], ieromu[3], ierem[3]]
ie415med = np.median(insitu,0)
window_size, poly_order = 41, 3
ie415smooth = savgol_filter(ie415med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Plot the individuals (smooth), median, and smoothed
plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redss, insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redss, insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redss, insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redss, insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redss, insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redss, insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redss, insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redss, insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redss, insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redss, insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.35)
plt.plot(redss, insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.35)
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redss, ie415smooth, 'k', linewidth=4.0, label='Median')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, inst.}}$', fontsize=46)
plt.title('d(z = 0) = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 14})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_415_med_ind_smooth_v5.pdf')
plt.close()