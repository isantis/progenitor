#!/usr/bin/python3

"""
 ==========================
 = Insitu v2 Plot all =
 ==========================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2018

 Goal: Read in the insitu fractions for all galaxies and plot them
       Calculate and plot the median insitu fraction.

 NOTE: These use the files made from "insitu_cumulative.py" and "insitu_cumulative_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

iecut = 0.5
#colors = ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#CC6677','#882255','#AA4499','#BBBBBB','#A50026','#726A83']
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']

## Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/insitu_cumulative_all.p', 'rb')
ieb = pickle.load(File1)
iec = pickle.load(File1)
ief = pickle.load(File1)
iei = pickle.load(File1)
iem = pickle.load(File1)
iew = pickle.load(File1)
ierom = pickle.load(File1)
iejul = pickle.load(File1)
iethe = pickle.load(File1)
ielou = pickle.load(File1)
ieromu = pickle.load(File1)
ierem = pickle.load(File1)
File1.close()

# d = [0, 300 kpc]
# Calculate the median insitu fraction for ALL hosts
insitu = [ieb[0], iec[0], ief[0], iei[0], iem[0], iew[0], ierom[0], iejul[0], iethe[0], ielou[0], ieromu[0], ierem[0]]
ie300med = np.median(insitu,0)
window_size, poly_order = 41, 3
ie300smooth = savgol_filter(ie300med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Calculate the median insitu fraction for isolated hosts hosts
insitu_iso = [ieb[0], iec[0], ief[0], iei[0], iem[0], iew[0]]
ie300med_iso = np.median(insitu_iso,0)
ie300smooth_iso = savgol_filter(ie300med_iso, window_size, poly_order)
insitu_smooth_iso = [savgol_filter(insitu_iso[i], window_size, poly_order) for i in range(0, len(insitu_iso))]
# Calculate the median insitu fraction for pair hosts
insitu_lg = [ierom[0], iejul[0], iethe[0], ielou[0], ieromu[0], ierem[0]]
ie300med_lg = np.median(insitu_lg,0)
ie300smooth_lg = savgol_filter(ie300med_lg, window_size, poly_order)
insitu_smooth_lg = [savgol_filter(insitu_lg[i], window_size, poly_order) for i in range(0, len(insitu_lg))]
"""
Don't use these anymore, but leaving them in just in case we need to go back...
a = np.percentile(insitu, 15.87, axis=0)
b = np.percentile(insitu, 84.13, axis=0)
asmooth = np.percentile(insitu_smooth, 15.87, axis=0)
bsmooth = np.percentile(insitu_smooth, 84.13, axis=0)
"""
"""
# Plot the individuals (smooth), median, and smoothed
plt.figure(1)
plt.figure(figsize=(10, 8))
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redshifts[:-1], insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.60)
#
plt.plot(redshifts[:-1], ie300smooth, 'k', linewidth=5.5, label='Median (All)')
plt.plot(redshifts[:-1], ie300smooth_iso, 'k', linewidth=4.0, alpha=0.60, label='Median (Isolated Hosts)')
plt.plot(redshifts[:-1], ie300smooth_lg, 'k', linewidth=4.0, linestyle='--', alpha=0.60, label='Median (LG Pairs)')
# Add in empty lines to make legend look good
plt.plot(redshifts[:-1], ie300smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie300smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie300smooth_lg, linestyle='', label=' ')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, cumul.}}$', fontsize=46)
plt.title('d(z = 0) < 300 kpc', fontsize=48, y=1.02)
plt.legend(ncol=3, prop={'size': 12})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_300_all_medians_cumulative_vf.pdf')
plt.close()
"""

# d = [0, 15 kpc]
# Calculate the median insitu fraction for ALL galaxies
insitu = [ieb[1], iec[1], ief[1], iei[1], iem[1], iew[1], ierom[1], iejul[1], iethe[1], ielou[1], ieromu[1], ierem[1]]
ie15med = np.median(insitu,0)
ie15smooth = savgol_filter(ie15med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Calculate the median insitu fraction for isolated hosts
insitu_iso = [ieb[1], iec[1], ief[1], iei[1], iem[1], iew[1]]
ie15med_iso = np.median(insitu_iso,0)
ie15smooth_iso = savgol_filter(ie15med_iso, window_size, poly_order)
insitu_smooth_iso = [savgol_filter(insitu_iso[i], window_size, poly_order) for i in range(0, len(insitu_iso))]
# Calculate the median insitu fraction for LG galaxies
insitu_lg = [ierom[1], iejul[1], iethe[1], ielou[1], ieromu[1], ierem[1]]
ie15med_lg = np.median(insitu_lg,0)
ie15smooth_lg = savgol_filter(ie15med_lg, window_size, poly_order)
insitu_smooth_lg = [savgol_filter(insitu_lg[i], window_size, poly_order) for i in range(0, len(insitu_lg))]
# Plot the individuals (smooth), median, and smoothed
plt.figure(2)
plt.figure(figsize=(10, 8))
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redshifts[:-1], insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[2], color=colors[6], linewidth=2.0, label=galaxies[2], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[6], color=colors[0], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[7], color=colors[1], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[8], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[9], color=colors[3], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[10], color=colors[4], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[11], color=colors[5], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.60)
#
plt.plot(redshifts[:-1], ie15smooth, 'k', linewidth=5.5, label='Median (All)')
plt.plot(redshifts[:-1], ie15smooth_iso, 'k', linewidth=4.0, alpha=0.60, label='Median (Isolated Hosts)')
plt.plot(redshifts[:-1], ie15smooth_lg, 'k', linewidth=4.0, linestyle='--', alpha=0.60, label='Median (LG Pairs)')
# Add in empty lines to make the legend look good
plt.plot(redshifts[:-1], ie15smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie15smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie15smooth_lg, linestyle='', label=' ')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, cumul.}}$', fontsize=46)
plt.title('d(z = 0) < 15 kpc', fontsize=48, y=1.02)
plt.legend(ncol=3, prop={'size': 12})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_15_med_ind_smooth_cumulative_vf.pdf')
plt.close()

# d = [0, 2 kpc]
# Calculate the median insitu fraction for ALL galaxies
insitu = [ieb[2], iec[2], ief[2], iei[2], iem[2], iew[2], ierom[2], iejul[2], iethe[2], ielou[2], ieromu[2], ierem[2]]
ie2med = np.median(insitu,0)
ie2smooth = savgol_filter(ie2med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Calculate the median insitu fraction for isolated galaxies
insitu_iso = [ieb[2], iec[2], ief[2], iei[2], iem[2], iew[2]]
ie2med_iso = np.median(insitu_iso,0)
ie2smooth_iso = savgol_filter(ie2med_iso, window_size, poly_order)
insitu_smooth_iso = [savgol_filter(insitu_iso[i], window_size, poly_order) for i in range(0, len(insitu_iso))]
# Calculate the median insitu fraction for LG pairs
insitu_lg = [ierom[2], iejul[2], iethe[2], ielou[2], ieromu[2], ierem[2]]
ie2med_lg = np.median(insitu_lg,0)
ie2smooth_lg = savgol_filter(ie2med_lg, window_size, poly_order)
insitu_smooth_lg = [savgol_filter(insitu_lg[i], window_size, poly_order) for i in range(0, len(insitu_lg))]

# Plot the individuals (smooth), median, and smoothed
plt.figure(3)
plt.figure(figsize=(10, 8))
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redshifts[:-1], insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[2], color=colors[6], linewidth=2.0, label=galaxies[2], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[6], color=colors[0], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[7], color=colors[1], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[8], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[9], color=colors[3], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[10], color=colors[4], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[11], color=colors[5], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.60)
#
plt.plot(redshifts[:-1], ie2smooth, 'k', linewidth=5.5, label='Median (All)')
plt.plot(redshifts[:-1], ie2smooth_iso, 'k', linewidth=4.0, alpha=0.60, label='Median (Isolated Hosts)')
plt.plot(redshifts[:-1], ie2smooth_lg, 'k', linewidth=4.0, linestyle='--', alpha=0.60, label='Median (LG Pairs)')
# Add in empty lines to make the legend look good
plt.plot(redshifts[:-1], ie2smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie2smooth_lg, linestyle='', label=' ')
plt.plot(redshifts[:-1], ie2smooth_lg, linestyle='', label=' ')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, cumul.}}$', fontsize=46)
plt.title('d(z = 0) < 2 kpc', fontsize=48, y=1.02)
#plt.legend(ncol=3, prop={'size': 12})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_2_med_ind_smooth_cumulative_vf.pdf')
plt.close()

"""
# d = [4, 15 kpc]
# Calculate the median insitu fraction for ALL galaxies
insitu = [ieb[3], iec[3], ief[3], iei[3], iem[3], iew[3], ierom[3], iejul[3], iethe[3], ielou[3], ieromu[3], ierem[3]]
ie415med = np.median(insitu,0)
ie415smooth = savgol_filter(ie415med, window_size, poly_order)
insitu_smooth = [savgol_filter(insitu[i], window_size, poly_order) for i in range(0, len(insitu))]
# Calculate the median insitu fraction for isolated hosts
insitu_iso = [ieb[3], iec[3], ief[3], iei[3], iem[3], iew[3]]
ie415med_iso = np.median(insitu_iso,0)
ie415smooth_iso = savgol_filter(ie415med_iso, window_size, poly_order)
insitu_smooth_iso = [savgol_filter(insitu_iso[i], window_size, poly_order) for i in range(0, len(insitu_iso))]
# Calculate the median insitu fraction for LG pairs
insitu_lg = [ierom[3], iejul[3], iethe[3], ielou[3], ieromu[3], ierem[3]]
ie415med_lg = np.median(insitu_lg,0)
ie415smooth_lg = savgol_filter(ie415med_lg, window_size, poly_order)
insitu_smooth_lg = [savgol_filter(insitu_lg[i], window_size, poly_order) for i in range(0, len(insitu_lg))]
# Plot the individuals (smooth), median, and smoothed
plt.figure(4)
plt.figure(figsize=(10, 8))
#
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
#
plt.plot(redshifts[:-1], insitu_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.60)
plt.plot(redshifts[:-1], insitu_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.60)
#
plt.plot(redshifts[:-1], ie415smooth, 'k', linewidth=5.5, label='Median (All)')
plt.plot(redshifts[:-1], ie415smooth_iso, 'k', linewidth=4.0, alpha=0.60, label='Median (Isolated Hosts)')
plt.plot(redshifts[:-1], ie415smooth_lg, 'k', linewidth=4.0, linestyle='--', alpha=0.60, label='Median (LG Pairs)')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('f$_{\\rm{in-situ, cumul.}}$', fontsize=46)
plt.title('d(z = 0) = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(ncol=3, prop={'size': 13})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/insitu_415_med_ind_smooth_cumulative_vf.pdf')
plt.close()
"""
