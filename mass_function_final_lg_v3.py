#!/usr/bin/python3

"""
 ==========================
 = Mass Function LG Plots =
 ==========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: To create mass function plots for the halos with baryons that satisfy the following conditions
		- Stellar density > 300 [M_sun / pc^3]
		- Low-res fraction > 2%
		- N_star > 10
        - Bound mass fraction > 4%
		
        for objects that merge within the following distance cuts:
        - d = [0, 300 kpc]
            - For this cut, going to normalize to z = 0 curve too 
        - d = [0, 15 kpc]
        - d = [0, 2 kpc]
        - d = [4, 15 kpc] 

        and save the data to pickle files with the names: 'mf_<galaxy1>.p' & 'mf_<galaxy2>.p'
"""

### Import all of the tools for analysis and read in the data
import halo_analysis as rockstar
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import distinct_colours as dc
import pickle

# 	Read in the halo catalogs at each redshift
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
simulation_dir_pel = '/home/awetzel/scratch/'+galaxy+'/'+galaxy+resolution
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

# Set up the colors
colors = dc.get_distinct(5)

# Some functions that help with analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
    d: distance
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind

# Read in the contribution fractions for the halos at each redshift
# GALAXY 1
pickle300_1 = home_dir_stam+'/scripts/pickles/contribution_300_fullres_'+gal1+'.p'
pickle15_1 = home_dir_stam+'/scripts/pickles/contribution_15_fullres_'+gal1+'.p'
pickle2_1 = home_dir_stam+'/scripts/pickles/contribution_2_fullres_'+gal1+'.p'
pickle415_1 = home_dir_stam+'/scripts/pickles/contribution_415_fullres_'+gal1+'.p'
# d = [0, 300 kpc]
Filep1 = open(pickle300_1, "rb")
ratio_300_1 = pickle.load(Filep1)
Filep1.close()
# d = [0, 15 kpc]
Filep2 = open(pickle15_1, "rb")
ratio_15_1 = pickle.load(Filep2)
Filep2.close()
#d = [0, 2 kpc]
Filep3 = open(pickle2_1, "rb")
ratio_2_1 = pickle.load(Filep3)
Filep3.close()
#d = [4, 15 kpc]
Filep4 = open(pickle415_1, "rb")
ratio_415_1 = pickle.load(Filep4)
Filep4.close()
# GALAXY 2
pickle300_2 = home_dir_stam+'/scripts/pickles/contribution_300_fullres_'+gal2+'.p'
pickle15_2 = home_dir_stam+'/scripts/pickles/contribution_15_fullres_'+gal2+'.p'
pickle2_2 = home_dir_stam+'/scripts/pickles/contribution_2_fullres_'+gal2+'.p'
pickle415_2 = home_dir_stam+'/scripts/pickles/contribution_415_fullres_'+gal2+'.p'
# d = [0, 300 kpc]
Filep6 = open(pickle300_2, "rb")
ratio_300_2 = pickle.load(Filep6)
Filep6.close()
# d = [0, 15 kpc]
Filep7 = open(pickle15_2, "rb")
ratio_15_2 = pickle.load(Filep7)
Filep7.close()
# d = [0, 2 kpc]
Filep8 = open(pickle2_2, "rb")
ratio_2_2 = pickle.load(Filep8)
Filep8.close()
# d = [4, 15 kpc]
Filep9 = open(pickle415_2, "rb")
ratio_415_2 = pickle.load(Filep9)
Filep9.close()

### Plot the data
## d = [0, 300 kpc] (Cumulative and Normalized)
### GALAXY 1
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_300_1[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 300 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host.distance.total')[his[0]] < 300))
"""
 Find the cumulative and differential mass functions for different mass bins with the d = [0, 300 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N1 - The cumulative mass function for a 0.5 dex mass bin for 300 kpc
        
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
        3. Normalize the cumulative mass function by the curves at
           z = 0 (if the values at z = 0 aren't equal to zero...)
    
"""
# Set up mass vectors
M1 = np.linspace(5., 10., 11)
# Count the number of halos at each redshift above a certain value in M
N1 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Normalize the vectors above to the values at z = 0
N1_norm = np.asarray(N1)/np.asarray(N1[0])
# Plot the data for the 0.5 dex bin
plt.figure(1)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(211)
plt.plot(10**(M1), N1[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N1[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N1[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N1[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N1[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 16})
plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
plt.tick_params(axis='y', which='both', labelsize=22, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.xscale('log')
#plt.yscale('log')
plt.ylabel('N(> $M_{star}$)', fontsize=38)
# Bottom
ax = plt.subplot(212)
plt.plot(10**(M1), N1_norm[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N1_norm[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N1_norm[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N1_norm[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N1_norm[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='both', labelsize=22, width=1.5)
plt.ylim(ymin=0)
plt.xlim(10**5,10**9)
plt.ylabel('N(z) / N(z = 0)', fontsize=30)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=34)
plt.xscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_300kpc_1percent_'+gal1+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N1_1 = N1
N1_norm_1 = N1_norm
### GALAXY 2
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_300_2[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 300 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host2.distance.total')[his[0]] < 300))
"""
 Find the cumulative and differential mass functions for different mass bins with the d = [0, 300 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N1 - The cumulative mass function for a 0.5 dex mass bin for 300 kpc
        
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
        3. Normalize the cumulative mass function by the curves at
           z = 0 (if the values at z = 0 aren't equal to zero...)
    
"""
# Set up mass vectors
M1 = np.linspace(5., 10., 11)
# Count the number of halos at each redshift above a certain value in M
N1 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Normalize the vectors above to the values at z = 0
N1_norm = np.asarray(N1)/np.asarray(N1[0])
# Plot the data for the 0.5 dex bin
plt.figure(1)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(211)
plt.plot(10**(M1), N1[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N1[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N1[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N1[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N1[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 16})
plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
plt.tick_params(axis='y', which='both', labelsize=22, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.xscale('log')
#plt.yscale('log')
plt.ylabel('N(> $M_{star}$)', fontsize=38)
# Bottom
ax = plt.subplot(212)
plt.plot(10**(M1), N1_norm[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N1_norm[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N1_norm[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N1_norm[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N1_norm[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='both', labelsize=22, width=1.5)
plt.ylim(ymin=0)
plt.xlim(10**5,10**9)
plt.ylabel('N(z) / N(z = 0)', fontsize=30)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=34)
plt.xscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_300kpc_1percent_'+gal2+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N1_2 = N1
N1_norm_2 = N1_norm

## d = [0, 15 kpc] (Only Cumulative)
### GALAXY 1
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_15_1[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 15 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host.distance.total')[his[0]] < 15))
"""
 Find the cumulative mass function for mass bin with the d = [0, 15 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N2 - The cumulative mass function for a 0.5 dex mass bin for 15 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N2 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N2[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N2[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N2[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N2[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N2[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_15kpc_1percent_'+gal1+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N2_1 = N2
### GALAXY 2
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_15_2[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 15 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host2.distance.total')[his[0]] < 15))
"""
 Find the cumulative mass function for mass bin with the d = [0, 15 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N2 - The cumulative mass function for a 0.5 dex mass bin for 15 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N2 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N2[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N2[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N2[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N2[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N2[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_15kpc_1percent_'+gal2+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N2_2 = N2

## d = [0, 2 kpc] (Only Cumulative)
### GALAXY 1
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_2_1[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 2 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host.distance.total')[his[0]] < 2))
"""
 Find the cumulative mass function for mass bin with the d = [0, 2 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N3 - The cumulative mass function for a 0.5 dex mass bin for 2 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N3 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N3[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N3[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N3[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N3[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N3[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_2kpc_1percent_'+gal1+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N3_1 = N3
### GALAXY 2
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_2_2[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 2 kpc of the host at z = 0
hal_masks.insert(0,(hal[0].prop('host2.distance.total')[his[0]] < 2))
"""
 Find the cumulative mass function for mass bin with the d = [0, 2 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N3 - The cumulative mass function for a 0.5 dex mass bin for 2 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N3 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N3[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N3[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N3[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N3[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N3[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_2kpc_1percent_'+gal2+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N3_2 = N3

## d = [4, 15 kpc] (Only Cumulative)
### GALAXY 1
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_415_1[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 4-15 kpc of the host at z = 0
hal_masks.insert(0,((hal[0].prop('host.distance.total')[his[0]] > 4) & (hal[0].prop('host.distance.total')[his[0]] < 15)))
"""
 Find the cumulative mass function for mass bin with the d = [4, 15 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N3 - The cumulative mass function for a 0.5 dex mass bin for 4-15 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N4 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N4[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N4[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N4[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N4[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N4[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_415kpc_1percent_'+gal1+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N4_1 = N4
### GALAXY 2
"""
 At each redshift, create masks for the halos that contribute more than 1% of their stars; Use this on his[i].
    
    hal_masks : List
        hal_masks[i] corresponds to a boolean mask of the halos at a redshift i that contribute > 0.01 of the stars
"""
hal_masks = []
for i in range(0, len(redshifts)-1):
    N = len(hal[i+1]['star.mass'][his[i+1]])
    check = np.zeros(N, bool)
    for k in range(0, N):
        if ratio_415_2[i][k] >= 0.01:
            check[k] = True
    hal_masks.append(check)
# Add in the halos that are within 4-15 kpc of the host at z = 0
hal_masks.insert(0,((hal[0].prop('host2.distance.total')[his[0]] > 4) & (hal[0].prop('host2.distance.total')[his[0]] < 15)))
"""
 Find the cumulative mass function for mass bin with the d = [4, 15 kpc] cut
 
    N* : List
        N*[i] corresponds to the number of halos above a certain mass range
        N3 - The cumulative mass function for a 0.5 dex mass bin for 4-15 kpc
                
    Method:
        1. Set up mass vector to correspond to the mass bin
        2. Calculate the cumulative mass function by adding all of the halos that have masses
           greater than the elements of the mass vector
"""
# Count the number of halos at each redshift above a certain value in M
N4 = [[np.sum(np.log10(hal[i]['star.mass'][his[i]][hal_masks[i]]) > j) for j in M1] for i in range(0, len(redshifts))]
# Plot the data for the 0.5 dex bin
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N4[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N4[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N4[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N4[40], color=colors[3], label='z = 4')
plt.plot(10**(M1), N4[50], color=colors[4], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
#plt.ylim(0.9, 100)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $M_{star}$)', fontsize=46)
plt.xlabel('$M_{star}$ [$M_{\odot}$]', fontsize=46)
plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_415kpc_1percent_'+gal2+'_v5.pdf')
# Rename the variables to make it easier to save the data to a file
N4_2 = N4

# Store all of the data to a file to be used for the median plots
# GALAXY 1
Filep1 = open(home_dir_stam+"/scripts/pickles/mf_"+gal1+".p", "wb")
pickle.dump(N1_1, Filep1)
pickle.dump(N1_norm_1, Filep1)
pickle.dump(N2_1, Filep1)
pickle.dump(N3_1, Filep1)
pickle.dump(N4_1, Filep1)
Filep1.close()
# GALAXY2
Filep2 = open(home_dir_stam+"/scripts/pickles/mf_"+gal2+".p", "wb")
pickle.dump(N1_2, Filep2)
pickle.dump(N1_norm_2, Filep2)
pickle.dump(N2_2, Filep2)
pickle.dump(N3_2, Filep2)
pickle.dump(N4_2, Filep2)
Filep2.close()