#!/usr/bin/python3

"""
 ================================
 = In-situ / Ex-situ Cumulative =
 ================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2019

 Goal: Determine the new in-situ and ex-situ fractions for a galaxy for the following 
       distance cuts:
            - d = [0, 300 kpc]
            - d = [0, 15 kpc]
            - d = [0, 2 kpc]
            - d = [4, 15 kpc]
        applying a 1% contribution criteria.
       
       Save the data in pickle files.
"""

#### Import all the necessary tools for analysis
import rockstar_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc

#### Read in halos for all redshifts
galaxy = 'm12w'
resolution = '_res7100'
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

#### Some functions for analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Infinity], ind)
    return ind
"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

 unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
 sort: unsort, sorted
 i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
 mass_i: the mass of element i in sort
 index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
# Read in the data of stars at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, simulation_directory=simulation_dir_stam, assign_host_principal_axes=True)
# Get the snapshot indices from the halo info
snapind = [hal[i].snapshot['index'] for i in range(1, len(redshifts))]
# Get pointers at other redshifts
sis_at_z = [gizmo.track.ParticleIndexPointer.io_pointers(snapshot_index=i, directory=simulation_dir_stam+'/track') for i in snapind]
# Get all of the reverse pointers together in an array/list
sis_at_z_rev = [gizmo.track.ParticleIndexPointer.get_pointers_reverse(sis_at_z[i]) for i in range(0, len(sis_at_z))]

###### From here to end of insitu calculation would be in loop...

dist_list = [300, 15, 2]
insitu_cum = []

for d in dist_list:
    # Read in the contribution fractions for the halos at each redshift
    pickled = home_dir_stam+'/scripts/pickles/contribution_'+str(d)+'_fullres_'+galaxy+'.p'
    
    Filep = open(pickled, "rb")
    ratio = pickle.load(Filep)
    Filep.close()

    '''
     Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

        mass_ind : List
            mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

        Method:
            1. Set initial guess that it will be the most massive
            2. Check whether it contributes > 1%
            3. If it doesn't, move to the second most massive.
               Check, repeat...
    '''
    mass_ind = []
    for i in range(0, len(ratio)):
        m = 0
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio[i][temp]
        mass_ind.append(temp)
        while (temp2 <= 0.01):
            m += 1
            temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
            temp2 = ratio[i][temp]
            mass_ind[i] = temp

    ### In-situ Fraction
    '''
    NEW
    '''
    # Get indices of stars within d kpc of host at z = 0.
    i_0 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, a[0]*d])
    # First, find the stars that formed between these redshifts in the most massive progenitor
    i_0 = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[0], redshifts[1]], i_0)
    M_0_01_insitu = np.sum(part['star']['mass'][i_0])
    # Create a vector to attach all of the masses in the numerator for f_insitu
    M_in_num = np.zeros(len(sis_at_z))
    M_in_num[0] = M_0_01_insitu

    # Create the distance mask
    mask1 = (part['star'].prop('star.host.distance.total') < a[0]*d)
    for i in range(1, len(sis_at_z)):
        # Create the redshift formation masks, combine them, and re-order
        mask2 = (part['star'].prop('star.form.redshift') > redshifts[i])
        mask3 = (part['star'].prop('star.form.redshift') < redshifts[i+1])
        tot_mask_0 = mask1 & mask2 & mask3
        tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
        # Get subset of the total mask for only stars in MMP
        tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind[i-1]]]
        # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
        hal_star_good = hal[i]['star.indices'][his[i]][mass_ind[i-1]][tot_mask_z_sub]
        # Use these indices on the reverse pointer array
        rev_point_good = sis_at_z_rev[i-1][hal_star_good]
        M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])

    '''
     Find the denominator for f_insitu for all redshifts
     (Around 16.55 seconds)
    
        M_in_den : Array
            M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
            eventually merge into the host (or within 300 kpc of it) at z = 0

        Method:
            1. Get the indices of all stars at z = 0 within 300 kpc of the host
            2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
            3. Add up all of the mass in each redshift bin.
    '''
    # Initialize an empty vector
    M_in_den = np.zeros(len(sis_at_z))
    # Get the indices of stars in some distance cut
    i_0 = ut.array.get_indices(part['star'].prop('star.host.distance.total'), [0, a[0]*d])
    # Loop through all of the redshift bins and add up the mass
    for i in range(0, len(sis_at_z)):
        i_z = ut.array.get_indices(part['star'].prop('star.form.redshift'), [redshifts[i], redshifts[i+1]], i_0)
        M_in_den[i] = np.sum(part['star']['mass'][i_z])
    # Calculate the in-situ fraction
    insitu_cum.append(np.flip(np.cumsum(np.flip(M_in_num)))/np.flip(np.cumsum(np.flip(M_in_den))))

### d = [4, 15 kpc], f_contr > 0.01
dist = 415
# Read in the contribution fractions for the halos at each redshift
pickled = home_dir_stam+'/scripts/pickles/contribution_'+str(dist)+'_fullres_'+galaxy+'.p'
Filep = open(pickled, "rb")
ratio = pickle.load(Filep)
Filep.close()
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind = []
for i in range(0, len(ratio)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio[i][temp]
    mass_ind.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio[i][temp]
        mass_ind[i] = temp

### In-situ Fraction
'''
 Find the mass of stars that formed in the progenitor between z = 0 and z = 0.1
 z = 0 shouldn't be in the loop below, do it manually here
'''
# Get indices of stars
# Create masks for R
masks_d01 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part['star'].prop('star.host.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r = part['star'].prop('star.host.velocity.principal.cylindrical')[:,0]
v_z = part['star'].prop('star.host.velocity.principal.cylindrical')[:,1]
v_phi = part['star'].prop('star.host.velocity.principal.cylindrical')[:,2]
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Create redshift formation masks
masks_z = ((part['star'].prop('star.form.redshift') > 0) & (part['star'].prop('star.form.redshift') <= 0.1))
# Combine all of the masks
masks_d0 = masks_pos & masks_v & masks_z

# First, find the stars that formed between these redshifts in the most massive progenitor
M_0_01_insitu = np.sum(part['star']['mass'][masks_d0])
# Create a vector to attach all of the masses in the numerator for f_insitu
M_in_num = np.zeros(len(sis_at_z))
M_in_num[0] = M_0_01_insitu
'''
 Find the numerator for f_insitu for all other redshift bins
 (Around 13.64 seconds)

    M_in_num : Array
        M_in_num[i] corresponds to the mass of stars that formed in-situ (or within the most massive progenitor)
                    in some redshift bin that also merged in within 4-15 kpc of the host at z = 0

    Method:
        1. Create some masks that select stars that formed in a redshift bin that eventually
           merged into the host at z = 0
        2. Combine all of those masks
        3. Re-order the mask to have the same order as the star indices at the lower end of
           the redshift bin
        4. Out of the entire mask, only select stars that are members of the MMP (now a subset of total mask)
        5. Use the new (subset) mask back on the MMP to only select stars that follow the criteria in step 1
        6. Use the reverse pointers to get the indices of these stars at z = 0
        7. Add up the mass of these stars for each redshift bin
'''
# Create the distance mask
masks_pos_v = masks_pos & masks_v
for i in range(1, len(sis_at_z)):
    # Create the redshift formation masks, combine them, and re-order
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    tot_mask_0 = masks_pos_v & maskz
    tot_mask_z = tot_mask_0[sis_at_z_rev[i-1]]
    # Get subset of the total mask for only stars in MMP
    tot_mask_z_sub = tot_mask_z[hal[i]['star.indices'][his[i]][mass_ind[i-1]]]
    # Use mask back on list of halo stars to get the ones that satisfy the distance and z_form conditions
    hal_star_good = hal[i]['star.indices'][his[i]][mass_ind[i-1]][tot_mask_z_sub]
    # Use these indices on the reverse pointer array
    rev_point_good = sis_at_z_rev[i-1][hal_star_good]
    M_in_num[i] = np.sum(part['star']['mass'][rev_point_good])
'''
 Find the denominator for f_insitu for all redshifts
 (Around 16.55 seconds)

    M_in_den : Array
        M_in_den[i] corresponds to the mass of stars that formed in a redshift bin in all space, that
        eventually merge into the host (or within 4-15 kpc of it) at z = 0

    Method:
        1. Get the indices of all stars at z = 0 within 4-15 kpc of the host
        2. Loop through all redshifts and select the stars from 1, that formed in each redshift bin
        3. Add up all of the mass in each redshift bin.
'''
# Initialize an empty vector
M_in_den = np.zeros(len(sis_at_z))
# Get the indices of stars in some distance cut
#masks_pos_v
# Loop through all of the redshift bins and add up the mass
for i in range(0, len(sis_at_z)):
    maskz = ((part['star'].prop('star.form.redshift') > redshifts[i]) & (part['star'].prop('star.form.redshift') < redshifts[i+1]))
    mask_tot = masks_pos_v & maskz
    M_in_den[i] = np.sum(part['star']['mass'][mask_tot])
# Calculate the in-situ fraction
insitu_cum.append(np.flip(np.cumsum(np.flip(M_in_num)))/np.flip(np.cumsum(np.flip(M_in_den))))

# Save the data to a file
Filep = open(home_dir_stam+"/scripts/pickles/in_ex_1percent_"+galaxy+"_cum.p", "wb")
pickle.dump(insitu_cum, Filep)
Filep.close()