#!/usr/bin/python3

"""
 ====================
 = Excess Check =
 ====================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2019

 Goal: Compare fractions of stars in halos vs stars that are not in halos at some redshift
"""

### Import all the necessary tools for analysis
import rockstar_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc

#### Read in halos for all redshifts
gal1 = 'm12i'
if gal1 == 'Romeo':
    gal2 = 'Juliet'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res3500'
elif gal1 == 'Thelma':
    gal2 = 'Louise'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
elif gal1 == 'Romulus':
    gal2 = 'Remus'
    galaxy = 'm12_elvis_'+gal1+gal2
    resolution = '_res4000'
else:
    galaxy = gal1
    resolution = '_res7100'
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

#### Some functions for analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    return ind
"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

 unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
 sort: unsort, sorted
 i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
 mass_i: the mass of element i in sort
 index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
# Read in the data of stars at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', [0,1,2,3,4,5,6], simulation_directory=simulation_dir)

# Create an array of indices of stars that are inside of halos
stars_in_halos = []
for i in range(0, len(hal[30]['star.mass'][his[30]])):
    for j in range(0, len(hal[30]['star.indices'][his[30]][i])):
        stars_in_halos.append(hal[30]['star.indices'][his[30]][i][j])

# Get an array of indices of all stars that formed before z = 3
stars_at_z_3 = ut.array.get_indices(part[3]['star'].prop('form.redshift'),[3,np.inf])

# Get the indices of stars that are not in halos
stars_not_in_halos = np.setdiff1d(stars_at_z_3, stars_in_halos)

# Create a histogram of distances from the host of both stars that are, and are not in halos
d_stars_in_halos = part[3]['star'].prop('star.host1.distance.total')[stars_in_halos]
d_stars_not_in_halos = part[3]['star'].prop('star.host1.distance.total')[stars_not_in_halos]
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.hist(d_stars_in_halos, bins=np.linspace(0,1200,61), density=True, alpha=0.6, label='stars in halos')
plt.hist(d_stars_not_in_halos, bins=np.linspace(0,1200,61), density=True, alpha=0.6, label='stars not in halos')
plt.xlim(0,1200)
plt.xlabel('$\\rm d_{\\rm MMP}(kpc)$',fontsize=26)
plt.ylabel('N',fontsize=26)
plt.tick_params(axis='x', which='major', labelsize=20)
plt.tick_params(axis='y', which='major', labelsize=20)
plt.title(galaxy+', z = 3', fontsize=32)
plt.legend(prop={'size': 18})
plt.tight_layout()
plt.savefig(home_dir+'/scripts/plots/excess_check'+gal1+'.pdf')
plt.close()