#!/usr/bin/python3

"""
 ======================================
 = Individual mass Ratio summary data =
 ======================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Break 2019

 Goal: Read in the mass ratios for each galaxy and get when the ratios are
       last above 0.5, 0.33, 0.25 and save the data to a file
 
 NOTE: Used files made from "mass_ratio_v2.py", "mass_ratio_v3.py", and "mass_ratio_lg.py"

"""

#### Import all of the tools for analysis
import numpy as np
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

cuts = [0.5, 0.33, 0.25]

## Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/mass_ratios_all.p', 'rb')
massb = pickle.load(File1)
massc = pickle.load(File1)
massf = pickle.load(File1)
massi = pickle.load(File1)
massm = pickle.load(File1)
massw = pickle.load(File1)
massrom = pickle.load(File1)
massjul = pickle.load(File1)
massthe = pickle.load(File1)
masslou = pickle.load(File1)
massromu = pickle.load(File1)
massrem = pickle.load(File1)
File1.close()

# Calculate the median mass ratio for each distance cut
# d = [0, 300 kpc]
mr300 = [massb[0], massc[0], massf[0], massi[0], massm[0], massw[0], massrom[0], massjul[0], massthe[0], masslou[0], massromu[0], massrem[0]]
# d = [0, 15 kpc]
mr15 = [massb[1], massc[1], massf[1], massi[1], massm[1], massw[1], massrom[1], massjul[1], massthe[1], masslou[1], massromu[1], massrem[1]]
# d = [0, 2 kpc]
mr2 = [massb[2], massc[2], massf[2], massi[2], massm[2], massw[2], massrom[2], massjul[2], massthe[2], masslou[2], massromu[2], massrem[2]]
# d = [4, 15 kpc]
mr415 = [massb[3], massc[3], massf[3], massi[3], massm[3], massw[3], massrom[3], massjul[3], massthe[3], masslou[3], massromu[3], massrem[3]]

## Get the last time that the fractions were above each of the ratio cuts
red_temp = np.flip(redshifts)
last_red_300 = [[red_temp[np.max(np.where(np.flip(mr300[i][0]) > j)[0])] if len(np.where(np.flip(mr300[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr300))] for j in cuts]
last_red_15 = [[red_temp[np.max(np.where(np.flip(mr15[i][0]) > j)[0])] if len(np.where(np.flip(mr15[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr15))] for j in cuts]
last_red_2 = [[red_temp[np.max(np.where(np.flip(mr2[i][0]) > j)[0])] if len(np.where(np.flip(mr2[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr2))] for j in cuts]
last_red_415 = [[red_temp[np.max(np.where(np.flip(mr415[i][0]) > j)[0])] if len(np.where(np.flip(mr415[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr415))] for j in cuts]

## Do the same thing, but with the smoothed data
# d = [0, 300 kpc]
window_size, poly_order = 41, 3
mr300smooth = [savgol_filter(mr300[i], window_size, poly_order) for i in range(0, len(mr300))]
mr15smooth = [savgol_filter(mr15[i], window_size, poly_order) for i in range(0, len(mr15))]
mr2smooth = [savgol_filter(mr2[i], window_size, poly_order) for i in range(0, len(mr2))]
mr415smooth = [savgol_filter(mr415[i], window_size, poly_order) for i in range(0, len(mr415))]

last_red_300_smooth = [[red_temp[np.max(np.where(np.flip(mr300smooth[i][0]) > j)[0])] if len(np.where(np.flip(mr300smooth[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr300smooth))] for j in cuts]
last_red_15_smooth = [[red_temp[np.max(np.where(np.flip(mr15smooth[i][0]) > j)[0])] if len(np.where(np.flip(mr15smooth[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr15smooth))] for j in cuts]
last_red_2_smooth = [[red_temp[np.max(np.where(np.flip(mr2smooth[i][0]) > j)[0])] if len(np.where(np.flip(mr2smooth[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr2smooth))] for j in cuts]
last_red_415_smooth = [[red_temp[np.max(np.where(np.flip(mr415smooth[i][0]) > j)[0])] if len(np.where(np.flip(mr415smooth[i][0]) > j)[0]) !=0 else 9. for i in range(0, len(mr415smooth))] for j in cuts]

# Save the redshifts to a file
Filep = open(home_dir_stam+"/scripts/pickles/last_mr_redshifts_ind_v2.p", "wb")
pickle.dump(last_red_300, Filep)
pickle.dump(last_red_15, Filep)
pickle.dump(last_red_2, Filep)
pickle.dump(last_red_415, Filep)
pickle.dump(last_red_300_smooth, Filep)
pickle.dump(last_red_15_smooth, Filep)
pickle.dump(last_red_2_smooth, Filep)
pickle.dump(last_red_415_smooth, Filep)
Filep.close()