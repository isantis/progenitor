#!/usr/bin/python3

"""
 ================================
 = In-situ / Ex-situ plotter LG =
 ================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: Plot the insitu and exsitu fractions for LG pair

       Saved in a file where you need to load it as:

       ie_300 = pickle.load(File)

       and then these are indexed like:

       ie_300[0] = insitu
       ie_300[1] = exsitu
       ie_300[2] = exsitu_contr
       ie_300[3] = total1
       ie_300[4] = total2
 
 NOTE: This uses the files made fom "in_ex_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
gal1 = 'Romulus'
gal2 = 'Remus'
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

# Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/insitu_all.p', 'rb')
ieb = pickle.load(File1)
iec = pickle.load(File1)
ief = pickle.load(File1)
iei = pickle.load(File1)
iem = pickle.load(File1)
iew = pickle.load(File1)
ierom = pickle.load(File1)
iejul = pickle.load(File1)
iethe = pickle.load(File1)
ielou = pickle.load(File1)
ieromu = pickle.load(File1)
ierem = pickle.load(File1)
File1.close()

########### REMEMBER TO CHANGE THESE!
ie_1 = ieromu
ie_2 = ierem
iecut = 0.5

# Plot the ratios
colors = dc.get_distinct(2)
# d = [0, 300 kpc]
# GALAXY 1
redss = [-1.95,-1.85,-1.75,-1.65,-1.55,-1.45,-1.35,-1.25,-1.15,-1.05,-0.95,-0.85,-0.75,-0.65,-0.55,-0.45,-0.35,-0.25,-0.15,-0.05,0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05,2.15,2.25,2.35,2.45,2.55,2.65,2.75,2.85,2.95,3.05,3.15,3.25,3.35,3.45,3.55,3.65,3.75,3.85,3.95,4.1,4.3,4.5,4.7,4.9,5.1,5.3,5.5,5.7,5.9,6.5,7.5,8.5]
insitu = ie_1[0]

window_size, poly_order = 41, 3
ie300_1_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie300_1_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal1+', d = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_300_smooth_'+gal1+'_v5.pdf')
plt.close()

# GALAXY 2
insitu = ie_2[0]

window_size, poly_order = 41, 3
ie300_2_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(2)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie300_2_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal2+', d = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_300_smooth_'+gal2+'_v5.pdf')
plt.close()

# d = [0, 15 kpc]
# GALAXY 1
insitu = ie_1[1]

window_size, poly_order = 41, 3
ie15_1_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(3)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie15_1_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal1+', d = [0, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_15_smooth_'+gal1+'_v5.pdf')
plt.close()

# GALAXY 2
insitu = ie_2[1]

window_size, poly_order = 41, 3
ie15_2_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie15_2_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal2+', d = [0, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_15_smooth_'+gal2+'_v5.pdf')
plt.close()

# d = [0, 2 kpc]
# GALAXY 1
insitu = ie_1[2]

window_size, poly_order = 41, 3
ie2_1_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(5)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie2_1_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal1+', d = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_2_smooth_'+gal1+'_v5.pdf')
plt.close()

# GALAXY 2
insitu = ie_2[2]

window_size, poly_order = 41, 3
ie2_2_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(6)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie2_2_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal2+', d = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_2_smooth_'+gal2+'_v5.pdf')
plt.close()

# d = [4, 15 kpc]
# GALAXY 1
insitu = ie_1[3]

window_size, poly_order = 41, 3
ie415_1_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(7)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie415_1_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal1+', d = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_415_smooth_'+gal1+'_v5.pdf')
plt.close()

# GALAXY 2
insitu = ie_2[3]

window_size, poly_order = 41, 3
ie415_2_smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(8)
plt.figure(figsize=(10, 8))
plt.plot(redss, insitu, color=colors[0], label='raw')
plt.plot(redss, ie415_2_smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(gal2+', d = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_415_smooth_'+gal2+'_v5.pdf')
plt.close()