#!/usr/bin/python3

"""
 =================
 = Mass Ratio LG =
 =================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: Determine the M2/M1, M3/M1, and M4/M1 ratios as a function of redshift for the following distance cuts:
        - d = [0, 300 kpc]
        - d = [0, 15 kpc]
        - d = [0, 2 kpc]
        - d = [4, 15 kpc]
        
 for both hosts and save the data to a pickle file.
"""

#### Import all of the tools for analysis
import halo_analysis as rockstar
import utilities as ut
import numpy as np
import pickle

# Read in halos for all redshifts
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
dist = 415
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

# Some tools for analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]

"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind

"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

    unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
    sort: unsort, sorted
    i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
    mass_i: the mass of element i in sort
    index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]

# Read in the contribution fractions for the halos at each redshift
distn = str(dist)
# GALAXY 1
pickle_1 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal1+'.p'
# d = [0, d kpc]
Filep1 = open(pickle_1, "rb")
ratio_1 = pickle.load(Filep1)
Filep1.close()
# GALAXY 2
pickle_2 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal2+'.p'
# d = [0, d kpc]
Filep2 = open(pickle_2, "rb")
ratio_2 = pickle.load(Filep2)
Filep2.close()

"""
### GALAXY 1
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0 and
 get their masses along with the second, third, and fourth most massive that contribute > 1%
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_1: Array
        MMHi_1[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_1[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the first, second, and third most massive halos for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
        5. Make sure the second most massive halo contributes more than 1% and also isn't equal to the first most massive halo
            - If there isn't one that satisfies this criteria, set the mass equal to zero
        6. Make sure the third most massive halo contributes more than 1% and also isn't equal to the first or second most massive halo
            - If there isn't one that satisfies these criteria, set the mass equal to zero
        7. Same with fourth most massive
'''
mass_ind_1 = np.zeros(len(redshifts))
mass_ind_2 = np.zeros(len(redshifts))
mass_ind_3 = np.zeros(len(redshifts))
mass_ind_4 = np.zeros(len(redshifts))
MMH1_h1 = np.zeros(len(redshifts))
MMH2_h1 = np.zeros(len(redshifts))
MMH3_h1 = np.zeros(len(redshifts))
MMH4_h1 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
check2 = np.zeros(len(redshifts))
check3 = np.zeros(len(redshifts))
check4 = np.zeros(len(redshifts))
for i in range(0, len(ratio_1)):
    # Get second, third, and fourth most massive right away
    m1 = 0
    m2 = 1
    m3 = 2
    m4 = 3
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
    t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
    t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
    # Check their ratios
    tt1 = ratio_1[i][t1]
    tt2 = ratio_1[i][t2]
    tt3 = ratio_1[i][t3]
    tt4 = ratio_1[i][t4]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    mass_ind_2[i+1] = t2
    mass_ind_3[i+1] = t3
    mass_ind_4[i+1] = t4
    # Set the masses to the initial values
    MMH1_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    MMH2_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
    MMH3_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
    MMH4_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    check2[i+1] = tt2
    check3[i+1] = tt3
    check4[i+1] = tt4
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_1[i])):
            MMH1_h1[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_1[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1
    # Make sure MMH2 has a contribution > 1% and isn't equal to MMH
    while (tt2 <= 0.01) or (np.asarray(MMH2_h1[i+1]) == np.asarray(MMH1_h1[i+1])):
        m2 += 1
        if (m2 == len(ratio_1[i])):
            MMH2_h1[i+1] = 0
            break
        t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
        tt2 = ratio_1[i][t2]
        mass_ind_2[i+1] = t2
        MMH2_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
        check2[i+1] = tt2
    # Make sure MMH3 contributes > 1% and isn't MMH1 or MMH2
    while (tt3 <= 0.01) or (np.asarray(MMH3_h1[i+1]) == np.asarray(MMH1_h1[i+1])) or (np.asarray(MMH3_h1[i+1]) == np.asarray(MMH2_h1[i+1])):
        m3 += 1
        if (m3 == len(ratio_1[i])):
            MMH3_h1[i+1] = 0
            break
        t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
        tt3 = ratio_1[i][t3]
        mass_ind_3[i+1] = t3
        MMH3_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
        check3[i+1] = tt3
    # Make sure MMH4 contributes > 1% and isn't MMH1 or MMH2 or MMH3
    while (tt4 <= 0.01) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH1_h1[i+1])) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH2_h1[i+1])) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH3_h1[i+1])):
        m4 += 1
        if (m4 == len(ratio_1[i])):
            MMH4_h1[i+1] = 0
            break
        t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
        tt4 = ratio_1[i][t4]
        mass_ind_4[i+1] = t4
        MMH4_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
        check4[i+1] = tt4

'''
 Get the elements of MMHi_h1 at z = 0

    m0 : float
        m0 corresponds to the most massive halo at z = 0
        
    m1 : float
        m1 corresponds to the second most massive halo at z = 0
        
    m2 : float
        m2 corresponds to the third most massive halo at z = 0
        
    m3 : float
        m3 corresponds to the fourth most massive halo at z = 0
        
    Method:
        1. Set up an initial value for the most massive halo
        2. Make sure the most massive halo is within 2 kpc of the host at z = 0
           This is actually really silly because the most massive halo at z = 0 is the host, but it's good to be thorough
        3. Set up an initial value for the second most massive halo
        4. Check to see if the second most massive halo is within 2 kpc of the host at z = 0 and that it isn't the first most massive
           If it's not within 2 kpc or if it's equal to the most massive one, move onto the next one
        5. Set up an initial value for the third most massive halo
        6. Check to see if the third most massive halo is within 2 kpc of the host at z = 0 and that it isn't the first or second most massive
           If it's not within 2 kpc or if it's equal to the most massive or second most massive, move onto the next one
        7. Same with the fourth most massive halo
'''
m0 = hal[0]['star.mass'][hal[0].prop('host.index')[0]]

# Get the second most massive halo within d kpc of the host
i1 = 1
temp1 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i1)
m1 = hal[0]['star.mass'][his[0]][temp1]
while (hal[0].prop('host.distance.total')[his[0]][temp1] > dist) or (m1 == m0):
    i1 += 1
    if (i1 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host.distance.total')[his[0]][temp1] > dist):
        m1 = 0.
        break
    temp1 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i1)
    m1 = hal[0]['star.mass'][his[0]][temp1]

# Get the third most massive halo within 2 kpc of the host
i2 = 2
temp2 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i2)
m2 = hal[0]['star.mass'][his[0]][temp2]
while (hal[0].prop('host.distance.total')[his[0]][temp2] > dist) or (m2 == m0) or (m2 == m1):
    i2 += 1
    if (i2 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host.distance.total')[his[0]][temp2] > dist):
        m2 = 0.
        break
    temp2 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i2)
    m2 = hal[0]['star.mass'][his[0]][temp2]

# Get the fourth most massive halo within 2 kpc of the host
i3 = 3
temp3 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i3)
m3 = hal[0]['star.mass'][his[0]][temp3]
while (hal[0].prop('host.distance.total')[his[0]][temp3] > dist) or (m3 == m0) or (m3 == m1) or (m3 == m2):
    i3 += 1
    if (i3 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host.distance.total')[his[0]][temp3] > dist):
        m3 = 0.
        break
    temp3 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i3)
    m3 = hal[0]['star.mass'][his[0]][temp3]

# Append the elements above to the MMH* lists
MMH1_h1[0] = m0
MMH2_h1[0] = m1
MMH3_h1[0] = m2
MMH4_h1[0] = m3

# Get the ratios
two_one_ratio1 = MMH2_h1/MMH1_h1
three_one_ratio1 = MMH3_h1/MMH1_h1
four_one_ratio1 = MMH4_h1/MMH1_h1
# Group them together
# mass_ratios[0] will be the two one ratio, mass_ratios[0][0] will be the ratio at z = 0
mass_ratios_h1 = [two_one_ratio1, three_one_ratio1, four_one_ratio1]

### GALAXY 2
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0 and
 get their masses along with the second, third, and fourth most massive that contribute > 1%
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_h2: Array
        MMHi_h2[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_1[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the first, second, and third most massive halos for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
        5. Make sure the second most massive halo contributes more than 1% and also isn't equal to the first most massive halo
            - If there isn't one that satisfies this criteria, set the mass equal to zero
        6. Make sure the third most massive halo contributes more than 1% and also isn't equal to the first or second most massive halo
            - If there isn't one that satisfies these criteria, set the mass equal to zero
        7. Same with fourth most massive
'''
mass_ind_1 = np.zeros(len(redshifts))
mass_ind_2 = np.zeros(len(redshifts))
mass_ind_3 = np.zeros(len(redshifts))
mass_ind_4 = np.zeros(len(redshifts))
MMH1_h2 = np.zeros(len(redshifts))
MMH2_h2 = np.zeros(len(redshifts))
MMH3_h2 = np.zeros(len(redshifts))
MMH4_h2 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
check2 = np.zeros(len(redshifts))
check3 = np.zeros(len(redshifts))
check4 = np.zeros(len(redshifts))
for i in range(0, len(ratio_2)):
    # Get second, third, and fourth most massive right away
    m1 = 0
    m2 = 1
    m3 = 2
    m4 = 3
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
    t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
    t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
    # Check their ratios
    tt1 = ratio_2[i][t1]
    tt2 = ratio_2[i][t2]
    tt3 = ratio_2[i][t3]
    tt4 = ratio_2[i][t4]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    mass_ind_2[i+1] = t2
    mass_ind_3[i+1] = t3
    mass_ind_4[i+1] = t4
    # Set the masses to the initial values
    MMH1_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    MMH2_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
    MMH3_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
    MMH4_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    check2[i+1] = tt2
    check3[i+1] = tt3
    check4[i+1] = tt4
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_2[i])):
            MMH1_h2[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_2[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1
    # Make sure MMH2 has a contribution > 1% and isn't equal to MMH
    while (tt2 <= 0.01) or (np.asarray(MMH2_h2[i+1]) == np.asarray(MMH1_h2[i+1])):
        m2 += 1
        if (m2 == len(ratio_2[i])):
            MMH2_h2[i+1] = 0
            break
        t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
        tt2 = ratio_2[i][t2]
        mass_ind_2[i+1] = t2
        MMH2_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
        check2[i+1] = tt2
    # Make sure MMH3 contributes > 1% and isn't MMH1 or MMH2
    while (tt3 <= 0.01) or (np.asarray(MMH3_h2[i+1]) == np.asarray(MMH1_h2[i+1])) or (np.asarray(MMH3_h2[i+1]) == np.asarray(MMH2_h2[i+1])):
        m3 += 1
        if (m3 == len(ratio_2[i])):
            MMH3_h2[i+1] = 0
            break
        t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
        tt3 = ratio_2[i][t3]
        mass_ind_3[i+1] = t3
        MMH3_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
        check3[i+1] = tt3
    # Make sure MMH4 contributes > 1% and isn't MMH1 or MMH2 or MMH3
    while (tt4 <= 0.01) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH1_h2[i+1])) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH2_h2[i+1])) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH3_h2[i+1])):
        m4 += 1
        if (m4 == len(ratio_2[i])):
            MMH4_h2[i+1] = 0
            break
        t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
        tt4 = ratio_2[i][t4]
        mass_ind_4[i+1] = t4
        MMH4_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
        check4[i+1] = tt4

'''
 Get the elements of MMHi_h2 at z = 0

    m0 : float
        m0 corresponds to the most massive halo at z = 0
        
    m1 : float
        m1 corresponds to the second most massive halo at z = 0
        
    m2 : float
        m2 corresponds to the third most massive halo at z = 0
        
    m3 : float
        m3 corresponds to the fourth most massive halo at z = 0
        
    Method:
        1. Set up an initial value for the most massive halo
        2. Make sure the most massive halo is within 2 kpc of the host at z = 0
           This is actually really silly because the most massive halo at z = 0 is the host, but it's good to be thorough
        3. Set up an initial value for the second most massive halo
        4. Check to see if the second most massive halo is within 2 kpc of the host at z = 0 and that it isn't the first most massive
           If it's not within 2 kpc or if it's equal to the most massive one, move onto the next one
        5. Set up an initial value for the third most massive halo
        6. Check to see if the third most massive halo is within 2 kpc of the host at z = 0 and that it isn't the first or second most massive
           If it's not within 2 kpc or if it's equal to the most massive or second most massive, move onto the next one
        7. Same with the fourth most massive halo
'''
m0 = hal[0]['star.mass'][hal[0].prop('host2.index')[0]]

# Get the second most massive halo within d kpc of the host
i1 = 1
temp1 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i1)
m1 = hal[0]['star.mass'][his[0]][temp1]
while (hal[0].prop('host.distance.total')[his[0]][temp1] > dist) or (m1 == m0):
    i1 += 1
    if (i1 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host2.distance.total')[his[0]][temp1] > dist):
        m1 = 0.
        break
    temp1 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i1)
    m1 = hal[0]['star.mass'][his[0]][temp1]

# Get the third most massive halo within d kpc of the host
i2 = 2
temp2 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i2)
m2 = hal[0]['star.mass'][his[0]][temp2]
while (hal[0].prop('host.distance.total')[his[0]][temp2] > dist) or (m2 == m0) or (m2 == m1):
    i2 += 1
    if (i2 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host2.distance.total')[his[0]][temp2] > dist):
        m2 = 0.
        break
    temp2 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i2)
    m2 = hal[0]['star.mass'][his[0]][temp2]

# Get the fourth most massive halo within 2 kpc of the host
i3 = 3
temp3 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i3)
m3 = hal[0]['star.mass'][his[0]][temp3]
while (hal[0].prop('host.distance.total')[his[0]][temp3] > dist) or (m3 == m0) or (m3 == m1) or (m3 == m2):
    i3 += 1
    if (i3 == len(hal[0]['star.mass'][his[0]])) and (hal[0].prop('host2.distance.total')[his[0]][temp3] > dist):
        m3 = 0.
        break
    temp3 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=i3)
    m3 = hal[0]['star.mass'][his[0]][temp3]

# Append the elements above to the MMH* lists
MMH1_h2[0] = m0
MMH2_h2[0] = m1
MMH3_h2[0] = m2
MMH4_h2[0] = m3

# Get the ratios
two_one_ratio2 = MMH2_h2/MMH1_h2
three_one_ratio2 = MMH3_h2/MMH1_h2
four_one_ratio2 = MMH4_h2/MMH1_h2
# Group them together
# mass_ratios[0] will be the two one ratio, mass_ratios[0][0] will be the ratio at z = 0
mass_ratios_h2 = [two_one_ratio2, three_one_ratio2, four_one_ratio2]

## Save the ratios to files
Fileptest1 = open(home_dir_stam+"/scripts/pickles/mass_ratio_1percent_"+gal1+distn+".p", "wb")
pickle.dump(mass_ratios_h1, Fileptest1)
Fileptest1.close()
Fileptest2 = open(home_dir_stam+"/scripts/pickles/mass_ratio_1percent_"+gal2+distn+".p", "wb")
pickle.dump(mass_ratios_h2, Fileptest2)
Fileptest2.close()
"""

### GALAXY 1
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0 and
 get their masses along with the second, third, and fourth most massive that contribute > 1%
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_1: Array
        MMHi_1[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_1[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the first, second, and third most massive halos for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
        5. Make sure the second most massive halo contributes more than 1% and also isn't equal to the first most massive halo
            - If there isn't one that satisfies this criteria, set the mass equal to zero
        6. Make sure the third most massive halo contributes more than 1% and also isn't equal to the first or second most massive halo
            - If there isn't one that satisfies these criteria, set the mass equal to zero
        7. Same with fourth most massive
'''
mass_ind_1 = np.zeros(len(redshifts))
mass_ind_2 = np.zeros(len(redshifts))
mass_ind_3 = np.zeros(len(redshifts))
mass_ind_4 = np.zeros(len(redshifts))
MMH1_h1 = np.zeros(len(redshifts))
MMH2_h1 = np.zeros(len(redshifts))
MMH3_h1 = np.zeros(len(redshifts))
MMH4_h1 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
check2 = np.zeros(len(redshifts))
check3 = np.zeros(len(redshifts))
check4 = np.zeros(len(redshifts))
for i in range(0, len(ratio_1)):
    # Get second, third, and fourth most massive right away
    m1 = 0
    m2 = 1
    m3 = 2
    m4 = 3
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
    t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
    t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
    # Check their ratios
    tt1 = ratio_1[i][t1]
    tt2 = ratio_1[i][t2]
    tt3 = ratio_1[i][t3]
    tt4 = ratio_1[i][t4]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    mass_ind_2[i+1] = t2
    mass_ind_3[i+1] = t3
    mass_ind_4[i+1] = t4
    # Set the masses to the initial values
    MMH1_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    MMH2_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
    MMH3_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
    MMH4_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    check2[i+1] = tt2
    check3[i+1] = tt3
    check4[i+1] = tt4
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_1[i])):
            MMH1_h1[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_1[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1
    # Make sure MMH2 has a contribution > 1% and isn't equal to MMH
    while (tt2 <= 0.01) or (np.asarray(MMH2_h1[i+1]) == np.asarray(MMH1_h1[i+1])):
        m2 += 1
        if (m2 == len(ratio_1[i])):
            MMH2_h1[i+1] = 0
            break
        t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
        tt2 = ratio_1[i][t2]
        mass_ind_2[i+1] = t2
        MMH2_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
        check2[i+1] = tt2
    # Make sure MMH3 contributes > 1% and isn't MMH1 or MMH2
    while (tt3 <= 0.01) or (np.asarray(MMH3_h1[i+1]) == np.asarray(MMH1_h1[i+1])) or (np.asarray(MMH3_h1[i+1]) == np.asarray(MMH2_h1[i+1])):
        m3 += 1
        if (m3 == len(ratio_1[i])):
            MMH3_h1[i+1] = 0
            break
        t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
        tt3 = ratio_1[i][t3]
        mass_ind_3[i+1] = t3
        MMH3_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
        check3[i+1] = tt3
    # Make sure MMH4 contributes > 1% and isn't MMH1 or MMH2 or MMH3
    while (tt4 <= 0.01) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH1_h1[i+1])) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH2_h1[i+1])) or (np.asarray(MMH4_h1[i+1]) == np.asarray(MMH3_h1[i+1])):
        m4 += 1
        if (m4 == len(ratio_1[i])):
            MMH4_h1[i+1] = 0
            break
        t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
        tt4 = ratio_1[i][t4]
        mass_ind_4[i+1] = t4
        MMH4_h1[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
        check4[i+1] = tt4

'''
 Get the elements of MMHi_415 at z = 0

    m0 : float
        m0 corresponds to the most massive halo at z = 0
        
    m1 : float
        m1 corresponds to the second most massive halo at z = 0
        
    m2 : float
        m2 corresponds to the third most massive halo at z = 0
        
    m3 : float
        m3 corresponds to the fourth most massive halo at z = 0
        
    Method:
        1. Set up an initial value for the most massive halo
        2. Make sure the most massive halo is within 4-15 kpc of the host at z = 0
           This is actually really silly because the most massive halo at z = 0 is the host, but it's good to be thorough
        3. Set up an initial value for the second most massive halo
        4. Check to see if the second most massive halo is within 4-15 kpc of the host at z = 0 and that it isn't the first most massive
           If it's not within 4-15 kpc or if it's equal to the most massive one, move onto the next one
        5. Set up an initial value for the third most massive halo
        6. Check to see if the third most massive halo is within 4-15 kpc of the host at z = 0 and that it isn't the first or second most massive
           If it's not within 4-15 kpc or if it's equal to the most massive or second most massive, move onto the next one
        7. Same with the fourth most massive halo
'''
m0 = hal[0]['star.mass'][hal[0].prop('host.index')[0]]

# Nothing aside from the main host within 4-15 kpc
m1 = 0
m2 = 0
m3 = 0

# Append the elements above to the MMH* lists
MMH1_h1[0] = m0
MMH2_h1[0] = m1
MMH3_h1[0] = m2
MMH4_h1[0] = m3

# Get the ratios
two_one_ratio1 = MMH2_h1/MMH1_h1
three_one_ratio1 = MMH3_h1/MMH1_h1
four_one_ratio1 = MMH4_h1/MMH1_h1
# Group them together
# mass_ratios[0] will be the two one ratio, mass_ratios[0][0] will be the ratio at z = 0
mass_ratios_h1 = [two_one_ratio1, three_one_ratio1, four_one_ratio1]

### GALAXY 2
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0 and
 get their masses along with the second, third, and fourth most massive that contribute > 1%
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_h2: Array
        MMHi_h2[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_1[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the first, second, and third most massive halos for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
        5. Make sure the second most massive halo contributes more than 1% and also isn't equal to the first most massive halo
            - If there isn't one that satisfies this criteria, set the mass equal to zero
        6. Make sure the third most massive halo contributes more than 1% and also isn't equal to the first or second most massive halo
            - If there isn't one that satisfies these criteria, set the mass equal to zero
        7. Same with fourth most massive
'''
mass_ind_1 = np.zeros(len(redshifts))
mass_ind_2 = np.zeros(len(redshifts))
mass_ind_3 = np.zeros(len(redshifts))
mass_ind_4 = np.zeros(len(redshifts))
MMH1_h2 = np.zeros(len(redshifts))
MMH2_h2 = np.zeros(len(redshifts))
MMH3_h2 = np.zeros(len(redshifts))
MMH4_h2 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
check2 = np.zeros(len(redshifts))
check3 = np.zeros(len(redshifts))
check4 = np.zeros(len(redshifts))
for i in range(0, len(ratio_2)):
    # Get second, third, and fourth most massive right away
    m1 = 0
    m2 = 1
    m3 = 2
    m4 = 3
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
    t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
    t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
    # Check their ratios
    tt1 = ratio_2[i][t1]
    tt2 = ratio_2[i][t2]
    tt3 = ratio_2[i][t3]
    tt4 = ratio_2[i][t4]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    mass_ind_2[i+1] = t2
    mass_ind_3[i+1] = t3
    mass_ind_4[i+1] = t4
    # Set the masses to the initial values
    MMH1_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    MMH2_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
    MMH3_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
    MMH4_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    check2[i+1] = tt2
    check3[i+1] = tt3
    check4[i+1] = tt4
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_2[i])):
            MMH1_h2[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_2[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1
    # Make sure MMH2 has a contribution > 1% and isn't equal to MMH
    while (tt2 <= 0.01) or (np.asarray(MMH2_h2[i+1]) == np.asarray(MMH1_h2[i+1])):
        m2 += 1
        if (m2 == len(ratio_2[i])):
            MMH2_h2[i+1] = 0
            break
        t2 = mass_index(hal[i+1]['star.mass'][his[i+1]], m2)
        tt2 = ratio_2[i][t2]
        mass_ind_2[i+1] = t2
        MMH2_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_2[i+1])]
        check2[i+1] = tt2
    # Make sure MMH3 contributes > 1% and isn't MMH1 or MMH2
    while (tt3 <= 0.01) or (np.asarray(MMH3_h2[i+1]) == np.asarray(MMH1_h2[i+1])) or (np.asarray(MMH3_h2[i+1]) == np.asarray(MMH2_h2[i+1])):
        m3 += 1
        if (m3 == len(ratio_2[i])):
            MMH3_h2[i+1] = 0
            break
        t3 = mass_index(hal[i+1]['star.mass'][his[i+1]], m3)
        tt3 = ratio_2[i][t3]
        mass_ind_3[i+1] = t3
        MMH3_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_3[i+1])]
        check3[i+1] = tt3
    # Make sure MMH4 contributes > 1% and isn't MMH1 or MMH2 or MMH3
    while (tt4 <= 0.01) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH1_h2[i+1])) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH2_h2[i+1])) or (np.asarray(MMH4_h2[i+1]) == np.asarray(MMH3_h2[i+1])):
        m4 += 1
        if (m4 == len(ratio_2[i])):
            MMH4_h2[i+1] = 0
            break
        t4 = mass_index(hal[i+1]['star.mass'][his[i+1]], m4)
        tt4 = ratio_2[i][t4]
        mass_ind_4[i+1] = t4
        MMH4_h2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_4[i+1])]
        check4[i+1] = tt4

'''
 Get the elements of MMHi_415 at z = 0

    m0 : float
        m0 corresponds to the most massive halo at z = 0
        
    m1 : float
        m1 corresponds to the second most massive halo at z = 0
        
    m2 : float
        m2 corresponds to the third most massive halo at z = 0
        
    m3 : float
        m3 corresponds to the fourth most massive halo at z = 0
        
    Method:
        1. Set up an initial value for the most massive halo
        2. Make sure the most massive halo is within 4-15 kpc of the host at z = 0
           This is actually really silly because the most massive halo at z = 0 is the host, but it's good to be thorough
        3. Set up an initial value for the second most massive halo
        4. Check to see if the second most massive halo is within 4-15 kpc of the host at z = 0 and that it isn't the first most massive
           If it's not within 4-15 kpc or if it's equal to the most massive one, move onto the next one
        5. Set up an initial value for the third most massive halo
        6. Check to see if the third most massive halo is within 4-15 kpc of the host at z = 0 and that it isn't the first or second most massive
           If it's not within 4-15 kpc or if it's equal to the most massive or second most massive, move onto the next one
        7. Same with the fourth most massive halo
'''
m0 = hal[0]['star.mass'][hal[0].prop('host2.index')[0]]

# Nothing aside from the main host within 4-15 kpc
m1 = 0
m2 = 0
m3 = 0

# Append the elements above to the MMH* lists
MMH1_h2[0] = m0
MMH2_h2[0] = m1
MMH3_h2[0] = m2
MMH4_h2[0] = m3

# Get the ratios
two_one_ratio2 = MMH2_h2/MMH1_h2
three_one_ratio2 = MMH3_h2/MMH1_h2
four_one_ratio2 = MMH4_h2/MMH1_h2
# Group them together
# mass_ratios[0] will be the two one ratio, mass_ratios[0][0] will be the ratio at z = 0
mass_ratios_h2 = [two_one_ratio2, three_one_ratio2, four_one_ratio2]

## Save the ratios to files
Fileptest1 = open(home_dir_stam+"/scripts/pickles/mass_ratio_1percent_"+gal1+distn+".p", "wb")
pickle.dump(mass_ratios_h1, Fileptest1)
Fileptest1.close()
Fileptest2 = open(home_dir_stam+"/scripts/pickles/mass_ratio_1percent_"+gal2+distn+".p", "wb")
pickle.dump(mass_ratios_h2, Fileptest2)
Fileptest2.close()
