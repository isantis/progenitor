#!/usr/bin/python3

"""
 =======================
 = Mass Ratio rewriter =
 =======================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2019

 Goal: Read in the mass ratios for all galaxies and add values to the curves. Want to 
       pad the values near z = 0 so that the smoothing function doesn't act weird.

 NOTE: Used files made from "mass_ratio_v2.py", "mass_ratio_v3.py", and "mass_ratio_lg.py"

"""

#### Import all of the tools for analysis
import numpy as np
import pickle

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12b.p', 'rb')
mass300b = pickle.load(File1)
mass15b = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12c.p', 'rb')
mass300c = pickle.load(File2)
mass15c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12f.p', 'rb')
mass300f = pickle.load(File3)
mass15f = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12i.p', 'rb')
mass300i = pickle.load(File4)
mass15i = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12m.p', 'rb')
mass300m = pickle.load(File5)
mass15m = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12w.p', 'rb')
mass300w = pickle.load(File6)
mass15w = pickle.load(File6)
File6.close()

# m12b
File7 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12b_v2.p', 'rb')
mass2b = pickle.load(File7)
mass415b = pickle.load(File7)
File7.close()
# m12c
File8 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12c_v2.p', 'rb')
mass2c = pickle.load(File8)
mass415c = pickle.load(File8)
File8.close()
# m12f
File9 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12f_v2.p', 'rb')
mass2f = pickle.load(File9)
mass415f = pickle.load(File9)
File9.close()
# m12i
File10 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12i_v2.p', 'rb')
mass2i = pickle.load(File10)
mass415i = pickle.load(File10)
File10.close()
# m12m
File11 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12m_v2.p', 'rb')
mass2m = pickle.load(File11)
mass415m = pickle.load(File11)
File11.close()
# m12w
File12 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_m12w_v2.p', 'rb')
mass2w = pickle.load(File12)
mass415w = pickle.load(File12)
File12.close()

# Romeo
File13 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romeo300.p', 'rb')
mass300rom = pickle.load(File13)
File13.close()
File14 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romeo15.p', 'rb')
mass15rom = pickle.load(File14)
File14.close()
File15 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romeo2.p', 'rb')
mass2rom = pickle.load(File15)
File15.close()
File16 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romeo415.p', 'rb')
mass415rom = pickle.load(File16)
File16.close()

# Juliet
File17 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Juliet300.p', 'rb')
mass300jul = pickle.load(File17)
File17.close()
File18 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Juliet15.p', 'rb')
mass15jul = pickle.load(File18)
File18.close()
File19 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Juliet2.p', 'rb')
mass2jul = pickle.load(File19)
File19.close()
File20 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Juliet415.p', 'rb')
mass415jul = pickle.load(File20)
File20.close()

# Thelma
File21 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Thelma300.p', 'rb')
mass300the = pickle.load(File21)
File21.close()
File22 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Thelma15.p', 'rb')
mass15the = pickle.load(File22)
File22.close()
File23 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Thelma2.p', 'rb')
mass2the = pickle.load(File23)
File23.close()
File24 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Thelma415.p', 'rb')
mass415the = pickle.load(File24)
File24.close()

# Louise
File25 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Louise300.p', 'rb')
mass300lou = pickle.load(File25)
File25.close()
File26 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Louise15.p', 'rb')
mass15lou = pickle.load(File26)
File26.close()
File27 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Louise2.p', 'rb')
mass2lou = pickle.load(File27)
File27.close()
File28 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Louise415.p', 'rb')
mass415lou = pickle.load(File28)
File28.close()

# Romulus
File29 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romulus300.p', 'rb')
mass300romu = pickle.load(File29)
File29.close()
File30 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romulus15.p', 'rb')
mass15romu = pickle.load(File30)
File30.close()
File31 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romulus2.p', 'rb')
mass2romu = pickle.load(File31)
File31.close()
File32 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Romulus415.p', 'rb')
mass415romu = pickle.load(File32)
File32.close()

# Remus
File33 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Remus300.p', 'rb')
mass300rem = pickle.load(File33)
File33.close()
File34 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Remus15.p', 'rb')
mass15rem = pickle.load(File34)
File34.close()
File35 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Remus2.p', 'rb')
mass2rem = pickle.load(File35)
File35.close()
File36 = open(home_dir_stam+'/scripts/pickles/mass_ratio_1percent_Remus415.p', 'rb')
mass415rem = pickle.load(File36)
File36.close()

### Collect all the data for each simulation and pad the values near zero
# m12b
massb = [mass300b, mass15b, mass2b, mass415b]
massb_new = []
for i in range(0, len(massb)):
    massb_temp = []
    for j in range(0, len(massb[i])):
        massb_temp.append(np.append(np.ones(20)*massb[i][j][0], massb[i][j]))
    massb_new.append(massb_temp)

# m12c
massc = [mass300c, mass15c, mass2c, mass415c]
massc_new = []
for i in range(0, len(massc)):
    massc_temp = []
    for j in range(0, len(massc[i])):
        massc_temp.append(np.append(np.ones(20)*massc[i][j][0], massc[i][j]))
    massc_new.append(massc_temp)

# m12f
massf = [mass300f, mass15f, mass2f, mass415f]
massf_new = []
for i in range(0, len(massf)):
    massf_temp = []
    for j in range(0, len(massf[i])):
        massf_temp.append(np.append(np.ones(20)*massf[i][j][0], massf[i][j]))
    massf_new.append(massf_temp)

# m12i
massi = [mass300i, mass15i, mass2i, mass415i]
massi_new = []
for i in range(0, len(massi)):
    massi_temp = []
    for j in range(0, len(massi[i])):
        massi_temp.append(np.append(np.ones(20)*massi[i][j][0], massi[i][j]))
    massi_new.append(massi_temp)

# m12m
massm = [mass300m, mass15m, mass2m, mass415m]
massm_new = []
for i in range(0, len(massm)):
    massm_temp = []
    for j in range(0, len(massm[i])):
        massm_temp.append(np.append(np.ones(20)*massm[i][j][0], massm[i][j]))
    massm_new.append(massm_temp)

# m12w
massw = [mass300w, mass15w, mass2w, mass415w]
massw_new = []
for i in range(0, len(massw)):
    massw_temp = []
    for j in range(0, len(massw[i])):
        massw_temp.append(np.append(np.ones(20)*massw[i][j][0], massw[i][j]))
    massw_new.append(massw_temp)

# m12rom
massrom = [mass300rom, mass15rom, mass2rom, mass415rom]
massrom_new = []
for i in range(0, len(massrom)):
    massrom_temp = []
    for j in range(0, len(massrom[i])):
        massrom_temp.append(np.append(np.ones(20)*massrom[i][j][0], massrom[i][j]))
    massrom_new.append(massrom_temp)

# m12jul
massjul = [mass300jul, mass15jul, mass2jul, mass415jul]
massjul_new = []
for i in range(0, len(massjul)):
    massjul_temp = []
    for j in range(0, len(massjul[i])):
        massjul_temp.append(np.append(np.ones(20)*massjul[i][j][0], massjul[i][j]))
    massjul_new.append(massjul_temp)

# m12the
massthe = [mass300the, mass15the, mass2the, mass415the]
massthe_new = []
for i in range(0, len(massthe)):
    massthe_temp = []
    for j in range(0, len(massthe[i])):
        massthe_temp.append(np.append(np.ones(20)*massthe[i][j][0], massthe[i][j]))
    massthe_new.append(massthe_temp)

# m12lou
masslou = [mass300lou, mass15lou, mass2lou, mass415lou]
masslou_new = []
for i in range(0, len(masslou)):
    masslou_temp = []
    for j in range(0, len(masslou[i])):
        masslou_temp.append(np.append(np.ones(20)*masslou[i][j][0], masslou[i][j]))
    masslou_new.append(masslou_temp)

# m12romu
massromu = [mass300romu, mass15romu, mass2romu, mass415romu]
massromu_new = []
for i in range(0, len(massromu)):
    massromu_temp = []
    for j in range(0, len(massromu[i])):
        massromu_temp.append(np.append(np.ones(20)*massromu[i][j][0], massromu[i][j]))
    massromu_new.append(massromu_temp)

# m12rem
massrem = [mass300rem, mass15rem, mass2rem, mass415rem]
massrem_new = []
for i in range(0, len(massrem)):
    massrem_temp = []
    for j in range(0, len(massrem[i])):
        massrem_temp.append(np.append(np.ones(20)*massrem[i][j][0], massrem[i][j]))
    massrem_new.append(massrem_temp)

# Print all the values to a file.
Fileptest = open(home_dir_stam+"/scripts/pickles/mass_ratios_all.p", "wb")
pickle.dump(massb_new, Fileptest)
pickle.dump(massc_new, Fileptest)
pickle.dump(massf_new, Fileptest)
pickle.dump(massi_new, Fileptest)
pickle.dump(massm_new, Fileptest)
pickle.dump(massw_new, Fileptest)
pickle.dump(massrom_new, Fileptest)
pickle.dump(massjul_new, Fileptest)
pickle.dump(massthe_new, Fileptest)
pickle.dump(masslou_new, Fileptest)
pickle.dump(massromu_new, Fileptest)
pickle.dump(massrem_new, Fileptest)
Fileptest.close()