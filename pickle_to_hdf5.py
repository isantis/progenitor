#!/usr/bin/python3

"""
    Read in pickle files, save the data as a dictionary, then save dictionary to
    an hdf5 file
"""


"""
=======
= MVZ =
=======
"""

# Read in the packages
import utilities as ut
import pickle

# Read in all of the pickle files
f1 = open('Mvz_1percent_m12b.p', 'rb')
star_m12b = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12c.p', 'rb')
star_m12c = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12f.p', 'rb')
star_m12f = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12i.p', 'rb')
star_m12i = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12m.p', 'rb')
star_m12m = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12w.p', 'rb')
star_m12w = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Romeo300.p', 'rb')
star_rom = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Juliet300.p', 'rb')
star_jul = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Thelma300.p', 'rb')
star_the = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Louise300.p', 'rb')
star_lou = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Romulus300.p', 'rb')
star_romu = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Remus300.p', 'rb')
star_rem = pickle.load(f1)
f1.close()

# Put the data in dictionaries
d1 = dict()
d1['m12b.stellar.mass'] = star_m12b
d1['m12c.stellar.mass'] = star_m12c
d1['m12f.stellar.mass'] = star_m12f
d1['m12i.stellar.mass'] = star_m12i
d1['m12m.stellar.mass'] = star_m12m
d1['m12w.stellar.mass'] = star_m12w
d1['Romeo.stellar.mass'] = star_rom
d1['Juliet.stellar.mass'] = star_jul
d1['Thelma.stellar.mass'] = star_the
d1['Louise.stellar.mass'] = star_lou
d1['Romulus.stellar.mass'] = star_romu
d1['Remus.stellar.mass'] = star_rem

# Save the data into an hdf5 file
ut.io.file_hdf5(file_name_base='stellar_mass_vs_redshift', dict_or_array_to_write=d1, verbose=True)


# Read in all of the pickle files
f1 = open('Mvz_1percent_m12b_DMO.p', 'rb')
halo_m12b = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12c_DMO.p', 'rb')
halo_m12c = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12f_DMO.p', 'rb')
halo_m12f = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12i_DMO.p', 'rb')
halo_m12i = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12m_DMO.p', 'rb')
halo_m12m = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_m12w_DMO.p', 'rb')
halo_m12w = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Romeo300_DMO.p', 'rb')
halo_rom = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Juliet300_DMO.p', 'rb')
halo_jul = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Thelma300_DMO.p', 'rb')
halo_the = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Louise300_DMO.p', 'rb')
halo_lou = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Romulus300_DMO.p', 'rb')
halo_romu = pickle.load(f1)
f1.close()
f1 = open('Mvz_1percent_Remus300_DMO.p', 'rb')
halo_rem = pickle.load(f1)
f1.close()

# Put the data in dictionaries
d1 = dict()
d1['m12b.halo.mass'] = halo_m12b
d1['m12c.halo.mass'] = halo_m12c
d1['m12f.halo.mass'] = halo_m12f
d1['m12i.halo.mass'] = halo_m12i
d1['m12m.halo.mass'] = halo_m12m
d1['m12w.halo.mass'] = halo_m12w
d1['Romeo.halo.mass'] = halo_rom
d1['Juliet.halo.mass'] = halo_jul
d1['Thelma.halo.mass'] = halo_the
d1['Louise.halo.mass'] = halo_lou
d1['Romulus.halo.mass'] = halo_romu
d1['Remus.halo.mass'] = halo_rem

# Save the data into an hdf5 file
ut.io.file_hdf5(file_name_base='halo_mass_vs_redshift', dict_or_array_to_write=d1, verbose=True)


#########################################################################

"""
==========
= Insitu =
==========
"""

# Read in the packages
import utilities as ut
import pickle

# Read in pickle file
f1 = open('insitu_cumulative_all.p', 'rb')
insitu_b = pickle.load(f1)
insitu_c = pickle.load(f1)
insitu_f = pickle.load(f1)
insitu_i = pickle.load(f1)
insitu_m = pickle.load(f1)
insitu_w = pickle.load(f1)
insitu_rom = pickle.load(f1)
insitu_jul = pickle.load(f1)
insitu_the = pickle.load(f1)
insitu_lou = pickle.load(f1)
insitu_romu = pickle.load(f1)
insitu_rem = pickle.load(f1)
f1.close()

d1_15 = dict()
d1_15['m12b'] = insitu_b[1][20:]
d1_15['m12c'] = insitu_c[1][20:]
d1_15['m12f'] = insitu_f[1][20:]
d1_15['m12i'] = insitu_i[1][20:]
d1_15['m12m'] = insitu_m[1][20:]
d1_15['m12w'] = insitu_w[1][20:]
d1_15['Romeo'] = insitu_rom[1][20:]
d1_15['Juliet'] = insitu_jul[1][20:]
d1_15['Thelma'] = insitu_the[1][20:]
d1_15['Louise'] = insitu_lou[1][20:]
d1_15['Romulus'] = insitu_romu[1][20:]
d1_15['Remus'] = insitu_rem[1][20:]

ut.io.file_hdf5(file_name_base='cumulative_insitu_fractions_vs_redshift_15_kpc', dict_or_array_to_write=d1_15, verbose=True)

d1_2 = dict()
d1_2['m12b'] = insitu_b[2][20:]
d1_2['m12c'] = insitu_c[2][20:]
d1_2['m12f'] = insitu_f[2][20:]
d1_2['m12i'] = insitu_i[2][20:]
d1_2['m12m'] = insitu_m[2][20:]
d1_2['m12w'] = insitu_w[2][20:]
d1_2['Romeo'] = insitu_rom[2][20:]
d1_2['Juliet'] = insitu_jul[2][20:]
d1_2['Thelma'] = insitu_the[2][20:]
d1_2['Louise'] = insitu_lou[2][20:]
d1_2['Romulus'] = insitu_romu[2][20:]
d1_2['Remus'] = insitu_rem[2][20:]

ut.io.file_hdf5(file_name_base='cumulative_insitu_fractions_vs_redshift_2_kpc', dict_or_array_to_write=d1_2, verbose=True)


#########################################################################
"""
=======
= Nvz =
=======
"""

# Read in the packages
import utilities as ut
import pickle

# Read in pickle file
f1 = open('nvz_all.p', 'rb')
nvz_b = pickle.load(f1)
nvz_c = pickle.load(f1)
nvz_f = pickle.load(f1)
nvz_i = pickle.load(f1)
nvz_m = pickle.load(f1)
nvz_w = pickle.load(f1)
nvz_rom = pickle.load(f1)
nvz_jul = pickle.load(f1)
nvz_the = pickle.load(f1)
nvz_lou = pickle.load(f1)
nvz_romu = pickle.load(f1)
nvz_rem = pickle.load(f1)
f1.close()

# 300 kpc
N5 = [nvz_b[0][0], nvz_c[0][0], nvz_f[0][0], nvz_i[0][0], nvz_m[0][0], nvz_w[0][0], nvz_rom[0][0], nvz_jul[0][0], nvz_the[0][0], nvz_lou[0][0], nvz_romu[0][0], nvz_rem[0][0]]
N6 = [nvz_b[0][1], nvz_c[0][1], nvz_f[0][1], nvz_i[0][1], nvz_m[0][1], nvz_w[0][1], nvz_rom[0][1], nvz_jul[0][1], nvz_the[0][1], nvz_lou[0][1], nvz_romu[0][1], nvz_rem[0][1]]
N7 = [nvz_b[0][2], nvz_c[0][2], nvz_f[0][2], nvz_i[0][2], nvz_m[0][2], nvz_w[0][2], nvz_rom[0][2], nvz_jul[0][2], nvz_the[0][2], nvz_lou[0][2], nvz_romu[0][2], nvz_rem[0][2]]
N8 = [nvz_b[0][3], nvz_c[0][3], nvz_f[0][3], nvz_i[0][3], nvz_m[0][3], nvz_w[0][3], nvz_rom[0][3], nvz_jul[0][3], nvz_the[0][3], nvz_lou[0][3], nvz_romu[0][3], nvz_rem[0][3]]
N9 = [nvz_b[0][4], nvz_c[0][4], nvz_f[0][4], nvz_i[0][4], nvz_m[0][4], nvz_w[0][4], nvz_rom[0][4], nvz_jul[0][4], nvz_the[0][4], nvz_lou[0][4], nvz_romu[0][4], nvz_rem[0][4]]
N5300med_iso = np.median(N5[:6],0)
N6300med_iso = np.median(N6[:6],0)
N7300med_iso = np.median(N7[:6],0)
N8300med_iso = np.median(N8[:6],0)
N9300med_iso = np.median(N9[:6],0)
N5300med_lg = np.median(N5[6:],0)
N6300med_lg = np.median(N6[6:],0)
N7300med_lg = np.median(N7[6:],0)
N8300med_lg = np.median(N8[6:],0)
N9300med_lg = np.median(N9[6:],0)

# 15 kpc
N5 = [nvz_b[1][0], nvz_c[1][0], nvz_f[1][0], nvz_i[1][0], nvz_m[1][0], nvz_w[1][0], nvz_rom[1][0], nvz_jul[1][0], nvz_the[1][0], nvz_lou[1][0], nvz_romu[1][0], nvz_rem[1][0]]
N6 = [nvz_b[1][1], nvz_c[1][1], nvz_f[1][1], nvz_i[1][1], nvz_m[1][1], nvz_w[1][1], nvz_rom[1][1], nvz_jul[1][1], nvz_the[1][1], nvz_lou[1][1], nvz_romu[1][1], nvz_rem[1][1]]
N7 = [nvz_b[1][2], nvz_c[1][2], nvz_f[1][2], nvz_i[1][2], nvz_m[1][2], nvz_w[1][2], nvz_rom[1][2], nvz_jul[1][2], nvz_the[1][2], nvz_lou[1][2], nvz_romu[1][2], nvz_rem[1][2]]
N8 = [nvz_b[1][3], nvz_c[1][3], nvz_f[1][3], nvz_i[1][3], nvz_m[1][3], nvz_w[1][3], nvz_rom[1][3], nvz_jul[1][3], nvz_the[1][3], nvz_lou[1][3], nvz_romu[1][3], nvz_rem[1][3]]
N9 = [nvz_b[1][4], nvz_c[1][4], nvz_f[1][4], nvz_i[1][4], nvz_m[1][4], nvz_w[1][4], nvz_rom[1][4], nvz_jul[1][4], nvz_the[1][4], nvz_lou[1][4], nvz_romu[1][4], nvz_rem[1][4]]
N515med_iso = np.median(N5[:6],0)
N615med_iso = np.median(N6[:6],0)
N715med_iso = np.median(N7[:6],0)
N815med_iso = np.median(N8[:6],0)
N915med_iso = np.median(N9[:6],0)
N515med_lg = np.median(N5[6:],0)
N615med_lg = np.median(N6[6:],0)
N715med_lg = np.median(N7[6:],0)
N815med_lg = np.median(N8[6:],0)
N915med_lg = np.median(N9[6:],0)

# 2 kpc
N5 = [nvz_b[2][0], nvz_c[2][0], nvz_f[2][0], nvz_i[2][0], nvz_m[2][0], nvz_w[2][0], nvz_rom[2][0], nvz_jul[2][0], nvz_the[2][0], nvz_lou[2][0], nvz_romu[2][0], nvz_rem[2][0]]
N6 = [nvz_b[2][1], nvz_c[2][1], nvz_f[2][1], nvz_i[2][1], nvz_m[2][1], nvz_w[2][1], nvz_rom[2][1], nvz_jul[2][1], nvz_the[2][1], nvz_lou[2][1], nvz_romu[2][1], nvz_rem[2][1]]
N7 = [nvz_b[2][2], nvz_c[2][2], nvz_f[2][2], nvz_i[2][2], nvz_m[2][2], nvz_w[2][2], nvz_rom[2][2], nvz_jul[2][2], nvz_the[2][2], nvz_lou[2][2], nvz_romu[2][2], nvz_rem[2][2]]
N8 = [nvz_b[2][3], nvz_c[2][3], nvz_f[2][3], nvz_i[2][3], nvz_m[2][3], nvz_w[2][3], nvz_rom[2][3], nvz_jul[2][3], nvz_the[2][3], nvz_lou[2][3], nvz_romu[2][3], nvz_rem[2][3]]
N9 = [nvz_b[2][4], nvz_c[2][4], nvz_f[2][4], nvz_i[2][4], nvz_m[2][4], nvz_w[2][4], nvz_rom[2][4], nvz_jul[2][4], nvz_the[2][4], nvz_lou[2][4], nvz_romu[2][4], nvz_rem[2][4]]
N52med_iso = np.median(N5[:6],0)
N62med_iso = np.median(N6[:6],0)
N72med_iso = np.median(N7[:6],0)
N82med_iso = np.median(N8[:6],0)
N92med_iso = np.median(N9[:6],0)
N52med_lg = np.median(N5[6:],0)
N62med_lg = np.median(N6[6:],0)
N72med_lg = np.median(N7[6:],0)
N82med_lg = np.median(N8[6:],0)
N92med_lg = np.median(N9[6:],0)

# Gather data in a dictionary
d1 = dict()
#
d1['300kpc.1e5.iso'] = N5300med_iso[20:]
d1['300kpc.1e6.iso'] = N6300med_iso[20:]
d1['300kpc.1e7.iso'] = N7300med_iso[20:]
d1['300kpc.1e8.iso'] = N8300med_iso[20:]
d1['300kpc.1e9.iso'] = N9300med_iso[20:]
#
d1['300kpc.1e5.lg'] = N5300med_lg[20:]
d1['300kpc.1e6.lg'] = N6300med_lg[20:]
d1['300kpc.1e7.lg'] = N7300med_lg[20:]
d1['300kpc.1e8.lg'] = N8300med_lg[20:]
d1['300kpc.1e9.lg'] = N9300med_lg[20:]
#
d1['15kpc.1e5.iso'] = N515med_iso[20:]
d1['15kpc.1e6.iso'] = N615med_iso[20:]
d1['15kpc.1e7.iso'] = N715med_iso[20:]
d1['15kpc.1e8.iso'] = N815med_iso[20:]
d1['15kpc.1e9.iso'] = N915med_iso[20:]
#
d1['15kpc.1e5.lg'] = N515med_lg[20:]
d1['15kpc.1e6.lg'] = N615med_lg[20:]
d1['15kpc.1e7.lg'] = N715med_lg[20:]
d1['15kpc.1e8.lg'] = N815med_lg[20:]
d1['15kpc.1e9.lg'] = N915med_lg[20:]
#
d1['2kpc.1e5.iso'] = N52med_iso[20:]
d1['2kpc.1e6.iso'] = N62med_iso[20:]
d1['2kpc.1e7.iso'] = N72med_iso[20:]
d1['2kpc.1e8.iso'] = N82med_iso[20:]
d1['2kpc.1e9.iso'] = N92med_iso[20:]
#
d1['2kpc.1e5.lg'] = N52med_lg[20:]
d1['2kpc.1e6.lg'] = N62med_lg[20:]
d1['2kpc.1e7.lg'] = N72med_lg[20:]
d1['2kpc.1e8.lg'] = N82med_lg[20:]
d1['2kpc.1e9.lg'] = N92med_lg[20:]

ut.io.file_hdf5(file_name_base='num_progenitor_gal_vs_redshift', dict_or_array_to_write=d1, verbose=True)


#########################################################################

"""
==================
= Mass Functions =
==================
"""

# Read in the packages
import utilities as ut
import pickle

# Read in all of the pickle files
f_m12b = open('mf_m12b.p', 'rb')
mf_m12b_300 = pickle.load(f_m12b)
mf_m12b_300_norm = pickle.load(f_m12b)
mf_m12b_15 = pickle.load(f_m12b)
mf_m12b_2 = pickle.load(f_m12b)
f_m12b.close()
#
f_m12c = open('mf_m12c.p', 'rb')
mf_m12c_300 = pickle.load(f_m12c)
mf_m12c_300_norm = pickle.load(f_m12c)
mf_m12c_15 = pickle.load(f_m12c)
mf_m12c_2 = pickle.load(f_m12c)
f_m12c.close()
#
f_m12f = open('mf_m12f.p', 'rb')
mf_m12f_300 = pickle.load(f_m12f)
mf_m12f_300_norm = pickle.load(f_m12f)
mf_m12f_15 = pickle.load(f_m12f)
mf_m12f_2 = pickle.load(f_m12f)
f_m12f.close()
#
f_m12i = open('mf_m12i.p', 'rb')
mf_m12i_300 = pickle.load(f_m12i)
mf_m12i_300_norm = pickle.load(f_m12i)
mf_m12i_15 = pickle.load(f_m12i)
mf_m12i_2 = pickle.load(f_m12i)
f_m12i.close()
#
f_m12m = open('mf_m12m.p', 'rb')
mf_m12m_300 = pickle.load(f_m12m)
mf_m12m_300_norm = pickle.load(f_m12m)
mf_m12m_15 = pickle.load(f_m12m)
mf_m12m_2 = pickle.load(f_m12m)
f_m12m.close()
#
f_m12w = open('mf_m12w.p', 'rb')
mf_m12w_300 = pickle.load(f_m12w)
mf_m12w_300_norm = pickle.load(f_m12w)
mf_m12w_15 = pickle.load(f_m12w)
mf_m12w_2 = pickle.load(f_m12w)
f_m12w.close()
#
f_Romeo = open('mf_Romeo.p', 'rb')
mf_Romeo_300 = pickle.load(f_Romeo)
mf_Romeo_300_norm = pickle.load(f_Romeo)
mf_Romeo_15 = pickle.load(f_Romeo)
mf_Romeo_2 = pickle.load(f_Romeo)
f_Romeo.close()
#
f_Juliet = open('mf_Juliet.p', 'rb')
mf_Juliet_300 = pickle.load(f_Juliet)
mf_Juliet_300_norm = pickle.load(f_Juliet)
mf_Juliet_15 = pickle.load(f_Juliet)
mf_Juliet_2 = pickle.load(f_Juliet)
f_Juliet.close()
#
f_Thelma = open('mf_Thelma.p', 'rb')
mf_Thelma_300 = pickle.load(f_Thelma)
mf_Thelma_300_norm = pickle.load(f_Thelma)
mf_Thelma_15 = pickle.load(f_Thelma)
mf_Thelma_2 = pickle.load(f_Thelma)
f_Thelma.close()
#
f_Louise = open('mf_Louise.p', 'rb')
mf_Louise_300 = pickle.load(f_Louise)
mf_Louise_300_norm = pickle.load(f_Louise)
mf_Louise_15 = pickle.load(f_Louise)
mf_Louise_2 = pickle.load(f_Louise)
f_Louise.close()
#
f_Romulus = open('mf_Romulus.p', 'rb')
mf_Romulus_300 = pickle.load(f_Romulus)
mf_Romulus_300_norm = pickle.load(f_Romulus)
mf_Romulus_15 = pickle.load(f_Romulus)
mf_Romulus_2 = pickle.load(f_Romulus)
f_Romulus.close()
#
f_Remus = open('mf_Remus.p', 'rb')
mf_Remus_300 = pickle.load(f_Remus)
mf_Remus_300_norm = pickle.load(f_Remus)
mf_Remus_15 = pickle.load(f_Remus)
mf_Remus_2 = pickle.load(f_Remus)
f_Remus.close()
#

# Group all of the galaxies together
mf_300 = [mf_m12b_300, mf_m12c_300, mf_m12f_300, mf_m12i_300, mf_m12m_300, mf_m12w_300, mf_Romeo_300, mf_Juliet_300, mf_Thelma_300, mf_Louise_300, mf_Romulus_300, mf_Remus_300] # 300 kpc
mf_300_norm = [mf_m12b_300_norm, mf_m12c_300_norm, mf_m12f_300_norm, mf_m12i_300_norm, mf_m12m_300_norm, mf_m12w_300_norm, mf_Romeo_300_norm, mf_Juliet_300_norm, mf_Thelma_300_norm, mf_Louise_300_norm, mf_Romulus_300_norm, mf_Remus_300_norm] # 300 kpc normalized
mf_15 = [mf_m12b_15, mf_m12c_15, mf_m12f_15, mf_m12i_15, mf_m12m_15, mf_m12w_15, mf_Romeo_15, mf_Juliet_15, mf_Thelma_15, mf_Louise_15, mf_Romulus_15, mf_Remus_15] # 15 kpc
mf_2 = [mf_m12b_2, mf_m12c_2, mf_m12f_2, mf_m12i_2, mf_m12m_2, mf_m12w_2, mf_Romeo_2, mf_Juliet_2, mf_Thelma_2, mf_Louise_2, mf_Romulus_2, mf_Remus_2] # 2 kpc
# Calculate the medians
mf_300_med = np.median(mf_300, 0)
mf_300_norm_med = np.median(mf_300_norm, 0)
mf_15_med = np.median(mf_15, 0)
mf_2_med = np.median(mf_2, 0)
#
M1 = np.linspace(5., 10., 11)

# Put the relevant data in a dictionary
d1 = dict()
d1['mass.array'] = 10**(M1)
d1['300kpc.z0'] = mf_300_med[0]
d1['300kpc.z1'] = mf_300_med[10]
d1['300kpc.z2'] = mf_300_med[20]
d1['300kpc.z4'] = mf_300_med[40]
d1['300kpc.z6'] = mf_300_med[50]
d1['300kpc.norm.z0'] = mf_300_norm_med[0]
d1['300kpc.norm.z1'] = mf_300_norm_med[10]
d1['300kpc.norm.z2'] = mf_300_norm_med[20]
d1['300kpc.norm.z4'] = mf_300_norm_med[40]
d1['300kpc.norm.z6'] = mf_300_norm_med[50]
d1['15kpc.z0'] = mf_15_med[0]
d1['15kpc.z1'] = mf_15_med[10]
d1['15kpc.z2'] = mf_15_med[20]
d1['15kpc.z4'] = mf_15_med[40]
d1['15kpc.z6'] = mf_15_med[50]
d1['2kpc.z0'] = mf_2_med[0]
d1['2kpc.z1'] = mf_2_med[10]
d1['2kpc.z2'] = mf_2_med[20]
d1['2kpc.z4'] = mf_2_med[40]
d1['2kpc.z6'] = mf_2_med[50]

ut.io.file_hdf5(file_name_base='mass_functions_vs_redshift', dict_or_array_to_write=d1, verbose=True)


#########################################################################

"""
===========================
= Weighted Mass Functions =
===========================
"""

# Read in the packages
import utilities as ut
import pickle

# I basically copied everything from mf_weighted_plot_new.py and manually saved the dictionary
d1['300.kpc.lower.scatter'] = np.asarray(a300_new)
d1['300.kpc.upper.scatter'] = np.asarray(b300)
d1['300.kpc.lower.scatter.cumul'] = np.asarray(a300c)
d1['300.kpc.upper.scatter.cumul'] = np.asarray(b300c)
d1['15.kpc.lower.scatter'] = np.asarray(a15_new)
d1['15.kpc.upper.scatter'] = np.asarray(b15)
d1['15.kpc.lower.scatter.cumul'] = np.asarray(a15c)
d1['15.kpc.upper.scatter.cumul'] = np.asarray(b15c)
d1['2.kpc.lower.scatter'] = np.asarray(a2_new)
d1['2.kpc.upper.scatter'] = np.asarray(b2)
d1['2.kpc.lower.scatter.cumul'] = np.asarray(a2c)
d1['2.kpc.upper.scatter.cumul'] = np.asarray(b2c)

ut.io.file_hdf5(file_name_base='exsitu_fractions', dict_or_array_to_write=d1, verbose=True)


#########################################################################

"""
===============
= Mass Ratios =
===============

Example for how to do the mass ratio file: 'mass_ratios_all.p'
"""

# Read in the packages
import utilities as ut
import pickle

# Copied from mass_ratio_plot_all_v2.py
d1 = dict()
#
d1['M2/M1.median'] = mass215smooth[np.asarray(redshifts) >= 0]
d1['M2/M1.iso.median'] = mass215smoothiso[np.asarray(redshifts) >= 0]
d1['M2/M1.lg.median'] = mass215smoothlg[np.asarray(redshifts) >= 0]
#
d1['M2/M1.median.lower.scatter'] = a2[np.asarray(redshifts) >= 0]
d1['M2/M1.median.upper.scatter'] = b2[np.asarray(redshifts) >= 0]
d1['M2/M1.iso.median.lower.scatter'] = a2iso[np.asarray(redshifts) >= 0]
d1['M2/M1.iso.median.upper.scatter'] = b2iso[np.asarray(redshifts) >= 0]
d1['M2/M1.lg.median.lower.scatter'] = a2lg[np.asarray(redshifts) >= 0]
d1['M2/M1.lg.median.upper.scatter'] = b2lg[np.asarray(redshifts) >= 0]
#
d1['M3/M1.median'] = mass315smooth[np.asarray(redshifts) >= 0]
d1['M3/M1.iso.median'] = mass315smoothiso[np.asarray(redshifts) >= 0]
d1['M3/M1.lg.median'] = mass315smoothlg[np.asarray(redshifts) >= 0]
#
d1['M3/M1.median.lower.scatter'] = a3[np.asarray(redshifts) >= 0]
d1['M3/M1.median.upper.scatter'] = b3[np.asarray(redshifts) >= 0]
d1['M3/M1.iso.median.lower.scatter'] = a3iso[np.asarray(redshifts) >= 0]
d1['M3/M1.iso.median.upper.scatter'] = b3iso[np.asarray(redshifts) >= 0]
d1['M3/M1.lg.median.lower.scatter'] = a3lg[np.asarray(redshifts) >= 0]
d1['M3/M1.lg.median.upper.scatter'] = b3lg[np.asarray(redshifts) >= 0]
#
d1['redshifts'] = np.asarray(redshifts)[np.asarray(redshifts) >= 0]

ut.io.file_hdf5(file_name_base='mass_ratios', dict_or_array_to_write=d1, verbose=True)

#####################################
