#!/usr/bin/python3

"""
 ==============================
 = Median Mass Function Plots =
 ==============================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session II, 2018

 Goal: To create median mass function plots by loading in the data, calculating the median, 
       and then plotting it all on the same figure.

 NOTE: Uses files made from 'mass_function_final_v2.py' and mass_function_final_lg_v3.py
"""

### Import all of the tools for analysis and read in the data
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import distinct_colours as dc
import pickle
import matplotlib.ticker as ticker
from matplotlib import gridspec

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/mf_m12b.p', 'rb')
N1b = pickle.load(File1)
N1_normb = pickle.load(File1)
N2b = pickle.load(File1)
N3b = pickle.load(File1)
N4b = pickle.load(File1)
N5b = pickle.load(File1)
N5_normb = pickle.load(File1)
N6b = pickle.load(File1)
N6_normb = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/mf_m12c.p', 'rb')
N1c = pickle.load(File2)
N1_normc = pickle.load(File2)
N2c = pickle.load(File2)
N3c = pickle.load(File2)
N4c = pickle.load(File2)
N5c = pickle.load(File2)
N5_normc = pickle.load(File2)
N6c = pickle.load(File2)
N6_normc = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/mf_m12f.p', 'rb')
N1f = pickle.load(File3)
N1_normf = pickle.load(File3)
N2f = pickle.load(File3)
N3f = pickle.load(File3)
N4f = pickle.load(File3)
N5f = pickle.load(File3)
N5_normf = pickle.load(File3)
N6f = pickle.load(File3)
N6_normf = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/mf_m12i.p', 'rb')
N1i = pickle.load(File4)
N1_normi = pickle.load(File4)
N2i = pickle.load(File4)
N3i = pickle.load(File4)
N4i = pickle.load(File4)
N5i = pickle.load(File4)
N5_normi = pickle.load(File4)
N6i = pickle.load(File4)
N6_normi = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/mf_m12m.p', 'rb')
N1m = pickle.load(File5)
N1_normm = pickle.load(File5)
N2m = pickle.load(File5)
N3m = pickle.load(File5)
N4m = pickle.load(File5)
N5m = pickle.load(File5)
N5_normm = pickle.load(File5)
N6m = pickle.load(File5)
N6_normm = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/mf_m12w.p', 'rb')
N1w = pickle.load(File6)
N1_normw = pickle.load(File6)
N2w = pickle.load(File6)
N3w = pickle.load(File6)
N4w = pickle.load(File6)
N5w = pickle.load(File6)
N5_normw = pickle.load(File6)
N6w = pickle.load(File6)
N6_normw = pickle.load(File6)
File6.close()
# Romeo
File7 = open(home_dir_stam+'/scripts/pickles/mf_Romeo.p', 'rb')
N1rom = pickle.load(File7)
N1_normrom = pickle.load(File7)
N2rom = pickle.load(File7)
N3rom = pickle.load(File7)
N4rom = pickle.load(File7)
N5rom = pickle.load(File7)
N5_normrom = pickle.load(File7)
N6rom = pickle.load(File7)
N6_normrom = pickle.load(File7)
File7.close()
# Juliet
File8 = open(home_dir_stam+'/scripts/pickles/mf_Juliet.p', 'rb')
N1jul = pickle.load(File8)
N1_normjul = pickle.load(File8)
N2jul = pickle.load(File8)
N3jul = pickle.load(File8)
N4jul = pickle.load(File8)
N5jul = pickle.load(File8)
N5_normjul = pickle.load(File8)
N6jul = pickle.load(File8)
N6_normjul = pickle.load(File8)
File8.close()
# Thelma
File9 = open(home_dir_stam+'/scripts/pickles/mf_Thelma.p', 'rb')
N1the = pickle.load(File9)
N1_normthe = pickle.load(File9)
N2the = pickle.load(File9)
N3the = pickle.load(File9)
N4the = pickle.load(File9)
N5the = pickle.load(File9)
N5_normthe = pickle.load(File9)
N6the = pickle.load(File9)
N6_normthe = pickle.load(File9)
File9.close()
# Louise
File10 = open(home_dir_stam+'/scripts/pickles/mf_Louise.p', 'rb')
N1lou = pickle.load(File10)
N1_normlou = pickle.load(File10)
N2lou = pickle.load(File10)
N3lou = pickle.load(File10)
N4lou = pickle.load(File10)
N5lou = pickle.load(File10)
N5_normlou = pickle.load(File10)
N6lou = pickle.load(File10)
N6_normlou = pickle.load(File10)
File10.close()
# Romulus
File11 = open(home_dir_stam+'/scripts/pickles/mf_Romulus.p', 'rb')
N1romu = pickle.load(File11)
N1_normromu = pickle.load(File11)
N2romu = pickle.load(File11)
N3romu = pickle.load(File11)
N4romu = pickle.load(File11)
File11.close()
# Remus
File12 = open(home_dir_stam+'/scripts/pickles/mf_Remus.p', 'rb')
N1rem = pickle.load(File12)
N1_normrem = pickle.load(File12)
N2rem = pickle.load(File12)
N3rem = pickle.load(File12)
N4rem = pickle.load(File12)
File12.close()

# Group all of the galaxies together
N1 = [N1b, N1c, N1f, N1i, N1m, N1w, N1rom, N1jul, N1the, N1lou, N1romu, N1rem] # 300 kpc Cumulative
N1_norm = [N1_normb, N1_normc, N1_normf, N1_normi, N1_normm,  N1_normw, N1_normrom, N1_normjul, N1_normthe, N1_normlou, N1_normromu, N1_normrem] # 300 kpc Normalized
N2 = [N2b, N2c, N2f, N2i, N2m, N2w, N2rom, N2jul, N2the, N2lou, N2romu, N2rem] # 15 kpc
N3 = [N3b, N3c, N3f, N3i, N3m, N3w, N3rom, N3jul, N3the, N3lou, N3romu, N3rem] # 2 kpc
N4 = [N4b, N4c, N4f, N4i, N4m, N4w, N4rom, N4jul, N4the, N4lou, N4romu, N4rem] # 4-15 kpc
#N5 = [N5b, N5c, N5f, N5i, N5m, N5w, N5rom, N5jul, N5the, N5lou, N5romu, N5rem] # 150 kpc Cumulative
#N5_norm = [N5_normb, N5_normc, N5_normf, N5_normi, N5_normm,  N5_normw, N5_normrom, N5_normjul, N5_normthe, N5_normlou, N5_normromu, N5_normrem] # 150 kpc Normalized
#N6 = [N6b, N6c, N6f, N6i, N6m, N6w, N6rom, N6jul, N6the, N6lou, N6romu, N6rem] # inf kpc Cumulative
#N6_norm = [N6_normb, N6_normc, N6_normf, N6_normi, N6_normm,  N6_normw, N6_normrom, N6_normjul, N6_normthe, N6_normlou, N6_normromu, N6_normrem] # inf kpc Normalized
# Calculate the medians
N1_med = np.median(N1, 0)
N1_norm_med = np.median(N1_norm, 0)
N2_med = np.median(N2, 0)
N3_med = np.median(N3, 0)
N4_med = np.median(N4, 0)
#N5_med = np.median(N5, 0)
#N5_norm_med = np.median(N5_norm, 0)
#N6_med = np.median(N6, 0)
#N6_norm_med = np.median(N6_norm, 0)

# Calculate the medians for iso and lg seperately
N1_med_iso = np.median(N1[:6], 0)
N1_norm_med_iso = np.median(N1_norm[:6], 0)
N2_med_iso = np.median(N2[:6], 0)
N3_med_iso = np.median(N3[:6], 0)
N4_med_iso = np.median(N4[:6], 0)
N1_med_lg = np.median(N1[6:], 0)
N1_norm_med_lg = np.median(N1_norm[6:], 0)
N2_med_lg = np.median(N2[6:], 0)
N3_med_lg = np.median(N3[6:], 0)
N4_med_lg = np.median(N4[6:], 0)

# Plot the data for the 0.5 dex bin (300 kpc)
M1 = np.linspace(5., 10., 11)
colors = ['#0077BB','#33BBEE','#762A83','#009988','#CC3311','#AA3377']
plt.figure(1)
plt.figure(figsize=(10, 8))
# Top
gs = gridspec.GridSpec(2, 1, height_ratios=[2.5, 1]) 
ax0 = plt.subplot(gs[0])
ax0.plot(10**(M1), N1_med[0], color=colors[0], label='z = 0')
ax0.plot(10**(M1), N1_med[10], color=colors[1], label='z = 1')
ax0.plot(10**(M1), N1_med[20], color=colors[2], label='z = 2')
ax0.plot(10**(M1), N1_med[40], color=colors[4], label='z = 4')
ax0.plot(10**(M1), N1_med[50], color=colors[5], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.xlim(10**5,10**9)
plt.xscale('log')
plt.yscale('log')
from matplotlib.ticker import FixedLocator, FixedFormatter
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax0.yaxis.set_major_formatter(y_formatter)
ax0.yaxis.set_major_locator(y_locator)
plt.ylabel('N(> $\\rm M_{\\rm{star}}$)', fontsize=38)
plt.title('d(z = 0) < 300 kpc', fontsize=44, y=1.03)
# Bottom
ax1 = plt.subplot(gs[1])
ax1.plot(10**(M1), N1_norm_med[0], color=colors[0], label='z = 0')
ax1.plot(10**(M1), N1_norm_med[10], color=colors[1], label='z = 1')
ax1.plot(10**(M1), N1_norm_med[20], color=colors[2], label='z = 2')
ax1.plot(10**(M1), N1_norm_med[40], color=colors[4], label='z = 4')
ax1.plot(10**(M1), N1_norm_med[50], color=colors[5], label='z = 6')
plt.tick_params(axis='x', which='both', labelsize=28, width=1.5)
plt.tick_params(axis='y', which='major', labelsize=18, width=1.5)
plt.tick_params(axis='y', which='minor', length=0.0, width=0.0)
plt.ylim([0,5])
plt.xlim(10**5,10**9)
plt.ylabel('N(z) / N(z = 0)', fontsize=18, labelpad=20)
plt.xlabel('$\\rm M_{\\rm{star}}$ [$\\rm M_{\odot}$]', fontsize=46)
plt.xscale('log')
y_formatter = FixedFormatter(["1", "2", "3", "4", "5"])
y_locator = FixedLocator([1, 2, 3, 4, 5])
ax1.yaxis.set_major_formatter(y_formatter)
ax1.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_300kpc_1percent_med_v5.pdf')
plt.close()

# Plot the data for the 0.5 dex bin (15 kpc)
plt.figure(2)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N2_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N2_med[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N2_med[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N2_med[40], color=colors[4], label='z = 4')
plt.plot(10**(M1), N2_med[50], color=colors[5], label='z = 6')
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $\\rm M_{\\rm{star}}$)', fontsize=46)
plt.xlabel('$\\rm M_{\\rm{star}}$ [$\\rm M_{\odot}$]', fontsize=46)
plt.title('d(z = 0) < 15 kpc', fontsize=46, y=1.02)
plt.xscale('log')
plt.yscale('log')
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_15kpc_1percent_med_v5.pdf')
plt.close()

# Plot the data for the 0.5 dex bin (2 kpc)
plt.figure(3)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N3_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N3_med[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N3_med[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N3_med[40], color=colors[4], label='z = 4')
plt.plot(10**(M1), N3_med[50], color=colors[5], label='z = 6')
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
plt.xlim(10**5,10**9)
plt.ylabel('N(> $\\rm M_{\\rm{star}}$)', fontsize=46)
plt.xlabel('$\\rm M_{\\rm{star}}$ [$\\rm M_{\odot}$]', fontsize=46)
plt.title('d(z = 0) < 2 kpc', fontsize=46, y=1.02)
plt.xscale('log')
plt.yscale('log')
y_formatter = FixedFormatter(["1", "10", "100"])
y_locator = FixedLocator([1, 10, 100])
ax.yaxis.set_major_formatter(y_formatter)
ax.yaxis.set_major_locator(y_locator)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_2kpc_1percent_med_v5.pdf')
plt.close()
"""
# Plot the data for the 0.5 dex bin (4-15 kpc)
plt.figure(4)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(111)
plt.plot(10**(M1), N4_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N4_med[10], color=colors[1], label='z = 1')
plt.plot(10**(M1), N4_med[20], color=colors[2], label='z = 2')
plt.plot(10**(M1), N4_med[40], color=colors[4], label='z = 4')
plt.plot(10**(M1), N4_med[50], color=colors[5], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='both', labelsize=30, width=1.5)
plt.xlim(10**5,10**9)
#plt.yticks(np.array([1,10,100]))
plt.ylabel('N(> $M_{\\rm{star}}$)', fontsize=46)
plt.xlabel('$M_{\\rm{star}}$ [$M_{\odot}$]', fontsize=46)
plt.title('d(z = 0) = [4, 15 kpc]', fontsize=46, y=1.02)
plt.xscale('log')
plt.yscale('log')
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_415kpc_1percent_med_v5.pdf')
plt.close()
"""
"""
# 150 kpc
plt.figure(5)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(211)
plt.plot(10**(M1), N5_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N5_med[50], color=colors[1], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.ylim(0.5, 200)
plt.yticks(np.array([1,10,100]))
plt.xlim(10**5,10**9)
plt.xscale('log')
plt.yscale('log')
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.ylabel('N(> $M_{\\rm{star}}$)', fontsize=38)
plt.title('d(z = 0) = [0, 150 kpc]', fontsize=44, y=1.02)
# Bottom
ax = plt.subplot(212)
plt.plot(10**(M1), N5_norm_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N5_norm_med[50], color=colors[1], label='z = 6')
plt.tick_params(axis='both', which='both', labelsize=28, width=1.5)
plt.ylim(ymin=0)
plt.yticks(np.array([1,2,3,4,5]))
plt.xlim(10**5,10**9)
plt.ylabel('N(z) / N(z = 0)', fontsize=30)
plt.xlabel('$M_{\\rm{star}}$ [$M_{\odot}$]', fontsize=34)
plt.xscale('log')
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_150kpc_1percent_med_v5.pdf')
plt.close()
"""
"""
# 2 Mpc
plt.figure(6)
plt.figure(figsize=(10, 8))
# Top
ax = plt.subplot(211)
plt.plot(10**(M1), N6_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N6_med[50], color=colors[1], label='z = 6')
plt.legend(prop={'size': 20})
plt.tick_params(axis='x', which='both', labelbottom='off', width=1.5)
plt.tick_params(axis='y', which='both', labelsize=28, width=1.5)
plt.ylim(0.5, 200)
plt.yticks(np.array([1,10,100]))
plt.xlim(10**5,10**9)
plt.xscale('log')
plt.yscale('log')
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.title('d(z = 0) = [0, 2 Mpc]', fontsize=44, y=1.02)
plt.ylabel('N(> $M_{\\rm{star}}$)', fontsize=38)
# Bottom
ax = plt.subplot(212)
plt.plot(10**(M1), N6_norm_med[0], color=colors[0], label='z = 0')
plt.plot(10**(M1), N6_norm_med[50], color=colors[1], label='z = 6')
plt.tick_params(axis='both', which='both', labelsize=28, width=1.5)
plt.ylim(ymin=0)
plt.yticks(np.array([1,2,3,4,5]))
plt.xlim(10**5,10**9)
plt.ylabel('N(z) / N(z = 0)', fontsize=30)
plt.xlabel('$M_{\\rm{star}}$ [$M_{\odot}$]', fontsize=34)
plt.xscale('log')
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mf_inf_1percent_med_v5.pdf')
plt.close()
"""