#!/usr/bin/python3

"""
 ==========================
 = Mass Ratio v2 Plot all =
 ==========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) for Spring Quarter, 2018

 Goal: Read in the mass ratios for all galaxies and plot them for:
       - M2/M1
       - Calculate the median M2/M1 ratio 

 NOTE: Used files made from "mass_ratio_v2.py", "mass_ratio_v3.py", and "mass_ratio_lg.py"

"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

colors = ['#332288','#88CCEE','#44AA99','#117733','#999933','#DDCC77','#CC6677','#882255','#AA4499','#BBBBBB','#A50026','#726A83']

mrcut1 = 0.5
mrcut2 = 0.33
mrcut3 = 0.25

## Read in the data
file1 = open(home_dir_stam+'/scripts/pickles/mass_ratios_all.p', 'rb')
massb = pickle.load(file1)
massc = pickle.load(file1)
massf = pickle.load(file1)
massi = pickle.load(file1)
massm = pickle.load(file1)
massw = pickle.load(file1)
massrom = pickle.load(file1)
massjul = pickle.load(file1)
massthe = pickle.load(file1)
masslou = pickle.load(file1)
massromu = pickle.load(file1)
massrem = pickle.load(file1)
file1.close()

"""
# d = [0, 300 kpc]
two_one_ratio = [massb[0][0], massc[0][0], massf[0][0], massi[0][0], massm[0][0], massw[0][0], massrom[0][0], massjul[0][0], massthe[0][0], masslou[0][0], massromu[0][0], massrem[0][0]]
three_one_ratio = [massb[0][1], massc[0][1], massf[0][1], massi[0][1], massm[0][1], massw[0][1], massrom[0][1], massjul[0][1], massthe[0][1], masslou[0][1], massromu[0][1], massrem[0][1]]
four_one_ratio = [massb[0][2], massc[0][2], massf[0][2], massi[0][2], massm[0][2], massw[0][2], massrom[0][2], massjul[0][2], massthe[0][2], masslou[0][2], massromu[0][2], massrem[0][2]]
mass2300med = np.median(two_one_ratio,0)
mass3300med = np.median(three_one_ratio,0)
mass4300med = np.median(four_one_ratio,0)
window_size, poly_order = 41, 3
mass2300smooth = savgol_filter(mass2300med, window_size, poly_order)
mass3300smooth = savgol_filter(mass3300med, window_size, poly_order)
mass4300smooth = savgol_filter(mass4300med, window_size, poly_order)
# Smooth all of the individual curves
two_one_ratio_smooth = [savgol_filter(two_one_ratio[i], window_size, poly_order) for i in range(0, len(two_one_ratio))]
three_one_ratio_smooth = [savgol_filter(three_one_ratio[i], window_size, poly_order) for i in range(0, len(three_one_ratio))]
four_one_ratio_smooth = [savgol_filter(four_one_ratio[i], window_size, poly_order) for i in range(0, len(four_one_ratio))]
a2 = np.percentile(two_one_ratio_smooth, 15.87, axis=0)
b2 = np.percentile(two_one_ratio_smooth, 84.13, axis=0)
a3 = np.percentile(three_one_ratio_smooth, 15.87, axis=0)
b3 = np.percentile(three_one_ratio_smooth, 84.13, axis=0)
a4 = np.percentile(four_one_ratio_smooth, 15.87, axis=0)
b4 = np.percentile(four_one_ratio_smooth, 84.13, axis=0)
# Plot the shaded version
plt.figure(1)
plt.figure(figsize=(10, 8))
#plt.plot(redshifts, mass2300med, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
plt.fill_between(redshifts, b2, a2, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass2300smooth, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
#plt.plot(redshifts, mass3300med, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
plt.fill_between(redshifts, b3, a3, color=colors[1], alpha=0.35)
plt.plot(redshifts, mass3300smooth, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
#plt.plot(redshifts, mass4300med, color=colors[2], linewidth=1.0, label='M4/M1')
#plt.fill_between(redshifts, b4, a4, color=colors[2], alpha=0.35)
#plt.plot(redshifts, mass4300smooth, color='k', linewidth=1.0, label='smoothed M4/M1')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('d(z = 0) = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_300_shaded_v5.pdf')
plt.close()
"""
"""
# Plot all of them
plt.figure(2)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redshifts, mass2300med, color='b', linewidth=4.0, label='median')
plt.plot(redshifts, mass2300smooth, color='k', linewidth=4.0, label='smoothed')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('M2/M1', fontsize=44)
plt.title('Stars, d = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_300_smooth_v5.pdf')
plt.close()
"""

### d = [0, 15 kpc]
# Ratios for ALL galaxies
two_one_ratio = [massb[1][0], massc[1][0], massf[1][0], massi[1][0], massm[1][0], massw[1][0], massrom[1][0], massjul[1][0], massthe[1][0], masslou[1][0], massromu[1][0], massrem[1][0]]
three_one_ratio = [massb[1][1], massc[1][1], massf[1][1], massi[1][1], massm[1][1], massw[1][1], massrom[1][1], massjul[1][1], massthe[1][1], masslou[1][1], massromu[1][1], massrem[1][1]]
four_one_ratio = [massb[1][2], massc[1][2], massf[1][2], massi[1][2], massm[1][2], massw[1][2], massrom[1][2], massjul[1][2], massthe[1][2], masslou[1][2], massromu[1][2], massrem[1][2]]
# Ratios for only isolated hosts
two_one_iso = [massb[1][0], massc[1][0], massf[1][0], massi[1][0], massm[1][0], massw[1][0]]
three_one_iso = [massb[1][1], massc[1][1], massf[1][1], massi[1][1], massm[1][1], massw[1][1]]
four_one_iso = [massb[1][2], massc[1][2], massf[1][2], massi[1][2], massm[1][2], massw[1][2]]
# Ratios for only the pairs
two_one_lg = [massrom[1][0], massjul[1][0], massthe[1][0], masslou[1][0], massromu[1][0], massrem[1][0]]
three_one_lg = [massrom[1][1], massjul[1][1], massthe[1][1], masslou[1][1], massromu[1][1], massrem[1][1]]
four_one_lg = [massrom[1][2], massjul[1][2], massthe[1][2], masslou[1][2], massromu[1][2], massrem[1][2]]

# Median for ALL galaxies
mass215med = np.median(two_one_ratio,0)
mass315med = np.median(three_one_ratio,0)
mass415med = np.median(four_one_ratio,0)
# Median for only the isolated galaxies
mass215iso = np.median(two_one_iso,0)
mass315iso = np.median(three_one_iso,0)
mass415iso = np.median(four_one_iso,0)
# Median for only the pairs
mass215lg = np.median(two_one_lg,0)
mass315lg = np.median(three_one_lg,0)
mass415lg = np.median(four_one_lg,0)

# Smooth median for ALL galaxies
window_size, poly_order = 41, 3
mass215smooth = savgol_filter(mass215med, window_size, poly_order)
mass315smooth = savgol_filter(mass315med, window_size, poly_order)
mass415smooth = savgol_filter(mass415med, window_size, poly_order)
# Smooth the isolated median
mass215smoothiso = savgol_filter(mass215iso, window_size, poly_order)
mass315smoothiso = savgol_filter(mass315iso, window_size, poly_order)
mass415smoothiso = savgol_filter(mass415iso, window_size, poly_order)
# Smooth the pair median
mass215smoothlg = savgol_filter(mass215lg, window_size, poly_order)
mass315smoothlg = savgol_filter(mass315lg, window_size, poly_order)
mass415smoothlg = savgol_filter(mass415lg, window_size, poly_order)

"""
# Median scatter, median curve, median solo, median pair

# Smooth all of the individual curves
two_one_ratio_smooth = [savgol_filter(two_one_ratio[i], window_size, poly_order) for i in range(0, len(two_one_ratio))]
three_one_ratio_smooth = [savgol_filter(three_one_ratio[i], window_size, poly_order) for i in range(0, len(three_one_ratio))]
four_one_ratio_smooth = [savgol_filter(four_one_ratio[i], window_size, poly_order) for i in range(0, len(four_one_ratio))]
a2 = np.percentile(two_one_ratio_smooth, 15.87, axis=0)
b2 = np.percentile(two_one_ratio_smooth, 84.13, axis=0)
a3 = np.percentile(three_one_ratio_smooth, 15.87, axis=0)
b3 = np.percentile(three_one_ratio_smooth, 84.13, axis=0)
a4 = np.percentile(four_one_ratio_smooth, 15.87, axis=0)
b4 = np.percentile(four_one_ratio_smooth, 84.13, axis=0)
# Plot the shaded version
plt.figure(3)
plt.figure(figsize=(10, 8))
#plt.plot(redshifts, mass215med, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
plt.fill_between(redshifts, b2, a2, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass215smooth, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$ (All)')
plt.plot(redshifts, mass215smoothiso, color='k', linewidth=4.0, label='M$_2$/M$_1$ (Isolated hosts)')
plt.plot(redshifts, mass215smoothlg, color='k', linestyle=':', linewidth=4.0, label='M$_2$/M$_1$ (LG pairs)')
#plt.plot(redshifts, mass315med, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
plt.fill_between(redshifts, b3, a3, color=colors[2], alpha=0.35)
plt.plot(redshifts, mass315smooth, color=colors[2], linewidth=2.5, label='M$_3$/M$_1$ (All)')
plt.plot(redshifts, mass315smoothiso, color='k', linewidth=2.5, alpha=0.85, label='M$_3$/M$_1$ (Isolated hosts)')
plt.plot(redshifts, mass315smoothlg, color='k', linestyle=':', linewidth=2.5, alpha=0.85, label='M$_3$/M$_1$ (LG pairs)')
#plt.plot(redshifts, mass415med, color=colors[2], linewidth=1.0, label='M4/M1')
#plt.fill_between(redshifts, b4, a4, color=colors[2], alpha=0.35)
#plt.plot(redshifts, mass415smooth, color='k', linewidth=1.0, label='smoothed M4/M1')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('d(z = 0) = [0, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mass_ratios_v5.pdf')
plt.close()
"""

# Smooth all of the individual curves and get scatter for ALL
two_one_ratio_smooth = [savgol_filter(two_one_ratio[i], window_size, poly_order) for i in range(0, len(two_one_ratio))]
three_one_ratio_smooth = [savgol_filter(three_one_ratio[i], window_size, poly_order) for i in range(0, len(three_one_ratio))]
four_one_ratio_smooth = [savgol_filter(four_one_ratio[i], window_size, poly_order) for i in range(0, len(four_one_ratio))]
a2 = np.percentile(two_one_ratio_smooth, 15.87, axis=0)
b2 = np.percentile(two_one_ratio_smooth, 84.13, axis=0)
a3 = np.percentile(three_one_ratio_smooth, 15.87, axis=0)
b3 = np.percentile(three_one_ratio_smooth, 84.13, axis=0)
a4 = np.percentile(four_one_ratio_smooth, 15.87, axis=0)
b4 = np.percentile(four_one_ratio_smooth, 84.13, axis=0)
# Smooth only the isolated curves and get scatter for isolated
two_one_iso_smooth = [savgol_filter(two_one_iso[i], window_size, poly_order) for i in range(0, len(two_one_iso))]
three_one_iso_smooth = [savgol_filter(three_one_iso[i], window_size, poly_order) for i in range(0, len(three_one_iso))]
four_one_iso_smooth = [savgol_filter(four_one_iso[i], window_size, poly_order) for i in range(0, len(four_one_iso))]
a2iso = np.percentile(two_one_iso_smooth, 15.87, axis=0)
b2iso = np.percentile(two_one_iso_smooth, 84.13, axis=0)
a3iso = np.percentile(three_one_iso_smooth, 15.87, axis=0)
b3iso = np.percentile(three_one_iso_smooth, 84.13, axis=0)
a4iso = np.percentile(four_one_iso_smooth, 15.87, axis=0)
b4iso = np.percentile(four_one_iso_smooth, 84.13, axis=0)
# Smooth only the isolated curves and get scatter for isolated
two_one_lg_smooth = [savgol_filter(two_one_lg[i], window_size, poly_order) for i in range(0, len(two_one_lg))]
three_one_lg_smooth = [savgol_filter(three_one_lg[i], window_size, poly_order) for i in range(0, len(three_one_lg))]
four_one_lg_smooth = [savgol_filter(four_one_lg[i], window_size, poly_order) for i in range(0, len(four_one_lg))]
a2lg = np.percentile(two_one_lg_smooth, 15.87, axis=0)
b2lg = np.percentile(two_one_lg_smooth, 84.13, axis=0)
a3lg = np.percentile(three_one_lg_smooth, 15.87, axis=0)
b3lg = np.percentile(three_one_lg_smooth, 84.13, axis=0)
a4lg = np.percentile(four_one_lg_smooth, 15.87, axis=0)
b4lg = np.percentile(four_one_lg_smooth, 84.13, axis=0)
# Plot the shaded version M2/M1
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.fill_between(redshifts, b2, a2, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass215smooth, color=colors[0], linewidth=5.5, label='All')
plt.fill_between(redshifts, b2iso, a2iso, color=colors[2], alpha=0.35)
plt.plot(redshifts, mass215smoothiso, color=colors[2], linewidth=4.0, label='Isolated Hosts')
plt.fill_between(redshifts, b2lg, a2lg, color=colors[6], alpha=0.35)
plt.plot(redshifts, mass215smoothlg, color=colors[6], linestyle='--', linewidth=4.0, label='LG Pairs')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('M$_2$/M$_1$, d(z = 0) < 15 kpc', fontsize=36, y=1.02)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mass_ratios_m2m1_v5.pdf')
plt.close()
# Plot the shaded version M3/M1
plt.figure(2)
plt.figure(figsize=(10, 8))
plt.fill_between(redshifts, b3, a3, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass315smooth, color=colors[0], linewidth=5.5, label='All')
plt.fill_between(redshifts, b3iso, a3iso, color=colors[2], alpha=0.35)
plt.plot(redshifts, mass315smoothiso, color=colors[2], linewidth=4.0, label='Isolated Hosts')
plt.fill_between(redshifts, b3lg, a3lg, color=colors[6], alpha=0.35)
plt.plot(redshifts, mass315smoothlg, color=colors[6], linestyle='--', linewidth=4.0, label='LG Pairs')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('M$_3$/M$_1$, d(z = 0) < 15 kpc', fontsize=36, y=1.02)
#plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mass_ratios_m3m1_v5.pdf')
plt.close()

"""
# Plot all of them
plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[10], color=colors[10], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[11], color=colors[11], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.35)
plt.plot(redshifts, mass215med, color='b', linewidth=4.0, label='median')
plt.plot(redshifts, mass215smooth, color='k', linewidth=4.0, label='smoothed')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('M2/M1', fontsize=44)
plt.title('Stars, d = [0, 15 kpc]', fontsize=48, y=1.02)
#plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mass_ratios_m2m1_ind_v5.pdf')
plt.close()
"""
"""
# d = [0, 2 kpc]
two_one_ratio = [massb[2][0], massc[2][0], massf[2][0], massi[2][0], massm[2][0], massw[2][0], massrom[2][0], massjul[2][0], massthe[2][0], masslou[2][0], massromu[2][0], massrem[2][0]]
three_one_ratio = [massb[2][1], massc[2][1], massf[2][1], massi[2][1], massm[2][1], massw[2][1], massrom[2][1], massjul[2][1], massthe[2][1], masslou[2][1], massromu[2][1], massrem[2][1]]
four_one_ratio = [massb[2][2], massc[2][2], massf[2][2], massi[2][2], massm[2][2], massw[2][2], massrom[2][2], massjul[2][2], massthe[2][2], masslou[2][2], massromu[2][2], massrem[2][2]]
mass22med = np.median(two_one_ratio,0)
mass32med = np.median(three_one_ratio,0)
mass42med = np.median(four_one_ratio,0)
window_size, poly_order = 41, 3
mass22smooth = savgol_filter(mass22med, window_size, poly_order)
mass32smooth = savgol_filter(mass32med, window_size, poly_order)
mass42smooth = savgol_filter(mass42med, window_size, poly_order)
# Smooth all of the individual curves
two_one_ratio_smooth = [savgol_filter(two_one_ratio[i], window_size, poly_order) for i in range(0, len(two_one_ratio))]
three_one_ratio_smooth = [savgol_filter(three_one_ratio[i], window_size, poly_order) for i in range(0, len(three_one_ratio))]
four_one_ratio_smooth = [savgol_filter(four_one_ratio[i], window_size, poly_order) for i in range(0, len(four_one_ratio))]
a2 = np.percentile(two_one_ratio_smooth, 15.87, axis=0)
b2 = np.percentile(two_one_ratio_smooth, 84.13, axis=0)
a3 = np.percentile(three_one_ratio_smooth, 15.87, axis=0)
b3 = np.percentile(three_one_ratio_smooth, 84.13, axis=0)
a4 = np.percentile(four_one_ratio_smooth, 15.87, axis=0)
b4 = np.percentile(four_one_ratio_smooth, 84.13, axis=0)
# Plot the shaded version
plt.figure(5)
plt.figure(figsize=(10, 8))
#plt.plot(redshifts, mass22med, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
plt.fill_between(redshifts, b2, a2, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass22smooth, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
#plt.plot(redshifts, mass32med, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
plt.fill_between(redshifts, b3, a3, color=colors[1], alpha=0.35)
plt.plot(redshifts, mass32smooth, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
#plt.plot(redshifts, mass42med, color=colors[2], linewidth=1.0, label='M4/M1')
#plt.fill_between(redshifts, b4, a4, color=colors[2], alpha=0.35)
#plt.plot(redshifts, mass42smooth, color='k', linewidth=1.0, label='smoothed M4/M1')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('d(z = 0) = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_2_shaded_v5.pdf')
plt.close()
"""
"""
# Plot all of them
plt.figure(6)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redshifts, mass22med, color='b', linewidth=4.0, label='median')
plt.plot(redshifts, mass22smooth, color='k', linewidth=4.0, label='smoothed')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('M2/M1', fontsize=44)
plt.title('Stars, d = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_2_smooth_v5.pdf')
plt.close()
"""
"""
# d = [4, 15 kpc]
two_one_ratio = [massb[3][0], massc[3][0], massf[3][0], massi[3][0], massm[3][0], massw[3][0], massrom[3][0], massjul[3][0], massthe[3][0], masslou[3][0], massromu[3][0], massrem[3][0]]
three_one_ratio = [massb[3][1], massc[3][1], massf[3][1], massi[3][1], massm[3][1], massw[3][1], massrom[3][1], massjul[3][1], massthe[3][1], masslou[3][1], massromu[3][1], massrem[3][1]]
four_one_ratio = [massb[3][2], massc[3][2], massf[3][2], massi[3][2], massm[3][2], massw[3][2], massrom[3][2], massjul[3][2], massthe[3][2], masslou[3][2], massromu[3][2], massrem[3][2]]
mass2415med = np.median(two_one_ratio,0)
mass3415med = np.median(three_one_ratio,0)
mass4415med = np.median(four_one_ratio,0)
window_size, poly_order = 41, 3
mass2415smooth = savgol_filter(mass2415med, window_size, poly_order)
mass3415smooth = savgol_filter(mass3415med, window_size, poly_order)
mass4415smooth = savgol_filter(mass4415med, window_size, poly_order)
# Smooth all of the individual curves
two_one_ratio_smooth = [savgol_filter(two_one_ratio[i], window_size, poly_order) for i in range(0, len(two_one_ratio))]
three_one_ratio_smooth = [savgol_filter(three_one_ratio[i], window_size, poly_order) for i in range(0, len(three_one_ratio))]
four_one_ratio_smooth = [savgol_filter(four_one_ratio[i], window_size, poly_order) for i in range(0, len(four_one_ratio))]
a2 = np.percentile(two_one_ratio_smooth, 15.87, axis=0)
b2 = np.percentile(two_one_ratio_smooth, 84.13, axis=0)
a3 = np.percentile(three_one_ratio_smooth, 15.87, axis=0)
b3 = np.percentile(three_one_ratio_smooth, 84.13, axis=0)
a4 = np.percentile(four_one_ratio_smooth, 15.87, axis=0)
b4 = np.percentile(four_one_ratio_smooth, 84.13, axis=0)
# Plot the shaded version
plt.figure(7)
plt.figure(figsize=(10, 8))
#plt.plot(redshifts, mass2415med, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
plt.fill_between(redshifts, b2, a2, color=colors[0], alpha=0.35)
plt.plot(redshifts, mass2415smooth, color=colors[0], linewidth=4.0, label='M$_2$/M$_1$')
#plt.plot(redshifts, mass3415med, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
plt.fill_between(redshifts, b3, a3, color=colors[1], alpha=0.35)
plt.plot(redshifts, mass3415smooth, color=colors[1], linewidth=2.5, label='M$_3$/M$_1$')
#plt.plot(redshifts, mass4415med, color=colors[2], linewidth=1.0, label='M4/M1')
#plt.fill_between(redshifts, b4, a4, color=colors[2], alpha=0.35)
#plt.plot(redshifts, mass4415smooth, color='k', linewidth=1.0, label='smoothed M4/M1')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Stellar Mass Ratio', fontsize=40)
plt.title('d(z = 0) = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_415_shaded_v5.pdf')
plt.close()
"""
"""
# Plot all of them
plt.figure(8)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio_smooth[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[2], color=colors[2], linewidth=2.0, label=galaxies[2], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[6], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[7], color=colors[7], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[8], color=colors[8], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.35)
plt.plot(redshifts, two_one_ratio_smooth[9], color=colors[9], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.35)
plt.plot(redshifts, mass2415med, color='b', linewidth=4.0, label='median')
plt.plot(redshifts, mass2415smooth, color='k', linewidth=4.0, label='smoothed')
#
plt.hlines(y=mrcut1, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut2, xmin=0, xmax=9, color='#808080', linestyles='dotted')
plt.hlines(y=mrcut3, xmin=0, xmax=9, color='#808080', linestyles='dotted')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('M2/M1', fontsize=44)
plt.title('Stars, d = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 24})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(0, 1)
plt.xlim(6, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_415_smooth_v5.pdf')
plt.close()
"""