#!/usr/bin/python3

"""
 ==================
 = Lookback Times =
 ==================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter

 Goal: Print out the lookback times and save them to a file so that I could access them later
"""

#### Import all the necessary tools for analysis
import rockstar_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import pickle

#### Read in halos for all redshifts
galaxy = 'm12c'
resolution = '_res7100'
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

#### Some functions for analysis
# Get list of scale factors
times = [hal[i].snapshot['time.lookback'] for i in range(len(redshifts))]

[print(times[i]) for i in range(0, len(times))]

file1 = open(home_dir_stam+'/scripts/pickles/lookbacks.p', 'wb')
pickle.dump(times, file1)
file1.close()