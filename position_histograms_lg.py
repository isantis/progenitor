#!/usr/bin/python3

"""
 ==========================
 = Position Histograms LG =
 ==========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Break, 2018

 Goal: Create plots of particles showing how they look for one projection (XY, YZ, or XZ) for redshifts
            - z = [0, 1, 2, 3, 4, 5, 6, 7]
            - i = [0, 10, 20, 30, 40, 45, 50, 51] (snapshot index)
       for particles that end up within
            - d = 300 kpc
            - d = 15 kpc
            - d = 2 kpc
            - d = 4-15 kpc
       of the host at z = 0

       Everything is in physical units now...
"""

# Import necessary tools for analysis
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import patches
from matplotlib import colors
import pickle

#### Read in the halo information
gal1 = 'Romulus'
gal2 = 'Remus'
dist = 300
lims1 = 500
lims2 = 500
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
    resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
    resolution = '_res4000'
else:
    print('Which galaxies are you working on??')
simulation_dir_pel = '/home/awetzel/scratch/'+galaxy+'/'+galaxy+resolution
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

### Read in the data of stars
part = gizmo.io.Read.read_snapshots('star', 'redshift', [0,1,2,3,4,5,6,7], simulation_directory=simulation_dir_stam, host_number=2, assign_host_principal_axes=True)
# Get the snapshot indices from the halo info
snapind = [hal[i].snapshot['index'] for i in range(1, len(redshifts))]
# Get pointers at other redshifts
temp_point = [gizmo.track.ParticlePointerIO.io_pointers(snapshot_index=i, track_directory=simulation_dir_stam+'/track') for i in snapind]
sis_at_z = [temp_point[i]['z0.to.z.index'] for i in range(0, len(snapind))]

# Read in the contribution fractions for the halos at each redshift
distn = str(dist)
# GALAXY 1
pickle_1 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal1+'.p'
# d = [0, d kpc]
Filep1 = open(pickle_1, "rb")
ratio_1 = pickle.load(Filep1)
Filep1.close()
# GALAXY 2
pickle_2 = home_dir_stam+'/scripts/pickles/contribution_'+distn+'_fullres_'+gal2+'.p'
# d = [0, d kpc]
Filep2 = open(pickle_2, "rb")
ratio_2 = pickle.load(Filep2)
Filep2.close()

### Some functions to help with analysis
"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

 unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
 sort: unsort, sorted
 i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
 mass_i: the mass of element i in sort
 index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
    d: distance
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]

### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
zs = [0,1,2,3,4,5,6,7] # to be looped over or something
hs = [0, 10, 20, 30, 40, 45, 50, 51]


#################################################   Galaxy 1   ######################################################
# Halos
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind = []
for i in range(0, len(ratio_1)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_1[i][temp]
    mass_ind.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_1[i][temp]
        mass_ind[i] = temp
# Get the index for z = 0
ind0 = mass_index(hal[0]['star.mass'][his[0]], 0)
# Get the halo indices for halos within 5 times the most massive halo that also have contr. fracs. > 1%
m0 = (hal[0]['star.mass'][his[0]] > (1/5)*hal[0]['star.mass'][his[0]][ind0])
m1 = (hal[10]['star.mass'][his[10]] > (1/5)*hal[10]['star.mass'][his[10]][mass_ind[9]]) & (ratio_1[9] > 0.01)
m2 = (hal[20]['star.mass'][his[20]] > (1/5)*hal[20]['star.mass'][his[20]][mass_ind[19]]) & (ratio_1[19] > 0.01)
m3 = (hal[30]['star.mass'][his[30]] > (1/5)*hal[30]['star.mass'][his[30]][mass_ind[29]]) & (ratio_1[29] > 0.01)
m4 = (hal[40]['star.mass'][his[40]] > (1/5)*hal[40]['star.mass'][his[40]][mass_ind[39]]) & (ratio_1[39] > 0.01)
m5 = (hal[45]['star.mass'][his[45]] > (1/5)*hal[45]['star.mass'][his[45]][mass_ind[44]]) & (ratio_1[44] > 0.01)
m6 = (hal[50]['star.mass'][his[50]] > (1/5)*hal[50]['star.mass'][his[50]][mass_ind[49]]) & (ratio_1[49] > 0.01)
m7 = (hal[51]['star.mass'][his[51]] > (1/5)*hal[51]['star.mass'][his[51]][mass_ind[50]]) & (ratio_1[50] > 0.01)
# Only select halos that follow all of the rules thus far, plus, impose the 1% contribution cut!
his0 = his[0][m0]
his1 = his[10][m1]
his2 = his[20][m2]
his3 = his[30][m3]
his4 = his[40][m4]
his5 = his[45][m5]
his6 = his[50][m6]
his7 = his[51][m7]
hiss = [his0, his1, his2, his3, his4, his5, his6, his7]
# Get the radii of the halos (convert them to comoving units by dividing out the scale factor at that redshift)
rad0 = hal[0]['star.radius.90'][hiss[0]]
rad1 = hal[10]['star.radius.90'][hiss[1]]
rad2 = hal[20]['star.radius.90'][hiss[2]]
rad3 = hal[30]['star.radius.90'][hiss[3]]
rad4 = hal[40]['star.radius.90'][hiss[4]]
rad5 = hal[45]['star.radius.90'][hiss[5]]
rad6 = hal[50]['star.radius.90'][hiss[6]]
rad7 = hal[51]['star.radius.90'][hiss[7]]
rads = [rad0, rad1, rad2, rad3, rad4, rad5, rad6, rad7]
# Stars
# Create a mask for the particles within d kpc of the host at z = 0
mask = (part[0]['star'].prop('star.host.distance.total') < dist)
# Get the indices for the particles within d kpc of the host
sis_z0 = ut.array.get_indices(part[0]['star'].prop('star.host.distance.total'), [0, np.Inf])
sis_z0_h1 = sis_z0[mask]
# Get indices corresponding to these particles at other redshifts (use these like part[z]['star'][*][sis_z*_h1]) 
sis_z1_h1 = sis_at_z[9][:temp_point[9]['z0.star.number']][mask][sis_at_z[9][:temp_point[9]['z0.star.number']][mask] > 0]
sis_z2_h1 = sis_at_z[19][:temp_point[19]['z0.star.number']][mask][sis_at_z[19][:temp_point[19]['z0.star.number']][mask] > 0]
sis_z3_h1 = sis_at_z[29][:temp_point[29]['z0.star.number']][mask][sis_at_z[29][:temp_point[29]['z0.star.number']][mask] > 0]
sis_z4_h1 = sis_at_z[39][:temp_point[39]['z0.star.number']][mask][sis_at_z[39][:temp_point[39]['z0.star.number']][mask] > 0]
sis_z5_h1 = sis_at_z[44][:temp_point[44]['z0.star.number']][mask][sis_at_z[44][:temp_point[44]['z0.star.number']][mask] > 0]
sis_z6_h1 = sis_at_z[49][:temp_point[49]['z0.star.number']][mask][sis_at_z[49][:temp_point[49]['z0.star.number']][mask] > 0]
sis_z7_h1 = sis_at_z[50][:temp_point[50]['z0.star.number']][mask][sis_at_z[50][:temp_point[50]['z0.star.number']][mask] > 0]
sis = [sis_z0_h1, sis_z1_h1, sis_z2_h1, sis_z3_h1, sis_z4_h1, sis_z5_h1, sis_z6_h1, sis_z7_h1]
'''
 Find the centers of mass for each redshift by doing a median of the positions of the stars of interest
'''
centers = [np.mean(part[i]['star']['position'][sis[i]]*a[hs[i]], axis=0) for i in range(0, len(sis))]
# Calculate what distance encompasses 95% of the stars from the centers... divide by a to make it a comoving thing
d0 = np.percentile(part[0]['star'].prop('star.host.distance.total')[sis[0]], [5,95])[1]
d1 = np.percentile(part[1]['star'].prop('star.host.distance.total')[sis[1]], [5,95])[1]
d2 = np.percentile(part[2]['star'].prop('star.host.distance.total')[sis[2]], [5,95])[1]
d3 = np.percentile(part[3]['star'].prop('star.host.distance.total')[sis[3]], [5,95])[1]
d4 = np.percentile(part[4]['star'].prop('star.host.distance.total')[sis[4]], [5,95])[1]
d5 = np.percentile(part[5]['star'].prop('star.host.distance.total')[sis[5]], [5,95])[1]
d6 = np.percentile(part[6]['star'].prop('star.host.distance.total')[sis[6]], [5,95])[1]
d7 = np.percentile(part[7]['star'].prop('star.host.distance.total')[sis[7]], [5,95])[1]
ds = [d0,d1,d2,d3,d4,d5,d6,d7]
print(ds)
# Plot all of the data
bins = int(lims1*2 + 1)
p0 = []
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []
ps = [p0,p1,p2,p3,p4,p5,p6,p7]
for i in range(0, len(zs)):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.hist2d(part[zs[i]]['star']['position'][sis[zs[i]]][:,0]*a[hs[i]]-centers[zs[i]][0], part[zs[i]]['star']['position'][sis[zs[i]]][:,1]*a[hs[i]]-centers[zs[i]][1], bins=bins, weights=part[zs[i]]['star']['mass'][sis[zs[i]]], norm=colors.LogNorm())
    N = len(hal[hs[i]]['star.mass'][hiss[i]])
    for j in range(0, N):
        ps[zs[i]].append(patches.Circle((hal[hs[i]]['position'][hiss[i]][:,0][j]*a[hs[i]]-centers[i][0], hal[hs[i]]['position'][hiss[i]][:,1][j]*a[hs[i]]-centers[i][1]), rads[i][j], edgecolor='r', facecolor='none', linewidth=0.5, alpha=0.8))
        ax.add_artist(ps[i][j])
    ax.set_xlim([-lims1,lims1])
    ax.set_ylim([-lims1,lims1])
    plt.axes().set_aspect('equal')
    plt.savefig(home_dir_stam+'/scripts/plots/'+gal1+'_pos_'+distn+'_%s.pdf' % i)
    plt.close()

#################################################   Galaxy 2   ######################################################
# Halos
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind = []
for i in range(0, len(ratio_2)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_2[i][temp]
    mass_ind.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_2[i][temp]
        mass_ind[i] = temp
# Get the index for z = 0
ind0 = mass_index(hal[0]['star.mass'][his[0]], 1)
# Get the halo indices for halos within 5 times the most massive halo that also have contr. fracs. > 1%
m0 = (hal[0]['star.mass'][his[0]] > (1/5)*hal[0]['star.mass'][his[0]][ind0])
m1 = (hal[10]['star.mass'][his[10]] > (1/5)*hal[10]['star.mass'][his[10]][mass_ind[9]]) & (ratio_2[9] > 0.01)
m2 = (hal[20]['star.mass'][his[20]] > (1/5)*hal[20]['star.mass'][his[20]][mass_ind[19]]) & (ratio_2[19] > 0.01)
m3 = (hal[30]['star.mass'][his[30]] > (1/5)*hal[30]['star.mass'][his[30]][mass_ind[29]]) & (ratio_2[29] > 0.01)
m4 = (hal[40]['star.mass'][his[40]] > (1/5)*hal[40]['star.mass'][his[40]][mass_ind[39]]) & (ratio_2[39] > 0.01)
m5 = (hal[45]['star.mass'][his[45]] > (1/5)*hal[45]['star.mass'][his[45]][mass_ind[44]]) & (ratio_2[44] > 0.01)
m6 = (hal[50]['star.mass'][his[50]] > (1/5)*hal[50]['star.mass'][his[50]][mass_ind[49]]) & (ratio_2[49] > 0.01)
m7 = (hal[51]['star.mass'][his[51]] > (1/5)*hal[51]['star.mass'][his[51]][mass_ind[50]]) & (ratio_2[50] > 0.01)
# Only select halos that follow all of the rules thus far, plus, impose the 1% contribution cut!
his0 = his[0][m0]
his1 = his[10][m1]
his2 = his[20][m2]
his3 = his[30][m3]
his4 = his[40][m4]
his5 = his[45][m5]
his6 = his[50][m6]
his7 = his[51][m7]
hiss = [his0, his1, his2, his3, his4, his5, his6, his7]
# Get the radii of the halos (convert them to comoving units by dividing out the scale factor at that redshift)
rad0 = hal[0]['star.radius.90'][hiss[0]]
rad1 = hal[10]['star.radius.90'][hiss[1]]
rad2 = hal[20]['star.radius.90'][hiss[2]]
rad3 = hal[30]['star.radius.90'][hiss[3]]
rad4 = hal[40]['star.radius.90'][hiss[4]]
rad5 = hal[45]['star.radius.90'][hiss[5]]
rad6 = hal[50]['star.radius.90'][hiss[6]]
rad7 = hal[51]['star.radius.90'][hiss[7]]
rads = [rad0, rad1, rad2, rad3, rad4, rad5, rad6, rad7]
# Stars
# Create a mask for the particles within d kpc of the host at z = 0
mask = (part[0]['star'].prop('star.host2.distance.total') < dist)
# Get the indices for the particles within d kpc of the host
sis_z0 = ut.array.get_indices(part[0]['star'].prop('star.host2.distance.total'), [0, np.Inf])
sis_z0_h1 = sis_z0[mask]
# Get indices corresponding to these particles at other redshifts (use these like part[z]['star'][*][sis_z*_h1]) 
sis_z1_h1 = sis_at_z[9][:temp_point[9]['z0.star.number']][mask][sis_at_z[9][:temp_point[9]['z0.star.number']][mask] > 0]
sis_z2_h1 = sis_at_z[19][:temp_point[19]['z0.star.number']][mask][sis_at_z[19][:temp_point[19]['z0.star.number']][mask] > 0]
sis_z3_h1 = sis_at_z[29][:temp_point[29]['z0.star.number']][mask][sis_at_z[29][:temp_point[29]['z0.star.number']][mask] > 0]
sis_z4_h1 = sis_at_z[39][:temp_point[39]['z0.star.number']][mask][sis_at_z[39][:temp_point[39]['z0.star.number']][mask] > 0]
sis_z5_h1 = sis_at_z[44][:temp_point[44]['z0.star.number']][mask][sis_at_z[44][:temp_point[44]['z0.star.number']][mask] > 0]
sis_z6_h1 = sis_at_z[49][:temp_point[49]['z0.star.number']][mask][sis_at_z[49][:temp_point[49]['z0.star.number']][mask] > 0]
sis_z7_h1 = sis_at_z[50][:temp_point[50]['z0.star.number']][mask][sis_at_z[50][:temp_point[50]['z0.star.number']][mask] > 0]
sis = [sis_z0_h1, sis_z1_h1, sis_z2_h1, sis_z3_h1, sis_z4_h1, sis_z5_h1, sis_z6_h1, sis_z7_h1]
'''
 Find the centers of mass for each redshift by doing a median of the positions of the stars of interest
'''
centers = [np.mean(part[i]['star']['position'][sis[i]]*a[hs[i]], axis=0) for i in range(0, len(sis))]
# Calculate what distance encompasses 95% of the stars from the centers... divide by a to make it a comoving thing
d0 = np.percentile(part[0]['star'].prop('star.host2.distance.total')[sis[0]], [5,95])[1]
d1 = np.percentile(part[1]['star'].prop('star.host2.distance.total')[sis[1]], [5,95])[1]
d2 = np.percentile(part[2]['star'].prop('star.host2.distance.total')[sis[2]], [5,95])[1]
d3 = np.percentile(part[3]['star'].prop('star.host2.distance.total')[sis[3]], [5,95])[1]
d4 = np.percentile(part[4]['star'].prop('star.host2.distance.total')[sis[4]], [5,95])[1]
d5 = np.percentile(part[5]['star'].prop('star.host2.distance.total')[sis[5]], [5,95])[1]
d6 = np.percentile(part[6]['star'].prop('star.host2.distance.total')[sis[6]], [5,95])[1]
d7 = np.percentile(part[7]['star'].prop('star.host2.distance.total')[sis[7]], [5,95])[1]
ds = [d0,d1,d2,d3,d4,d5,d6,d7]
print(ds)
# Plot all of the data
bins = int(lims2*2 + 1)
p0 = []
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []
ps = [p0,p1,p2,p3,p4,p5,p6,p7]
for i in range(0, len(zs)):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.hist2d(part[zs[i]]['star']['position'][sis[zs[i]]][:,0]*a[hs[i]]-centers[zs[i]][0], part[zs[i]]['star']['position'][sis[zs[i]]][:,1]*a[hs[i]]-centers[zs[i]][1], bins=bins, weights=part[zs[i]]['star']['mass'][sis[zs[i]]], norm=colors.LogNorm())
    N = len(hal[hs[i]]['star.mass'][hiss[i]])
    for j in range(0, N):
        ps[zs[i]].append(patches.Circle((hal[hs[i]]['position'][hiss[i]][:,0][j]*a[hs[i]]-centers[i][0], hal[hs[i]]['position'][hiss[i]][:,1][j]*a[hs[i]]-centers[i][1]), rads[i][j], edgecolor='r', facecolor='none', linewidth=0.5, alpha=0.8))
        ax.add_artist(ps[i][j])
    ax.set_xlim([-lims2,lims2])
    ax.set_ylim([-lims2,lims2])
    plt.axes().set_aspect('equal')
    plt.savefig(home_dir_stam+'/scripts/plots/'+gal2+'_pos_'+distn+'_%s.pdf' % i)
    plt.close()

"""
#################################################   Galaxy 1   ######################################################
# Halos
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind = []
for i in range(0, len(ratio_1)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_1[i][temp]
    mass_ind.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_1[i][temp]
        mass_ind[i] = temp
# Get the index for z = 0
ind0 = mass_index(hal[0]['star.mass'][his[0]], 0)
# Get the halo indices for halos within 5 times the most massive halo that also have contr. fracs. > 1%
m0 = (hal[0]['star.mass'][his[0]] > (1/5)*hal[0]['star.mass'][his[0]][ind0])
m1 = (hal[10]['star.mass'][his[10]] > (1/5)*hal[10]['star.mass'][his[10]][mass_ind[9]]) & (ratio_1[9] > 0.01)
m2 = (hal[20]['star.mass'][his[20]] > (1/5)*hal[20]['star.mass'][his[20]][mass_ind[19]]) & (ratio_1[19] > 0.01)
m3 = (hal[30]['star.mass'][his[30]] > (1/5)*hal[30]['star.mass'][his[30]][mass_ind[29]]) & (ratio_1[29] > 0.01)
m4 = (hal[40]['star.mass'][his[40]] > (1/5)*hal[40]['star.mass'][his[40]][mass_ind[39]]) & (ratio_1[39] > 0.01)
m5 = (hal[45]['star.mass'][his[45]] > (1/5)*hal[45]['star.mass'][his[45]][mass_ind[44]]) & (ratio_1[44] > 0.01)
m6 = (hal[50]['star.mass'][his[50]] > (1/5)*hal[50]['star.mass'][his[50]][mass_ind[49]]) & (ratio_1[49] > 0.01)
m7 = (hal[51]['star.mass'][his[51]] > (1/5)*hal[51]['star.mass'][his[51]][mass_ind[50]]) & (ratio_1[50] > 0.01)
# Only select halos that follow all of the rules thus far, plus, impose the 1% contribution cut!
his0 = his[0][m0]
his1 = his[10][m1]
his2 = his[20][m2]
his3 = his[30][m3]
his4 = his[40][m4]
his5 = his[45][m5]
his6 = his[50][m6]
his7 = his[51][m7]
hiss = [his0, his1, his2, his3, his4, his5, his6, his7]
# Get the radii of the halos (convert them to comoving units by dividing out the scale factor at that redshift)
rad0 = hal[0]['star.radius.90'][hiss[0]]
rad1 = hal[10]['star.radius.90'][hiss[1]]
rad2 = hal[20]['star.radius.90'][hiss[2]]
rad3 = hal[30]['star.radius.90'][hiss[3]]
rad4 = hal[40]['star.radius.90'][hiss[4]]
rad5 = hal[45]['star.radius.90'][hiss[5]]
rad6 = hal[50]['star.radius.90'][hiss[6]]
rad7 = hal[51]['star.radius.90'][hiss[7]]
rads = [rad0, rad1, rad2, rad3, rad4, rad5, rad6, rad7]
# Stars
# Create a mask for the particles within 2-15 kpc of the host at z = 0
masks_d01 = (part[0]['star'].prop('star.host.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part[0]['star'].prop('star.host.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part[0]['star'].prop('star.host.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part[0]['star'].prop('star.host.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r = part[0]['star'].prop('star.host.velocity.principal.cylindrical')[:,0]
v_z = part[0]['star'].prop('star.host.velocity.principal.cylindrical')[:,1]
v_phi = part[0]['star'].prop('star.host.velocity.principal.cylindrical')[:,2]
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Combine all of the masks
mask = masks_pos & masks_v
# Get the indices for the particles within d kpc of the host
sis_z0 = ut.array.get_indices(part[0]['star'].prop('star.host.distance.total'), [0, np.Inf])
sis_z0_h1 = sis_z0[mask]
# Get indices corresponding to these particles at other redshifts (use these like part[z]['star'][*][sis_z*_h1]) 
sis_z1_h1 = sis_at_z[9][:temp_point[9]['z0.star.number']][mask][sis_at_z[9][:temp_point[9]['z0.star.number']][mask] > 0]
sis_z2_h1 = sis_at_z[19][:temp_point[19]['z0.star.number']][mask][sis_at_z[19][:temp_point[19]['z0.star.number']][mask] > 0]
sis_z3_h1 = sis_at_z[29][:temp_point[29]['z0.star.number']][mask][sis_at_z[29][:temp_point[29]['z0.star.number']][mask] > 0]
sis_z4_h1 = sis_at_z[39][:temp_point[39]['z0.star.number']][mask][sis_at_z[39][:temp_point[39]['z0.star.number']][mask] > 0]
sis_z5_h1 = sis_at_z[44][:temp_point[44]['z0.star.number']][mask][sis_at_z[44][:temp_point[44]['z0.star.number']][mask] > 0]
sis_z6_h1 = sis_at_z[49][:temp_point[49]['z0.star.number']][mask][sis_at_z[49][:temp_point[49]['z0.star.number']][mask] > 0]
sis_z7_h1 = sis_at_z[50][:temp_point[50]['z0.star.number']][mask][sis_at_z[50][:temp_point[50]['z0.star.number']][mask] > 0]
sis = [sis_z0_h1, sis_z1_h1, sis_z2_h1, sis_z3_h1, sis_z4_h1, sis_z5_h1, sis_z6_h1, sis_z7_h1]
'''
 Find the centers of mass for each redshift by doing a median of the positions of the stars of interest
'''
centers = [np.mean(part[i]['star']['position'][sis[i]]*a[hs[i]], axis=0) for i in range(0, len(sis))]
# Calculate what distance encompasses 95% of the stars from the centers... divide by a to make it a comoving thing
d0 = np.percentile(part[0]['star'].prop('star.host.distance.total')[sis[0]], [5,95])[1]
d1 = np.percentile(part[1]['star'].prop('star.host.distance.total')[sis[1]], [5,95])[1]
d2 = np.percentile(part[2]['star'].prop('star.host.distance.total')[sis[2]], [5,95])[1]
d3 = np.percentile(part[3]['star'].prop('star.host.distance.total')[sis[3]], [5,95])[1]
d4 = np.percentile(part[4]['star'].prop('star.host.distance.total')[sis[4]], [5,95])[1]
d5 = np.percentile(part[5]['star'].prop('star.host.distance.total')[sis[5]], [5,95])[1]
d6 = np.percentile(part[6]['star'].prop('star.host.distance.total')[sis[6]], [5,95])[1]
d7 = np.percentile(part[7]['star'].prop('star.host.distance.total')[sis[7]], [5,95])[1]
ds = [d0,d1,d2,d3,d4,d5,d6,d7]
print(ds)
# Plot all of the data
bins = int(lims1*2 + 1)
p0 = []
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []
ps = [p0,p1,p2,p3,p4,p5,p6,p7]
for i in range(0, len(zs)):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.hist2d(part[zs[i]]['star']['position'][sis[zs[i]]][:,0]*a[hs[i]]-centers[zs[i]][0], part[zs[i]]['star']['position'][sis[zs[i]]][:,1]*a[hs[i]]-centers[zs[i]][1], bins=bins, weights=part[zs[i]]['star']['mass'][sis[zs[i]]], norm=colors.LogNorm())
    N = len(hal[hs[i]]['star.mass'][hiss[i]])
    for j in range(0, N):
        ps[zs[i]].append(patches.Circle((hal[hs[i]]['position'][hiss[i]][:,0][j]*a[hs[i]]-centers[i][0], hal[hs[i]]['position'][hiss[i]][:,1][j]*a[hs[i]]-centers[i][1]), rads[i][j], edgecolor='r', facecolor='none', linewidth=0.5, alpha=0.8))
        ax.add_artist(ps[i][j])
    ax.set_xlim([-lims1,lims1])
    ax.set_ylim([-lims1,lims1])
    plt.axes().set_aspect('equal')
    plt.savefig(home_dir_stam+'/scripts/plots/'+gal1+'_pos_'+distn+'_%s.pdf' % i)
    plt.close()

#################################################   Galaxy 2   ######################################################
# Halos
'''
 Find the indices of the most massive halos at each redshift that contribute > 1% of stars to host at z = 0

    mass_ind : List
        mass_ind[i] corresponds to the index of the most massive halo at redshifts[i+1] that contributes > 1%

    Method:
        1. Set initial guess that it will be the most massive
        2. Check whether it contributes > 1%
        3. If it doesn't, move to the second most massive.
           Check, repeat...
'''
mass_ind = []
for i in range(0, len(ratio_2)):
    m = 0
    temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
    temp2 = ratio_2[i][temp]
    mass_ind.append(temp)
    while (temp2 <= 0.01):
        m += 1
        temp = mass_index(hal[i+1]['star.mass'][his[i+1]], m)
        temp2 = ratio_2[i][temp]
        mass_ind[i] = temp
# Get the index for z = 0
ind0 = mass_index(hal[0]['star.mass'][his[0]], 1)
# Get the halo indices for halos within 5 times the most massive halo that also have contr. fracs. > 1%
m0 = (hal[0]['star.mass'][his[0]] > (1/5)*hal[0]['star.mass'][his[0]][ind0])
m1 = (hal[10]['star.mass'][his[10]] > (1/5)*hal[10]['star.mass'][his[10]][mass_ind[9]]) & (ratio_2[9] > 0.01)
m2 = (hal[20]['star.mass'][his[20]] > (1/5)*hal[20]['star.mass'][his[20]][mass_ind[19]]) & (ratio_2[19] > 0.01)
m3 = (hal[30]['star.mass'][his[30]] > (1/5)*hal[30]['star.mass'][his[30]][mass_ind[29]]) & (ratio_2[29] > 0.01)
m4 = (hal[40]['star.mass'][his[40]] > (1/5)*hal[40]['star.mass'][his[40]][mass_ind[39]]) & (ratio_2[39] > 0.01)
m5 = (hal[45]['star.mass'][his[45]] > (1/5)*hal[45]['star.mass'][his[45]][mass_ind[44]]) & (ratio_2[44] > 0.01)
m6 = (hal[50]['star.mass'][his[50]] > (1/5)*hal[50]['star.mass'][his[50]][mass_ind[49]]) & (ratio_2[49] > 0.01)
m7 = (hal[51]['star.mass'][his[51]] > (1/5)*hal[51]['star.mass'][his[51]][mass_ind[50]]) & (ratio_2[50] > 0.01)
# Only select halos that follow all of the rules thus far, plus, impose the 1% contribution cut!
his0 = his[0][m0]
his1 = his[10][m1]
his2 = his[20][m2]
his3 = his[30][m3]
his4 = his[40][m4]
his5 = his[45][m5]
his6 = his[50][m6]
his7 = his[51][m7]
hiss = [his0, his1, his2, his3, his4, his5, his6, his7]
# Get the radii of the halos (convert them to comoving units by dividing out the scale factor at that redshift)
rad0 = hal[0]['star.radius.90'][hiss[0]]
rad1 = hal[10]['star.radius.90'][hiss[1]]
rad2 = hal[20]['star.radius.90'][hiss[2]]
rad3 = hal[30]['star.radius.90'][hiss[3]]
rad4 = hal[40]['star.radius.90'][hiss[4]]
rad5 = hal[45]['star.radius.90'][hiss[5]]
rad6 = hal[50]['star.radius.90'][hiss[6]]
rad7 = hal[51]['star.radius.90'][hiss[7]]
rads = [rad0, rad1, rad2, rad3, rad4, rad5, rad6, rad7]
# Stars
# Create a mask for the particles within 2-15 kpc of the host at z = 0
masks_d01 = (part[0]['star'].prop('star.host2.distance.principal.cylindrical')[:,0] < 15)
masks_d02 = (part[0]['star'].prop('star.host2.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03 = (part[0]['star'].prop('star.host2.distance.principal.cylindrical')[:,1] < 2)
masks_d04 = (part[0]['star'].prop('star.host2.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r = part[0]['star'].prop('star.host2.velocity.principal.cylindrical')[:,0]
v_z = part[0]['star'].prop('star.host2.velocity.principal.cylindrical')[:,1]
v_phi = part[0]['star'].prop('star.host2.velocity.principal.cylindrical')[:,2]
masks_pos = masks_d01 & masks_d02 & masks_d03 & masks_d04
v_rot = np.median(v_phi[masks_pos])
R = np.sqrt((v_phi - v_rot)**2 + v_r**2 + v_z**2)
masks_v = (R < v_rot)
# Combine all of the masks
mask = masks_pos & masks_v
# Get the indices for the particles within d kpc of the host
sis_z0 = ut.array.get_indices(part[0]['star'].prop('star.host2.distance.total'), [0, np.Inf])
sis_z0_h1 = sis_z0[mask]
# Get indices corresponding to these particles at other redshifts (use these like part[z]['star'][*][sis_z*_h1]) 
sis_z1_h1 = sis_at_z[9][:temp_point[9]['z0.star.number']][mask][sis_at_z[9][:temp_point[9]['z0.star.number']][mask] > 0]
sis_z2_h1 = sis_at_z[19][:temp_point[19]['z0.star.number']][mask][sis_at_z[19][:temp_point[19]['z0.star.number']][mask] > 0]
sis_z3_h1 = sis_at_z[29][:temp_point[29]['z0.star.number']][mask][sis_at_z[29][:temp_point[29]['z0.star.number']][mask] > 0]
sis_z4_h1 = sis_at_z[39][:temp_point[39]['z0.star.number']][mask][sis_at_z[39][:temp_point[39]['z0.star.number']][mask] > 0]
sis_z5_h1 = sis_at_z[44][:temp_point[44]['z0.star.number']][mask][sis_at_z[44][:temp_point[44]['z0.star.number']][mask] > 0]
sis_z6_h1 = sis_at_z[49][:temp_point[49]['z0.star.number']][mask][sis_at_z[49][:temp_point[49]['z0.star.number']][mask] > 0]
sis_z7_h1 = sis_at_z[50][:temp_point[50]['z0.star.number']][mask][sis_at_z[50][:temp_point[50]['z0.star.number']][mask] > 0]
sis = [sis_z0_h1, sis_z1_h1, sis_z2_h1, sis_z3_h1, sis_z4_h1, sis_z5_h1, sis_z6_h1, sis_z7_h1]
'''
 Find the centers of mass for each redshift by doing a median of the positions of the stars of interest
'''
centers = [np.mean(part[i]['star']['position'][sis[i]]*a[hs[i]], axis=0) for i in range(0, len(sis))]
# Calculate what distance encompasses 95% of the stars from the centers... divide by a to make it a comoving thing
d0 = np.percentile(part[0]['star'].prop('star.host2.distance.total')[sis[0]], [5,95])[1]
d1 = np.percentile(part[1]['star'].prop('star.host2.distance.total')[sis[1]], [5,95])[1]
d2 = np.percentile(part[2]['star'].prop('star.host2.distance.total')[sis[2]], [5,95])[1]
d3 = np.percentile(part[3]['star'].prop('star.host2.distance.total')[sis[3]], [5,95])[1]
d4 = np.percentile(part[4]['star'].prop('star.host2.distance.total')[sis[4]], [5,95])[1]
d5 = np.percentile(part[5]['star'].prop('star.host2.distance.total')[sis[5]], [5,95])[1]
d6 = np.percentile(part[6]['star'].prop('star.host2.distance.total')[sis[6]], [5,95])[1]
d7 = np.percentile(part[7]['star'].prop('star.host2.distance.total')[sis[7]], [5,95])[1]
ds = [d0,d1,d2,d3,d4,d5,d6,d7]
print(ds)
# Plot all of the data
bins = int(lims2*2 + 1)
p0 = []
p1 = []
p2 = []
p3 = []
p4 = []
p5 = []
p6 = []
p7 = []
ps = [p0,p1,p2,p3,p4,p5,p6,p7]
for i in range(0, len(zs)):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.hist2d(part[zs[i]]['star']['position'][sis[zs[i]]][:,0]*a[hs[i]]-centers[zs[i]][0], part[zs[i]]['star']['position'][sis[zs[i]]][:,1]*a[hs[i]]-centers[zs[i]][1], bins=bins, weights=part[zs[i]]['star']['mass'][sis[zs[i]]], norm=colors.LogNorm())
    N = len(hal[hs[i]]['star.mass'][hiss[i]])
    for j in range(0, N):
        ps[zs[i]].append(patches.Circle((hal[hs[i]]['position'][hiss[i]][:,0][j]*a[hs[i]]-centers[i][0], hal[hs[i]]['position'][hiss[i]][:,1][j]*a[hs[i]]-centers[i][1]), rads[i][j], edgecolor='r', facecolor='none', linewidth=0.5, alpha=0.8))
        ax.add_artist(ps[i][j])
    ax.set_xlim([-lims2,lims2])
    ax.set_ylim([-lims2,lims2])
    plt.axes().set_aspect('equal')
    plt.savefig(home_dir_stam+'/scripts/plots/'+gal2+'_pos_'+distn+'_%s.pdf' % i)
    plt.close()
"""