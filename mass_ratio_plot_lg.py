#!/usr/bin/python3

"""
 ======================
 = Mass Ratio Plot LG =
 ======================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Break, 2018

 Goal: Read in the mass ratios for a galaxy and plot them for:
       - M2/M1
       - M3/M1
       - M4/M1
       in different line thicknesses 
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
	resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
	resolution = '_res4000'
else:
	print('Which galaxies are you working on??')
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

# Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/mass_ratios_all.p', 'rb')
massb = pickle.load(File1)
massc = pickle.load(File1)
massf = pickle.load(File1)
massi = pickle.load(File1)
massm = pickle.load(File1)
massw = pickle.load(File1)
massrom = pickle.load(File1)
massjul = pickle.load(File1)
massthe = pickle.load(File1)
masslou = pickle.load(File1)
massromu = pickle.load(File1)
massrem = pickle.load(File1)
File1.close()

# Get the right data
# Galaxy 1
mass300_1 = massromu[0]
mass15_1 = massromu[1]
mass2_1 = massromu[2]
mass415_1 = massromu[3]
# Galaxy 2
mass300_2 = massrem[0]
mass15_2 = massrem[1]
mass2_2 = massrem[2]
mass415_2 = massrem[3]

colors = dc.get_distinct(1)
# GALAXY 1
# d = [0, 300 kpc]
# Plot the ratios
two_one_ratio = mass300_1[0]
three_one_ratio = mass300_1[1]
four_one_ratio = mass300_1[2]

window_size, poly_order = 41, 3
mr300_1_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr300_1_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal1+', d = [0, 300 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_300kpc_1percent_'+gal1+'_v5.pdf')

# d = [0, 15 kpc]
# Plot the ratios
two_one_ratio = mass15_1[0]
three_one_ratio = mass15_1[1]
four_one_ratio = mass15_1[2]

window_size, poly_order = 41, 3
mr15_1_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(2)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr15_1_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal1+', d = [0, 15 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_15kpc_1percent_'+gal1+'_v5.pdf')

# d = [0, 2 kpc]
# Plot the ratios
two_one_ratio = mass2_1[0]
three_one_ratio = mass2_1[1]
four_one_ratio = mass2_1[2]

window_size, poly_order = 41, 3
mr2_1_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(3)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr2_1_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal1+', d = [0, 2 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_2kpc_1percent_'+gal1+'_v5.pdf')

# d = [4, 15 kpc]
# Plot the ratios
two_one_ratio = mass415_1[0]
three_one_ratio = mass415_1[1]
four_one_ratio = mass415_1[2]

window_size, poly_order = 41, 3
mr415_1_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr415_1_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal1+', d = [4, 15 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_415kpc_1percent_'+gal1+'_v5.pdf')

# GALAXY 2
# d = [0, 300 kpc]
# Plot the ratios
two_one_ratio = mass300_2[0]
three_one_ratio = mass300_2[1]
four_one_ratio = mass300_2[2]

window_size, poly_order = 41, 3
mr300_2_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(5)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr300_2_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal2+', d = [0, 300 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_300kpc_1percent_'+gal2+'_v5.pdf')

# d = [0, 15 kpc]
# Plot the ratios
two_one_ratio = mass15_2[0]
three_one_ratio = mass15_2[1]
four_one_ratio = mass15_2[2]

window_size, poly_order = 41, 3
mr15_2_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(6)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr15_2_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal2+', d = [0, 15 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_15kpc_1percent_'+gal2+'_v5.pdf')

# d = [0, 2 kpc]
# Plot the ratios
two_one_ratio = mass2_2[0]
three_one_ratio = mass2_2[1]
four_one_ratio = mass2_2[2]

window_size, poly_order = 41, 3
mr2_2_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(7)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr2_2_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal2+', d = [0, 2 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_2kpc_1percent_'+gal2+'_v5.pdf')

# d = [4, 15 kpc]
# Plot the ratios
two_one_ratio = mass415_2[0]
three_one_ratio = mass415_2[1]
four_one_ratio = mass415_2[2]

window_size, poly_order = 41, 3
mr415_2_smooth = savgol_filter(two_one_ratio, window_size, poly_order)

plt.figure(8)
plt.figure(figsize=(10, 8))
plt.plot(redshifts, two_one_ratio, color=colors[0], linewidth=4.0, label='$M_{2}/M_{1}$')
plt.plot(redshifts, three_one_ratio, color=colors[0], linewidth=2.5, label='$M_{3}/M_{1}$')
plt.plot(redshifts, four_one_ratio, color=colors[0], linewidth=1.0, label='$M_{4}/M_{1}$')
plt.plot(redshifts, mr415_2_smooth, color='k', linewidth=4.0, label='Smoothed $M_{2}/M_{1}$')
plt.xlabel('$z$', fontsize=25)
plt.ylabel('Mass Ratios', fontsize=25)
plt.title(gal2+', d = [4, 15 kpc]', fontsize=25)
plt.legend(prop={'size': 20})
plt.tick_params(axis='both', which='major', labelsize=20)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/mr_415kpc_1percent_'+gal2+'_v5.pdf')