#!/usr/bin/python3

"""
 ================================
 = Halo properties for galaxies =
 ================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: Determine the contribution fractions for halos at each redshift for a galaxy
       pair and write these to a file for the following distance cuts:

       d = [0, 300 kpc]
       d = [0, 15 kpc]
       d = [0, 2 kpc] 
       d = [4, 15 kpc]

       The contribution fractions are defined as the following ratio:

            (Number of stars in a halo at some z' that end up within D kpc of the host at z = 0)
            ------------------------------------------------------------------------------------
                             (Total number of stars in a halo at some z')

 NOTES:
 ------
 - This generates files named: 
    - contribution_+'distance'+_fullres_+'gal1'+.p
    - contribution_+'distance'+_fullres_+'gal2'+.p
 - This file only works on the LG pair galaxies
"""

### Import all of the tools for analysis
import halo_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import pickle
print('Read in the tools')

#### Read in the halo information
gal1 = 'Romulus'
gal2 = 'Remus'
galaxy = 'm12_elvis_'+gal1+gal2
if gal1 == 'Romeo':
    resolution = '_res3500'
elif gal1 == 'Thelma' or 'Romulus':
    resolution = '_res4000'
else:
    print('Which galaxies are you working on??')
simulation_dir = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir, all_snapshot_list=False)
# Reverse the array so that it starts with z = 0
hal = np.flip(hal)
print('Read in the halos')

#### Some functions for analysis
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

    z: redshift
    ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    ind = ut.array.get_indices(hal[z].prop('mass.bound / mass'), [0.4, np.Inf], ind)
    return ind
print('Defined halo index function')

#### Analysis

# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
print('Defined halo indices')

# Read in the data of stars at z = 0
part = gizmo.io.Read.read_snapshots('star', 'redshift', 0, simulation_directory=simulation_dir, host_number=2, assign_host_principal_axes=True)
# Get the snapshot indices from the halo info
snapind = [hal[i].snapshot['index'] for i in range(1, len(redshifts))]
# Get pointers at other redshifts
temp = [gizmo.track.ParticlePointerIO.io_pointers(snapshot_index=i, track_directory=simulation_dir+'/track') for i in snapind]
# I don't actually need the regular pointers anymore...
#sis_at_z = [temp[i]['z0.to.z.index'] for i in range(0, len(snapind))]
# Get all of the reverse pointers together in an array/list
sis_at_z_rev = [temp[i].get_pointers('star','star',forward=True) for i in range(0, len(snapind))]
print('Read in stars at z = 0 and their reverse pointers')

"""
 Get the number of halos at each redshift. This is used to loop over redshift in the contribution fraction calculations.
    
    N: A list of lists
    
       Each element, N[i], corresponds to redshifts[i] and gives the number of
       halos at that redshift
"""
N = [len(hal[i]['star.mass'][his[i]]) for i in range(0, len(redshifts))]
print('Calculated the number of halos at each redshift')

"""
 Get the number of stars in each halo at each redshift. This is used in the contribution fraction calculation
 
    N_tot: A list of lists
    
           Each element, N_tot[i], corresponds to redshifts[i] and gives the number of stars
           in each halo for that redshift
"""
N_tot = [[len(hal[i]['star.indices'][his[i]][j]) for j in range(0, N[i])] for i in range(0, len(redshifts))]
print('Calculated the number of stars in halos at each redshift')

### Loop over the distance list to calculate the contribution fractions
dist_list = [300, 15, 2]
ratios_1 = []
ratios_2 = []
for d in dist_list:

    """
     Find the number of stars that merge into the host at z = 0 as well as the ratio of merged/tot stars for each halo

        r_tot_300: A list of lists

                   Each element r_tot_300[i] corresponds to redshifts[i] and gives the fraction of stars in each halo j
                   that merge into the host (or within 300 kpc of it) by z = 0

        Method:
                1. Create some empty lists to store the quantities that I want
                2. Loop over redshifts (except for z = 0)
                3. Create temporary vectors to later append to the empty lists above
                   Create a mask for only stars in some distance cut
                   Using that mask, select only the stars that existed at redshifts[i+1]
                4. Loop over the number of halos at redshifts[i+1]
                5. Using the mask, get the stars that belong to each halo and sum them up
                6. Divide the sum by the total number of stars in that halo to get the ratio

        Some quantities:
                        masks_d0 : mask for only the stars that are within some distance at z = 0
                        masks_d0_at_z : re-ordered mask corresponding to the particles at redshifts[i+1]
    """
    ratio_temp_1 = []
    ratio_temp_2 = []
    masks_d0_1 = (part['star'].prop('star.host.distance.total') < d)
    masks_d0_2 = (part['star'].prop('star.host2.distance.total') < d)
    for i in range(0, len(snapind)):
        n_merge_1 = np.zeros(N[i+1])
        n_merge_2 = np.zeros(N[i+1])
        rr_1 = np.zeros(N[i+1])
        rr_2 = np.zeros(N[i+1])
        
        # Fix for the negative forward pointers (Only occurs with R&R)
        negatives = (sis_at_z_rev[i] < 0)
        sis_at_z_rev[i][negatives] = 10537 # point them to a particle within the main galaxy at z = 0
        
        masks_d0_at_z_1 = masks_d0_1[sis_at_z_rev[i]]
        masks_d0_at_z_2 = masks_d0_2[sis_at_z_rev[i]]
        for j in range(0, N[i+1]):
            n_merge_1[j] = np.sum(masks_d0_at_z_1[hal[i+1]['star.indices'][his[i+1]][j]])
            n_merge_2[j] = np.sum(masks_d0_at_z_2[hal[i+1]['star.indices'][his[i+1]][j]])
            rr_1[j] = n_merge_1[j]/N_tot[i+1][j]
            rr_2[j] = n_merge_2[j]/N_tot[i+1][j]
        ratio_temp_1.append(rr_1)
        ratio_temp_2.append(rr_2)
    ratios_1.append(ratio_temp_1)
    ratios_2.append(ratio_temp_2)
    print('Done with %d kpc' % d)

## d = [4, 15 kpc]
"""
 Find the number of stars that merge into the host at z = 0 as well as the ratio of merged/tot stars for each halo
                     
    ratio_temp_*: A list of lists
    
               Each element ratio_temp_*[i] corresponds to redshifts[i] and gives the fraction of stars in each halo j
               that merge into the host (or within 4-15 kpc of it) by z = 0
               
    Method:
            1. Create some empty lists to store the quantities that I want
            2. Creat a position and velocity mask
            3. Loop over redshifts (except for z = 0)
            4. Create temporary vectors to later append to the empty lists above
               Create a mask for only stars in some distance cut
               Using that mask, select only the stars that existed at redshifts[i+1]
            5. Loop over the number of halos at redshifts[i+1]
            6. Using the mask, get the stars that belong to each halo and sum them up
            7. Divide the sum by the total number of stars in that halo to get the ratio
            
    Some quantities:
                    masks_d0 : mask for only the stars that are within some distance at z = 0
                    masks_d0_at_z : re-ordered mask corresponding to the particles at redshifts[i+1]
"""
ratio_temp_1 = []
ratio_temp_2 = []
# Host 1
# Create masks for R
masks_d01_1 = (part['star'].prop('host.distance.principal.cylindrical')[:,0] < 15)
masks_d02_1 = (part['star'].prop('host.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03_1 = (part['star'].prop('host.distance.principal.cylindrical')[:,1] < 2)
masks_d04_1 = (part['star'].prop('host.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r_1 = part['star'].prop('host.velocity.principal.cylindrical')[:,0]
v_z_1 = part['star'].prop('host.velocity.principal.cylindrical')[:,1]
v_phi_1 = part['star'].prop('host.velocity.principal.cylindrical')[:,2]
masks_pos_1 = masks_d01_1 & masks_d02_1 & masks_d03_1 & masks_d04_1
v_rot_1 = np.median(v_phi_1[masks_pos_1])
R_1 = np.sqrt((v_phi_1 - v_rot_1)**2 + v_r_1**2 + v_z_1**2)
masks_v_1 = (R_1 < v_rot_1)
# Combine all of the masks
masks_d0_1 = masks_pos_1 & masks_v_1

# Host 2
# Create masks for R
masks_d01_2 = (part['star'].prop('host2.distance.principal.cylindrical')[:,0] < 15)
masks_d02_2 = (part['star'].prop('host2.distance.principal.cylindrical')[:,0] > 4)
# Create masks for Z
masks_d03_2 = (part['star'].prop('host2.distance.principal.cylindrical')[:,1] < 2)
masks_d04_2 = (part['star'].prop('host2.distance.principal.cylindrical')[:,1] > -2)
# Create velocity mask & calculate the rotational velocity
v_r_2 = part['star'].prop('host2.velocity.principal.cylindrical')[:,0]
v_z_2 = part['star'].prop('host2.velocity.principal.cylindrical')[:,1]
v_phi_2 = part['star'].prop('host2.velocity.principal.cylindrical')[:,2]
masks_pos_2 = masks_d01_2 & masks_d02_2 & masks_d03_2 & masks_d04_2
v_rot_2 = np.median(v_phi_2[masks_pos_2])
R_2 = np.sqrt((v_phi_2 - v_rot_2)**2 + v_r_2**2 + v_z_2**2)
masks_v_2 = (R_2 < v_rot_2)
# Combine all of the masks
masks_d0_2 = masks_pos_2 & masks_v_2

for i in range(0, len(snapind)):
    n_merge_1 = np.zeros(N[i+1])
    n_merge_2 = np.zeros(N[i+1])
    rr_1 = np.zeros(N[i+1])
    rr_2 = np.zeros(N[i+1])
    
    # Fix for the negative forward pointers
    negatives = (sis_at_z_rev[i] < 0)
    sis_at_z_rev[i][negatives] = 10537 # point them to a particle within the main galaxy at z = 0
    
    masks_d0_at_z_1 = masks_d0_1[sis_at_z_rev[i]]
    masks_d0_at_z_2 = masks_d0_2[sis_at_z_rev[i]]
    for j in range(0, N[i+1]):
        n_merge_1[j] = np.sum(masks_d0_at_z_1[hal[i+1]['star.indices'][his[i+1]][j]])
        n_merge_2[j] = np.sum(masks_d0_at_z_2[hal[i+1]['star.indices'][his[i+1]][j]])
        rr_1[j] = n_merge_1[j]/N_tot[i+1][j]
        rr_2[j] = n_merge_2[j]/N_tot[i+1][j]
    ratio_temp_1.append(rr_1)
    ratio_temp_2.append(rr_2)
ratios_1.append(ratio_temp_1)
ratios_2.append(ratio_temp_2)
print('Rotational velocity of', v_rot_1, 'km/s for', gal1)
print('Rotational velocity of', v_rot_2, 'km/s for', gal2)
print('Done with 4-15 kpc')

"""
 Write the ratios to a file
 
 How to call/read the file:
    
    Filepp = open("contribution_300_fullres.p", "rb")
    test = pickle.load(Filepp)
    Filepp.close()
    
    'test' will be the variable which is a list of arrays containing the contribution fractions for each halo in each redshift
    test[0]   : the contribution fractions for the first redshift (z = 0.1)
    test[0][0]: the contribution fraction for the first halo in the first redshift, etc.
"""

r_tot_300_1 = ratios_1[0]
r_tot_15_1 = ratios_1[1]
r_tot_2_1 = ratios_1[2]
r_tot_415_1 = ratios_1[3]
r_tot_300_2 = ratios_2[0]
r_tot_15_2 = ratios_2[1]
r_tot_2_2 = ratios_2[2]
r_tot_415_2 = ratios_2[3]

Filep1 = open(home_dir+"/scripts/pickles/contribution_300_fullres_"+gal1+".p", "wb")
pickle.dump(r_tot_300_1, Filep1)
Filep1.close()

Filep2 = open(home_dir+"/scripts/pickles/contribution_15_fullres_"+gal1+".p", "wb")
pickle.dump(r_tot_15_1, Filep2)
Filep2.close()

Filep3 = open(home_dir+"/scripts/pickles/contribution_2_fullres_"+gal1+".p", "wb")
pickle.dump(r_tot_2_1, Filep3)
Filep3.close()

Filep4 = open(home_dir+"/scripts/pickles/contribution_415_fullres_"+gal1+".p", "wb")
pickle.dump(r_tot_415_1, Filep4)
Filep4.close()

Filep5 = open(home_dir+"/scripts/pickles/contribution_300_fullres_"+gal2+".p", "wb")
pickle.dump(r_tot_300_2, Filep5)
Filep5.close()

Filep6 = open(home_dir+"/scripts/pickles/contribution_15_fullres_"+gal2+".p", "wb")
pickle.dump(r_tot_15_2, Filep6)
Filep6.close()

Filep7 = open(home_dir+"/scripts/pickles/contribution_2_fullres_"+gal2+".p", "wb")
pickle.dump(r_tot_2_2, Filep7)
Filep7.close()

Filep8 = open(home_dir+"/scripts/pickles/contribution_415_fullres_"+gal2+".p", "wb")
pickle.dump(r_tot_415_2, Filep8)
Filep8.close()