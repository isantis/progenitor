#!/usr/bin/python3

"""
 =================
 = Insitu Padder =
 =================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2019

 Goal: Read in the insitu fractions and pad the data at z = 0 to smooth better later on

 NOTE: These use the files made from "in_ex_v2.py" or "in_ex_v3.py" or "in_ex_lg.py"
"""

#### Import all of the tools for analysis
import numpy as np
import pickle

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12b.p', 'rb')
ie300b = pickle.load(File1)
ie15b = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12c.p', 'rb')
ie300c = pickle.load(File2)
ie15c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12f.p', 'rb')
ie300f = pickle.load(File3)
ie15f = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12i.p', 'rb')
ie300i = pickle.load(File4)
ie15i = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12m.p', 'rb')
ie300m = pickle.load(File5)
ie15m = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12w.p', 'rb')
ie300w = pickle.load(File6)
ie15w = pickle.load(File6)
File6.close()

# m12b
File7 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12b_v2.p', 'rb')
ie2b = pickle.load(File7)
ie415b = pickle.load(File7)
File7.close()
# m12c
File8 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12c_v2.p', 'rb')
ie2c = pickle.load(File8)
ie415c = pickle.load(File8)
File8.close()
# m12f
File9 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12f_v2.p', 'rb')
ie2f = pickle.load(File9)
ie415f = pickle.load(File9)
File9.close()
# m12i
File10 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12i_v2.p', 'rb')
ie2i = pickle.load(File10)
ie415i = pickle.load(File10)
File10.close()
# m12m
File11 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12m_v2.p', 'rb')
ie2m = pickle.load(File11)
ie415m = pickle.load(File11)
File11.close()
# m12w
File12 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_m12w_v2.p', 'rb')
ie2w = pickle.load(File12)
ie415w = pickle.load(File12)
File12.close()

# Romeo
File13 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romeo300.p', 'rb')
ie300rom = pickle.load(File13)
File13.close()
File14 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romeo15.p', 'rb')
ie15rom = pickle.load(File14)
File14.close()
File15 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romeo2.p', 'rb')
ie2rom = pickle.load(File15)
File15.close()
File16 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romeo415.p', 'rb')
ie415rom = pickle.load(File16)
File16.close()

# Juliet
File17 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Juliet300.p', 'rb')
ie300jul = pickle.load(File17)
File17.close()
File18 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Juliet15.p', 'rb')
ie15jul = pickle.load(File18)
File18.close()
File19 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Juliet2.p', 'rb')
ie2jul = pickle.load(File19)
File19.close()
File20 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Juliet415.p', 'rb')
ie415jul = pickle.load(File20)
File20.close()

# Thelma
File21 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Thelma300.p', 'rb')
ie300the = pickle.load(File21)
File21.close()
File22 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Thelma15.p', 'rb')
ie15the = pickle.load(File22)
File22.close()
File23 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Thelma2.p', 'rb')
ie2the = pickle.load(File23)
File23.close()
File24 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Thelma415.p', 'rb')
ie415the = pickle.load(File24)
File24.close()

# Louise
File25 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Louise300.p', 'rb')
ie300lou = pickle.load(File25)
File25.close()
File26 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Louise15.p', 'rb')
ie15lou = pickle.load(File26)
File26.close()
File27 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Louise2.p', 'rb')
ie2lou = pickle.load(File27)
File27.close()
File28 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Louise415.p', 'rb')
ie415lou = pickle.load(File28)
File28.close()

# Romulus
File25 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romulus300.p', 'rb')
ie300romu = pickle.load(File25)
File25.close()
File26 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romulus15.p', 'rb')
ie15romu = pickle.load(File26)
File26.close()
File27 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romulus2.p', 'rb')
ie2romu = pickle.load(File27)
File27.close()
File28 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Romulus415.p', 'rb')
ie415romu = pickle.load(File28)
File28.close()

# Remus
File25 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Remus300.p', 'rb')
ie300rem = pickle.load(File25)
File25.close()
File26 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Remus15.p', 'rb')
ie15rem = pickle.load(File26)
File26.close()
File27 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Remus2.p', 'rb')
ie2rem = pickle.load(File27)
File27.close()
File28 = open(home_dir_stam+'/scripts/pickles/in_ex_1percent_Remus415.p', 'rb')
ie415rem = pickle.load(File28)
File28.close()

ieb = [ie300b[0], ie15b[0], ie2b[0], ie415b[0]]
ieb_new = []
for i in range(0, len(ieb)):
    ieb_new.append(np.append(np.ones(20)*ieb[i][0], ieb[i]))

iec = [ie300c[0], ie15c[0], ie2c[0], ie415c[0]]
iec_new = []
for i in range(0, len(iec)):
    iec_new.append(np.append(np.ones(20)*iec[i][0], iec[i]))

ief = [ie300f[0], ie15f[0], ie2f[0], ie415f[0]]
ief_new = []
for i in range(0, len(ief)):
    ief_new.append(np.append(np.ones(20)*ief[i][0], ief[i]))

iei = [ie300i[0], ie15i[0], ie2i[0], ie415i[0]]
iei_new = []
for i in range(0, len(iei)):
    iei_new.append(np.append(np.ones(20)*iei[i][0], iei[i]))

iem = [ie300m[0], ie15m[0], ie2m[0], ie415m[0]]
iem_new = []
for i in range(0, len(iem)):
    iem_new.append(np.append(np.ones(20)*iem[i][0], iem[i]))

iew = [ie300w[0], ie15w[0], ie2w[0], ie415w[0]]
iew_new = []
for i in range(0, len(iew)):
    iew_new.append(np.append(np.ones(20)*iew[i][0], iew[i]))

ierom = [ie300rom[0], ie15rom[0], ie2rom[0], ie415rom[0]]
ierom_new = []
for i in range(0, len(ierom)):
    ierom_new.append(np.append(np.ones(20)*ierom[i][0], ierom[i]))

iejul = [ie300jul[0], ie15jul[0], ie2jul[0], ie415jul[0]]
iejul_new = []
for i in range(0, len(iejul)):
    iejul_new.append(np.append(np.ones(20)*iejul[i][0], iejul[i]))

iethe = [ie300the[0], ie15the[0], ie2the[0], ie415the[0]]
iethe_new = []
for i in range(0, len(iethe)):
    iethe_new.append(np.append(np.ones(20)*iethe[i][0], iethe[i]))

ielou = [ie300lou[0], ie15lou[0], ie2lou[0], ie415lou[0]]
ielou_new = []
for i in range(0, len(ielou)):
    ielou_new.append(np.append(np.ones(20)*ielou[i][0], ielou[i]))

ieromu = [ie300romu[0], ie15romu[0], ie2romu[0], ie415romu[0]]
ieromu_new = []
for i in range(0, len(ieromu)):
    ieromu_new.append(np.append(np.ones(20)*ieromu[i][0], ieromu[i]))

ierem = [ie300rem[0], ie15rem[0], ie2rem[0], ie415rem[0]]
ierem_new = []
for i in range(0, len(ierem)):
    ierem_new.append(np.append(np.ones(20)*ierem[i][0], ierem[i]))

# Save the data
Fileptest = open(home_dir_stam+"/scripts/pickles/insitu_all.p", "wb")
pickle.dump(ieb_new, Fileptest)
pickle.dump(iec_new, Fileptest)
pickle.dump(ief_new, Fileptest)
pickle.dump(iei_new, Fileptest)
pickle.dump(iem_new, Fileptest)
pickle.dump(iew_new, Fileptest)
pickle.dump(ierom_new, Fileptest)
pickle.dump(iejul_new, Fileptest)
pickle.dump(iethe_new, Fileptest)
pickle.dump(ielou_new, Fileptest)
pickle.dump(ieromu_new, Fileptest)
pickle.dump(ierem_new, Fileptest)
Fileptest.close()