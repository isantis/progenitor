#!/usr/bin/python3

"""
 ===========================
 = Contribution Histograms =
 ===========================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Spring Quarter, 2018

 Goal: Plot the histograms for the 2 kpc cuts of the contribution fractions
  
 NOTE: Going to do this for both values taken by eye and from files: last_insitu_redshifts.p & last_mr_redshifts.p

 This is a test to see if things are going to work out with git  
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc

home_dir_stam = '/home1/05400/ibsantis'
contribution = str(300)

# Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12b.p', 'rb')
conb = pickle.load(File1)
File1.close()
File2 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12c.p', 'rb')
conc = pickle.load(File2)
File2.close()
File3 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12f.p', 'rb')
conf = pickle.load(File3)
File3.close()
File4 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12i.p', 'rb')
coni = pickle.load(File4)
File4.close()
File5 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12m.p', 'rb')
conm = pickle.load(File5)
File5.close()
File6 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_m12w.p', 'rb')
conw = pickle.load(File6)
File6.close()
File7 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_Romeo.p', 'rb')
conrom = pickle.load(File7)
File7.close()
File8 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_Juliet.p', 'rb')
conjul = pickle.load(File8)
File8.close()
File9 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_Thelma.p', 'rb')
conthe = pickle.load(File9)
File9.close()
File10 = open(home_dir_stam+'/scripts/pickles/contribution_'+contribution+'_fullres_Louise.p', 'rb')
conlou = pickle.load(File10)
File10.close()

bins = 20

# Plot each individual galaxy histogram first
# m12b
plt.figure(1)
plt.figure(figsize=(10, 8))
plt.hist(conb[4][conb[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conb[9][conb[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conb[14][conb[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conb[19][conb[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conb[29][conb[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conb[39][conb[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conb[44][conb[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conb[49][conb[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conb[50][conb[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12b', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12b_'+contribution+'_test.pdf')
plt.close()

# m12c
plt.figure(2)
plt.figure(figsize=(10, 8))
plt.hist(conc[4][conc[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conc[9][conc[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conc[14][conc[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conc[19][conc[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conc[29][conc[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conc[39][conc[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conc[44][conc[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conc[49][conc[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conc[50][conc[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12c', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12c_'+contribution+'_test.pdf')
plt.close()

# m12f
plt.figure(3)
plt.figure(figsize=(10, 8))
plt.hist(conf[4][conf[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conf[9][conf[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conf[14][conf[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conf[19][conf[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conf[29][conf[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conf[39][conf[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conf[44][conf[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conf[49][conf[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conf[50][conf[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12f', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12f_'+contribution+'.pdf')
plt.close()

# m12i
plt.figure(4)
plt.figure(figsize=(10, 8))
plt.hist(coni[4][coni[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(coni[9][coni[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(coni[14][coni[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(coni[19][coni[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(coni[29][coni[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(coni[39][coni[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(coni[44][coni[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(coni[49][coni[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(coni[50][coni[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12i', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12i_'+contribution+'.pdf')
plt.close()

# m12m
plt.figure(5)
plt.figure(figsize=(10, 8))
plt.hist(conm[4][conm[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conm[9][conm[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conm[14][conm[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conm[19][conm[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conm[29][conm[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conm[39][conm[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conm[44][conm[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conm[49][conm[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conm[50][conm[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12m', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12m_'+contribution+'.pdf')
plt.close()

# m12w
plt.figure(6)
plt.figure(figsize=(10, 8))
plt.hist(conw[4][conw[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conw[9][conw[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conw[14][conw[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conw[19][conw[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conw[29][conw[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conw[39][conw[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conw[44][conw[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conw[49][conw[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conw[50][conw[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('m12w', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_m12w_'+contribution+'.pdf')
plt.close()

# Romeo
plt.figure(7)
plt.figure(figsize=(10, 8))
plt.hist(conrom[4][conrom[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conrom[9][conrom[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conrom[14][conrom[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conrom[19][conrom[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conrom[29][conrom[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conrom[39][conrom[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conrom[44][conrom[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conrom[49][conrom[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conrom[50][conrom[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('Romeo', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_romeo_'+contribution+'.pdf')
plt.close()

# Juliet
plt.figure(8)
plt.figure(figsize=(10, 8))
plt.hist(conjul[4][conjul[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conjul[9][conjul[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conjul[14][conjul[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conjul[19][conjul[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conjul[29][conjul[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conjul[39][conjul[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conjul[44][conjul[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conjul[49][conjul[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conjul[50][conjul[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('Juliet', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_juliet_'+contribution+'.pdf')
plt.close()

# Thelma
plt.figure(9)
plt.figure(figsize=(10, 8))
plt.hist(conthe[4][conthe[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conthe[9][conthe[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conthe[14][conthe[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conthe[19][conthe[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conthe[29][conthe[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conthe[39][conthe[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conthe[44][conthe[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conthe[49][conthe[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conthe[50][conthe[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('Thelma', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_thelma_'+contribution+'.pdf')
plt.close()

# Louise
plt.figure(10)
plt.figure(figsize=(10, 8))
plt.hist(conlou[4][conlou[4] >= 0.01], bins, alpha=0.3, label='z = 0.5') # z = 0.5
plt.hist(conlou[9][conlou[9] >= 0.01], bins, alpha=0.3, label='z = 1') # z = 1
plt.hist(conlou[14][conlou[14] >= 0.01], bins, alpha=0.3, label='z = 1.5') # z = 1.5
plt.hist(conlou[19][conlou[19] >= 0.01], bins, alpha=0.3, label='z = 2') # z = 2
plt.hist(conlou[29][conlou[29] >= 0.01], bins, alpha=0.3, label='z = 3') # z = 3
plt.hist(conlou[39][conlou[39] >= 0.01], bins, alpha=0.3, label='z = 4') # z = 4
plt.hist(conlou[44][conlou[44] >= 0.01], bins, alpha=0.3, label='z = 5') # z = 5
plt.hist(conlou[49][conlou[49] >= 0.01], bins, alpha=0.3, label='z = 6') # z = 6
plt.hist(conlou[50][conlou[50] >= 0.01], bins, alpha=0.3, label='z = 7') # z = 7
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.title('Louise', fontsize=48)
plt.xlim(-0.1, 1.1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.legend(prop={'size': 14})
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/contr_louise_'+contribution+'.pdf')
plt.close()

"""
# Plot all of them on top of each other...?
plt.figure(10)
plt.figure(figsize=(10, 8))
plt.hist(conlou, bins)
plt.xlabel('Contribution Fractions', fontsize=40)
plt.ylabel('N', fontsize=40)
plt.xlim(0, 1)
plt.tick_params(axis='both', which='major', labelsize=32)
plt.tight_layout()
plt.savefig(home_dir+'/scripts/plots/contr_med.pdf')
plt.close()
"""