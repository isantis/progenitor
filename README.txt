
===========
= READ ME =
===========

All files included here should be able to reproduce the figures in my paper:
"The Formation Times and Building Blocks of Milky Way-mass Galaxies in the
FIRE Simulations".

Included .hdf5 files + important notes about them:

    - stellar_mass_vs_redshift.hdf5
      -----------------------------
        - This data can be used to reproduce Figure 2.
        - Each element in the dictionary is labeled for a simulated galaxy,
          i.e., dict['m12b.stellar.mass'] is an array containing the stellar
          mass of the host galaxy for m12b.
        - Each element in an array corresponds to a different redshift. The
          order of an array goes as the following:
            z = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1,
                 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2,
                 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3,
                 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4, 4.2, 4.4, 4.6, 4.8, 5,
                 5.2, 5.4, 5.6, 5.8, 6, 7, 8, 9]


    - halo_mass_vs_redshift.hdf5
      --------------------------
        - This data can be used to reproduce Figure 8.
        - See description of "stellar_mass_vs_redshift.hdf5"


    - cumulative_insitu_fractions_vs_redshift_15_kpc.hdf5
      ---------------------------------------------------
        - This data can be used to reproduce the LEFT panel of Figure 5.
        - These data were smoothed with the savgol_filter from scipy.signal.
        - Each element in the dictionary is labeled for a simulated galaxy,
          i.e., dict['m12b'] is an array containing the cumulative stellar
          in-situ fractions for the host galaxy m12b.
        - Each element in an array corresponds to a different redshift BIN. The
          order of an array goes as the following:
            - dict['m12b'][0] is the in-situ fraction for m12b between z = 0 and
              z = 0.1
            - dict['m12b'][1] is the in-situ fraction for m12b between z = 0.1
              and z = 0.2
            - The rest are similar, increasing along the redshift array written
              above.


    - cumulative_insitu_fractions_vs_redshift_2_kpc.hdf5
      --------------------------------------------------
        - This data can be used to reproduce the RIGHT panel of Figure 5.
        - These data were smoothed with the savgol_filter from scipy.signal.
        - See description of "cumulative_insitu_fractions_vs_redshift_15_kpc.hdf5"


    - num_progenitor_gal_vs_redshift.hdf5
      -----------------------------------
        - This data was used to produce the left panels of Figure 3
        - These data were smoothed with the savgol_filter from scipy.signal
        - The dictionary uses keys in the following manner:
            dict['[distance cut].[progenitor stellar mass].[isolated or LG-like host]']
        - Each element in an array corresponds to a different redshift ordered
          in the same way as the redshift array written above, i.e., starting at
          z = 0 and going to z = 9.


    - mass_functions_vs_redshift.hdf5
      -------------------------------
        - This data was used to produce the right panels of Figure 3
        - The dictionary contains a mass array to be plotted with the mass functions
        - The dictionary uses keys in the following manner:
            dict['[distance cut].[redshift]']
            Contains mass functions for z = [0, 1, 2, 4, 6]
        - Each element in an array corresponds to the number of progenitor galaxies
          above a given stellar mass, starting at M = 1e5 to 1e10 in 0.5 dex bins


    - exsitu_fractions.hdf5
      ---------------------
        - This data was used to produce the panels in Figure 4
        - The dictionary contains a mass array to be plotted with the mass functions
        - The dictionary contains both binned exsitu fractions as well as
          cumulative exsitu fractions for each of the three distance cuts.
        - The dictionary also contains the upper and lower scatter values for
          both types of exsitu fractions. Note from the paper: when the lower scatter
          is zero, we set it equal to the median, i.e., there is no scatter.
        - For a given key (aside from the mass array) each array corresponds to
          a different redshift z = [0, 1, 2, 3, 4, 5, 6]
        - Each element in an array corresponds to a different mass bin.

    - mass_ratios.hdf5
      ----------------
        - These data were used to produce the panels in Figure 6
        - These data were smoothed with the savgol_filter from scipy.signal.
        - The dictionary uses keys in the following manner:
            - dict['[ratio].[type]']
              where [ratio] is either a M2/M1 or M3/M1 ratio
                    [type] is either the total, isolated galaxy, or LG-type galaxy median
        - Each element in an array corresponds to a different redshift (see
          redshift array above).
