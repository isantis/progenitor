#!/usr/bin/python3

"""
 ====================
 = Redshift Plot v2 =
 ====================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2019

 Goal: Plot the redshift estimates for progenitor formation
  
 NOTE: Going to do this for both values taken by eye and from files: last_insitu_redshifts.p & last_mr_redshifts.p
"""

#### Import all of the tools for analysis
from utilities import cosmology
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

home_dir_stam = '/home1/05400/ibsantis'
cosmo_class = cosmology.CosmologyClass()

"""
 Read in the median point data
"""
File1 = open(home_dir_stam+'/scripts/pickles/last_insitu_cum_redshifts_v2.p', 'rb')
junk1 = pickle.load(File1) # Raw, won't need these anymore
ie_cum = pickle.load(File1) # Smoothed
File1.close()
File2 = open(home_dir_stam+'/scripts/pickles/last_mr_redshifts_v2.p', 'rb')
junk2 = pickle.load(File2) # Raw, won't need these anymore
mr = pickle.load(File2) # Smoothed
File2.close()

# Mass ratio, 0.33 metric, 15 kpc cut
mr_033 = mr[1][1]
# Mass ratio, 0.25 metric, 15 kpc cut
mr_025 = mr[1][2]
"""
ie_cum is all we want from the in-situ file and it is already in order:
[300, 15, 2, 4-15]
"""

"""
 Read in all of the individual data so that we can calculate the error-bars
"""
## Cumulative Insitu
File3 = open(home_dir_stam+'/scripts/pickles/last_insitu_redshifts_indiv_cum_v2.p', 'rb')
junk3 = pickle.load(File3)
junk4 = pickle.load(File3) # Won't need any of the 'junk' variables...
junk5 = pickle.load(File3)
junk6 = pickle.load(File3)
ind_ie_300_cum_sm = pickle.load(File3)
ind_ie_15_cum_sm = pickle.load(File3)
ind_ie_2_cum_sm = pickle.load(File3)
ind_ie_415_cum_sm = pickle.load(File3)
File3.close()
# Group the smoothed data together
ind_iec_sm = [ind_ie_300_cum_sm, ind_ie_15_cum_sm, ind_ie_2_cum_sm, ind_ie_415_cum_sm]
## Mass Ratios
File4 = open(home_dir_stam+'/scripts/pickles/last_mr_redshifts_ind_v2.p', 'rb')
junk7 = pickle.load(File4)
junk8 = pickle.load(File4)
junk9 = pickle.load(File4) # Won't need any of the 'junk' variables...
junk10 = pickle.load(File4)
junk11 = pickle.load(File4)
ind_mr_15_sm = pickle.load(File4)
junk12 = pickle.load(File4)
junk13 = pickle.load(File4)
File4.close()
# Group the smoothed 0.33 data together
ind_mr_sm_033 = ind_mr_15_sm[1]
# Group the smoothed 0.25 data together
ind_mr_sm_025 = ind_mr_15_sm[2]

"""
 Calculate the one and two sigma error bars for the median data points
"""
onesigp = 84.13
onesigm = 15.87
twosigp = 97.72
twosigm = 2.28
# Insitu cumulative smooth
err_iecsm_op = np.zeros(len(ind_iec_sm))
err_iecsm_om = np.zeros(len(ind_iec_sm))
err_iecsm_tp = np.zeros(len(ind_iec_sm))
err_iecsm_tm = np.zeros(len(ind_iec_sm))
for i in range(0, len(ind_iec_sm)):
    temp_r = np.asarray(ind_iec_sm[i])
    err_iecsm_op[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iecsm_om[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iecsm_tp[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iecsm_tm[i] = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.33 smoothed
temp_r = np.asarray(ind_mr_sm_033)
err_mrsm033_op = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm033_om = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm033_tp = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm033_tm = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.25 smoothed
temp_r = np.asarray(ind_mr_sm_025)
err_mrsm025_op = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm025_om = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm025_tp = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm025_tm = np.percentile(temp_r[temp_r < 7], twosigm)

"""
 Organize the in-situ data, separating the isolated and pair hosts
"""
## Read in the data
File5 = open(home_dir_stam+'/scripts/pickles/insitu_cumulative_all.p', 'rb')
ieb = pickle.load(File5)
iec = pickle.load(File5)
ief = pickle.load(File5)
iei = pickle.load(File5)
iem = pickle.load(File5)
iew = pickle.load(File5)
ierom = pickle.load(File5)
iejul = pickle.load(File5)
iethe = pickle.load(File5)
ielou = pickle.load(File5)
ieromu = pickle.load(File5)
ierem = pickle.load(File5)
File5.close()
## Organize all of the data
# d = [0, 300 kpc]
insitu300_iso = [ieb[0], iec[0], ief[0], iei[0], iem[0], iew[0]]
insitu300_lg = [ierom[0], iejul[0], iethe[0], ielou[0], ieromu[0], ierem[0]]
# d = [0, 15 kpc]
insitu15_iso = [ieb[1], iec[1], ief[1], iei[1], iem[1], iew[1]]
insitu15_lg = [ierom[1], iejul[1], iethe[1], ielou[1], ieromu[1], ierem[1]]
# d = [0, 2 kpc]
insitu2_iso = [ieb[2], iec[2], ief[2], iei[2], iem[2], iew[2]]
insitu2_lg = [ierom[2], iejul[2], iethe[2], ielou[2], ieromu[2], ierem[2]]
# d = [4, 15 kpc]
insitu415_iso = [ieb[3], iec[3], ief[3], iei[3], iem[3], iew[3]]
insitu415_lg = [ierom[3], iejul[3], iethe[3], ielou[3], ieromu[3], ierem[3]]

"""
 Smooth out the isolated and pair in-situ data and calculate the medians for isolated and pair galaxies
"""
ifrac = 0.5
window_size, poly_order = 41, 3
redss = [0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05,2.15,2.25,2.35,2.45,2.55,2.65,2.75,2.85,2.95,3.05,3.15,3.25,3.35,3.45,3.55,3.65,3.75,3.85,3.95,4.1,4.3,4.5,4.7,4.9,5.1,5.3,5.5,5.7,5.9,6.5,7.5,8.5]
redss = np.flip(redss)
## Isolated galaxies
# Smooth the fractions for each distance cut
insitu300smooth_iso = [savgol_filter(insitu300_iso[i], window_size, poly_order) for i in range(0, len(insitu300_iso))]
insitu15smooth_iso = [savgol_filter(insitu15_iso[i], window_size, poly_order) for i in range(0, len(insitu300_iso))]
insitu2smooth_iso = [savgol_filter(insitu2_iso[i], window_size, poly_order) for i in range(0, len(insitu300_iso))]
insitu415smooth_iso = [savgol_filter(insitu415_iso[i], window_size, poly_order) for i in range(0, len(insitu300_iso))]
# The median curves for each distance cut
insitu300smoothmed_iso = np.median(insitu300smooth_iso,axis=0)
insitu15smoothmed_iso = np.median(insitu15smooth_iso,axis=0)
insitu2smoothmed_iso = np.median(insitu2smooth_iso,axis=0)
insitu415smoothmed_iso = np.median(insitu415smooth_iso,axis=0)
insitusmoothmed_iso = [insitu300smoothmed_iso, insitu15smoothmed_iso, insitu2smoothmed_iso, insitu415smoothmed_iso]
# Redshifts, FOR THE ERRORS
last_red_300_smooth_iso = [redss[np.max(np.where(np.flip(insitu300smooth_iso[i]) < ifrac)[0])] if len(np.where(np.flip(insitu300smooth_iso[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu300smooth_iso))]
last_red_15_smooth_iso = [redss[np.max(np.where(np.flip(insitu15smooth_iso[i]) < ifrac)[0])] if len(np.where(np.flip(insitu15smooth_iso[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu15smooth_iso))]
last_red_2_smooth_iso = [redss[np.max(np.where(np.flip(insitu2smooth_iso[i]) < ifrac)[0])] if len(np.where(np.flip(insitu2smooth_iso[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu2smooth_iso))]
last_red_415_smooth_iso = [redss[np.max(np.where(np.flip(insitu415smooth_iso[i]) < ifrac)[0])] if len(np.where(np.flip(insitu415smooth_iso[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu415smooth_iso))]
# Get the median last redshift
ie_cum_iso = [redss[np.max(np.where(np.flip(insitusmoothmed_iso[i]) < ifrac)[0])] if len(np.where(np.flip(insitusmoothmed_iso[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitusmoothmed_iso))]
# Group them together
ie_ind_iso = [last_red_300_smooth_iso, last_red_15_smooth_iso, last_red_2_smooth_iso, last_red_415_smooth_iso]
# ERROR BARS
err_iecsm_op_iso = np.zeros(len(ie_ind_iso))
err_iecsm_om_iso = np.zeros(len(ie_ind_iso))
err_iecsm_tp_iso = np.zeros(len(ie_ind_iso))
err_iecsm_tm_iso = np.zeros(len(ie_ind_iso))
for i in range(0, len(ie_ind_iso)):
    temp_r = np.asarray(ie_ind_iso[i])
    err_iecsm_op_iso[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iecsm_om_iso[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iecsm_tp_iso[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iecsm_tm_iso[i] = np.percentile(temp_r[temp_r < 7], twosigm)
## Pair galaxies
# Smooth
insitu300smooth_lg = [savgol_filter(insitu300_lg[i], window_size, poly_order) for i in range(0, len(insitu300_lg))]
insitu15smooth_lg = [savgol_filter(insitu15_lg[i], window_size, poly_order) for i in range(0, len(insitu300_lg))]
insitu2smooth_lg = [savgol_filter(insitu2_lg[i], window_size, poly_order) for i in range(0, len(insitu300_lg))]
insitu415smooth_lg = [savgol_filter(insitu415_lg[i], window_size, poly_order) for i in range(0, len(insitu300_lg))]
# The median curves for each distance cut
insitu300smoothmed_lg = np.median(insitu300smooth_lg,axis=0)
insitu15smoothmed_lg = np.median(insitu15smooth_lg,axis=0)
insitu2smoothmed_lg = np.median(insitu2smooth_lg,axis=0)
insitu415smoothmed_lg = np.median(insitu415smooth_lg,axis=0)
insitusmoothmed_lg = [insitu300smoothmed_lg, insitu15smoothmed_lg, insitu2smoothmed_lg, insitu415smoothmed_lg]
# Redshifts FOR ERRORS
last_red_300_smooth_lg = [redss[np.max(np.where(np.flip(insitu300smooth_lg[i]) < ifrac)[0])] if len(np.where(np.flip(insitu300smooth_lg[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu300smooth_lg))]
last_red_15_smooth_lg = [redss[np.max(np.where(np.flip(insitu15smooth_lg[i]) < ifrac)[0])] if len(np.where(np.flip(insitu15smooth_lg[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu15smooth_lg))]
last_red_2_smooth_lg = [redss[np.max(np.where(np.flip(insitu2smooth_lg[i]) < ifrac)[0])] if len(np.where(np.flip(insitu2smooth_lg[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu2smooth_lg))]
last_red_415_smooth_lg = [redss[np.max(np.where(np.flip(insitu415smooth_lg[i]) < ifrac)[0])] if len(np.where(np.flip(insitu415smooth_lg[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitu415smooth_lg))]
# Calculate the median last redshift
ie_cum_lg = [redss[np.max(np.where(np.flip(insitusmoothmed_lg[i]) < ifrac)[0])] if len(np.where(np.flip(insitusmoothmed_lg[i]) < ifrac)[0]) != 0 else 6 for i in range(0, len(insitusmoothmed_lg))]
# Group them together
ie_ind_lg = [last_red_300_smooth_lg, last_red_15_smooth_lg, last_red_2_smooth_lg, last_red_415_smooth_lg]
# ERROR BARS
err_iecsm_op_lg = np.zeros(len(ie_ind_lg))
err_iecsm_om_lg = np.zeros(len(ie_ind_lg))
err_iecsm_tp_lg = np.zeros(len(ie_ind_lg))
err_iecsm_tm_lg = np.zeros(len(ie_ind_lg))
for i in range(0, len(ie_ind_lg)):
    temp_r = np.asarray(ie_ind_lg[i])
    err_iecsm_op_lg[i] = np.percentile(temp_r[temp_r < 7], onesigp)
    err_iecsm_om_lg[i] = np.percentile(temp_r[temp_r < 7], onesigm)
    err_iecsm_tp_lg[i] = np.percentile(temp_r[temp_r < 7], twosigp)
    err_iecsm_tm_lg[i] = np.percentile(temp_r[temp_r < 7], twosigm)

"""
 Group the isolated and pair MR data together
"""
# Isolated
ind_mr_sm_033_isomed = np.median(ind_mr_sm_033[:6])
ind_mr_sm_025_isomed = np.median(ind_mr_sm_025[:6])
# Pairs
ind_mr_sm_033_lgmed = np.median(ind_mr_sm_033[6:])
ind_mr_sm_025_lgmed = np.median(ind_mr_sm_025[6:])

"""
 Calculate the errorbars
"""
# Isolated
# Mass ratio 0.33
temp_r = np.asarray(ind_mr_sm_033[:6])
err_mrsm033_op_iso = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm033_om_iso = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm033_tp_iso = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm033_tm_iso = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.25
temp_r = np.asarray(ind_mr_sm_025[:6])
err_mrsm025_op_iso = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm025_om_iso = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm025_tp_iso = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm025_tm_iso = np.percentile(temp_r[temp_r < 7], twosigm)
# Pairs
# Mass ratio 0.33
temp_r = np.asarray(ind_mr_sm_033[6:])
err_mrsm033_op_lg = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm033_om_lg = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm033_tp_lg = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm033_tm_lg = np.percentile(temp_r[temp_r < 7], twosigm)
# Mass ratio 0.25
temp_r = np.asarray(ind_mr_sm_025[6:])
err_mrsm025_op_lg = np.percentile(temp_r[temp_r < 7], onesigp)
err_mrsm025_om_lg = np.percentile(temp_r[temp_r < 7], onesigm)
err_mrsm025_tp_lg = np.percentile(temp_r[temp_r < 7], twosigp)
err_mrsm025_tm_lg = np.percentile(temp_r[temp_r < 7], twosigm)

"""
 Finally plot all of the damn data
"""
colors = dc.get_distinct(4)
xs = np.arange(1, 7)
inc = [0.0, 0.2]
leg = ['d(z = 0) < 15 kpc', 'd(z = 0) < 2 kpc']
#
fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
ax1.tick_params(axis='x', which='minor', bottom=False, top=False)
ax1.tick_params(axis='y', which='minor', right=False)
###### Insitu
### Isolated Galaxies
# errorbars
plt.errorbar(xs[0]+inc[0], ie_cum_iso[1], yerr=np.array([[ie_cum_iso[1]-err_iecsm_tm_iso[1]], [err_iecsm_tp_iso[1]-ie_cum_iso[1]]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[0]+inc[0], ie_cum_iso[1], yerr=np.array([[ie_cum_iso[1]-err_iecsm_om_iso[1]], [err_iecsm_op_iso[1]-ie_cum_iso[1]]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[0]+inc[1], ie_cum_iso[2], yerr=np.array([[ie_cum_iso[2]-err_iecsm_tm_iso[2]], [err_iecsm_tp_iso[2]-ie_cum_iso[2]]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[0]+inc[1], ie_cum_iso[2], yerr=np.array([[ie_cum_iso[2]-err_iecsm_om_iso[2]], [err_iecsm_op_iso[2]-ie_cum_iso[2]]]), color=colors[1], alpha=0.80)
# Plot ie_cum_iso
plt.scatter(xs[0]+inc[0], ie_cum_iso[1], color=colors[0], s=50.0, marker='s', label=leg[0])
plt.scatter(xs[0]+inc[1], ie_cum_iso[2], color=colors[1], s=50.0, marker='s', label=leg[1])

### Median for ALL
# errorbars
plt.errorbar(xs[1]+inc[0], ie_cum[1], yerr=np.array([[ie_cum[1]-err_iecsm_tm[1]], [err_iecsm_tp[1]-ie_cum[1]]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[1]+inc[0], ie_cum[1], yerr=np.array([[ie_cum[1]-err_iecsm_om[1]], [err_iecsm_op[1]-ie_cum[1]]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[1]+inc[1], ie_cum[2], yerr=np.array([[ie_cum[2]-err_iecsm_tm[2]], [err_iecsm_tp[2]-ie_cum[2]]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[1]+inc[1], ie_cum[2], yerr=np.array([[ie_cum[2]-err_iecsm_om[2]], [err_iecsm_op[2]-ie_cum[2]]]), color=colors[1], alpha=0.80)
# Plot ie_cum
plt.scatter(xs[1]+inc[0], ie_cum[1], color=colors[0], s=50.0)
plt.scatter(xs[1]+inc[1], ie_cum[2], color=colors[1], s=50.0)

### Pair Galaxies
# Fix the median point for d = 2 kpc, want to set it to z = 6
##ie_cum_lg[2] = 6
# errorbars
plt.errorbar(xs[2]+inc[0], ie_cum_lg[1], yerr=np.array([[ie_cum_lg[1]-err_iecsm_tm_lg[1]], [err_iecsm_tp_lg[1]-ie_cum_lg[1]]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[2]+inc[0], ie_cum_lg[1], yerr=np.array([[ie_cum_lg[1]-err_iecsm_om_lg[1]], [err_iecsm_op_lg[1]-ie_cum_lg[1]]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[2]+inc[1], ie_cum_lg[2], yerr=np.array([[ie_cum_lg[2]-err_iecsm_tm_lg[2]], [err_iecsm_tp_lg[2]-ie_cum_lg[2]]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[2]+inc[1], ie_cum_lg[2], yerr=np.array([[ie_cum_lg[2]-err_iecsm_om_lg[2]], [err_iecsm_op_lg[2]-ie_cum_lg[2]]]), color=colors[1], alpha=0.80)
# Plot ie_cum_lg
plt.scatter(xs[2]+inc[0], ie_cum_lg[1], color=colors[0], s=50.0, marker='D')
plt.plot(3.2, 6.0, markersize=9.0, marker='D', color=colors[1])
#plt.scatter(xs[2]+inc[1], ie_cum_lg[2], color=colors[1], s=50.0, marker='D')

##### Mass Ratio (0.33), 15 kpc
### Isolated Galaxies
# errorbars
plt.errorbar(xs[3]+inc[0], ind_mr_sm_033_isomed, yerr=np.array([[ind_mr_sm_033_isomed-err_mrsm033_tm_iso], [err_mrsm033_tp_iso-ind_mr_sm_033_isomed]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[3]+inc[0], ind_mr_sm_033_isomed, yerr=np.array([[ind_mr_sm_033_isomed-err_mrsm033_om_iso], [err_mrsm033_op_iso-ind_mr_sm_033_isomed]]), color=colors[0], alpha=0.80)
# Plot ind_mr_sm_033_isomed
plt.scatter(xs[3]+inc[0], ind_mr_sm_033_isomed, color=colors[0], s=50.0, marker='s')

### Median for ALL
# errorbars
plt.errorbar(xs[4]+inc[0], mr_033, yerr=np.array([[mr_033-err_mrsm033_tm], [err_mrsm033_tp-mr_033]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[4]+inc[0], mr_033, yerr=np.array([[mr_033-err_mrsm033_om], [err_mrsm033_op-mr_033]]), color=colors[0], alpha=0.80)
# Plot mr_033
plt.scatter(xs[4]+inc[0], mr_033, color=colors[0], s=50.0)

### Paired Galaxies
# errorbars
plt.errorbar(xs[5]+inc[0], ind_mr_sm_033_lgmed, yerr=np.array([[ind_mr_sm_033_lgmed-err_mrsm033_tm_lg], [err_mrsm033_tp_lg-ind_mr_sm_033_lgmed]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[5]+inc[0], ind_mr_sm_033_lgmed, yerr=np.array([[ind_mr_sm_033_lgmed-err_mrsm033_om_lg], [err_mrsm033_op_lg-ind_mr_sm_033_lgmed]]), color=colors[0], alpha=0.80)
# Plot ind_mr_sm_033_lgmed
plt.scatter(xs[5]+inc[0], ind_mr_sm_033_lgmed, color=colors[0], s=50.0, marker='D')

"""
##### Mass Ratio (0.25), 15 kpc
### Isolated Galaxies
# errorbars
plt.errorbar(xs[6]+inc[0], ind_mr_sm_025_isomed, yerr=np.array([[ind_mr_sm_025_isomed-err_mrsm025_tm_iso], [err_mrsm025_tp_iso-ind_mr_sm_025_isomed]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[6]+inc[0], ind_mr_sm_025_isomed, yerr=np.array([[ind_mr_sm_025_isomed-err_mrsm025_om_iso], [err_mrsm025_op_iso-ind_mr_sm_025_isomed]]), color=colors[1], alpha=0.80)
# Plot ind_mr_sm_025_isomed
plt.scatter(xs[6]+inc[0], ind_mr_sm_025_isomed, color=colors[1], s=50.0, marker='s')

### Median for ALL
# errorbars
plt.errorbar(xs[7]+inc[0], mr_025, yerr=np.array([[mr_025-err_mrsm025_tm], [err_mrsm025_tp-mr_025]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[7]+inc[0], mr_025, yerr=np.array([[mr_025-err_mrsm025_om], [err_mrsm025_op-mr_025]]), color=colors[1], alpha=0.80)
# Plot mr_025
plt.scatter(xs[7]+inc[0], mr_025, color=colors[1], s=50.0)

### Paired Galaxies
# errorbars
plt.errorbar(xs[8]+inc[0], ind_mr_sm_025_lgmed, yerr=np.array([[ind_mr_sm_025_lgmed-err_mrsm025_tm_lg], [err_mrsm025_tp_lg-ind_mr_sm_025_lgmed]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[8]+inc[0], ind_mr_sm_025_lgmed, yerr=np.array([[ind_mr_sm_025_lgmed-err_mrsm025_om_lg], [err_mrsm025_op_lg-ind_mr_sm_025_lgmed]]), color=colors[1], alpha=0.80)
# Plot ind_mr_sm_025_lgmed
plt.scatter(xs[8]+inc[0], ind_mr_sm_025_lgmed, color=colors[1], s=50.0, marker='D')
"""

## Plot some vertical dotted lines to separate the metrics
plt.vlines(3.5, 0, 7, colors='k', linestyles='dotted')

# Label stuff
my_xticks1 = ['in-situ (iso)','in-situ (all)','in-situ (pair)']
my_xticks2 = ['MR (0.33, iso)','MR (0.33, all)','MR (0.33, pair)']
my_xticks_tot = my_xticks1+my_xticks2
my_xs = np.arange(1, 7)
plt.xticks(my_xs, my_xticks_tot, rotation=45)
plt.ylabel('Formation Redshift', fontsize=40)
plt.legend(prop={'size': 14})
plt.tick_params(axis='x', which='major', labelsize=20)
plt.tick_params(axis='y', which='major', labelsize=32)
plt.ylim(1, 6)
plt.yticks(np.arange(1, 7, 1.0))

# put the new axis stuff here 
ax2 = ax1.twinx()
ax2.tick_params(axis='y', which='minor', right=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [1,2,3,4,5,6]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_locations = [0,1,2,3,4,5]

ax2.set_yticks(axis_2_tick_locations)
ax2.set_yticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_ylabel(axis_2_label, labelpad=9,fontsize=34)
ax2.tick_params(pad=3)

plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/redshift_form_v5.pdf')