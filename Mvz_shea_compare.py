#!/usr/bin/python3

"""
 ====================
 = M1 vs z plot all =
 ====================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2018

 Goal: Read in the values for M1 and plot this vs redshift for all galaxies

 NOTE: Uses files made from "Mvz_v2.py" or "Mvz_v3.py" or "Mvz_lg.py"
       Used to be for all 4 distance cuts, but there were no significant differences between all of them.
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
from scipy import interpolate
from utilities import cosmology

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

colors = ['#332288','#88CCEE','#44AA99','#117733','#999933','#2F4F4F','#CC6677','#882255','#AA4499','#BBBBBB','#A50026','#726A83']

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12b_DMO.p', 'rb')
M300b = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12c_DMO.p', 'rb')
M300c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12f_DMO.p', 'rb')
M300f = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12i_DMO.p', 'rb')
M300i = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12m_DMO.p', 'rb')
M300m = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12w_DMO.p', 'rb')
M300w = pickle.load(File6)
File6.close()
# Romeo
File7 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Romeo300_DMO.p', 'rb')
M300rom = pickle.load(File7)
File7.close()
# Juliet
File8 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Juliet300_DMO.p', 'rb')
M300jul = pickle.load(File8)
File8.close()
# Thelma
File9 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Thelma300_DMO.p', 'rb')
M300the = pickle.load(File9)
File9.close()
# Louise
File10 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Louise300_DMO.p', 'rb')
M300lou = pickle.load(File10)
File10.close()
# Romulus
File11 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Romulus300_DMO.p', 'rb')
M300romu = pickle.load(File11)
File11.close()
# Remus
File12 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Remus300_DMO.p', 'rb')
M300rem = pickle.load(File12)
File12.close()

### d = [0, 300 kpc]
# Calculate the median M1 for all, isolated, and pairs
M = [M300b, M300c, M300f, M300i, M300m, M300w, M300rom, M300jul, M300the, M300lou, M300romu, M300rem]
M300med = np.median(M,0)
Miso = [M300b, M300c, M300f, M300i, M300m, M300w]
M300iso = np.median(Miso,0)
Mlg = [M300rom, M300jul, M300the, M300lou, M300romu, M300rem]
M300lg = np.median(Mlg,0)

# Normalize all of the curves to their values at z = 0
M_z0norm = [M[i]/M[i][0] for i in range(0, len(M))]
M300med_z0norm = M300med/M300med[0]
Miso_z0norm = [M_z0norm[0],M_z0norm[1],M_z0norm[2],M_z0norm[3],M_z0norm[4],M_z0norm[5]]
M300iso_z0norm = np.median(Miso_z0norm,axis=0)
Mlg_z0norm = [M_z0norm[6],M_z0norm[7],M_z0norm[8],M_z0norm[9],M_z0norm[10],M_z0norm[11]]
M300lg_z0norm = np.median(Mlg_z0norm,axis=0)

# Gather z_0.5s from Shea's paper for isolated and LG galaxies
z_lg_gkim = np.array([0.79, 1.08, 1.24, 0.89, 1.57, 1.53, 1.61, 1.08, 0.64, 1.10, 1.47, 1.18, 1.36, 0.99, 1.77, 0.98, 0.30, 0.66, 1.04, 0.62, 1.44, 1.61, 0.67, 1.14])
z_iso_gkim = np.array([0.8, 1.3, 0.97, 1.4, 0.88, 0.91, 1.64, 1.36, 0.74, 0.97, 2.11, 0.75, 0.89, 1.76, 1.15, 1.41, 0.69, 0.6, 1.13, 0.72, 1.56, 1.22, 1.42, 1.11])
# Group them all together
z_tot_gkim = np.concatenate((z_lg_gkim, z_iso_gkim), axis=0)

# Calculate the median redshifts of half formation
z_med_lg_gkim = np.median(z_lg_gkim) # 1.09
z_med_iso_gkim = np.median(z_iso_gkim) # 1.12
z_med_tot_gkim = np.median(z_tot_gkim) # 1.105

# Gather all of the z_05 data from our results
# M_z0norm is the one to play with for our data
z_iso_me = np.array([0.9, 0.6, 0.9, 1.3, 1.0, 0.8])
z_lg_me = np.array([1.4, 0.9, 1.4, 1.0, 1.4, 1.4])
z_tot_me = np.concatenate((z_iso_me, z_lg_me), axis=0)

# Calculate the medians
z_med_iso_me = np.median(z_iso_me) # 0.9
z_med_lg_me = np.median(z_lg_me) # 1.4
z_med_tot_me = np.median(z_tot_me) # 1.0

