#!/usr/bin/python3

"""
 =====================================
 = In-situ / Ex-situ Cumulative Plot =
 =====================================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Winter Quarter, 2019
 
 Goal: Plot the cumulative insitu and exsitu fractions for a galaxy 
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

# Set up some initial stuff
galaxy = 'm12w'
resolution = '_res7100'
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

# Read in the data
File1 = open(home_dir_stam+'/scripts/pickles/insitu_cumulative_all.p', 'rb')
ieb = pickle.load(File1)
iec = pickle.load(File1)
ief = pickle.load(File1)
iei = pickle.load(File1)
iem = pickle.load(File1)
iew = pickle.load(File1)
ierom = pickle.load(File1)
iejul = pickle.load(File1)
iethe = pickle.load(File1)
ielou = pickle.load(File1)
File1.close()

######### REMEMBER TO CHANGE THIS!
ie = iew
iecut = 0.5

# Plot the ratios
colors = dc.get_distinct(2)

# d = [0, 300 kpc]
insitu = ie[0]

window_size, poly_order = 41, 3
ie300smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(1)
plt.figure(figsize=(10, 8))
plt.plot(redshifts[:-1], insitu, color=colors[0], label='raw')
plt.plot(redshifts[:-1], ie300smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(galaxy+', d = [0, 300 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.xticks(np.arange(0, 8, 1.0))
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_300_smooth_'+galaxy+'_cum_v4.pdf')
plt.close()

# d = [0, 15 kpc]
insitu = ie[1]

window_size, poly_order = 41, 3
#ie15smooth = savgol_filter(insitu[:52], window_size, poly_order)
ie15smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(2)
plt.figure(figsize=(10, 8))
plt.plot(redshifts[:-1], insitu, color=colors[0], label='raw')
#plt.plot(redss[:52], ie15smooth, color=colors[1], label='smoothed')
plt.plot(redshifts[:-1], ie15smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(galaxy+', d = [0, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.xticks(np.arange(0, 8, 1.0))
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_15_smooth_'+galaxy+'_cum_v4.pdf')
plt.close()

# d = [0, 2 kpc]
insitu = ie[2]

window_size, poly_order = 41, 3
ie2smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(3)
plt.figure(figsize=(10, 8))
plt.plot(redshifts[:-1], insitu, color=colors[0], label='raw')
plt.plot(redshifts[:-1], ie2smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(galaxy+', d = [0, 2 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.xticks(np.arange(0, 8, 1.0))
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_2_smooth_'+galaxy+'_cum_v4.pdf')
plt.close()

# d = [4, 15 kpc]
insitu = ie[3]

window_size, poly_order = 41, 3
ie415smooth = savgol_filter(insitu, window_size, poly_order)

plt.figure(4)
plt.figure(figsize=(10, 8))
plt.plot(redshifts[:-1], insitu, color=colors[0], label='raw')
plt.plot(redshifts[:-1], ie415smooth, color=colors[1], label='smoothed')
plt.hlines(y=iecut, xmin=0, xmax=9, linestyles='dotted')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('$f_{\\rm{in-situ}}$', fontsize=46)
plt.title(galaxy+', d = [4, 15 kpc]', fontsize=48, y=1.02)
plt.legend(prop={'size': 16})
plt.tick_params(axis='both', which='major', labelsize=32)
plt.ylim(0, 1)
plt.xlim(7, 0)
plt.xticks(np.arange(0, 8, 1.0))
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/ionly_415_smooth_'+galaxy+'_cum_v4.pdf')
plt.close()