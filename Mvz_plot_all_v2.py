#!/usr/bin/python3

"""
 ====================
 = M1 vs z plot all =
 ====================

 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2018

 Goal: Read in the values for M1 and plot this vs redshift for all galaxies

 NOTE: Uses files made from "Mvz_v2.py" or "Mvz_v3.py" or "Mvz_lg.py"
       Used to be for all 4 distance cuts, but there were no significant differences between all of them.
"""

#### Import all of the tools for analysis
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
from scipy import interpolate
from utilities import cosmology

cosmo_class = cosmology.CosmologyClass()

# Set up some initial stuff
home_dir_stam = '/home1/05400/ibsantis'
galaxies = ['m12b','m12c','m12f','m12i','m12m','m12w','romeo','juliet','thelma','louise','romulus','remus']
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]

# Old colors
#colors = ['#332288','#88CCEE','#44AA99','#117733','#999933','#2F4F4F','#CC6677','#882255','#AA4499','#BBBBBB','#A50026','#726A83']
colors = ['#e6194B','#3cb44b','#FFFF00','#4363d8','#f58231','#911eb4','#42d4f4','#f032e6','#bfef45','#fabebe','#469990','#9A6324']

## Read in the data
# m12b
File1 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12b.p', 'rb')
M300b = pickle.load(File1)
File1.close()
# m12c
File2 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12c.p', 'rb')
M300c = pickle.load(File2)
File2.close()
# m12f
File3 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12f.p', 'rb')
M300f = pickle.load(File3)
File3.close()
# m12i
File4 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12i.p', 'rb')
M300i = pickle.load(File4)
File4.close()
# m12m
File5 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12m.p', 'rb')
M300m = pickle.load(File5)
File5.close()
# m12w
File6 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_m12w.p', 'rb')
M300w = pickle.load(File6)
File6.close()
# Romeo
File7 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Romeo300.p', 'rb')
M300rom = pickle.load(File7)
File7.close()
# Juliet
File8 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Juliet300.p', 'rb')
M300jul = pickle.load(File8)
File8.close()
# Thelma
File9 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Thelma300.p', 'rb')
M300the = pickle.load(File9)
File9.close()
# Louise
File10 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Louise300.p', 'rb')
M300lou = pickle.load(File10)
File10.close()
# Romulus
File11 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Romulus300.p', 'rb')
M300romu = pickle.load(File11)
File11.close()
# Remus
File12 = open(home_dir_stam+'/scripts/pickles/Mvz_1percent_Remus300.p', 'rb')
M300rem = pickle.load(File12)
File12.close()

### d = [0, 300 kpc]
# Calculate the median M1 for all, isolated, and pairs
M = [M300b, M300c, M300f, M300i, M300m, M300w, M300rom, M300jul, M300the, M300lou, M300romu, M300rem]
M300med = np.median(M,0)
Miso = [M300b, M300c, M300f, M300i, M300m, M300w]
M300iso = np.median(Miso,0)
Mlg = [M300rom, M300jul, M300the, M300lou, M300romu, M300rem]
M300lg = np.median(Mlg,0)
# Calculate the normalized curves
M_norm = np.asarray(M)/M300med
M300med_norm = M300med/M300med
M300iso_norm = np.asarray(M300iso)/M300med
M300lg_norm = np.asarray(M300lg)/M300med
# plot all of them
fig = plt.figure(figsize=(10, 8))
# Top
ax1 = fig.add_subplot(1,1,1)
plt.plot(redshifts, M[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.80)
plt.plot(redshifts, M[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.80)
plt.plot(redshifts, M[2], color=colors[6], linewidth=2.0, label=galaxies[2], alpha=0.80)
plt.plot(redshifts, M[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.80)
plt.plot(redshifts, M[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.80)
plt.plot(redshifts, M[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.80)
plt.plot(redshifts, M[6], color=colors[0], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.80)
plt.plot(redshifts, M[7], color=colors[1], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.80)
plt.plot(redshifts, M[8], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.80)
plt.plot(redshifts, M[9], color=colors[3], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.80)
plt.plot(redshifts, M[10], color=colors[4], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.80)
plt.plot(redshifts, M[11], color=colors[5], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.80)
plt.plot(redshifts, M300med, color='k', linewidth=4.0, label='Median (All)')
plt.plot(redshifts, M300iso, color='k', linewidth=4.0, alpha=0.50, label='Median (Isolated Hosts)')
plt.plot(redshifts, M300lg, color='k', linestyle='--', linewidth=4.0, alpha=0.50, label='Median (LG Pairs)')
# Try to add some fake lines so that I can fill up the legend
plt.plot(redshifts, M300lg_norm, linestyle='', label=' ')
plt.plot(redshifts, M300lg_norm, linestyle='', label=' ')
plt.plot(redshifts, M300lg_norm, linestyle='', label=' ')
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Host M$_{\\rm{star}}$ [M$_{\odot}$]', fontsize=48)
plt.legend(ncol=3, loc='upper left', prop={'size': 12.5})
plt.tick_params(axis='x', which='major', labelsize=32)
plt.tick_params(axis='y', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(5*10**6, 2*10**(11))
plt.yscale('log')
plt.xlim(7, 0)
# put the new axis stuff here
ax2 = ax1.twiny()
ax2.tick_params(axis='x', which='minor', top=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [7,6,5,4,3,2,1]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_labels.append(0)
axis_2_tick_locations = [0,1,2,3,4,5,6,7]
ax2.set_xticks(axis_2_tick_locations)
ax2.set_xticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_xlabel(axis_2_label, labelpad=9, fontsize=40)
ax2.tick_params(pad=3)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Mvz_top_vf.pdf')
plt.close()

# Bottom
fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
plt.plot(redshifts, M300med_norm, color='k', linewidth=4.0, label='Median (All)')
plt.plot(redshifts, M_norm[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.80)
plt.plot(redshifts, M_norm[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.80)
plt.plot(redshifts, M_norm[2], color=colors[6], linewidth=2.0, label=galaxies[2], alpha=0.80)
plt.plot(redshifts, M_norm[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.80)
plt.plot(redshifts, M_norm[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.80)
plt.plot(redshifts, M_norm[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.80)
plt.plot(redshifts, M_norm[6], color=colors[0], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.80)
plt.plot(redshifts, M_norm[7], color=colors[1], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.80)
plt.plot(redshifts, M_norm[8], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.80)
plt.plot(redshifts, M_norm[9], color=colors[3], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.80)
plt.plot(redshifts, M_norm[10], color=colors[4], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.80)
plt.plot(redshifts, M_norm[11], color=colors[5], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.80)
plt.plot(redshifts, M300iso_norm, color='k', linewidth=4.0, alpha=0.50, label='Median (Isolated Hosts)')
plt.plot(redshifts, M300lg_norm, color='k', linestyle='--', linewidth=4.0, alpha=0.50, label='Median (LG Pairs)')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Ratio w.r.t. Median', fontsize=40)
plt.tick_params(axis='x', which='major', labelsize=32)
plt.tick_params(axis='y', which='major', labelsize=32)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(-0.1, 4.1)
plt.yticks(np.arange(0, 5, 1.0))
plt.xlim(7, 0)
# put the new axis stuff here
ax2 = ax1.twiny()
ax2.tick_params(axis='x', which='minor', top=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [7,6,5,4,3,2,1]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_labels.append(0)
axis_2_tick_locations = [0,1,2,3,4,5,6,7]
ax2.set_xticks(axis_2_tick_locations)
ax2.set_xticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_xlabel(axis_2_label, labelpad=9, fontsize=40)
ax2.tick_params(pad=3)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Mvz_bottom_vf.pdf')
plt.close()

# Normalize all of the curves to their values at z = 0
M_z0norm = [M[i]/M[i][0] for i in range(0, len(M))]
M300med_z0norm = M300med/M300med[0]
Miso_z0norm = [M_z0norm[0],M_z0norm[1],M_z0norm[2],M_z0norm[3],M_z0norm[4],M_z0norm[5]]
M300iso_z0norm = np.median(Miso_z0norm,axis=0)
Mlg_z0norm = [M_z0norm[6],M_z0norm[7],M_z0norm[8],M_z0norm[9],M_z0norm[10],M_z0norm[11]]
M300lg_z0norm = np.median(Mlg_z0norm,axis=0)

# Calculate the ratios of the normalized to the total median
M_z0norm_norm = np.asarray(M_z0norm)/M300med_z0norm
M300med_z0norm_norm = M300med_z0norm/M300med_z0norm
M300iso_z0norm_norm = np.asarray(M300iso_z0norm)/M300med_z0norm
M300lg_z0norm_norm = np.asarray(M300lg_z0norm)/M300med_z0norm

# Plot the data
# Top
fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
plt.hlines(y=0.1, xmin=0, xmax=9, linestyles='dotted')
plt.hlines(y=0.5, xmin=0, xmax=9, linestyles='dotted')
plt.plot(redshifts, M300med_z0norm, color='k', linewidth=4.0)
plt.plot(redshifts, M_z0norm[0], color=colors[0], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[1], color=colors[1], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[2], color=colors[6], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[3], color=colors[3], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[4], color=colors[4], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[5], color=colors[5], linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[6], color=colors[0], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[7], color=colors[1], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[8], color=colors[6], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[9], color=colors[3], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[10], color=colors[4], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M_z0norm[11], color=colors[5], linestyle=':', linewidth=2.0, alpha=0.80)
plt.plot(redshifts, M300iso_z0norm, color='k', linewidth=4.0, alpha=0.50)
plt.plot(redshifts, M300lg_z0norm, color='k', linestyle='--', linewidth=4.0, alpha=0.50)
# Plot the model points I'm comparing to
# Lietner 10^11
plt.plot(2.0, 0.1, 'o', color='#720000', alpha=0.9, markersize=12, markeredgecolor='k') # 10%
plt.plot(1.1, 0.5, 'o', color='#720000', alpha=0.9, markersize=12, markeredgecolor='k', label='Leitner (2012)') # 50 %
## Behroozi 10^12 halo (2013)
#plt.plot(1.6, 0.1, '^', color='#720000', alpha=0.5, markersize=12, label='Behroozi et al. (2013)') # 10%
#plt.plot(0.8, 0.5, '^', color='#720000', alpha=0.5, markersize=12) # 50%
# Behroozi 10^12 halo (2019)
plt.plot(1.3, 0.1, '^', color=colors[9], alpha=0.9, markersize=12, markeredgecolor='k', label='Behroozi et al. (2019)') # 10%
plt.plot(0.7, 0.5, '^', color=colors[9], alpha=0.9, markersize=12, markeredgecolor='k') # 50%
# Hill 10^10.5
plt.hlines(y=0.1, xmin=1.3, xmax=1.90, linewidth=6, colors='#720000', label='Hill et al. (2017)') # 10% line
plt.hlines(y=0.5, xmin=0.6, xmax=0.92, linewidth=6, colors='#720000') # 50% line
#plt.plot(1.3, 0.1, 's', color='#720000', alpha=0.5, markersize=12) # 10%
#plt.plot(0.6, 0.5, 's', color='#720000', alpha=0.5, markersize=12, label='Hill et al. (2017)') # 50%
# Hill 10^11
#plt.plot(1.9, 0.1, 's', color='#720000', alpha=0.5, markersize=12) # 10%
#plt.plot(0.92, 0.5, 's', color='#720000', alpha=0.5, markersize=12) # 50%
#
## van Dokkum 10^11-ish
#plt.plot(10, 0.5, 'v', color='#009688', alpha=0.8, markersize=12, label='van Dokkum (2013)') # plotting this one just to get the color in the legend right
#plt.plot(1.3, 0.5, 'v', color='#720000', alpha=0.8, markersize=12) # 50%
#plt.plot(2.6, 0.1, 'v', color='#720000', alpha=0.8, markersize=12) # 10%
#
# Papovich
plt.hlines(y=0.1, xmin=2.05, xmax=2.95, linewidth=6, alpha=0.9, colors=colors[11], label='Papovich et al. (2015)') # 10% line
plt.hlines(y=0.5, xmin=1.05, xmax=1.45, linewidth=6, alpha=0.9, colors=colors[11]) # 50% line
#
plt.xlabel('redshift', fontsize=40)
plt.ylabel('M$_{\\rm{star}}$/M$_{\\rm{star}}(z = 0)$', fontsize=48)
plt.legend(loc='lower right', prop={'size': 18})
plt.tick_params(axis='x', which='major', labelsize=32)
plt.tick_params(axis='y', which='both', labelsize=26)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(5*10**(-5), 2*10**(0))
plt.xlim(7, 0)
plt.yscale('log')
# put the new axis stuff here
ax2 = ax1.twiny()
ax2.tick_params(axis='x', which='minor', top=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [7,6,5,4,3,2,1]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_labels.append(0)
axis_2_tick_locations = [0,1,2,3,4,5,6,7]
ax2.set_xticks(axis_2_tick_locations)
ax2.set_xticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_xlabel(axis_2_label, labelpad=9, fontsize=40)
ax2.tick_params(pad=3)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Mvz_normalized_top_vf.pdf')
plt.close()

# Bottom
fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
plt.plot(redshifts, M300med_z0norm_norm, color='k', linewidth=4.0, label='Median (All)')
plt.plot(redshifts, M_z0norm_norm[0], color=colors[0], linewidth=2.0, label=galaxies[0], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[1], color=colors[1], linewidth=2.0, label=galaxies[1], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[2], color=colors[6], linewidth=2.0, label=galaxies[2], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[3], color=colors[3], linewidth=2.0, label=galaxies[3], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[4], color=colors[4], linewidth=2.0, label=galaxies[4], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[5], color=colors[5], linewidth=2.0, label=galaxies[5], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[6], color=colors[0], linestyle=':', linewidth=2.0, label=galaxies[6], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[7], color=colors[1], linestyle=':', linewidth=2.0, label=galaxies[7], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[8], color=colors[6], linestyle=':', linewidth=2.0, label=galaxies[8], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[9], color=colors[3], linestyle=':', linewidth=2.0, label=galaxies[9], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[10], color=colors[4], linestyle=':', linewidth=2.0, label=galaxies[10], alpha=0.80)
plt.plot(redshifts, M_z0norm_norm[11], color=colors[5], linestyle=':', linewidth=2.0, label=galaxies[11], alpha=0.80)
plt.plot(redshifts, M300iso_z0norm_norm, color='k', linewidth=4.0, alpha=0.50, label='Median (Isolated Hosts)')
plt.plot(redshifts, M300lg_z0norm_norm, color='k', linestyle='--', linewidth=4.0, alpha=0.50, label='Median (LG Pairs)')
plt.xlabel('redshift', fontsize=40)
plt.ylabel('Ratio w.r.t. Median', fontsize=40)
plt.tick_params(axis='x', which='major', labelsize=32)
plt.tick_params(axis='y', which='major', labelsize=26)
plt.xticks(np.arange(0, 8, 1.0))
plt.ylim(-0.1, 6.2)
plt.yticks(np.arange(0, 7, 1.0))
plt.xlim(7, 0)
# put the new axis stuff here
ax2 = ax1.twiny()
ax2.tick_params(axis='x', which='minor', top=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [7,6,5,4,3,2,1]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_labels.append(0)
axis_2_tick_locations = [0,1,2,3,4,5,6,7]
ax2.set_xticks(axis_2_tick_locations)
ax2.set_xticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_xlabel(axis_2_label, labelpad=9, fontsize=40)
ax2.tick_params(pad=3)
plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/Mvz_normalized_bottom_vf.pdf')
plt.close()
