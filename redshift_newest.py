#!/usr/bin/python3

"""
 ====================
 = Redshift Plot v3 =
 ====================
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Summer Session I, 2019

 Goal: Plot the redshift estimates for progenitor formation
  
 NOTE: Going to do this for both values taken by eye and from files: last_insitu_redshifts.p & last_mr_redshifts.p
"""

#### Import all of the tools for analysis
from utilities import cosmology
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import pickle
import distinct_colours as dc
from scipy import interpolate
from scipy.signal import savgol_filter

home_dir_stam = '/home1/05400/ibsantis'
cosmo_class = cosmology.CosmologyClass()

# Group the smoothed 0.33 data together, isolated first
ind_mr_sm_033 = np.array([3.7,3.1,2.9,1.7,1.5,1.2,6,4.7,4.4,3.5,1.7,1.2])
ind_mr_sm_033_iso = np.array([3.7,3.1,2.9,1.7,1.5,1.2])
ind_mr_sm_033_lg = np.array([6,4.7,4.4,3.5,1.7,1.2])

# Mass ratio, 0.33 metric, 15 kpc cut
mr_033 = 3.3

# In-situ data, 15 kpc
insitu_tot_15 = np.array([6,5.3,5.3,4.9,3.7,1.7,3.7,3.1,2.7,2.4,1.9,1.9])
insitu_lg_15 = np.array([6,5.3,5.3,4.9,3.7,1.7])
insitu_iso_15 = np.array([3.7,3.1,2.7,2.4,1.9,1.9])

# In-situ data, 2 kpc
insitu_tot_2 = np.array([6,6,6,5.5,5,1.7,6,5.3,4.2,3.4,3.5,2.1])
insitu_lg_2 = np.array([6,6,6,5.5,5,1.7])
insitu_iso_2 = np.array([6,5.3,4.2,3.4,3.5,2.1])

"""
 Calculate the one and two sigma error bars for the median data points
"""
onesigp = 84.13
onesigm = 15.87
twosigp = 97.72
twosigm = 2.28
# Insitu cumulative smooth, 15 kpc
err_iecsm_op_15 = np.percentile(insitu_tot_15, onesigp)
err_iecsm_om_15 = np.percentile(insitu_tot_15, onesigm)
err_iecsm_tp_15 = np.percentile(insitu_tot_15, twosigp)
err_iecsm_tm_15 = np.percentile(insitu_tot_15, twosigm)
# Insitu cumulative smooth, 2 kpc
err_iecsm_op_2 = np.percentile(insitu_tot_2, onesigp)
err_iecsm_om_2 = np.percentile(insitu_tot_2, onesigm)
err_iecsm_tp_2 = np.percentile(insitu_tot_2, twosigp)
err_iecsm_tm_2 = np.percentile(insitu_tot_2, twosigm)
# Mass ratio 0.33 smoothed
err_mrsm033_op = np.percentile(ind_mr_sm_033, onesigp)
err_mrsm033_om = np.percentile(ind_mr_sm_033, onesigm)
err_mrsm033_tp = np.percentile(ind_mr_sm_033, twosigp)
err_mrsm033_tm = np.percentile(ind_mr_sm_033, twosigm)

"""
 Smooth out the isolated and pair in-situ data and calculate the medians for isolated and pair galaxies
"""
ifrac = 0.5
window_size, poly_order = 41, 3
redss = [0.05,0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95,1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05,2.15,2.25,2.35,2.45,2.55,2.65,2.75,2.85,2.95,3.05,3.15,3.25,3.35,3.45,3.55,3.65,3.75,3.85,3.95,4.1,4.3,4.5,4.7,4.9,5.1,5.3,5.5,5.7,5.9,6.5,7.5,8.5]
redss = np.flip(redss)

### LG galaxies
## Insitu cumulative smooth, 15 kpc
err_iecsm_op_15_lg = np.percentile(insitu_lg_15, onesigp)
err_iecsm_om_15_lg = np.percentile(insitu_lg_15, onesigm)
err_iecsm_tp_15_lg = np.percentile(insitu_lg_15, twosigp)
err_iecsm_tm_15_lg = np.percentile(insitu_lg_15, twosigm)
## Insitu cumulative smooth, 2 kpc
err_iecsm_op_2_lg = np.percentile(insitu_lg_2, onesigp)
err_iecsm_om_2_lg = np.percentile(insitu_lg_2, onesigm)
err_iecsm_tp_2_lg = np.percentile(insitu_lg_2, twosigp)
err_iecsm_tm_2_lg = np.percentile(insitu_lg_2, twosigm)

# Isolated galaxies
# Insitu cumulative smooth, 15 kpc
err_iecsm_op_15_iso = np.percentile(insitu_iso_15, onesigp)
err_iecsm_om_15_iso = np.percentile(insitu_iso_15, onesigm)
err_iecsm_tp_15_iso = np.percentile(insitu_iso_15, twosigp)
err_iecsm_tm_15_iso = np.percentile(insitu_iso_15, twosigm)
# Insitu cumulative smooth, 2 kpc
err_iecsm_op_2_iso = np.percentile(insitu_iso_2, onesigp)
err_iecsm_om_2_iso = np.percentile(insitu_iso_2, onesigm)
err_iecsm_tp_2_iso = np.percentile(insitu_iso_2, twosigp)
err_iecsm_tm_2_iso = np.percentile(insitu_iso_2, twosigm)


"""
 Group the isolated and pair MR data together
"""
# Isolated
ind_mr_sm_033_isomed = 2.3
# Pairs
ind_mr_sm_033_lgmed = 4.6

"""
 Calculate the errorbars
"""
# Isolated
# Mass ratio 0.33
err_mrsm033_op_iso = np.percentile(ind_mr_sm_033_iso, onesigp)
err_mrsm033_om_iso = np.percentile(ind_mr_sm_033_iso, onesigm)
err_mrsm033_tp_iso = np.percentile(ind_mr_sm_033_iso, twosigp)
err_mrsm033_tm_iso = np.percentile(ind_mr_sm_033_iso, twosigm)
# Pairs
# Mass ratio 0.33
err_mrsm033_op_lg = np.percentile(ind_mr_sm_033_lg, onesigp)
err_mrsm033_om_lg = np.percentile(ind_mr_sm_033_lg, onesigm)
err_mrsm033_tp_lg = np.percentile(ind_mr_sm_033_lg, twosigp)
err_mrsm033_tm_lg = np.percentile(ind_mr_sm_033_lg, twosigm)

"""
 Finally plot all of the damn data
"""
colors = dc.get_distinct(4)
xs = np.arange(1, 7)
inc = [0.0, 0.2]
leg = ['d(z = 0) < 15 kpc', 'd(z = 0) < 2 kpc']
#
fig = plt.figure(figsize=(10, 8))
ax1 = fig.add_subplot(1,1,1)
ax1.tick_params(axis='x', which='minor', bottom=False, top=False)
ax1.tick_params(axis='y', which='minor', right=False)
###### Insitu
### Isolated Galaxies, median = 2.7 and 3.9 for 15 and 2 kpc
# errorbars
plt.errorbar(xs[0]+inc[0], 2.7, yerr=np.array([[2.7-err_iecsm_tm_15_iso], [err_iecsm_tp_15_iso-2.7]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[0]+inc[0], 2.7, yerr=np.array([[2.7-err_iecsm_om_15_iso], [err_iecsm_op_15_iso-2.7]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[0]+inc[1], 3.9, yerr=np.array([[3.9-err_iecsm_tm_2_iso], [err_iecsm_tp_2_iso-3.9]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[0]+inc[1], 3.9, yerr=np.array([[3.9-err_iecsm_om_2_iso], [err_iecsm_op_2_iso-3.9]]), color=colors[1], alpha=0.80)
# Plot ie_cum_iso
plt.scatter(xs[0]+inc[0], 2.7, color=colors[0], s=50.0, marker='s', label=leg[0])
plt.scatter(xs[0]+inc[1], 3.9, color=colors[1], s=50.0, marker='s', label=leg[1])

### Median for ALL, median = 3.5 and 5.1 for 15 and 2 kpc
# errorbars
plt.errorbar(xs[1]+inc[0], 3.5, yerr=np.array([[3.5-err_iecsm_tm_15], [err_iecsm_tp_15-3.5]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[1]+inc[0], 3.5, yerr=np.array([[3.5-err_iecsm_om_15], [err_iecsm_op_15-3.5]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[1]+inc[1], 5.1, yerr=np.array([[5.1-err_iecsm_tm_2], [err_iecsm_tp_2-5.1]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[1]+inc[1], 5.1, yerr=np.array([[5.1-err_iecsm_om_2], [err_iecsm_op_2-5.1]]), color=colors[1], alpha=0.80)
# Plot ie_cum
plt.scatter(xs[1]+inc[0], 3.5, color=colors[0], s=50.0)
plt.scatter(xs[1]+inc[1], 5.1, color=colors[1], s=50.0)

### Pair Galaxies, median = 4.9 and 6 for 15 and 2 kpc
# errorbars
plt.errorbar(xs[2]+inc[0], 4.9, yerr=np.array([[4.9-err_iecsm_tm_15_lg], [err_iecsm_tp_15_lg-4.9]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[2]+inc[0], 4.9, yerr=np.array([[4.9-err_iecsm_om_15_lg], [err_iecsm_op_15_lg-4.9]]), color=colors[0], alpha=0.80)
plt.errorbar(xs[2]+inc[1], 6.0, yerr=np.array([[6.0-err_iecsm_tm_2_lg], [err_iecsm_tp_2_lg-6.0]]), color=colors[1], alpha=0.30)
plt.errorbar(xs[2]+inc[1], 6.0, yerr=np.array([[6.0-err_iecsm_om_2_lg], [err_iecsm_op_2_lg-6.0]]), color=colors[1], alpha=0.80)
# Plot ie_cum_lg
plt.scatter(xs[2]+inc[0], 4.9, color=colors[0], s=50.0, marker='D')
plt.plot(3.2, 6.0, markersize=9.0, marker='D', color=colors[1])

##### Mass Ratio (0.33), 15 kpc
### Isolated Galaxies
# errorbars
plt.errorbar(xs[3]+inc[0], ind_mr_sm_033_isomed, yerr=np.array([[ind_mr_sm_033_isomed-err_mrsm033_tm_iso], [err_mrsm033_tp_iso-ind_mr_sm_033_isomed]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[3]+inc[0], ind_mr_sm_033_isomed, yerr=np.array([[ind_mr_sm_033_isomed-err_mrsm033_om_iso], [err_mrsm033_op_iso-ind_mr_sm_033_isomed]]), color=colors[0], alpha=0.80)
# Plot ind_mr_sm_033_isomed
plt.scatter(xs[3]+inc[0], ind_mr_sm_033_isomed, color=colors[0], s=50.0, marker='s')

### Median for ALL
# errorbars
plt.errorbar(xs[4]+inc[0], mr_033, yerr=np.array([[mr_033-err_mrsm033_tm], [err_mrsm033_tp-mr_033]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[4]+inc[0], mr_033, yerr=np.array([[mr_033-err_mrsm033_om], [err_mrsm033_op-mr_033]]), color=colors[0], alpha=0.80)
# Plot mr_033
plt.scatter(xs[4]+inc[0], mr_033, color=colors[0], s=50.0)

### Paired Galaxies
# errorbars
plt.errorbar(xs[5]+inc[0], ind_mr_sm_033_lgmed, yerr=np.array([[ind_mr_sm_033_lgmed-err_mrsm033_tm_lg], [err_mrsm033_tp_lg-ind_mr_sm_033_lgmed]]), color=colors[0], alpha=0.30)
plt.errorbar(xs[5]+inc[0], ind_mr_sm_033_lgmed, yerr=np.array([[ind_mr_sm_033_lgmed-err_mrsm033_om_lg], [err_mrsm033_op_lg-ind_mr_sm_033_lgmed]]), color=colors[0], alpha=0.80)
# Plot ind_mr_sm_033_lgmed
plt.scatter(xs[5]+inc[0], ind_mr_sm_033_lgmed, color=colors[0], s=50.0, marker='D')

## Plot some vertical dotted lines to separate the metrics
plt.vlines(3.5, 0, 7, colors='k', linestyles='dotted')

# Label stuff
my_xticks1 = ['in-situ (iso)','in-situ (all)','in-situ (pair)']
my_xticks2 = ['MR (0.33, iso)','MR (0.33, all)','MR (0.33, pair)']
my_xticks_tot = my_xticks1+my_xticks2
my_xs = np.arange(1, 7)
plt.xticks(my_xs, my_xticks_tot, rotation=45)
plt.ylabel('Formation Redshift', fontsize=40)
plt.legend(prop={'size': 14})
plt.tick_params(axis='x', which='major', labelsize=20)
plt.tick_params(axis='y', which='major', labelsize=32)
plt.ylim(1, 6)
plt.yticks(np.arange(1, 7, 1.0))

# put the new axis stuff here 
ax2 = ax1.twinx()
ax2.tick_params(axis='y', which='minor', right=False)
axis_2_label = 'lookback time $\\left[ {\\rm Gyr} \\right]$'
axis_2_tick_values = [1,2,3,4,5,6]
axis_2_tick_labels = cosmo_class.convert_time(time_kind_get='time.lookback', time_kind_input='redshift', values=axis_2_tick_values)
axis_2_tick_labels = [np.around(axis_2_tick_labels[i], decimals=1) for i in range(0, len(axis_2_tick_labels))]
axis_2_tick_locations = [0,1,2,3,4,5]

ax2.set_yticks(axis_2_tick_locations)
ax2.set_yticklabels(axis_2_tick_labels, fontsize=24)
ax2.set_ylabel(axis_2_label, labelpad=9,fontsize=34)
ax2.tick_params(pad=3)

plt.tight_layout()
plt.savefig(home_dir_stam+'/scripts/plots/redshift_form_v5_new.pdf')