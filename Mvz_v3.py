#!/usr/bin/python3

"""
 ==============
 = M1 vs z v3 =
 ==============
 
 Written by Isaiah Santistevan (ibsantistevan@ucdavis.edu) during Fall Quarter, 2018

 Goal: Get the most massive halo that contributes > 1% of it's stars at each redshift 
       and save it to a pickle file for the following distance cuts:

       - d = [0, 2 kpc]
       - d = [4, 15 kpc]
"""

#### Import all of the tools for analysis
import rockstar_analysis as rockstar
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import pickle

# Read in halos for all redshifts
galaxy = 'm12f'
resolution = '_res7100'
simulation_dir_pel = '/home/awetzel/scratch/'+galaxy+'/'+galaxy+resolution
simulation_dir_stam = '/scratch/projects/xsede/GalaxiesOnFIRE/metal_diffusion/'+galaxy+resolution
home_dir_stam = '/home1/05400/ibsantis'
redshifts = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.2,4.4,4.6,4.8,5,5.2,5.4,5.6,5.8,6,7,8,9]
hal = rockstar.io.IO.read_catalogs('redshift', redshifts, file_kind='hdf5', simulation_directory=simulation_dir_stam, all_snapshot_list=False)
# Reverse array to the way it was originally...
hal = np.flip(hal)

# Some tools for analysis
# Get list of scale factors
a = [hal[i].snapshot['scalefactor'] for i in range(len(redshifts))]
"""
 Function that gets indices for halos that have a certain star number, star density, and low resolution mass fraction.
 Currently a function of redshift, but can make it a function of anything really.

	z: redshift
	ind: indices that satisfy the conditions below
"""
def halo_cond_ind(z):
    ind = ut.array.get_indices(hal[z]['star.number'], [10, np.Infinity])
    ind = ut.array.get_indices(hal[z].prop('star.density.50'), [300, np.Infinity], ind)
    ind = ut.array.get_indices(hal[z].prop('lowres.mass.frac'), [0, 0.02], ind)
    return ind
"""
 Function that sorts a vector, gets element i, then finds where it is in the unsorted vector

 unsort: unsorted vector (eg. a vector of masses of halos at some redshift)
 sort: unsort, sorted
 i: the element in sort that I want (eg. 0 would be the most massive halo; 1, the second most massive, etc.)
 mass_i: the mass of element i in sort
 index: the index in the unsorted vector of mass_i
"""
def mass_index(unsort, i):
    sort = np.flip(np.sort(unsort), 0)
    mass_i = sort[i]
    index = np.where(mass_i == unsort)[0][0]
    return index

#### Analysis
# Find halos that meet the conditions of halo_cond_ind
his = [halo_cond_ind(i) for i in range(0, len(redshifts))]
# Read in the contribution fractions for the halos at each redshift
pickle2 = home_dir_stam+'/scripts/pickles/contribution_2_fullres_'+galaxy+'.p'
pickle415 =  home_dir_stam+'/scripts/pickles/contribution_415_fullres_'+galaxy+'.p'
# d = [0, 2 kpc]
Filep1 = open(pickle2, "rb")
ratio_2 = pickle.load(Filep1)
Filep1.close()
# d = [4, 15 kpc]
Filep2 = open(pickle415, "rb")
ratio_415 = pickle.load(Filep2)
Filep2.close()

### d = [0, 2 kpc], f_contr > 0.01
'''
 Find the indices of the most massive halo at each redshift that contributes > 1% of stars to host at z = 0 and
 get their masses
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_2: Array
        MMHi_2[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_2[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the most massive halo for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
'''
mass_ind_1 = np.zeros(len(redshifts))
MMH1_2 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
for i in range(0, len(ratio_2)):
    # Get the most massive right away
    m1 = 0
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    # Check the ratio
    tt1 = ratio_2[i][t1]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    # Set the masses to the initial values
    MMH1_2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    # Make sure MMH has a contribution > 1%
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_2[i])):
            MMH1_2[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_2[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_2[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1

'''
 Get the elements of MMHi_2 at z = 0 
    NOTE this is good for all distance cuts!

    m0 : float
        m0 corresponds to the most massive halo at z = 0
    Method:
        1. Set up an initial value for the most massive halo
        2. Make sure the most massive halo is within 2 kpc of the host at z = 0
           This is actually really silly because the most massive halo at z = 0 is the host, but it's good to be thorough
'''
temp0 = mass_index(unsort=hal[0]['star.mass'][his[0]], i=0)
m0 = hal[0]['star.mass'][his[0]][temp0]
# Append the elements above to the MMH* lists
MMH1_2[0] = m0

### d = [4, 15 kpc], f_contr > 0.01
'''
 Find the indices of the most massive halo at each redshift that contributes > 1% of stars to host at z = 0 and
 get their masses
 
    mass_ind_i : Array
        mass_ind_i[j] corresponds to the index j of the ith most massive halo at redshifts[j+1]
        
    MMHi_415: Array
        MMHi_415[mass_ind_i[j]] corresponds to the mass of the halo with index mass_ind_i[j] at redshifts[j+1]
        
    checki : Array
        checki[j] corresponds to the contribution fraction (ratio[i][j]) for the halo with mass MMHi_415[j]
        These are mainly used to check what the contribution fractions are when weird things crop up. Not really necessary
 
    Method:
        1. Set up some null arrays
        2. Loop over all redshifts aside from zero
        3. Set up some initial values for the most massive halo for a given redshift
        4. Make sure the first most massive halo contributes more than 1%
'''
mass_ind_1 = np.zeros(len(redshifts))
MMH1_415 = np.zeros(len(redshifts))
check1 = np.zeros(len(redshifts))
for i in range(0, len(ratio_415)):
    # Get the most massive right away
    m1 = 0
    t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
    # Check the ratio
    tt1 = ratio_415[i][t1]
    # Set the indices to the initial values
    mass_ind_1[i+1] = t1
    # Set the masses to the initial values
    MMH1_415[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
    # Check to see what the ratios are...?
    check1[i+1] = tt1
    # Make sure MMH has a contribution > 1%
    while (tt1 <= 0.01):
        m1 += 1
        if (m1 == len(ratio_415[i])):
            MMH1_415[i+1] = 0
            break
        t1 = mass_index(hal[i+1]['star.mass'][his[i+1]], m1)
        tt1 = ratio_415[i][t1]
        mass_ind_1[i+1] = t1
        MMH1_415[i+1] = hal[i+1]['star.mass'][his[i+1]][int(mass_ind_1[i+1])]
        check1[i+1] = tt1

# Append the elements above to the MMH* lists
MMH1_415[0] = m0

# Store the data
Filep = open(home_dir_stam+"/scripts/pickles/Mvz_1percent_"+galaxy+"_v2.p", "wb")
pickle.dump(MMH1_2, Filep)
pickle.dump(MMH1_415, Filep)
Filep.close()